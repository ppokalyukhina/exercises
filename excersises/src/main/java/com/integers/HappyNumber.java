package com.integers;

import java.util.HashSet;
import java.util.Set;

/**
 * Write an algorithm to determine if a number is "happy".
 *
 * A happy number is a number defined by the following process:
 * Starting with any positive integer, replace the number by the sum of the squares of its digits,
 * and repeat the process until the number equals 1 (where it will stay),
 * or it loops endlessly in a cycle which does not include 1.
 * Those numbers for which this process ends in 1 are happy numbers.
 */
public class HappyNumber {
    public static void main(String[] args) {
        var happyN = new HappyNumber();

        System.out.println(happyN.isHappy(19));
    }

    public boolean isHappy(int n) {
        Set<Integer> generatedNums = new HashSet<>();
        return powNumbers(n, generatedNums);
    }

    private boolean powNumbers(int n, Set<Integer> generatedNums) {
        if (n == 1) {
            return true;
        }

        if (generatedNums.contains(n)) {
            return false;
        }

        generatedNums.add(n);

        int calculatedSum = 0;
        String givenN = String.valueOf(n);
        String[] numbers = givenN.split("");
        for (String strNumber : numbers) {
            int number = Integer.parseInt(strNumber);
            if (number < 0) {
                continue;
            }

            calculatedSum += Math.pow(number, 2);
        }

        return powNumbers(calculatedSum, generatedNums);
    }
}
