package com.integers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Given an integer array arr, count element x such that x + 1 is also in arr.
 *
 * If there're duplicates in arr, count them seperately.
 */
public class CountInArray {
    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2};
        var countArr = new CountInArray();
        System.out.println(countArr.countElements2(arr));
    }

    public int countElements2(int[] arr) {
        Set<Integer> sums = new HashSet<>();
        int result = 0;

        for (int i : arr) {
            sums.add(i - 1);
        }

        for (int i : arr) {
            if (sums.contains(i)) {
                result++;
            }
        }

        return result;
    }

    public int countElements1(int[] arr) {
        Arrays.sort(arr);
        int result = 0;
        for(int i = 0; i < arr.length - 1; i++) {
            if (arr[i+1] - arr[i] == 1) {
                result++;
            }
        }

        return result;
    }

    public int countElements(int[] arr) {
        Set<Integer> sums = new HashSet<>();
        int result = 0;

        for (int value : arr) {
            int sum = value + 1;
            sums.add(sum);
        }

        for (int i : arr) {
            if (sums.contains(i)) {
                result++;
            }
        }

        return result;
    }
}
