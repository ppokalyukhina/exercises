package com.integers;

public class Duplicates {
    /**
     * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
     *
     * Note:
     *
     * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
     */
    public int singleNumber(int[] nums) {
        if (nums.length == 0) {
            return -1;
        }

        int result = -1;
        int i = 0;
        while (i < nums.length) {
            int j = 0;
            while (j < nums.length) {
                if (i == j) {
                    j++;
                    continue;
                }

                if (nums[i] == nums[j]) {
                    break;
                } else {
                    j++;
                }
            }

            if (j == nums.length) {
                return nums[i];
            }

            i++;
        }

        return result;
    }

    public static void main(String[] args) {
        Duplicates duplicates = new Duplicates();

        int[] givenArr = {2,2,1};
        System.out.println(duplicates.singleNumber(givenArr));
    }
}
