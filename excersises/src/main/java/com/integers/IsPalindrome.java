package com.integers;

public class IsPalindrome {
    public static void main(String[] args) {
        int n = 0;
        System.out.println(isPalindrome(n));
    }

    private static boolean isPalindrome(int x) {
        String intToString = String.valueOf(x);
        int left = 0;
        int right = intToString.length() - 1;
        while(left < right) {
            if (intToString.charAt(left) != intToString.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }

        return true;
    }
}
