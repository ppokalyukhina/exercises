package com.integers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LargestRange {
    public static void main(String[] args) {
//        int[] arr = {8, 4, 2, 10, 3, 6, 7, 9, 1}; // [6, 10]
        int[] arr = {1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6}; // [0, 7]
        int[] result = largestRangeLinear(arr);
        for (int i : result) {
            System.out.println(i);
        }
    }

    // not an optimal solution. O(logN) time and O(1) space
    // todo need to fix
    private static int[] largestRangeLinear(int[] array) {
        if (array.length == 1) {
            return new int[]{array[0], array[0]};
        }
        int[] bestRange = new int[2];
        int longestLength = 0;
        Arrays.sort(array);
        int i = 1;
        while (i < array.length) {
            int diff = array[i] - array[i - 1];
            if (diff != 1) {
                i++;
                continue;
            }

            int currentRange = 1;
            int firstNumInRange = array[i - 1];
            while(diff == 1) {
                if (i == array.length - 1) {
                    i++;
                    currentRange++;
                    break;
                }
                diff = array[++i] - array[i - 1];
                currentRange++;
            }

            if (currentRange > longestLength) {
                longestLength = currentRange;
                bestRange[0] = firstNumInRange;
                bestRange[1] = array[i - 1];
            }
        }

        return bestRange;
    }

    // use a hash table with linear runtime O(N) and space O(N)
    public static int[] largestRange(int[] array) {
        int[] bestRange = new int[2];
        int longestLength = 0;
        Map<Integer, Boolean> visited = new HashMap<>();
        for (int i : array) {
            visited.put(i, true);
        }

        for (int i : array) {
            if (!visited.get(i)) {
                continue;
            }

            visited.put(i, false);
            int currentLength = 1;
            int left = i - 1;
            int right = i + 1;
            while(visited.containsKey(left)) {
                visited.put(left, false);
                left--;
                currentLength++;
            }

            while(visited.containsKey(right)) {
                visited.put(right, false);
                right++;
                currentLength++;
            }

            if (currentLength > longestLength) {
                longestLength = currentLength;
                bestRange[0] = left + 1;
                bestRange[1] = right - 1;
            }
        }

        return bestRange;
    }
}
