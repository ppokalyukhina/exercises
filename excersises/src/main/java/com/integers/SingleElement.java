package com.integers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Given a non-empty array of integers, every element appears three times except for one, which appears exactly once. Find that single one.
 *
 * Note:
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 *
 * Example 1:
 * Input: [2,2,3,2]
 * Output: 3
 *
 * Example 2:
 * Input: [0,1,0,1,0,1,99]
 * Output: 99
 */
public class SingleElement {
    private static final Logger logger = LoggerFactory.getLogger(SingleElement.class);

    public static void main(String[] args) {
        // expected 2147483647
//        int[] input = {43,16,45,89,45,
//                -2147483648,45,2147483646,
//                -2147483647,-2147483648,43,
//                2147483647,-2147483646,-2147483648,89,-2147483646,89,-2147483646,-2147483647,2147483646,-2147483647,16,16,2147483646,43};
        int[] input = {-3, -3, -3, 3, 2, 2, 2};
        logger.info("Single element in array is {}", singleNumberWoExtraSpace(input));
    }

    private static int singleNumberWoExtraSpace(int[] nums) {
        int rst = 0;
        for (int i = 0; i < 32; i++) {
            int set = 1 << i, sum = 0;
            for (int num : nums) {
                sum = (num & set) != 0 ? sum + 1 : sum;
            }
            rst = sum % 3 == 0 ? rst : rst | set;
        }
        return rst;
    }

    private static int singleNumber(int[] nums) {
        // num -> frequency
        Map<Integer, Integer> ints = new HashMap<>();
        for (int num : nums) {
            if (!ints.containsKey(num)) {
                ints.put(num, 1);
            } else {
                if (ints.get(num) == 2) {
                    ints.remove(num);
                    continue;
                }

                int frequency = ints.get(num);
                ints.put(num, ++frequency);
            }
        }

        if (ints.isEmpty()) {
            return -1;
        }

        return ints.keySet().stream().findFirst().get();
    }
}
