package com.integers;

import java.util.Arrays;

public class SmallestNum {
    public static void main(String[] args) {
        int[] given = {1, -3, 6, -4, 1, 2};
        System.out.println(solution(given));
    }

    public static int solution(int[] A) {
        Arrays.sort(A);

        int lastElem = A[A.length - 1];
        if (lastElem < 0) {
            return 1;
        }

        for(int i = 0; i < A.length - 1; i++) {
            if (A[i] < 0) {
                continue;
            }

            int first = A[i];
            int next = A[i + 1];

            if (next - first > 1) {
                return first + 1;
            }
        }

        return lastElem + 1;
    }
}
