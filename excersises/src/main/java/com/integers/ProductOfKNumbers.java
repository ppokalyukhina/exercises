package com.integers;

public class ProductOfKNumbers {
    public static void main(String[] args) {
        int[] givenArr = {1, 3, 3, 6, 5, 7, 0, -3};
        int k = 3;

        int[] res = numbers(givenArr, k);

        for (int re : res) {
            System.out.println(re);
        }
    }

    private static int[] numbers(int[] givenArr, int k) {
        int[] result = new int[givenArr.length];
        int firstPointer = 0;

        int pointerK = firstPointer;
        while(firstPointer < givenArr.length) {
            if (pointerK < k) {
                int nextVal = givenArr[firstPointer];
                for (int i = firstPointer - 1; i >= 0 && i >= firstPointer - k + 1; i--) {
                    nextVal *= givenArr[i];
                }

                result[firstPointer] = nextVal;
                pointerK++;
            } else {
                pointerK = 0;
            }

            if (pointerK != 0) {
                firstPointer++;
            }
        }

        return result;
    }
}
