package com.integers;

import java.util.HashSet;
import java.util.Set;

public class NextSmallestN {
    public static void main(String[] args) {
        int[] input = {-1, -3};

        var r = solution(input);
        System.out.println(r);
    }

    public static int solution(int[] A) {
        int smallest = 1;
        if (A.length == 0) {
            return smallest;
        }

        Set<Integer> originalNums = new HashSet<>();
        for (int i : A) {
            originalNums.add(i);
        }

        while(originalNums.contains(smallest)) {
            smallest++;
        }

        return smallest;
    }
}
