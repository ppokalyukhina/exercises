package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class FirstDuplicateValue {
    private static final Logger logger = LoggerFactory.getLogger(FirstDuplicateValue.class);

    public static void main(String[] args) {
//        int[] input = {2, 1, 5, 2, 3, 3, 4};
        int[] input = {7, 6, 5, 3, 6, 4, 3, 5, 2};

        logger.info(String.format("First duplicated value is %d", firstDuplicateValue(input)));
    }

    /**
     * O(N) time
     * O(1) space
     */
    private static int firstDuplicateValue(int[] array) {
        int i = 0;
        while(i < array.length) {
            int current = Math.abs(array[i]);
            if (array[current - 1] < 0) {
                return current;
            }

            array[current - 1] *= -1;
            i++;
        }

        return -1;
    }

    /**
     * O(N*N) time
     * O(1) space
     */
    private static int firstDuplicateValue1(int[] array) {
        if (array.length <= 1) {
            return -1;
        }

        for (int i = 1; i < array.length; i++) {
            int current = array[i - 1];
            int next = array[i];

            if (current == next) {
                return next;
            }

            if (current < next) {
                continue;
            }

            int nextIndx = i;
            while(nextIndx > 0 && array[nextIndx] <= array[nextIndx - 1]) {
                if (array[nextIndx] == array[nextIndx - 1]) {
                    return array[nextIndx];
                }
                // swap
                int tmp = array[nextIndx];
                array[nextIndx] = array[nextIndx - 1];
                array[nextIndx - 1] = tmp;
                nextIndx--;
            }
        }

        return -1;
    }

    /**
     * Solution with Set
     * time O(N)
     * space o(N)
     */
    private static int firstDuplicateValueSet(int[] array) {
        Set<Integer> set = new HashSet<>();
        for (int i : array) {
            if (set.contains(i)) {
                return i;
            }

            set.add(i);
        }

        return -1;
    }
}
