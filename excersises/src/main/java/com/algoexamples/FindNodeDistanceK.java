package com.algoexamples;

import java.util.*;

public class FindNodeDistanceK {
    public static void main(String[] args) {
        /**
         * {"id": "1", "left": "2", "right": "3", "value": 1},
         *     {"id": "2", "left": "4", "right": "5", "value": 2},
         *     {"id": "3", "left": null, "right": "6", "value": 3},
         *     {"id": "4", "left": null, "right": null, "value": 4},
         *     {"id": "5", "left": null, "right": null, "value": 5},
         *     {"id": "6", "left": "7", "right": "8", "value": 6},
         *     {"id": "7", "left": null, "right": null, "value": 7},
         *     {"id": "8", "left": null, "right": null, "value": 8}
         */

        BinaryTree root = new BinaryTree(1);
        BinaryTree two = new BinaryTree(2);
        BinaryTree three = new BinaryTree(3);
        root.left = two;
        root.right = three;
        BinaryTree four = new BinaryTree(4);
        BinaryTree five = new BinaryTree(5);
        two.left = four;
        two.right = five;
        BinaryTree six = new BinaryTree(6);
        three.right = six;
        BinaryTree seven = new BinaryTree(7);
        BinaryTree eight = new BinaryTree(8);
        six.left = seven;
        six.right = eight;

        int target = 3;
        int k = 2;
        var result = findNodesDistanceKNodeToParentsMap(root, target, k);
        for (Integer integer : result) {
            System.out.println(integer);
        }
    }

    private static ArrayList<Integer> findNodesDistanceKNodeToParentsMap(BinaryTree tree, int target, int k) {
        // key -> node.value
        // value -> node.parent
        Map<Integer, BinaryTree> parents = new HashMap<>();
        assignParents(tree, null, parents);

        // find target node
        BinaryTree targetNode = findTargetNode(target, parents, tree);
        Queue<BinaryTree> stack = new LinkedList<>();
        stack.add(targetNode);

        ArrayList<Integer> nodes = new ArrayList<>();
        bfs(k, nodes, stack, parents);
        return nodes;
    }

    private static void bfs(int k, ArrayList<Integer> nodes, Queue<BinaryTree> qeue, Map<Integer, BinaryTree> parents) {
        while(!qeue.isEmpty()) {
            var currentNode = qeue.poll();
            if (currentNode.distanceFromNode == k) {
                nodes.add(currentNode.value);
                continue;
            }
            int currentDistance = currentNode.distanceFromNode;
            currentNode.visited = true;
            // push neighbours to qeue
            // left child
            if (currentNode.left != null && !currentNode.left.visited) {
                currentNode.left.distanceFromNode = currentDistance + 1;
                qeue.add(currentNode.left);
            }

            // right child
            if (currentNode.right != null && !currentNode.right.visited) {
                currentNode.right.distanceFromNode = currentDistance + 1;
                qeue.add(currentNode.right);
            }

            // parent
            if (parents.containsKey(currentNode.value) && parents.get(currentNode.value) != null) {
                if (parents.get(currentNode.value).visited) {
                    continue;
                }
                BinaryTree parentNode = parents.get(currentNode.value);
                parentNode.distanceFromNode = currentDistance + 1;
                qeue.add(parentNode);
            }
        }
    }

    /**
     * O(N) space -> Map, N -> n of nodes
     * O(V+E) time, V -> vertices, E -> edges
     */
    private static void assignParents(BinaryTree node, BinaryTree parent, Map<Integer, BinaryTree> parents) {
        if (node == null) {
            return;
        }

        parents.put(node.value, parent);
        assignParents(node.left, node, parents);
        assignParents(node.right, node, parents);
    }

    /**
     * O(1) time
     */
    private static BinaryTree findTargetNode(int target, Map<Integer, BinaryTree> parents, BinaryTree node) {
        if (node.value == target) {
            return node;
        }
        BinaryTree parent = parents.get(target);
        if (parent.left != null && parent.left.value == target) {
            return parent.left;
        }

        return parent.right;
    }

    private static ArrayList<Integer> findNodesDistanceK(BinaryTree tree, int target, int k) {
        assignParents(tree, null);

        // find target node
        BinaryTree targetNode = findTargetNode(tree, target);
        Stack<BinaryTree> stack = new Stack<>();
        stack.push(targetNode);

        ArrayList<Integer> nodes = new ArrayList<>();
        bfs(k, nodes, stack);
        return nodes;
    }

    /**
     * O(KlogK) time; k -> distance
     * O(K*N) space -> k is distance, N -> nodes
     */
    private static void bfs(int k, ArrayList<Integer> nodes, Stack<BinaryTree> stack) {
        while(!stack.isEmpty()) {
            var currentNode = stack.pop();
            if (currentNode.distanceFromNode == k) {
                nodes.add(currentNode.value);
                continue;
            }
            int currentDistance = currentNode.distanceFromNode;
            currentNode.visited = true;
            // push neighbours to stack
            // left child
            if (currentNode.left != null && !currentNode.left.visited) {
                currentNode.left.distanceFromNode = currentDistance + 1;
                stack.push(currentNode.left);
            }

            // right child
            if (currentNode.right != null && !currentNode.right.visited) {
                currentNode.right.distanceFromNode = currentDistance + 1;
                stack.push(currentNode.right);
            }

            // parent
            if (currentNode.parent != null && !currentNode.parent.visited) {
                currentNode.parent.distanceFromNode = currentDistance + 1;
                stack.push(currentNode.parent);
            }
        }
    }

    /**
     * O(d) time; d -> depth of the tree
     * O(d) space -> stack calls
     */
    private static BinaryTree findTargetNode(BinaryTree tree, int target) {
        if (tree == null) {
            return null;
        }

        if (tree.value == target) {
            return tree;
        }

        var left = findTargetNode(tree.left, target);
        var right = findTargetNode(tree.right, target);

        if (left != null) {
            return left;
        }

        return right;
    }

    /**
     * O(d) time; d -> depth of the tree
     * O(d) space -> stack calls
     */
    private static void assignParents(BinaryTree root, BinaryTree parent) {
        if (root == null) {
            return;
        }

        root.parent = parent;
        assignParents(root.left, root);
        assignParents(root.right, root);
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;
        public BinaryTree parent = null;
        public int distanceFromNode = 0;
        public boolean visited = false;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
