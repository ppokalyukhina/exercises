package com.algoexamples;

public class RemoveDuplicatesFromLinkedList {
    public static void main(String[] args) {
        LinkedList root = new LinkedList(1);
        LinkedList one = new LinkedList(1);
        LinkedList three = new LinkedList(3);
        LinkedList four = new LinkedList(4);
        LinkedList four1 = new LinkedList(4);
        LinkedList four2 = new LinkedList(4);
        LinkedList five = new LinkedList(5);
        LinkedList six = new LinkedList(6);
        LinkedList six1 = new LinkedList(6);
        root.next = one;
        one.next = three;
        three.next = four;
        four.next = four1;
        four1.next = four2;
        four2.next = five;
        five.next = six;
        six.next = six1;
        var result = removeDuplicatesFromLinkedList(root);
        while(result != null) {
            System.out.println(result.value);
            result = result.next;
        }
    }

    public static LinkedList removeDuplicatesFromLinkedList(LinkedList linkedList) {
        LinkedList previous = null;
        LinkedList current = linkedList;
        while(current != null) {
            if (previous != null && current.value == previous.value) {
                // detach current from the list
                previous.next = current.next;
            } else {
                previous = current;
            }
            current = current.next;
        }

        return linkedList;
    }

    public static class LinkedList {
        public int value;
        public LinkedList next;

        public LinkedList(int value) {
            this.value = value;
            this.next = null;
        }
    }
}
