package com.algoexamples;

public class SumOfLinkedLists {
    public static void main(String[] args) {
        LinkedList headOne = new LinkedList(2);
        LinkedList four = new LinkedList(4);
        LinkedList seven = new LinkedList(7);
        LinkedList one = new LinkedList(1);
        seven.next = one;
        four.next = seven;
        headOne.next = four;

        LinkedList headTwo = new LinkedList(9);
        LinkedList fourTwo = new LinkedList(4);
        LinkedList five = new LinkedList(5);
        fourTwo.next = five;
        headTwo.next = fourTwo;

        var newHead = sumOfLinkedListsBetterCode(headOne, headTwo);
        while(newHead != null) {
            System.out.println(newHead.value);
            newHead = newHead.next;
        }
    }

    private static LinkedList sumOfLinkedListsBetterCode(LinkedList linkedListOne, LinkedList linkedListTwo) {
        LinkedList sumHead = new LinkedList(0);
        LinkedList previous = sumHead;
        LinkedList headOne = linkedListOne;
        LinkedList headTwo = linkedListTwo;
        int carryOver = 0;

        while(headOne != null || headTwo != null || carryOver != 0) {
            int valueOne = headOne != null ? headOne.value : 0;
            int valueTwo = headTwo != null ? headTwo.value : 0;
            int sum = valueOne + valueTwo + carryOver;

            if (sum >= 10) {
                sum = sum % 10;
                carryOver = 1;
            } else {
                carryOver = 0;
            }

            previous.next = new LinkedList(sum);
            previous = previous.next;
            headOne = headOne != null ? headOne.next : null;
            headTwo = headTwo != null ? headTwo.next : null;
        }

        return sumHead.next;
    }

    // O(max(n, m)) time, n -> list1, m -> list2
    // O(max(n, m)) space, n -> list1, m -> list2
    private static LinkedList sumOfLinkedListsMoreOptimal(LinkedList linkedListOne, LinkedList linkedListTwo) {
        LinkedList sumHead = null;
        LinkedList previous = null;
        int carryOver = 0;
        LinkedList headOne = linkedListOne;
        LinkedList headTwo = linkedListTwo;

        while(headOne != null && headTwo != null) {
            int sum = headOne.value + headTwo.value + carryOver;
            if (sum >= 10) {
                sum = sum % 10;
                carryOver = 1;
            } else {
                carryOver = 0;
            }

            LinkedList currentNewNode = new LinkedList(sum);
            if (previous == null) {
                previous = currentNewNode;
                sumHead = previous;
            } else {
                previous.next = currentNewNode;
                previous = previous.next;
            }

            headOne = headOne.next;
            headTwo = headTwo.next;
        }

        // check for leftovers in first list
        while(headOne != null) {
            LinkedList currentNewNode = new LinkedList(headOne.value + carryOver);
            carryOver = 0;
            if (previous == null) {
                previous = currentNewNode;
                sumHead = previous;
            } else {
                previous.next = currentNewNode;
                previous = previous.next;
            }
            headOne = headOne.next;
        }

        // check for leftovers in second list
        while(headTwo != null) {
            LinkedList currentNewNode = new LinkedList(headTwo.value + carryOver);
            carryOver = 0;
            if (previous == null) {
                previous = currentNewNode;
                sumHead = previous;
            } else {
                previous.next = currentNewNode;
                previous = previous.next;
            }
            headTwo = headTwo.next;
        }

        if (carryOver != 0) {
            previous.next = new LinkedList(carryOver);
        }

        return sumHead;
    }

    // O(L1 + L2 + L3) time complexity
    // L1 -> first list
    // L2 -> second given List
    // L3 -> sum list
    // O(L3) memory -> creating a new list
    private static LinkedList sumOfLinkedLists(LinkedList linkedListOne, LinkedList linkedListTwo) {
        StringBuilder firstN = new StringBuilder();
        LinkedList headOne = linkedListOne;
        while(headOne != null) {
            firstN.insert(0, headOne.value);
            headOne = headOne.next;
        }

        StringBuilder secondN = new StringBuilder();
        LinkedList headTwo = linkedListTwo;
        while(headTwo != null) {
            secondN.insert(0, headTwo.value);
            headTwo = headTwo.next;
        }

        int sumOfLists = Integer.parseInt(firstN.toString()) + Integer.parseInt(secondN.toString());
        LinkedList summedList = null;
        LinkedList previous = null;
        String sums = Integer.toString(sumOfLists);
        for (int i = sums.length() - 1; i >= 0; i--) {
            LinkedList currentNode = new LinkedList(sums.charAt(i) - '0');
            if (previous == null) {
                previous = currentNode;
                summedList = previous;
            } else {
                previous.next = currentNode;
                previous = previous.next;
            }
        }
        return summedList;
    }

    public static class LinkedList {
        public int value;
        public LinkedList next;

        public LinkedList(int value) {
            this.value = value;
            this.next = null;
        }
    }
}
