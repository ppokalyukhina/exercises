package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SuffixTrie {
    private static final Logger logger = LoggerFactory.getLogger(SuffixTrie.class);

    public static void main(String[] args) {
        var trie = new SuffixTrie("babc");
        String nodeToFind = "";
        logger.info(String.format("Found node '%s': %s", nodeToFind, trie.contains(nodeToFind)));
    }

    TrieNode root = new TrieNode();
    char endSymbol = '*';

    public SuffixTrie(String str) {
        populateSuffixTrieFrom(str);
    }

    // O(Npow2) time complexity
    // O(Npow2) space complexity
    private void populateSuffixTrieFrom(String str) {
        if (str.isEmpty()) {
            return;
        }

        root.children.put(str.charAt(0), new TrieNode());
        for (int i = 1; i < str.length(); i++) {
            TrieNode previous = root.children.get(str.charAt(i - 1));
            int currentIndex = i;
            while(currentIndex < str.length()) {
                previous.children.putIfAbsent(str.charAt(currentIndex), new TrieNode());
                previous = previous.children.get(str.charAt(currentIndex));
                currentIndex++;
            }
            // add '*' to the last child node.
            previous.children.put(endSymbol, null);
            root.children.putIfAbsent(str.charAt(i), new TrieNode());
        }

        // add '*' to the last child node.
        root.children.get(str.charAt(str.length() - 1)).children.put(endSymbol, null);
    }

    public boolean contains(String str) {
        TrieNode node = root;
        for (int i = 0; i < str.length(); i++) {
            if (!node.children.containsKey(str.charAt(i))) {
                return false;
            }
            node = node.children.get(str.charAt(i));
        }

        return node.children.containsKey(endSymbol);
    }

    static class TrieNode {
        Map<Character, TrieNode> children = new HashMap<>();
    }
}
