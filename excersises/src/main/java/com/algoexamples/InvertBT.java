package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class InvertBT {
    private static final Logger logger = LoggerFactory.getLogger(InvertBT.class);

    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        BinaryTree two = new BinaryTree(2);
        BinaryTree three = new BinaryTree(3);
        BinaryTree four = new BinaryTree(4);
        BinaryTree five = new BinaryTree(5);
        BinaryTree six = new BinaryTree(6);
        BinaryTree seven = new BinaryTree(7);
        BinaryTree eight = new BinaryTree(8);
        BinaryTree nine = new BinaryTree(9);

        // left wing
        four.left = eight;
        four.right = nine;
        two.left = four;
        two.right = five;
        root.left = two;

        // right wing
        three.left = six;
        three.right = seven;
        root.right = three;

        invertBinaryTree(root);
    }

    private static void invertBinaryTree(BinaryTree tree) {
        invertIterative(tree);
    }

    private static void invertIterative(BinaryTree tree) {
        Stack<BinaryTree> stack = new Stack<>();
        stack.push(tree);
        while(!stack.isEmpty()) {
            BinaryTree current = stack.pop();

            BinaryTree left = current.left;
            current.left = current.right;
            current.right = left;

            if (current.left != null) {
                stack.push(current.left);
            }

            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    private static void queueIterative(BinaryTree tree) {
        Queue<BinaryTree> queue = new ArrayDeque<>();
        queue.add(tree);
        while(!queue.isEmpty()) {
            BinaryTree current = queue.poll();

            BinaryTree left = current.left;
            current.left = current.right;
            current.right = left;

            if (current.left != null) {
                queue.add(current.left);
            }

            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }

    private static void invert(BinaryTree current) {
        if (current == null) {
            return;
        }

        BinaryTree left = current.left;
        current.left = current.right;
        current.right = left;

        invert(current.left);
        invert(current.right);
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left;
        public BinaryTree right;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
