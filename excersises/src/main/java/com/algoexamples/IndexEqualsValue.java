package com.algoexamples;

public class IndexEqualsValue {
    public static void main(String[] args) {
        int[] inputArr = {-5, -3, 0, 3, 4, 5, 9};
//        int[] inputArr = {-12, 1, 2, 3, 12};
        System.out.println(indexEqualsValue(inputArr));
    }

    private static int indexEqualsValue(int[] array) {
        int low = 0;
        int high = array.length - 1;
        while(low <= high) {
            int mid = low + high >>> 1;
            int midVal = array[mid];
            if (midVal == mid) {
                if (mid == 0 || array[mid - 1] != mid - 1) {
                    return mid;
                }

                // else go left
                // to find the smallest element
                high = mid - 1;
                continue;
            }

            if (midVal < mid) {
                // go and explore right
                low = mid + 1;
            }
            else {
                // go and explore left
                high = mid - 1;
            }
        }

        return -1;
    }
}
