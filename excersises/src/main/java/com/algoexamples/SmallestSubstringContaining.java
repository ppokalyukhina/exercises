package com.algoexamples;

import java.util.HashMap;
import java.util.Map;

public class SmallestSubstringContaining {
    public static void main(String[] args) {
        String big = "a$fuu+afff+affaffa+a$Affab+a+a+$a$";
        String small = "a+$aaAaaaa$++";
        System.out.println(smallestSubstringContainingOptimal(big, small));
    }

    public static String smallestSubstringContainingOptimal(String bigString, String smallString) {
        String result = "";
        // key -> characters in small str
        // value -> N of their appearance in the str
        Map<Character, Integer> smallStrChars = new HashMap<>();
        fillMap(smallStrChars, smallString);
        int uniqueCharsSize = smallStrChars.size();
        Map<Character, Integer> visitedChars = new HashMap<>();
        int visitedCharsSize = 0;
        int left = 0;
        int right = 0;
        while(right < bigString.length()) {
            char rightChar = bigString.charAt(right);
            if (!smallStrChars.containsKey(rightChar)) {
                right++;
                continue;
            }

            increaseVisit(rightChar, visitedChars);
            if (smallStrChars.get(rightChar).equals(visitedChars.get(rightChar))) {
                visitedCharsSize++;
            }

            // move left pointer
            while (uniqueCharsSize == visitedCharsSize && left <= right) {
                String currentSubstring = bigString.substring(left, right + 1);
                if (result.length() == 0 || result.length() > currentSubstring.length()) {
                    result = currentSubstring;
                }

                char leftChar = bigString.charAt(left);
                if (!smallStrChars.containsKey(leftChar)) {
                    left++;
                    continue;
                }

                if (smallStrChars.get(leftChar).equals(visitedChars.get(leftChar))) {
                    visitedCharsSize--;
                }
                // remove first char from visited
                decreaseVisit(bigString.charAt(left), visitedChars);
                left++;
            }
            right++;
        }

        return result;
    }

    private static void increaseVisit(Character c, Map<Character, Integer> visitedChars) {
        visitedChars.putIfAbsent(c, 0);
        visitedChars.put(c, visitedChars.get(c) + 1);
    }

    private static void decreaseVisit(Character c, Map<Character, Integer> visitedChars) {
        int visited = visitedChars.get(c);
        visitedChars.put(c, visited - 1);
    }

    public static String smallestSubstringContainingOptimal1(String bigString, String smallString) {
        if (bigString.length() <= smallString.length()) {
            return bigString;
        }

        StringBuilder smallest = new StringBuilder();
        // key -> characters in small str
        // value -> N of their appearance in the str
        Map<Character, Integer> smallStrValues = new HashMap<>();
        // fill the map
        fillMap(smallStrValues, smallString);
        int uniqueCharsSize = smallStrValues.size();
        // iterate through big string
        // nested loop
        // check for each substring
        int leftIndex = 0;
        Map<Character, Integer> visited = new HashMap<>();
        int visitedSize = 0;
       int right = leftIndex;
        while(leftIndex < bigString.length()) {
           if (!smallStrValues.containsKey(bigString.charAt(right))) {
               leftIndex++;
               continue;
           }
           StringBuilder currentSubstring = new StringBuilder();
           while(right < bigString.length()) {
               if (visitedSize == uniqueCharsSize) {
                   break;
               }
               char current = bigString.charAt(right);
               currentSubstring.append(current);
               if (smallStrValues.containsKey(current)) {
                   visited.putIfAbsent(current, 1);
                   int visitedN = visited.get(current);
                   if (visitedN < smallStrValues.get(current)) {
                       visited.put(current, ++visitedN);
                   }
                   if (visited.get(current).equals(smallStrValues.get(current))) {
                       visitedSize++;
                   }
               }
               right++;
           }

           if (smallest.length() == 0 || smallest.length() > currentSubstring.length()) {
               smallest = currentSubstring;
           }

           // move left pointer
           while(visitedSize == uniqueCharsSize && leftIndex < bigString.length()) {
               char current = bigString.charAt(leftIndex);
               if (visited.containsKey(current)) {
                   visited.put(current, visited.get(current) - 1);
                   if (visited.get(current) == 0) {
                       visitedSize--;
                   }
               }
               leftIndex++;
           }
        }

        return smallest.toString();
    }

    // O(s) space -> S is small string length
    // O(N*N) time -> N is big string length
    public static String smallestSubstringContaining(String bigString, String smallString) {
        if (bigString.length() <= smallString.length()) {
            return bigString;
        }

        StringBuilder smallest = new StringBuilder();
        // key -> characters in small str
        // value -> N of their appearance in the str
        Map<Character, Integer> smallStrValues = new HashMap<>();
        // fill the map
        fillMap(smallStrValues, smallString);
        // iterate through big string
        // nested loop
        // check for each substring
        for (int i = 0; i < bigString.length(); i++) {
            StringBuilder currentSubstring = new StringBuilder();
            int remainingStrLength = smallString.length();
            // copy existing map
            Map<Character, Integer> currentSmallStrValues = new HashMap<>(smallStrValues);
            for (int j = i; j < bigString.length(); j++) {
                if (remainingStrLength == 0) {
                    break;
                }
                Character current = bigString.charAt(j);
                currentSubstring.append(current);
                if (currentSmallStrValues.containsKey(current) && currentSmallStrValues.get(current) != 0) {
                    currentSmallStrValues.put(current, currentSmallStrValues.get(current) - 1);
                    remainingStrLength--;
                }
            }

            if (remainingStrLength != 0) {
                continue;
            }

            if (smallest.length() == 0 || currentSubstring.length() < smallest.length()) {
                smallest = currentSubstring;
            }
        }

        return smallest.toString();
    }

    private static void fillMap(Map<Character, Integer> smallStrValues, String smallString) {
        for (int i = 0; i < smallString.length(); i++) {
            Character current = smallString.charAt(i);
            if (smallStrValues.containsKey(current)) {
                int count = smallStrValues.get(current);
                smallStrValues.put(current, count + 1);
            } else {
                smallStrValues.put(current, 1);
            }
        }
    }
}
