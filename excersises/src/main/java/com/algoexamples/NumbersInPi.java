package com.algoexamples;

import java.util.*;

public class NumbersInPi {
    public static void main(String[] args) {
        String pi = "3141592653589793238462643383279";
        String[] numbers = {
                "314159265358979323846",
                "26433",
                "8",
                "3279",
                "314159265",
                "35897932384626433832",
                "79"
        };
        System.out.println(numbersInPi(pi, numbers));
    }

    public static int numbersInPi(String pi, String[] numbers) {
        // convert arr of numbers to a set
        Set<String> set = new HashSet<>(Arrays.asList(numbers));
        // key -> index
        // value -> min spaces FROM that index
        Map<Integer, Integer> cache = new HashMap<>();
        var min = numbersRecursive(pi, set, 0, cache);
        return min == Integer.MAX_VALUE ? -1 : min;
    }

    private static int numbersRecursive(String pi, Set<String> numbers, int fromIndex, Map<Integer, Integer> cache) {
        if (fromIndex == pi.length()) {
            return -1;
        }

        if (cache.containsKey(fromIndex)) {
            return cache.get(fromIndex);
        }

        int minSpace = Integer.MAX_VALUE;
        for (int i = fromIndex; i < pi.length(); i++) {
            String substr = pi.substring(fromIndex, i + 1);
            if (numbers.contains(substr)) {
                var minSpaceInSuffix = numbersRecursive(pi, numbers, i + 1, cache);
                if (minSpaceInSuffix != Integer.MAX_VALUE) {
                    minSpace = Math.min(minSpace, minSpaceInSuffix + 1);
                }
            }
        }
        cache.put(fromIndex, minSpace);
        return cache.get(fromIndex);
    }
}
