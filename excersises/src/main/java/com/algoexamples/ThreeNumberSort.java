package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreeNumberSort {
    private static final Logger logger = LoggerFactory.getLogger(ThreeNumberSort.class);

    public static void main(String[] args) {
//        int[] input = {-2, -3, -3, -3, -3, -3, -2, -2, -3};
        int[] input = {0, 10, 10, 10, 10, 10, 25, 25, 25, 25, 25};
//        int[] order = {-2, -3, 0};
        int[] order = {25, 10, 0};
        var sorted = threeNumberSort1(input, order);

        for (int i : sorted) {
            logger.info(String.format("Next element is %d", i));
        }
    }

    // O(N) time
    // O(1) complexity
    private static int[] threeNumberSort1(int[] array, int[] order) {
        if (array.length == 0) {
            return new int[] {};
        }

        int left = 0;
        int current = 0;
        int last = array.length - 1;

        while(left < array.length && array[left] == order[0]) {
            left++;
            current++;
        }

        while(last >= 0 && array[last] == order[2]) {
            last--;
        }

        while(current <= last) {
            if (array[current] == order[0]) {
                int tmpCurrent = array[current];
                array[current] = array[left];
                array[left] = tmpCurrent;
                left++;
                current = Math.max(current, left);
                continue;
            }

            // if last elem in order
            if (array[current] == order[2]) {
                int tmpCurrent = array[current];
                array[current] = array[last];
                array[last] = tmpCurrent;
                last--;
                continue;
            }

            current++;
        }

        return array;
    }

    private static int[] threeNumberSort(int[] array, int[] order) {
        if (array.length == 0) {
            return new int[] {};
        }

        int leftCount = 0;
        int midCount = 0;

        for (int i : array) {
            if (i == order[0]) {
                leftCount++;
            }

            if (i == order[1]) {
                midCount++;
            }
        }

        for (int i = 0; i < leftCount; i++) {
            array[i] = order[0];
        }

        for (int j = leftCount; j < leftCount + midCount; j++) {
            array[j] = order[1];
        }

        for (int n = leftCount + midCount; n < array.length; n++) {
            array[n] = order[2];
        }

        return array;
    }

    public static int[] threeNumberSort2(int[] array, int[] order) {
        int first = 0;
        int current = first;
        int last = array.length - 1;

        while(current <= last) {
            int val = array[current];
            if (val == order[0]) {
                // swap current and first
                swap(array, first, current);
                current++;
                first++;
            }

            else if (val == order[1]) {
                current++;
            }

            else if (val == order[2]) {
                // swap
                swap(array, current, last);
                last--;
            }
        }

        return array;
    }

    private static void swap(int[] array, int indx1,  int indx2) {
        int tmp = array[indx1];
        array[indx1] = array[indx2];
        array[indx2] = tmp;
    }
}
