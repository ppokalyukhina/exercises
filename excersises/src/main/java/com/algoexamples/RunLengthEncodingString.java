package com.algoexamples;

import com.integers.LargestRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunLengthEncodingString {
    private static final Logger logger = LoggerFactory.getLogger(RunLengthEncodingString.class);

    public static void main(String[] args) {
        String given = "AAAAAAAAAAAAABBCCCCDD";
//        String given = "ABC";
        logger.info(String.format("Encoded string is: %s", runLengthEncoding(given)));
    }

    private static String runLengthEncoding(String string) {
        StringBuilder result = new StringBuilder();
        int length = 1;

        for (int i = 1; i < string.length(); i++) {
            char current = string.charAt(i);
            char previous = string.charAt(i - 1);

            if (current != previous || length == 9) {
                result.append(length).append(previous);
                length = 0;
            }

            length++;
        }
        result.append(length).append(string.charAt(string.length() - 1));
        return result.toString();
    }
}
