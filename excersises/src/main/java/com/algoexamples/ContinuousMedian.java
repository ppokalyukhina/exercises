package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ContinuousMedian {
    private static final Logger logger = LoggerFactory.getLogger(ContinuousMedian.class);

    public static void main(String[] args) {
        var median = new ContinuousMedianHandler();
        median.insert(5);
        logger.info(String.format("Median is: %s", median.getMedian()));
        median.insert(10);
        logger.info(String.format("Median is: %s", median.getMedian()));
        median.insert(100);
        logger.info(String.format("Median is: %s", median.getMedian()));
        median.insert(200);
        logger.info(String.format("Median is: %s", median.getMedian()));
    }

    static class ContinuousMedianHandler {
        double median = 0;
        private final List<Integer> leftMaxHeap;
        private final List<Integer> rightMinHeap;

        public ContinuousMedianHandler() {
            leftMaxHeap = new ArrayList<>();
            rightMinHeap = new ArrayList<>();
        }

        public void insert(int number) {
            if (leftMaxHeap.isEmpty()) {
                leftMaxHeap.add(number);
                median = number;
                return;
            }

            Integer leftRoot = leftMaxHeap.get(0);
            if (number <= leftRoot) {
                insertIntoMaxHeap(number, leftMaxHeap);
            } else {
                insertIntoMinHeap(number, rightMinHeap);
            }

            checkAndResizeHeaps();
            median = calculateMedian();
        }

        private double calculateMedian() {
            int leftHeapSize = leftMaxHeap.size();
            int rightHeapSize = rightMinHeap.size();
            if (leftHeapSize == rightHeapSize) {
                median = (double) (leftMaxHeap.get(0) + rightMinHeap.get(0)) / 2;
            } else if (leftHeapSize > rightHeapSize) {
                median = leftMaxHeap.get(0);
            } else {
                median = rightMinHeap.get(0);
            }

            return median;
        }

        private void checkAndResizeHeaps() {
            int leftHeapSize = leftMaxHeap.size();
            int rightHeapSize = rightMinHeap.size();
            if (leftHeapSize - rightHeapSize > 1) {
                // move left root to the right heap
                int removed = removeFromMaxHeap(leftMaxHeap);
                insertIntoMinHeap(removed, rightMinHeap);
            }
            else if (rightHeapSize - leftHeapSize > 1) {
                // move right root to the left heap
                int removed = removeFromMinHeap(rightMinHeap);
                insertIntoMaxHeap(removed, leftMaxHeap);
            }
        }

        public double getMedian() {
            return median;
        }

        private void insertIntoMaxHeap(int numberToInsert, List<Integer> heap) {
            heap.add(numberToInsert);

            int currentIndex = heap.size() - 1;
            int parentIndex = (heap.size() - 2) / 2;
            while(parentIndex >= 0 && heap.get(parentIndex) < heap.get(currentIndex)) {
                int tmp = heap.get(parentIndex);
                heap.set(parentIndex, heap.get(currentIndex));
                heap.set(currentIndex, tmp);
                currentIndex = parentIndex;
                parentIndex = (currentIndex - 1) / 2;
            }
        }

        private int removeFromMaxHeap(List<Integer> heap) {
            int toRemove = heap.get(0);
            // swap root and last elements
            heap.set(0, heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);
            // sift down element if needed
            siftMaxDown(heap, 0);

            return toRemove;
        }

        private int removeFromMinHeap(List<Integer> heap) {
            int toRemove = heap.get(0);
            // swap root and last elements
            heap.set(0, heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);
            // sift down element if needed
            siftMinDown(heap, 0);

            return toRemove;
        }

        private void siftMinDown(List<Integer> heap, int index) {
            int leftChild = index * 2 + 1;
            int currentIndex = index;

            while(leftChild < heap.size()) {
                int indexToCompare = leftChild;
                if (leftChild + 1 < heap.size() && heap.get(leftChild + 1) < heap.get(leftChild)) {
                    indexToCompare = leftChild + 1;
                }

                if (heap.get(currentIndex) > heap.get(indexToCompare)) {
                    // swap
                    int tmp = heap.get(currentIndex);
                    heap.set(currentIndex, heap.get(indexToCompare));
                    heap.set(indexToCompare, tmp);
                }

                currentIndex = indexToCompare;
                leftChild = currentIndex * 2 + 1;
            }

        }

        private void siftMaxDown(List<Integer> heap, int index) {
            int leftChild = index * 2 + 1;
            int currentIndex = index;

            while(leftChild < heap.size()) {
                int indexToCompare = leftChild;
                if (leftChild + 1 < heap.size() && heap.get(leftChild + 1) > heap.get(leftChild)) {
                    indexToCompare = leftChild + 1;
                }

                if (heap.get(currentIndex) < heap.get(indexToCompare)) {
                    // swap
                    int tmp = heap.get(currentIndex);
                    heap.set(currentIndex, heap.get(indexToCompare));
                    heap.set(indexToCompare, tmp);
                }

                currentIndex = indexToCompare;
                leftChild = currentIndex * 2 + 1;
            }
        }


        private void insertIntoMinHeap(int numberToInsert, List<Integer> heap) {
            heap.add(numberToInsert);

            int currentIndex = heap.size() - 1;
            int parentIndex = (heap.size() - 2) / 2;
            while(parentIndex >= 0 && heap.get(parentIndex) > heap.get(currentIndex)) {
                int tmp = heap.get(parentIndex);
                heap.set(parentIndex, heap.get(currentIndex));
                heap.set(currentIndex, tmp);
                currentIndex = parentIndex;
                parentIndex = (currentIndex - 1) / 2;
            }
        }
    }
}
