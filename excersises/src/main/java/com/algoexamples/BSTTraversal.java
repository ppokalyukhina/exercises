package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BSTTraversal {
    private static final Logger logger = LoggerFactory.getLogger(BSTTraversal.class);

    public static void main(String[] args) {
        //       10
        //     /    \
        //    5     15
        //  /   \     \
        // 2    5      22
        // /
        // 1

        BST root = new BST(10);

        // left leaf
        var one = new BST(1);
        var two = new BST(2);
        two.left = one;
        var five = new BST(5);
        var five1 = new BST(5);
        five.left = two;
        five.right = five1;

        root.left = five;

        // right leaf
        var fifteen = new BST(15);
        fifteen.right = new BST(22);
        root.right = fifteen;

        var inOrder = inOrderTraverse(root, new ArrayList<>());
        var preOrder = preOrderTraverse(root, new ArrayList<>());
        var postOrder = postOrderTraverse(root, new ArrayList<>());
    }

    public static List<Integer> inOrderTraverse(BST tree, List<Integer> array) {
        if (tree == null) {
            return array;
        }

        inOrderTraverse(tree.left, array);
        array.add(tree.value);
        inOrderTraverse(tree.right, array);

        return array;
    }

    public static List<Integer> preOrderTraverse(BST tree, List<Integer> array) {
        if (tree == null) {
            return array;
        }

        array.add(tree.value);
        preOrderTraverse(tree.left, array);
        preOrderTraverse(tree.right, array);

        return array;
    }

    public static List<Integer> postOrderTraverse(BST tree, List<Integer> array) {
        if (tree == null) {
            return array;
        }

        postOrderTraverse(tree.left, array);
        postOrderTraverse(tree.right, array);
        array.add(tree.value);

        return array;
    }

    static class BST {
        public int value;
        public BST left;
        public BST right;

        public BST(int value) {
            this.value = value;
        }
    }
}
