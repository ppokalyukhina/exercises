package com.algoexamples;

import com.nodes.InsertSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InsertionSort {
    private static final Logger logger = LoggerFactory.getLogger(InsertSort.class);

    public static void main(String[] args) {
        int[] arr = {8, 5, 2, 9, 5, 6, 3};
        var result = solution3(arr);

        for (int i : result) {
            logger.info(String.format("Next element is: %d", i));
        }
    }

    private static int[] solution3(int[] array) {
        if (array.length == 0) {
            return array;
        }

        for (int i = 1; i < array.length; i++) {
            int current = i;
            while(current > 0 && array[current] < array[current - 1]) {
                // swap
                int tmp = array[current];
                array[current] = array[current - 1];
                array[current - 1] = tmp;

                current--;
            }
        }

        return array;
    }

    private static int[] solution2(int[] array) {
        int mid = array.length / 2;
        for (int j = 0; j < mid - 1; j++) {
            for (int i = 0; i < mid - 1; i++) {
                sort(i, array);
            }
        }

        int counter = array.length - mid;
        for (int i = mid; i < array.length; i++) {
            if (counter == 0) {
                break;
            }
            // move element into a right place
            int current = i;
            while(current > 0 && array[current - 1] > array[current]) {
                int tmp = array[current];
                array[current] = array[current - 1];
                array[current - 1] = tmp;
                current--;
            }

            counter--;
        }

        return array;
    }

    private static int[] insertionSort(int[] array) {
        for (int j = 0; j < array.length - 1; j++) {
            for (int i = 0; i < array.length - 1; i++) {
                int current = i;
                while(current < array.length - 1) {
                    int currentElem = array[current];
                    int next = array[current + 1];
                    if (currentElem <= next) {
                        break;
                    }

                    // swap
                    array[current] = next;
                    array[current + 1] = currentElem;
                    current++;
                }
            }
        }

        return array;
    }

    private static void sort(int i, int[] array) {
        int current = i;
        while(current < array.length - 1) {
            int currentElem = array[current];
            int next = array[current + 1];
            if (currentElem <= next) {
                break;
            }

            // swap
            array[current] = next;
            array[current + 1] = currentElem;
            current++;
        }
    }
}
