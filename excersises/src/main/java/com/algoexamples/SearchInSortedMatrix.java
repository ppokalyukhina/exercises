package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchInSortedMatrix {
    private static final Logger logger = LoggerFactory.getLogger(SearchInSortedMatrix.class);

    public static void main(String[] args) {
        int[][] input = {
                {1, 4, 7, 12, 15, 1000},
                {2, 5, 19, 31, 32, 1001},
                {3, 8, 24, 33, 35, 1002},
                {40, 41, 42, 44, 45, 1003},
                {99, 100, 103, 106, 128, 1004}
        };
        int target = 100;
        int[] result = searchInSortedMatrix1(input, target);
        logger.info(String.format("The location is [%d][%d]", result[0], result[1]));
    }

    public static int[] searchInSortedMatrix1(int[][] matrix, int target) {
        int[] noResult = new int[] {-1, -1};
        int row = 0;
        int col = matrix[row].length - 1;
        while(row < matrix.length && col >= 0) {
            if (matrix[row][col] == target) {
                return new int[]{row, col};
            }
            if (matrix[row][col] < target) {
                row++;
            } else {
                col--;
            }
        }

        return noResult;
    }

    private static boolean isOutOfBounds(int[][] matrix, int row, int col) {
        return row < 0 || col < 0 || row >= matrix.length || col >= matrix[row].length;
    }

    public static int[] searchInSortedMatrix(int[][] matrix, int target) {
        int row = 0;
        int col = 0;
        int currentVal = 0;
        while(row < matrix.length && col < matrix[row].length && matrix[row][col] <= target) {
            currentVal = matrix[row][col];
            if (currentVal == target) {
                return new int[]{row, col};
            }
            row++;
            col++;
        }

        row--;
        col--;
        // search in the current row
        while(col < matrix[row].length && matrix[row][col] <= target) {
            if (matrix[row][col] == target) {
                return new int[]{row, col};
            }
            col++;
        }

        // search in the current column
        if (matrix[row][col] > target) {
            int tmpRow = row;
            while(tmpRow >= 0) {
                if (matrix[tmpRow][col] == target) {
                    return new int[]{tmpRow, col};
                }
                tmpRow--;
            }
        }

        row++;
        col = 0;
        if (matrix[row][col] == target) {
            return new int[]{row, col};
        }

        // search in the next row
        while(matrix[row][col] <= currentVal) {
            if (matrix[row][col] == target) {
                return new int[]{row, col};
            }
            col++;
        }

        // the value was not found
        return new int[] {-1, -1};
    }
}
