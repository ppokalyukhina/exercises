package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaxNonAdjSum {
    private static final Logger logger = LoggerFactory.getLogger(MaxNonAdjSum.class);

    public static void main(String[] args) {
        int[] input = {75, 105, 120, 75, 90, 135};

        logger.info(String.format("Max sum is: %d", maxSubsetSumNoAdjacent(input)));
    }

    private static int maxSubsetSumNoAdjacent(int[] array) {
        if (array.length == 0) {
            return 0;
        }

        int inclusive = 0;
        int exclusive = 0;

        for (int j : array) {
            int tmpExclusive = exclusive;
            exclusive = Math.max(inclusive, exclusive);
            inclusive = Math.max(tmpExclusive + j, inclusive);
        }

        return Math.max(inclusive, exclusive);
    }
}
