package com.algoexamples;

public class HeightBalancedBinaryTree {
    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        BinaryTree two = new BinaryTree(2);
        BinaryTree three = new BinaryTree(3);
        BinaryTree four = new BinaryTree(4);
        BinaryTree five = new BinaryTree(5);
        BinaryTree six = new BinaryTree(6);
        BinaryTree seven = new BinaryTree(7);
        BinaryTree eight = new BinaryTree(8);
        BinaryTree nine = new BinaryTree(9);

        root.left = two;
        root.right = three;
        three.right = six;
        two.left = four;
        two.right = five;
        five.left = seven;
        five.right = eight;
        eight.right = nine;

        System.out.println(heightBalancedBinaryTree(root));
    }

    // O(N) time (visit each node to check the height
    // O(h) space -> stack of recursion methods
    public static boolean heightBalancedBinaryTree(BinaryTree tree) {
        var treeInfo = getTreeInfo(tree);
        return treeInfo.isBalanced;
    }

    private static SubTreeInfo getTreeInfo(BinaryTree currentRoot) {
        if (currentRoot == null) {
            return new SubTreeInfo(true, 0);
        }

        SubTreeInfo leftHeight = getTreeInfo(currentRoot.left);
        SubTreeInfo rightHeight = getTreeInfo(currentRoot.right);

        boolean leftIsBalanced = leftHeight.isBalanced;
        boolean rightIsBalanced = rightHeight.isBalanced;
        boolean leftAndRightBalanced = Math.abs(leftHeight.maxHeight - rightHeight.maxHeight) <= 1;

        return new SubTreeInfo(leftIsBalanced && rightIsBalanced && leftAndRightBalanced, Math.max(leftHeight.maxHeight, rightHeight.maxHeight) + 1);
    }

    private static class SubTreeInfo {
        boolean isBalanced;
        int maxHeight;

        public SubTreeInfo(boolean isBalanced, int maxHeight) {
            this.isBalanced = isBalanced;
            this.maxHeight = maxHeight;
        }
    }

    private static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
