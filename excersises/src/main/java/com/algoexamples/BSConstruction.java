package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BSConstruction {
    private static final Logger logger = LoggerFactory.getLogger(BSConstruction.class);

    public static void main(String[] args) {
        BST bst = new BST(10);
        bst.insert(5);
        bst.insert(15);
        bst.insert(2);
        bst.insert(5);
//        bst.insert(13);
//        bst.insert(22);
//        bst.insert(12);
//        bst.insert(14);

        bst.remove(5);
//        bst.remove(15);
//        bst.remove(10);

//        int searchValue = 15;
//        logger.info(String.format("BST contains value %d: %s", searchValue, bst.contains(searchValue)));
    }

    static class BST {
        public int value;
        public BST left;
        public BST right;

        public BST(int value) {
            this.value = value;
        }

        public BST insert(int value) {
            BST current = this;

            while(true) {
                // left wing only
                if (value < current.value) {
                    if(current.left == null) {
                        current.left = new BST(value);
                        break;
                    }

                    current = current.left;
                }

                // else value >= this.value
                else {
                    if (current.right == null) {
                        current.right = new BST(value);
                        break;
                    }

                    current = current.right;
                }
            }

            return this;
        }

        public boolean contains(int value) {
            BST current = this;
            while(current != null) {
                if (value == current.value) {
                    return true;
                }

                if (value < this.value) {
                    current = current.left;
                }
                if (value >= this.value) {
                    current = current.right;
                }
            }

            return false;
        }

        public BST remove(int value) {
            remove(value, null);
            return this;
        }

        private void remove(int value, BST parent) {
            BST current = this;
            while(current != null) {
                if (value < current.value) {
                    parent = current;
                    current = current.left;
                }

                else if (value > current.value) {
                    parent = current;
                    current = current.right;
                }

                else {
                    // if there are both children present..
                    if(current.left != null && current.right != null) {
                        current.value = current.right.getMinValue();
                        current.right.remove(current.value, current);
                    }

                    else if (parent == null) {
                        // if ony left branch is present
                        if (current.left != null) {
                            current.value = current.left.value;
                            current.right = current.left.right;
                            current.left = current.left.left;
                        }

                        // if only right branch is present
                        else if (current.right != null) {
                            current.value = current.right.value;
                            current.left = current.right.left;
                            current.right = current.right.right;
                        }
                    }

                    else if (parent.left == current) {
                        parent.left = current.left != null ? current.left : current.right;
                    }

                    else if (parent.right == current) {
                        parent.right = current.left != null ? current.left : current.right;
                    }
                    break;
                }
            }
        }

        private int getMinValue() {
            if (left == null) {
                return value;
            }

            return left.getMinValue();
        }
    }

    /**
     * Recursive solution
     */
    static class BSTRecursive {
        public int value;
        public BSTRecursive left;
        public BSTRecursive right;

        public BSTRecursive(int value) {
            this.value = value;
        }

        public BSTRecursive insert(int value) {
            if (value < this.value) {
                if (left == null) {
                    left = new BSTRecursive(value);
                } else {
                    left.insert(value);
                }
            } else {
                if (right == null) {
                    right = new BSTRecursive(value);
                } else {
                    right.insert(value);
                }
            }

            return this;
        }

        public boolean contains(int value) {
            if (value == this.value) {
                return true;
            }

            if (value < this.value) {
                if (left == null) {
                    return false;
                }
                return left.contains(value);
            }

            if (right == null) {
                return false;
            }

            return right.contains(value);
        }

        public BSTRecursive remove(int value) {
            remove(value, null);
            return this;
        }

        private void remove(int value, BSTRecursive parent) {
            // search for value in the left subtree
            if (value < this.value) {
                if (left != null) {
                    left.remove(value, this);
                }
            }

            // search for value in the right subtree
            else if (value > this.value) {
                if (right != null) {
                    right.remove(value, this);
                }
            }

            else {
                if (left != null && right != null) {
                    this.value = right.getMinValue();
                    right.remove(this.value, this);
                }
                // if requested value is root (no parent)
                // and one of branches is null
                // so basically one branch tree
                else if (parent == null) {
                    // only left branch is present
                    if (left != null) {
                        this.value = left.value;
                        right = left.right;
                        left = left.left;
                    }

                    // only right branch is present
                    else if (right != null) {
                        this.value = right.value;
                        left = right.left;
                        right = right.right;
                    }

                    // if it's a single node BST
                    // do nothing
                }

                // not a root
                else if (parent.left == this) {
                    parent.left = left != null ? left : right;
                }

                else if (parent.right == this) {
                    parent.right = left != null ? left : right;
                }
            }
        }

        private int getMinValue() {
            if (left == null) {
                return this.value;
            }

            return left.getMinValue();
        }
    }
}
