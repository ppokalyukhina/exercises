package com.algoexamples;

public class SearchForRange {
    public static void main(String[] args) {
        int[] arr = {0, 1, 21, 33, 45, 45, 45, 45, 45, 45, 61, 71, 73};
        int target = 45;

        int[] resultRange = searchForRange(arr, target);
        System.out.println("[" + resultRange[0] + "," + resultRange[1] + "]");
    }

    public static int[] searchForRange(int[] array, int target) {
        int[] result = new int[]{-1, -1};
        doSearch(array, target, result, true);
        doSearch(array, target, result, false);

        return result;
    }

    private static void doSearch(int[] array, int target, int[] result, boolean goLeft) {
        int left = 0;
        int right = array.length - 1;

        while(left <= right) {
            int mid = left + right >>> 1;
            if (target < array[mid]) {
                right = mid - 1;
            } else if (target > array[mid]) {
                left = mid + 1;
            }

            // if target == array[mid]
            else {
                if (goLeft) {
                    if (mid == 0 || array[mid - 1] != target) {
                        result[0] = mid;
                        return;
                    } else {
                        right = mid - 1;
                    }
                } else {
                    if (mid == array.length - 1 || array[mid + 1] != target) {
                        result[1] = mid;
                        return;
                    } else {
                        left = mid + 1;
                    }
                }
            }
        }
    }

    /**
     * O(1) space
     * O(log(N)) time
     */
    public static int[] searchForRangeVariation(int[] array, int target) {
        int left = 0;
        int right = array.length - 1;
        while(left <= right) {
            int mid = left + right >>> 1;

            if (target == array[mid]) {
                left = mid;
                right = mid;
                while(left >= 0 && array[left] == target) {
                    left--;
                }

                while(right < array.length && array[right] == target) {
                    right++;
                }

                return new int[]{left + 1, right - 1};
            }

            if (target < array[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return new int[] {-1, -1};
    }
}
