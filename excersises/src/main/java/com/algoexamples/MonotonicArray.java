package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MonotonicArray {
    private static final Logger logger = LoggerFactory.getLogger(MonotonicArray.class);

    public static void main(String[] args) {
        int[] input = {-1, -5, -10, -1100, -1100, -1101, -1102, -9001};
        logger.info(String.format("Array is monolitic: %s", isMonotonic(input)));
    }

    private static boolean isMonotonic(int[] array) {
        if (array.length <= 1) {
            return true;
        }

        int first = array[0];
        int last = array[array.length - 1];
        int diff = last - first;

        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int previous = array[i - 1];

            // if array is increasing
            if (diff > 0) {
                if (current < previous || current > last) {
                    return false;
                }
                continue;
            }

            // if array is decreasing
            if (diff < 0) {
                if (current > previous || current < last) {
                    return false;
                }

                continue;
            }

            // if array has same elements
            if (current != previous) {
                return false;
            }
        }

        return true;
    }
}
