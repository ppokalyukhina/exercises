package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class BalancedBrakets {
    private static final Logger logger = LoggerFactory.getLogger(BalancedBrakets.class);

    public static void main(String[] args) {
        String s = "([]){0}";
        logger.info(String.format("String is balanced: %s", balancedBrackets(s)));
    }

    public static boolean balancedBrackets(String str) {
        if (str.isEmpty()) {
            return true;
        }
        // map of closing and opening brackets
        Map<String, String> brackets = new HashMap<>();
        brackets.put(")", "(");
        brackets.put("]", "[");
        brackets.put("}", "{");
        brackets.put("{", "}");
        brackets.put("[", "]");
        brackets.put("(", ")");

        Stack<String> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            String current = str.substring(i, i + 1);
            if (!brackets.containsKey(current)) {
                continue;
            }

            if (!stack.isEmpty() && brackets.get(stack.peek()).equals(current)) {
                stack.pop();
            } else {
                stack.push(current);
            }
        }

        return stack.isEmpty();
    }
}
