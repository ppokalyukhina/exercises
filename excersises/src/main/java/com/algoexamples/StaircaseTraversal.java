package com.algoexamples;

import java.util.HashMap;
import java.util.Map;

public class StaircaseTraversal {
    public static void main(String[] args) {
        int height = 10;
        int maxSteps = 1;
        System.out.println(staircaseTraversalBestSolution(height, maxSteps));
    }

    public static int staircaseTraversalBestSolution(int height, int maxSteps) {
        int[] steps = new int[height + 1];
        steps[0] = 1;
        steps[1] = 1;
        int maxWays = 1;

        for (int stair = 1; stair < steps.length; stair++) {
            steps[stair] = maxWays;
            if (stair >= maxSteps) {
                // else subtract last value from the maxWays
                maxWays = maxWays - steps[stair - maxSteps];
            }
            maxWays += steps[stair];
        }

        return steps[height];
    }

    public static int staircaseTraversalRecursive(int height, int maxSteps) {
        // key -> height
        // value -> max ways
        Map<Integer, Integer> cache = new HashMap<>();
        cache.put(0, 1);
        cache.put(1, 1);
        return calculateMaxWays(height, maxSteps, cache);
    }

    private static int calculateMaxWays(int height, int maxSteps, Map<Integer, Integer> cache) {
        if (cache.containsKey(height)) {
            return cache.get(height);
        }

        if (height <= 1) {
            return 1;
        }

        int maxWays = 0;
        for (int step = 1; step <= Math.min(height, maxSteps); step++) {
            int nextStair = height - step;
            int maxWaysBefore = calculateMaxWays(nextStair, maxSteps, cache);
            cache.put(nextStair, maxWaysBefore);
            maxWays += maxWaysBefore;
        }

        return maxWays;
    }

    // O(h*s) time
    // O(h) space
    // -> h is height
    // -> s are max steps
    public static int staircaseTraversal(int height, int maxSteps) {
        int[] steps = new int[height + 1];

        for (int stair = 0; stair < steps.length; stair++) {
            if (stair <= 1) {
                steps[stair] = 1;
                continue;
            }

            int stairIndex = stair;
            int currentMaxWays = 0;
            while (stairIndex >= 0 && stairIndex >= stair - maxSteps) {
                currentMaxWays += steps[stairIndex];
                stairIndex--;
            }

            steps[stair] = currentMaxWays;
        }

        return steps[height];
    }
}
