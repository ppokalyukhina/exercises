package com.algoexamples;

public class ReverseWordsInString {
    public static void main(String[] args) {
//        String s = "1 12 23 34 56";
        String s = "AlgoExpert is the best!";
        var reversed = reverseWordsInString(s);
        System.out.println(reversed);
    }

    public static String reverseWordsInString(String string) {
        if (string.isEmpty()) {
            return string;
        }

        StringBuilder builder = new StringBuilder();
        int index = 0;
        while(index < string.length()) {
            var space = string.substring(index).indexOf(' ');
            if (space == -1) {
                builder.insert(0, string.substring(index));
                break;
            }
            String substr = string.substring(index, index + space);
            builder.insert(0, substr);
            builder.insert(0, ' ');
            index = index + space + 1;
        }

        return builder.toString();
    }
}
