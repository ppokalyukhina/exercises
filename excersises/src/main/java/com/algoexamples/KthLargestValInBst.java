package com.algoexamples;

import java.util.ArrayList;
import java.util.List;

public class KthLargestValInBst {
    public static void main(String[] args) {
        BST root = new BST(15);
        BST five = new BST(5);
        BST five1 = new BST(5);
        BST two = new BST(2);
        BST one = new BST(1);
        BST three = new BST(3);
        BST twenty = new BST(20);
        BST seventeen = new BST(17);
        BST twentyTwo = new BST(22);
        root.left = five;
        five.right = five1;
        five.left = two;
        two.left = one;
        two.right = three;

        root.right = twenty;
        twenty.left = seventeen;
        twenty.right = twentyTwo;

        int k = 3;
        System.out.println(findKthLargestValueInBst(root, k));
    }

    public static int findKthLargestValueInBst1(BST tree, int k) {
        ArrayList<Integer> sorted = new ArrayList<>();
        postOrder(tree, sorted);
        return sorted.get(sorted.size() - k);
    }

    private static void postOrder(BST root, List<Integer> latest) {
        if (root == null) {
            return;
        }

        postOrder(root.left, latest);
        latest.add(root.value);
        postOrder(root.right, latest);
    }

    public static int findKthLargestValueInBst(BST tree, int k) {
        BSTinfo info = new BSTinfo(0, -1);
        findLatest(tree, k, info);
        return info.value;
    }

    private static void findLatest(BST current, int k, BSTinfo treeInfo) {
        if (current == null || treeInfo.visited >= k) {
            return;
        }

        findLatest(current.right, k, treeInfo);
        if (treeInfo.visited < k) {
            treeInfo.visited += 1;
            treeInfo.value = current.value;
            findLatest(current.left, k, treeInfo);
        }
    }

    private static class BSTinfo {
        int visited;
        int value;

        public BSTinfo(int visited, int value) {
            this.visited = visited;
            this.value = value;
        }
    }

    private static class BST {
        public int value;
        public BST left = null;
        public BST right = null;

        public BST(int value) {
            this.value = value;
        }
    }
}
