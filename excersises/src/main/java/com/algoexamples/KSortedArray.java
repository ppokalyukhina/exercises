package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class KSortedArray {
    private static final Logger logger = LoggerFactory.getLogger(KSortedArray.class);

    public static void main(String[] args) {
//        int[] array = {3, 2, 1, 5, 4, 7, 6, 5};
        int[] array = {1, 2, 3, 4, 5, 6, 1};
        int k = 8;
//        int k = 2;
        var result = sortKSortedArrayinPlace(array, k);
        for (int i : result) {
            logger.info(String.format("%d", i));
        }
    }

    public static int[] sortKSortedArrayinPlace(int[] array, int k) {
        List<Integer> minHeap = new ArrayList<>();
        // add to min heap first 4 elements
        int arrayIndex;
        for (arrayIndex = 0; arrayIndex <= k && arrayIndex < array.length; arrayIndex++) {
            addToHeap(array[arrayIndex], minHeap);
        }

        int sortedIndex = 0;
        for (int i = k + 1; i < array.length; i++) {
            int min = removeMin(minHeap);
            array[sortedIndex++] = min;
            addToHeap(array[i], minHeap);
        }

        while(!minHeap.isEmpty() && sortedIndex < array.length) {
            int min = removeMin(minHeap);
            array[sortedIndex++] = min;
        }

        return array;
    }

    public static int[] sortKSortedArray(int[] array, int k) {
        int[] sorted = new int[array.length];

        List<Integer> minHeap = new ArrayList<>();
        // add to min heap first 4 elements
        int arrayIndex;
        for (arrayIndex = 0; arrayIndex <= k && arrayIndex < array.length; arrayIndex++) {
            addToHeap(array[arrayIndex], minHeap);
        }

        int sortedIndex = 0;
        for (int i = k + 1; i < array.length; i++) {
            int min = removeMin(minHeap);
            sorted[sortedIndex++] = min;
            addToHeap(array[i], minHeap);
        }

        while(!minHeap.isEmpty() && sortedIndex < sorted.length) {
            int min = removeMin(minHeap);
            sorted[sortedIndex++] = min;
        }

        return sorted;
    }

    private static void addToHeap(int element, List<Integer> miniHeap) {
        miniHeap.add(element);
        // siftUp if needed
        // last element - 1 / 2 -> formula for getting parent index
        siftUpLastElem(miniHeap);
    }

    private static void siftUpLastElem(List<Integer> miniHeap) {
        int currentIndex = miniHeap.size() - 1;
        while(currentIndex > 0) {
            int parentIndex = (currentIndex - 1) / 2;
            int parent = miniHeap.get(parentIndex);
            int current = miniHeap.get(currentIndex);

            if (parent > current) {
                swap(miniHeap, currentIndex, parentIndex);
            }

            currentIndex = parentIndex;
        }
    }

    private static int removeMin(List<Integer> miniHeap) {
        int min = miniHeap.get(0);
        // set root to last element
        int lastElemIndex = miniHeap.size() - 1;
        miniHeap.set(0, miniHeap.get(lastElemIndex));
        // remove last element
        miniHeap.remove(lastElemIndex);
        // sift down root element if needed
        siftRootDown(miniHeap);

        return min;
    }

    private static void siftRootDown(List<Integer> miniHeap) {
        int currentIndex = 0;
        int leftChildIndex = 1;
        while(leftChildIndex <= miniHeap.size() - 1) {
            int leftChild = miniHeap.get(leftChildIndex);
            int current = miniHeap.get(currentIndex);
            int indexToSwap = leftChildIndex;
            int rightChildIndex = currentIndex * 2 + 2;

            if (rightChildIndex <= miniHeap.size() - 1 && miniHeap.get(rightChildIndex) < leftChild) {
                indexToSwap = rightChildIndex;
            }

            if (current <= miniHeap.get(indexToSwap)) {
                break;
            }

            // swap left and current
            swap(miniHeap, currentIndex, indexToSwap);
            currentIndex = indexToSwap;
            leftChildIndex = currentIndex * 2 + 1;
        }
    }

    private static void swap(List<Integer> miniHeap, int index1, int index2) {
        int tmpElem1 = miniHeap.get(index1);
        miniHeap.set(index1, miniHeap.get(index2));
        miniHeap.set(index2, tmpElem1);
    }
}
