package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FourNumbersSums {
    private static final Logger logger = LoggerFactory.getLogger(FourNumbersSums.class);

    public static void main(String[] args) {
        int[] inputArr = {7, 6, 4, -1, 1, 2};
        int target = 16;

        var result = fourNumberSum(inputArr, target);
        for (Integer[] integers : result) {
            logger.info(String.format("Next array sum is: [%d, %d, %d, %d]", integers[0], integers[1], integers[2], integers[3]));
        }
    }

    public static List<Integer[]> fourNumberSum(int[] array, int targetSum) {
        ArrayList<Integer[]> sums = new ArrayList<>();
        // key -> sum of 2 numbers
        // value -> values which create sum
        Map<Integer, List<Integer[]>> subSums = new HashMap<>();
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int sum = array[i] + array[j];
                int potentialPairSum = targetSum - sum;
                if (subSums.containsKey(potentialPairSum)) {
                    List<Integer[]> allExistingSums = subSums.get(potentialPairSum);
                    for (Integer[] allExistingSum : allExistingSums) {
                        sums.add(new Integer[]{array[i], array[j], array[allExistingSum[0]], array[allExistingSum[1]]});
                    }
                }
            }

            for (int lower = 0; lower < i; lower++) {
                int firstNum = array[lower];
                int secondNum = array[i];
                int sum = firstNum + secondNum;
                Integer[] currentPair = {lower, i};
                subSums.putIfAbsent(sum, new ArrayList<>());
                subSums.get(sum).add(currentPair);
            }
        }

        return sums;
    }
}
