package com.algoexamples;

import java.util.*;

public class Astar {
    private int endRow;
    private int endCol;
    private int[][] graph;

    public static void main(String[] args) {
        int[][] graph = {
                {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0},
        {0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0},
        {0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0},
        {0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1},
        {0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
        {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}
        };

        int startRow = 1;
        int startCol = 1;
        int endRow = 18;
        int endCol = 17;
        Astar astar = new Astar();
        var result = astar.aStarAlgorithm(startRow, startCol, endRow, endCol, graph);
        for (int[] ints : result) {
            System.out.println("[" + ints[0] + ", " + ints[1] + "]");
        }
    }

    /**
     * Picks a path based on the heuristic value
     * @link {com.algoexamples.Astar.Node.compareTo}
     */
    public int[][] aStarAlgorithm(int startRow, int startCol, int endRow, int endCol, int[][] graph) {
        this.endRow = endRow;
        this.endCol = endCol;
        this.graph = graph;

        // check if given graph is empty
        if (graph.length == 0) {
            return new int[][]{};
        }

        // instantiate starting node
        // traverse
        // use the last node and backtrack to the starting node

        int rootH = (endRow - startRow) + (endCol - startCol);
        Node root = new Node(rootH, 0, startRow, startCol);
        boolean[][] visited = new boolean[graph.length][graph[0].length];

        // traverse

        // queue for storing nodes
        // based on their F value
        // @see {com.algoexamples.Astar.Node.compareTo}
        Queue<Node> pq = new PriorityQueue<>();
        pq.add(root);
        while(!pq.isEmpty()) {
            Node current = pq.poll();
            // if target is reached
            if (current.col == endCol && current.row == endRow) {
                // collect all nodes into the final array
                // return it
                return processResult(current);
            }

            int row = current.row;
            int col = current.col;
            // ignore visited nodes
            if (visited[row][col]) {
                continue;
            }

            visited[row][col] = true;
            int[][] neighbours = {
                    {row - 1, col},
                    {row + 1, col},
                    {row, col - 1},
                    {row, col + 1}
            };

            // find all neighbours
            for (int[] position : neighbours) {
                int neighbourRow = position[0];
                int neighbourCol = position[1];
                if (isValidNeighbour(neighbourRow, neighbourCol) && !visited[neighbourRow][neighbourCol]) {
                    int h = calculateH(neighbourRow, neighbourCol);
                    pq.add(new Node(h, current.G + 1, neighbourRow, neighbourCol, current));
                }
            }
        }

        // if reached that point
        // no final node has been found
        return new int[][]{};
    }

    private int[][] processResult(Node last) {
        Stack<Integer[]> stack = new Stack<>();
        stack.push(new Integer[]{last.row, last.col});

        Node current = last.previous;
        while(current != null) {
            stack.push(new Integer[]{current.row, current.col});
            current = current.previous;
        }

        int[][] result = new int[stack.size()][2];
        int index = 0;
        while(!stack.isEmpty()) {
            Integer[] position = stack.pop();
            result[index++] = new int[]{position[0], position[1]};
        }
        return result;
    }

    private int calculateH(int row, int col) {
        return Math.abs(endRow - row) + Math.abs(endCol - col);
    }

    private boolean isValidNeighbour(int row, int col) {
        boolean validRow = (row >= 0) && row < graph.length;
        boolean validCol = (col >= 0) && col < graph[0].length;

        return validRow && validCol && graph[row][col] != 1;
    }

    private static class Node implements Comparable<Node> {
        int row;
        int col;
        /**
         * heuristic val = N of steps towards the goal cell
         * H = (target row - curr.row) + (target.col - curr.col)
         */
        int H;
        /**
         * Distance from the root node
         * G = previousNode.G + 1
         */
        int G;
        /**
         * Sum of H and G
         * F = H + G
         */
        int F;

        Node previous = null;

        public Node(int h, int g, int row, int col) {
            H = h;
            G = g;
            F = h + g;
            this.row = row;
            this.col = col;
        }

        public Node(int h, int g, int row, int col, Node previous) {
            H = h;
            G = g;
            F = h + g;
            this.row = row;
            this.col = col;
            this.previous = previous;
        }

        @Override
        public int compareTo(Node o) {
            if (F == o.F) {
                return 0;
            }

            return this.F - o.F;
        }
    }
}
