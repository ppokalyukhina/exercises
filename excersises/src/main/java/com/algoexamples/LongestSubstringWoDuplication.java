package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWoDuplication {
    private static final Logger logger = LoggerFactory.getLogger(LongestSubstringWoDuplication.class);

    public static void main(String[] args) {
        String input = "clementisacap";
        logger.info(String.format("Longest substring is: %s", longestSubstringWithoutDuplication(input)));
    }

    private static String longestSubstringWithoutDuplication(String str) {
        if (str.length() <= 1) {
            return str;
        }
        int leftPointer = 0;
        int rightPointer = 1;
        // key -> character
        // value -> position of the character in input string
        Map<Character, Integer> lettersPositions = new HashMap<>();
        String longestUniqueSubstr = str.substring(leftPointer, rightPointer);
        while(rightPointer < str.length() && leftPointer <= rightPointer) {
            StringBuilder substr = new StringBuilder();
            substr.append(str.charAt(leftPointer));
            lettersPositions.put(str.charAt(leftPointer), leftPointer);

            if (lettersPositions.containsKey(str.charAt(rightPointer))) {
                rightPointer++;
                continue;
            }

            while(rightPointer < str.length() && !lettersPositions.containsKey(str.charAt(rightPointer))) {
                substr.append(str.charAt(rightPointer));
                lettersPositions.put(str.charAt(rightPointer), rightPointer);
                rightPointer++;
            }

            if (rightPointer < str.length()) {
                leftPointer = lettersPositions.get(str.charAt(rightPointer)) + 1;
                rightPointer = leftPointer + 1;
            }

            if (substr.length() > longestUniqueSubstr.length()) {
                longestUniqueSubstr = substr.toString();
            }

            lettersPositions = new HashMap<>();
        }

        return longestUniqueSubstr;
    }
}
