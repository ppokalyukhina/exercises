package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CaesarCipherEncryptor {
    private static final Logger logger = LoggerFactory.getLogger(CaesarCipherEncryptor.class);

    public static void main(String[] args) {
        String st = "ovmqkwtujqmfkao";
        int key = 52;

        logger.info(String.format("Encrypted string is: %s", usingModularOperator(st, key)));
    }

    private static String usingModularOperator(String str, int key) {
        StringBuilder result = new StringBuilder();
        key = key % 26;

        for (int i = 0; i < str.length(); i++) {
            int currentPosition = str.charAt(i);
            result.append(getLetter(currentPosition, key));
        }

        return result.toString();
    }

    private static char getLetter(int currentPosition, int key) {
        int newPosition = currentPosition + key;
        if (newPosition > 122) {
            newPosition = newPosition % 122 + 96;
        }

        return (char) newPosition;
    }

    private static String mapSolution(String str, int key) {
        Map<Integer, Character> alphabet = new HashMap<>();
        char[] charactersAlphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (char c : charactersAlphabet) {
            alphabet.put((int) c, c);
        }

        key = key % 26;
        StringBuilder resultString = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char current = str.charAt(i);
            int position = current + key;
            if (charactersAlphabet.length <= (position - 97)) {
                position = (position - 97) % 26 + 97;
            }
            resultString.append(alphabet.get(position));
        }

        return resultString.toString();
    }

    public static String caesarCypherEncryptor(String str, int key) {
        if (str.length() == 0) {
            return str;
        }

        // alphabet
        char[] charactersAlphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder resultString = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char current = str.charAt(i);
            int position = findCharPosition(charactersAlphabet, current) + key;
            while (charactersAlphabet.length <= position) {
                position = position - charactersAlphabet.length;
            }
            resultString.append(charactersAlphabet[position]);
        }

        return resultString.toString();
    }

    private static int findCharPosition(char[] arr, char current) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == current) {
                return i;
            }
        }

        return -1;
    }
}
