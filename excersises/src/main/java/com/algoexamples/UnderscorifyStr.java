package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class UnderscorifyStr {
    private static final Logger logger = LoggerFactory.getLogger(UnderscorifyStr.class);

    public static void main(String[] args) {
        String main = "abababababababababababababaababaaabbababaa";
        String substr = "a";

        var positions = positions(main, substr);
        var collapsed = collapseArray(positions);
        logger.info(underscorifySubstring(main, substr));
    }

    private static String underscorifySubstring(String str, String substring) {
        StringBuilder underscorified = new StringBuilder();
        if (str.equals(substring)) {
            return String.format("_%s_", str);
        }

        if (str.length() <= substring.length()) {
            return str;
        }

        List<Integer[]> substrPositions = positions(str, substring);
        var collapsed = collapseArray(substrPositions);

        int lastIndex = 0;
        for (int i = 0; i < collapsed.size(); i++) {
            Integer[] positions = collapsed.get(i);
            int from = positions[0];
            int to = positions[1];
            underscorified.append(str, lastIndex, from).append("_").append(str, from, to).append("_");
            lastIndex = to;
        }

        if (lastIndex < str.length()) {
            underscorified.append(str, lastIndex, str.length());
        }

        return underscorified.toString();
    }

    // merge all crossing substrings
    private static List<Integer[]> collapseArray(List<Integer[]> positions) {
        if (positions.size() == 0) {
            return positions;
        }

        List<Integer[]> newPositions = new ArrayList<>();
        newPositions.add(positions.get(0));
        int pointer = 1;
        while (pointer < positions.size()) {
            Integer[] current = newPositions.get(newPositions.size() - 1);
            Integer[] next = positions.get(pointer);
            int currentLast = current[1];
            int nextFirst = next[0];

            if (nextFirst <= currentLast) {
                newPositions.remove(newPositions.size() - 1);
                Integer[] newLocation = new Integer[]{current[0], next[1]};
                newPositions.add(newLocation);
            } else {
                newPositions.add(next);
            }

            pointer++;
        }

        return newPositions;
    }

    private static List<Integer[]> positions(String str, String substring) {
        List<Integer[]> substrPositions = new ArrayList<>();

        int index = 0;
        while(index < str.length()) {
            int startOfSubstring = str.indexOf(substring, index);
            if (startOfSubstring != -1) {
                Integer[] positions = new Integer[]{startOfSubstring, startOfSubstring + substring.length()};
                substrPositions.add(positions);
                index = startOfSubstring + 1;
            } else {
                break;
            }
        }

        return substrPositions;
    }
}
