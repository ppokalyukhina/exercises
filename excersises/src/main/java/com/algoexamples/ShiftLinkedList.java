package com.algoexamples;

public class ShiftLinkedList {
    public static void main(String[] args) {
        LinkedList head = new LinkedList(0);
        LinkedList one = new LinkedList(1);
        LinkedList two = new LinkedList(2);
        LinkedList three = new LinkedList(3);
        LinkedList four = new LinkedList(4);
        LinkedList five = new LinkedList(5);
        head.next = one;
        one.next = two;
        two.next = three;
        three.next = four;
        four.next = five;

        int k = -1;

        LinkedList current = shiftLinkedList(head, k);
        while(current != null) {
            System.out.println(current.value);
            current = current.next;
        }
    }

    public static LinkedList shiftLinkedList(LinkedList head, int k) {
        int listLength = 0;
        var current = head;
        var oldTail = head;
        while(current != null) {
            listLength++;
            oldTail = current;
            current = current.next;
        }

        k = k % listLength;
        int newHeadIndex = Math.abs(listLength - k);
        if (k < 0) {
            newHeadIndex = Math.abs(k);
        }

        if (newHeadIndex == 0 || k == 0) {
            return head;
        }
        var newTail = head;
        for (int i = 1; i < newHeadIndex; i++) {
            newTail = newTail.next;
        }

        var newHead = newTail.next;
        newTail.next = null;
        oldTail.next = head;
        return newHead;
    }

    public static LinkedList shiftLinkedList1(LinkedList head, int k) {
        int listLength = 0;
        var current = head;
        while(current != null) {
            listLength++;
            current = current.next;
        }

        k = k % listLength;
        int newHeadIndex = Math.abs(listLength - k);
        if (k < 0) {
            newHeadIndex = Math.abs(k);
        }

        if (newHeadIndex == 0 || k == 0) {
            return head;
        }

        LinkedList newHead = head;
        LinkedList previous = null;
        current = head;
        int count = 0;
        while(current != null) {
            if (count == newHeadIndex) {
                // set previous.next = null
                if (previous != null) {
                    previous.next = null;
                }
                newHead = current;
            }

            previous = current;
            current = current.next;
            count++;
        }

        previous.next = head;
        return newHead;
    }

    static class LinkedList {
        public int value;
        public LinkedList next;

        public LinkedList(int value) {
            this.value = value;
            next = null;
        }
    }
}
