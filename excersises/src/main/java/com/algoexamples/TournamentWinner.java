package com.algoexamples;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TournamentWinner {
    public static void main(String[] args) {
        // ["HTML", "C#"], ["C#", "Python"], ["Python", "HTML"]
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        competitions.add(new ArrayList<>(List.of("HTML", "C#")));
        competitions.add(new ArrayList<>(List.of("C#", "Python")));
        competitions.add(new ArrayList<>(List.of("Python", "HTML")));

        ArrayList<Integer> results = new ArrayList<>();
        results.add(0);
        results.add(0);
        results.add(1);
        System.out.println(tournamentWinner(competitions, results));
    }

    /**
     * O(N) time complexity, N -> competitions
     * O(T) space complexity, T -> teams
     */
    private static String tournamentWinner(ArrayList<ArrayList<String>> competitions, ArrayList<Integer> results) {
        if (competitions.size() == 0 || competitions.size() != results.size()) {
            return "";
        }

        Map<String, Integer> winners = new HashMap<>();
        String finalWinner = "";
        int maxPoints = 0;
        for (int i = 0; i < competitions.size(); i++) {
            List<String> teams = competitions.get(i);
            int winnerIndex = results.get(i) == 0 ? 1 : 0;
            String winner = teams.get(winnerIndex);
            // if one wins, earns 3 points
            int currentPoints = 3;
            if (winners.containsKey(winner)) {
                int pointsSoFar = winners.get(winner);
                currentPoints = currentPoints + pointsSoFar;
            }
            winners.put(winner, currentPoints);
            if (currentPoints > maxPoints) {
                maxPoints = currentPoints;
                finalWinner = winner;
            }
        }

        return finalWinner;
    }
}
