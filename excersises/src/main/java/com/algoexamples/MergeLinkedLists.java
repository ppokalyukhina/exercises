package com.algoexamples;

public class MergeLinkedLists {
    public static void main(String[] args) {
        LinkedList head1 = new LinkedList(1);
        LinkedList three = new LinkedList(3);
        LinkedList four = new LinkedList(4);
        LinkedList five = new LinkedList(5);
        LinkedList nine = new LinkedList(9);
        LinkedList ten = new LinkedList(10);
        head1.next = three;
        three.next = four;
        four.next = five;
        five.next = nine;
        nine.next = ten;

        LinkedList head2 = new LinkedList(2);
        LinkedList six = new LinkedList(6);
        LinkedList seven = new LinkedList(7);
        LinkedList eight = new LinkedList(8);
        head2.next = six;
        six.next = seven;
        seven.next = eight;
        eight.next = new LinkedList(16);


        LinkedList current = mergeLinkedLists(head2, head1);
        while(current != null) {
            System.out.println(current.value);
            current = current.next;
        }
    }

    public static class LinkedList {
        int value;
        LinkedList next;

        LinkedList(int value) {
            this.value = value;
            this.next = null;
        }
    }

    public static LinkedList mergeLinkedLists(LinkedList headOne, LinkedList headTwo) {
        LinkedList currentOne = headOne;
        LinkedList currentTwo = headTwo;
        LinkedList previous = null;

        while(currentOne != null && currentTwo != null) {
            if (currentOne.value < currentTwo.value) {
                previous = currentOne;
                currentOne = currentOne.next;
            } else {
                var tmpCurrentTwoNext = currentTwo.next;
                currentTwo.next = currentOne;
                if (previous != null) {
                    previous.next = currentTwo;
                }
                previous = currentTwo;
                currentTwo = tmpCurrentTwoNext;
            }
        }
        if (currentOne == null) {
            previous.next = currentTwo;
        }

        return headOne.value < headTwo.value ? headOne : headTwo;
    }

    public static LinkedList mergeLinkedLists1(LinkedList headOne, LinkedList headTwo) {
        LinkedList currentOne = headOne;
        LinkedList currentTwo = headTwo;
        LinkedList previous = null;

        while(currentOne != null && currentTwo != null) {
            // compare c1 and c2
            // if c1 > c2, set c2 as c1 previous
            // else keep reference c2 and move on with c1
            var c2Next = currentTwo.next;
            if (currentOne.value > currentTwo.value) {
                // detach currentTwo from the second linked list
                // and attach it to the first linked list
                currentTwo.next = currentOne;
                if (previous != null) {
                    previous.next = currentTwo;
                    previous = previous.next;
                } else {
                    headOne = currentTwo;
                    previous = headOne;
                }
                currentTwo = c2Next;
            } else {
                previous = currentOne;
                currentOne = currentOne.next;
            }
        }
        if (currentTwo != null) {
            // in that example there won't be ever null heads given
            // therefore previous will be set in the loop for sure
            previous.next = currentTwo;
        }

        return headOne;
    }
}
