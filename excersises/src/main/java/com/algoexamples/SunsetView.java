package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;

public class SunsetView {
    private static final Logger logger = LoggerFactory.getLogger(SunsetView.class);

    public static void main(String[] args) {
        int[] buildings = {1};
        String direction = "EAST";
        var result = sunsetViews(buildings, direction);

        for (Integer buildingIndex : result) {
            logger.info(String.format("Next building is %d", buildingIndex));
        }
    }

    private static ArrayList<Integer> sunsetViews(int[] buildings, String direction) {
        ArrayList<Integer> indexes = new ArrayList<>();

        if (direction.equals("WEST")) {
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < buildings.length; i++) {
                if (buildings[i] <= max) {
                    continue;
                }

                max = buildings[i];
                indexes.add(i);
            }
        }

        else if (direction.equals("EAST")) {
            int index = buildings.length - 1;
            int max = Integer.MIN_VALUE;
            while (index >= 0) {
                if (buildings[index] <= max) {
                    index--;
                    continue;
                }

                max = buildings[index];
                indexes.add(index);
                index--;
            }

            Collections.sort(indexes);
        }

        return indexes;
    }
}
