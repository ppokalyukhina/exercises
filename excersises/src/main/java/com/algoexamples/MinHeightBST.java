package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MinHeightBST {
    private static final Logger logger = LoggerFactory.getLogger(MinHeightBST.class);

    public static void main(String[] args) {
        List<Integer> input = List.of(1, 2, 5, 7, 10, 13, 14, 15, 22);
        var result = minHeightBst(input);
    }

    public static BST minHeightBst(List<Integer> array) {
        return constructMinHeightBST(array, null, 0, array.size() - 1);
    }

    private static BST constructMinHeightBST(List<Integer> array, BST root, int startIndx, int endIndx) {
        if (startIndx > endIndx) {
            return null;
        }

        int mid = (endIndx + startIndx) / 2;
        if (root == null) {
            root = new BST(array.get(mid));
        } else {
            root.insert(array.get(mid));
        }

        // left leaf
        constructMinHeightBST(array, root, startIndx, mid - 1);
        // right leaf
        constructMinHeightBST(array, root, mid + 1, endIndx);

        return root;
    }

    static class BST {
        public int value;
        public BST left;
        public BST right;

        public BST(int value) {
            this.value = value;
            left = null;
            right = null;
        }

        public void insert(int value) {
            if (value < this.value) {
                if (left == null) {
                    left = new BST(value);
                } else {
                    left.insert(value);
                }
            } else {
                if (right == null) {
                    right = new BST(value);
                } else {
                    right.insert(value);
                }
            }
        }
    }
}
