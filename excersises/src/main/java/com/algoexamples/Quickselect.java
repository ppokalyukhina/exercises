package com.algoexamples;

public class Quickselect {
    public static void main(String[] args) {
        int[] arr = {8, 5, 2, 9, 7, 6, 3};
        int k = 3;
//        int[] arr = {43, 24, 37};
//        int k = 1;

//        int[] arr = {1};
//        int k = 1;
        int smallestK = quickselect(arr, k);
        System.out.println(smallestK);
    }

    public static int quickselect(int[] array, int k) {
        return doSearchIterative(array, k, 0, array.length - 1);
    }

    private static int doSearchIterative(int[] array, int k, int pivot, int endIndex) {
        while(true) {
            if (pivot > endIndex) {
                throw new RuntimeException("This should not happen");
            }

            int left = pivot + 1;
            int right = endIndex;
            while(left <= right) {
                if (array[left] > array[pivot] && array[right] < array[pivot]) {
                    swap(array, left, right);
                }

                if (array[left] <= array[pivot]) {
                    left++;
                }

                if (array[right] >= array[pivot]) {
                    right--;
                }
            }
            swap(array, pivot, right);
            if (right == k - 1) {
                return array[right];
            }

            if (right < k - 1) {
                // explore left wing only
                pivot = right + 1;
            } else {
                // explore right wing only
                endIndex = right - 1;
            }
        }
    }

    private static int doSearch(int[] array, int k, int low, int high) {
        if (low > high) {
            return -1;
        }

        int left = low + 1;
        int right = high;
        while(left <= right) {
            if (array[left] > array[low] && array[right] < array[low]) {
                swap(array, left, right);
            }

            if (array[left] <= array[low]) {
                left++;
            }

            if (array[right] >= array[low]) {
                right--;
            }
        }
        swap(array, low, right);
        if (right == k - 1) {
            return array[right];
        }

        if (right > k - 1) {
            // explore only left wing
            return doSearch(array, k, low, right - 1);
        } else {
            // explore only right wing
            return doSearch(array, k, right + 1, high);
        }
    }

    private static void swap(int[] array, int indexOne, int indexTwo) {
        int tmpOne = array[indexOne];
        array[indexOne] = array[indexTwo];
        array[indexTwo] = tmpOne;
    }
}
