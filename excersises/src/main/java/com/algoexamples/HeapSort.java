package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeapSort {
    private static final Logger logger = LoggerFactory.getLogger(HeapSort.class);

    public static void main(String[] args) {
//        int[] inputArray = {8, 5, 2, 9, 5, 6, 3};
        int[] inputArray = {1, 2};
//        int[] inputArray = {1, 3, 2};
        int[] sorted = heapSort(inputArray);
        for (int i : sorted) {
            logger.info(String.format("%d", i));
        }
    }

    public static int[] heapSort(int[] array) {
        buildMaxHeap(array);
        for (int i = array.length - 1; i >= 0; i--) {
            placeMaxInLastElem(array, i);
        }

        return array;
    }

    private static void placeMaxInLastElem(int[] array, int index) {
        // swap root and last elem
        swap(array, 0, index);
        // move root to the appropriate position
        siftDown(array, 0, index - 1);
    }

    private static void buildMaxHeap(int[] array) {
       for (int i = array.length - 1; i >= 0; i--) {
           siftDown(array, i, array.length - 1);
       }
    }

    private static void siftDown(int[] array, int parent, int lastElem) {
        int leftChild = (parent * 2) + 1;
        while(leftChild <= lastElem) {
            int rightIndx = leftChild + 1;
            int indexToCompare = leftChild;
            if (rightIndx <= lastElem && array[leftChild] < array[rightIndx]) {
                indexToCompare = leftChild + 1;
            }
            if (array[indexToCompare] < array[parent]) {
                break;
            }

            swap(array, parent, indexToCompare);
            parent = indexToCompare;
            leftChild = parent * 2 + 1;
        }
    }

    private static void swap(int[] array, int index1, int index2) {
        int tmpOne = array[index1];
        array[index1] = array[index2];
        array[index2] = tmpOne;
    }
}
