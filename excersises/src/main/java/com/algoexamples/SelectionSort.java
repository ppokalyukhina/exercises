package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelectionSort {
    private static final Logger logger = LoggerFactory.getLogger(SelectionSort.class);

    public static void main(String[] args) {
        int[] input = {8, 5, 2, 9, 5, 6, 3};

        var sorted = selectionSort(input);
        for (int i : sorted) {
            logger.info(String.format("%d", i));
        }
    }

    /**
     * O(N * N) time
     * O(1) space
     */
    public static int[] selectionSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minInx = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minInx = j;
                }
            }

            int tmp = array[i];
            array[i] = array[minInx];
            array[minInx] = tmp;
        }

        return array;
    }
}
