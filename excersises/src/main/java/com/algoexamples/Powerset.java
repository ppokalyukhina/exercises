package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Powerset {
    private static final Logger logger = LoggerFactory.getLogger(Powerset.class);

    public static void main(String[] args) {
        List<Integer> input = List.of(1, 2, 3);
        var result = recursive(input);
        for (List<Integer> sets : result) {
            StringBuilder powerset = new StringBuilder();
            for (Integer set : sets) {
                powerset.append(set);
            }

            logger.info(String.format("Next set is: %s", powerset));
        }
    }

    /**
     * O(2powN*N) space complexity
     * O(2powN*N) time complexity, N - array.length, M - sets.length
     */
    public static List<List<Integer>> recursive(List<Integer> array) {
        return getSubsets(array, array.size() - 1);
    }

    private static List<List<Integer>> getSubsets(List<Integer> array, int index) {
        if (index < 0) {
            return List.of(new ArrayList<>());
        }

        int element = array.get(index);
        List<List<Integer>> subsets = getSubsets(array, index - 1);
        List<List<Integer>> subsetsCopy = new ArrayList<>(subsets);
        int size = subsets.size();
        for (int i = 0; i < size; i++) {
            List<Integer> updatedSubset = new ArrayList<>(subsets.get(i));
            updatedSubset.add(element);
            subsetsCopy.add(updatedSubset);
        }

        return subsetsCopy;
    }

    /**
     * O(2powN*N) space complexity
     * O(2powN*N) time complexity, N - array.length, M - sets.length
     */
    public static List<List<Integer>> powerset(List<Integer> array) {
        List<List<Integer>> sets = new ArrayList<>();
        sets.add(Collections.emptyList());
        if (array.isEmpty()) {
            return sets;
        }

        for (Integer integer : array) {
            int size = sets.size();
            for (int j = 0; j < size; j++) {
                List<Integer> currentSubSet = new ArrayList<>(sets.get(j));
                currentSubSet.add(integer);
                sets.add(currentSubSet);
            }
        }

        return sets;
    }
}
