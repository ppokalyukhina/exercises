package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MinNumberOfCoinsForChange {
    private static final Logger logger = LoggerFactory.getLogger(MinNumberOfCoinsForChange.class);

    public static void main(String[] args) {
        int[] denoms = {2, 1};
        int i = 3;

        logger.info(String.format("Min number of coins for change is %d", minNumberOfCoinsForChange(i, denoms)));
    }

    private static int minNumberOfCoinsForChange(int n, int[] denoms) {
        if (n == 0) {
            return 0;
        }

        int[] minCoins = new int[n + 1];
        Arrays.fill(minCoins, Integer.MAX_VALUE);
        minCoins[0] = 0;
        for (int currentDenomination : denoms) {
            if (currentDenomination > n) {
                continue;
            }

            for (int amount = 1; amount < minCoins.length; amount++) {
                if (currentDenomination <= amount) {
                    int diff = amount - currentDenomination;
                    if (minCoins[diff] == Integer.MAX_VALUE) {
                        continue;
                    }
                    minCoins[amount] = Math.min(minCoins[diff] + 1, minCoins[amount]);
                }
            }
        }

        // Write your code here.
        return minCoins[n] != Integer.MAX_VALUE ? minCoins[n] : -1;
    }
}
