package com.algoexamples;

import com.sun.source.tree.Tree;

public class BinaryTreeDiameter {
    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        BinaryTree three = new BinaryTree(3);
        BinaryTree two = new BinaryTree(2);
        BinaryTree four = new BinaryTree(4);
        BinaryTree seven = new BinaryTree(7);
        BinaryTree eight = new BinaryTree(8);
        BinaryTree five = new BinaryTree(5);
        BinaryTree nine = new BinaryTree(9);
        BinaryTree six = new BinaryTree(6);

        eight.left = nine;
        seven.left = eight;
        three.left = seven;

        five.right = six;
        four.right = five;
        three.right = four;

        root.left = three;
        root.right = two;

        BinaryTreeDiameter btd = new BinaryTreeDiameter();
        var max = btd.binaryTreeDiameter(root);
        System.out.println("Max diameter is " + max);
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;
        public int diameter;
        public int height;

        public BinaryTree(int value) {
            this.value = value;
        }
    }

    public int binaryTreeDiameter(BinaryTree tree) {
        return getTreeInfo(tree).diameter;
    }

    private TreeInfo getTreeInfo(BinaryTree tree) {
        if (tree == null) {
            return new TreeInfo(0, 0);
        }

        TreeInfo left = getTreeInfo(tree.left);
        TreeInfo right = getTreeInfo(tree.right);

        int longestPathForCurrentRoot = left.height + right.height;
        int longestDiameterSoFar = Math.max(left.diameter, right.diameter);
        int currentDiameter = Math.max(longestDiameterSoFar, longestPathForCurrentRoot);
        int currentHeight = 1 + Math.max(left.height, right.height);

        return new TreeInfo(currentDiameter, currentHeight);
    }

    static class TreeInfo {
        public int diameter;
        public int height;

        public TreeInfo(int diameter, int height) {
            this.diameter = diameter;
            this.height = height;
        }
    }
}
