package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class KnapsackProblem {
    private static final Logger logger = LoggerFactory.getLogger(KnapsackProblem.class);

    public static void main(String[] args) {
//        int[][] items = {
//                {1, 2},
//                {4, 3},
//                {5, 6},
//                {6, 7}
//        };

        int[][] items = {
                {1, 2},
                {70, 70},
                {30, 30},
                {69, 69},
                {99, 100}
        };
        int capacity = 100;
        var result = knapsackProblemSimplifiedCode(items, capacity);
        for (List<Integer> integers : result) {
            StringBuilder builder = new StringBuilder();
            for (Integer integer : integers) {
                builder.append(integer).append(',');
            }
            logger.info(builder.toString());
        }
    }

    /**
     * O(N*c) space complexity (values)
     * O(N*c) time complexity + 1 iteration over values[][] arr
     * c -> capacity
     */
    public static List<List<Integer>> knapsackProblemSimplifiedCode(int[][] items, int capacity) {
        // rows -> items
        // cols -> capacity
        // first row and col -> 0s
        int[][] values = new int[items.length + 1][capacity + 1];

        // fill matrix with maximum values
        // current value = Math.max(max value in the top row (before) OR currentValue + (currentVal + valOfLeftoverCapacity) PR currentValue)
        for (int row = 1; row < values.length; row++) {
            for (int col = 1; col < values[row].length; col++) {
                int[] currentItem = items[row - 1];
                int currentVal = currentItem[0];
                int currentItemWeight = currentItem[1];
                int topVal = values[row - 1][col];

                // if current capacity is out of bounds,
                // use the top (previous) max value
                if (currentItemWeight > col) {
                    values[row][col] = topVal;
                    continue;
                }

                int currentMaxVal = Math.max(currentVal, currentVal + values[row - 1][col - currentItemWeight]);
                int maxVal = Math.max(topVal, currentMaxVal);

                values[row][col] = maxVal;
            }
        }

        // build sequences
        int lastElement = values[values.length - 1][values[0].length - 1];
        List<Integer> maxValue = List.of(lastElement);
        List<Integer> sequences = new ArrayList<>();
        int currentRow = values.length - 1;
        int currentCol = values[currentRow].length - 1;
        while(currentRow > 0 && currentCol >= 0) {
            int current = values[currentRow][currentCol];
            int top = values[currentRow - 1][currentCol];
            int currentCapacity = items[currentRow - 1][1];
            if (current == 0) {
                break;
            }
            // in case the value was not changed
            // current item should NOT be added to the sequence
            // move on to the top item
            if (current == top) {
                currentRow--;
            } else {
             sequences.add(0, currentRow - 1);
             currentRow--;
             currentCol = currentCol - currentCapacity;
            }
        }

        return List.of(maxValue, sequences);
    }

    public static List<List<Integer>> knapsackProblem(int[][] items, int capacity) {
        // maxItems[][]
        // cols => capacity
        // rows => indexes of items in given arr
        // calculate val per capacity
        // if there is leftover capacity, search for a value in capacity col

        List<List<MaxItems>> maxItems = new ArrayList<>();
        // fill first row and column with emptu elements
        for (int i = 0; i <= items.length; i++) {
            maxItems.add(new ArrayList<>());
            for (int j = 0; j <= capacity; j++) {
                maxItems.get(i).add(new MaxItems(new ArrayList<>(), 0));
            }
        }

        for (int row = 1; row < maxItems.size(); row++) {
            for (int col = 1; col < maxItems.get(row).size(); col++) {
                int[] item = items[row -1];
                int itemValue = item[0];
                int itemCapacity = item[1];
                if (itemCapacity > col) {
                    // set top value
                    MaxItems previousItem = maxItems.get(row - 1).get(col);
                    var currentItems = new MaxItems(previousItem.indexes, previousItem.maxVal);
                    maxItems.get(row).set(col, currentItems);
                    continue;
                }

                // get item value from the element = leftover capacity
                var tmpMaxVal = itemValue;
                List<Integer> listOfIndices = new ArrayList<>();
                // if there is any leftover capacity
                if (col - itemCapacity > 0) {
                    int leftOverItemValue = maxItems.get(row - 1).get(col - itemCapacity).maxVal;
                    if (leftOverItemValue > 0) {
                        tmpMaxVal += leftOverItemValue;
                        listOfIndices.addAll(maxItems.get(row - 1).get(col - itemCapacity).indexes);
                    }
                }
                // top item values
                MaxItems previousItem = maxItems.get(row - 1).get(col);

                MaxItems currentMaxItem;
                // if top value is bigger than current value
                if (previousItem.maxVal > tmpMaxVal) {
                    currentMaxItem = new MaxItems(previousItem.indexes, previousItem.maxVal);
                } else {
                    listOfIndices.add(row - 1);
                    currentMaxItem = new MaxItems(listOfIndices, tmpMaxVal);
                }
                // add max val to matrix
                maxItems.get(row).set(col, currentMaxItem);
            }
        }

        MaxItems last = maxItems.get(maxItems.size() - 1).get(maxItems.get(0).size() - 1);
        List<Integer> totalValue = Collections.singletonList(last.maxVal);
        List<Integer> finalItems = new ArrayList<>(last.indexes);
        var result = new ArrayList<List<Integer>>();
        result.add(totalValue);
        result.add(finalItems);
        return result;
    }

    private static class MaxItems {
        public List<Integer> indexes;
        public int maxVal;

        public MaxItems(List<Integer> indexes, int maxVal) {
            this.indexes = indexes;
            this.maxVal = maxVal;
        }
    }
}
