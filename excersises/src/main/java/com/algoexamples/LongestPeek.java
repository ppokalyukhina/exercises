package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LongestPeek {
    private static final Logger logger = LoggerFactory.getLogger(LongestPeek.class);

    public static void main(String[] args) {
//        int[] input = {1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3};
        int[] input = {1, 2, 3, 4, 5, 6, 10, 100, 1000};

        var longestPeek = longestPeak1(input);
        logger.info(String.format("Longest peek is: %d", longestPeek));
    }

    private static int longestPeak1(int[] array) {
        int longestPeek = 0;
        if (array.length <= 2) {
            return 0;
        }

        for (int i = 1; i < array.length - 1; i++) {
            int left = i - 1;
            int right = i + 1;
            int current = array[i];
            int previous = array[left];
            int next = array[right];

            if (current > next && current > previous) {
                int currentPeek = 3;
                left--;
                right++;

                while(left >= 0 && array[left] < array[left + 1]) {
                    currentPeek++;
                    left--;
                }

                while(right < array.length && array[right] < array[right - 1]) {
                    currentPeek++;
                    right++;
                }

                longestPeek = Math.max(currentPeek, longestPeek);
            }
        }

        return longestPeek;
    }
}
