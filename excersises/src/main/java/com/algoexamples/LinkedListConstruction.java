package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LinkedListConstruction {
    private static final Logger logger = LoggerFactory.getLogger(LinkedListConstruction.class);

    public static void main(String[] args) {
        DoublyLinkedList linkedList = new DoublyLinkedList();
        var head = new Node(1);
        var two = new Node(2);
        var three = new Node(3);
        var four = new Node(4);
        var five = new Node(5);
        five.prev = four;
        four.next = five;
        four.prev = three;
        three.next = four;
        three.prev = two;
        two.next = three;
        two.prev = head;
        head.next = two;
        linkedList.head = head;
        linkedList.tail = five;
        Node six = new Node(6);
        linkedList.setHead(six);
        linkedList.setTail(six);
        linkedList.insertBefore(two, three);
        linkedList.insertAfter(four, new Node(3));
        linkedList.insertAtPosition(1, new Node(3));
        linkedList.removeNodesWithValue(3);
        linkedList.remove(two);
        logger.info(String.format("Linked list contains value 5 : %s", linkedList.containsNodeWithValue(5)));
    }

    static class DoublyLinkedList {
        public Node head;
        public Node tail;

        public void setHead(Node node) {
            if (isEmpty()) {
                head = node;
                tail = node;
                return;
            }
            insertBefore(head, node);
        }

        public void setTail(Node node) {
            if (isEmpty()) {
                head = node;
                tail = node;
                return;
            }

            insertAfter(tail, node);
        }

        private boolean isEmpty() {
            return head == null && tail == null;
        }

        public void insertBefore(Node node, Node nodeToInsert) {
            if (oneNodeList(nodeToInsert)) {
                return;
            }
            deleteIfNecessary(nodeToInsert);
            nodeToInsert.next = node;
            nodeToInsert.prev = node.prev;
            // if it's a head
            if (node.prev == null) {
                head = nodeToInsert;
            } else {
                node.prev.next = nodeToInsert;
            }
            node.prev = nodeToInsert;
        }

        public void insertAfter(Node node, Node nodeToInsert) {
            if (oneNodeList(nodeToInsert)) {
                return;
            }
            deleteIfNecessary(nodeToInsert);
            nodeToInsert.prev = node;
            nodeToInsert.next = node.next;
            // if current node is a tail
            if (node.next == null) {
                tail = nodeToInsert;
            } else {
                node.next.prev = nodeToInsert;
            }
            node.next = nodeToInsert;
        }

        private boolean oneNodeList(Node node) {
            // if given node is the only node in the list
            return head == node && tail == node;
        }

        private void deleteIfNecessary(Node nodeToDelete) {
            // if node already exists, remove it
            if (nodeToDelete.prev != null || nodeToDelete.next != null) {
                // else if node already exists
                remove(nodeToDelete);
            }
        }

        public void insertAtPosition(int position, Node nodeToInsert) {
            if (position == 1) {
                setHead(nodeToInsert);
                return;
            }

            int currentPos = 1;
            Node current = head;
            while(current != null && currentPos++ != position) {
                current = current.next;
                currentPos++;
            }

            if (current == null) {
                setTail(nodeToInsert);
            } else {
                insertBefore(current, nodeToInsert);
            }
        }

        public void removeNodesWithValue(int value) {
            Node current = head;
            while(current != null) {
                Node next = current.next;
                if (current.value == value) {
                    remove(current);
                }
                current = next;
            }
        }

        public void remove(Node node) {
            // if it's a head
            if (node.prev == null) {
                head = head.next;
                head.prev = null;
            }

            // if it's a tail
            else if (node.next == null) {
                tail = tail.prev;
                tail.next = null;
            } else {
                node.prev.next = node.next;
                node.next.prev = node.prev;
            }

            node.prev = null;
            node.next = null;
        }



        public boolean containsNodeWithValue(int value) {
            Node current = head;
            while(current != null) {
                if (current.value == value) {
                    return true;
                }
                current = current.next;
            }

            return false;
        }
    }

    // Do not edit the class below.
    static class Node {
        public int value;
        public Node prev;
        public Node next;

        public Node(int value) {
            this.value = value;
        }
    }
}
