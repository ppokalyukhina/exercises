package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BubbleSort {
    private static final Logger logger = LoggerFactory.getLogger(BubbleSort.class);

    public static void main(String[] args) {
        int[] arr = {8, 5, 2, 9, 5, 6, 3};

        int[] sorted = diffSolution(arr);
        for (int i : sorted) {
            logger.info(String.format("%d", i));
        }
    }

    private static int[] diffSolution(int[] array) {
        if (array.length == 0) {
            return new int[]{};
        }

        boolean sorted = false;
        int counter = 0;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1 - counter; i++) {
                int current = array[i];
                int next = array[i + 1];
                if (current > next) {
                    array[i] = next;
                    array[i + 1] = current;
                    sorted = false;
                }
            }
            counter++;
        }

        return array;
    }

    public static int[] bubbleSort(int[] array) {
        for (int i: array) {
            int index = 0;
            int swaps = 0;
            for (int j = index; j < array.length - 1; j++) {
                int current = array[j];
                int next = array[j + 1];

                if (current > next) {
                    array[j + 1] = current;
                    array[j] = next;
                    swaps++;
                }
            }

            if (swaps == 0) {
                break;
            }
        }

        return array;
    }
}
