package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class KadanesAlgorithm {
    private static final Logger logger = LoggerFactory.getLogger(KadanesAlgorithm.class);

    public static void main(String[] args) {
//        int[] input = {3, 5, -9, 1, 3, -2, 3, 4, 7, 2, -9, 6, 3, 1, -5, 4};
//        int[] input = {-1, -2, -3, -4, -5, -6, -7, -8, -9, -10};
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        logger.info(String.format("The maximum sum of sub-array is: %d", kadanesAlgorithm(input)));
    }

    private static int easy(int[] array) {
        if (array.length == 0) {
            return -1;
        }

        int[] maxSums = new int[array.length];
        maxSums[0] = array[0];
        for (int i = 1; i < array.length; i++) {
            int previousSum = maxSums[i - 1];
            int current = array[i];
            maxSums[i] = Math.max(previousSum + current, current);
        }

        Arrays.sort(maxSums);
        return maxSums[array.length - 1];
    }

    private static int kadanesAlgorithm(int[] array) {
        if (array.length == 0) {
            return -1;
        }

        int maxSum = array[0];
        int previous = array[0];
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            previous = Math.max(previous + current, current);
            maxSum = Math.max(maxSum, previous);
        }

        return maxSum;
    }
}
