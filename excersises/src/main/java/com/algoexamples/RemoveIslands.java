package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class RemoveIslands {
    private static final Logger logger = LoggerFactory.getLogger(RemoveIslands.class);

    public static void main(String[] args) {
        int[][] input = {
                {1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 1, 1},
                {0, 0, 1, 0, 1, 0},
                {1, 1, 0, 0, 1, 0},
                {1, 0, 1, 1, 0, 0},
                {1, 0, 0, 0, 0, 1}
        };

        var result = removeIslands(input);
    }

    private static int[][] removeIslands(int[][] matrix) {
        boolean[][] visited = new boolean[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (notPartOfIsland(i, j, matrix) || visited[i][j]) {
                    continue;
                }

                // here elem is 1 and not on the edge of the matrix
                // traverse
                var positions = traverseAndGetPositions(i, j, matrix, visited);
                if (!positions.isEmpty()) {
                    replace(positions, matrix);
                }
            }
        }

        return matrix;
    }

    private static List<int[]> traverseAndGetPositions(int row, int col, int[][] matrix, boolean[][] visited) {
        int[] position = {row, col};
        List<int[]> positions = new ArrayList<>();
        positions.add(position);
        Stack<int[]> stack = new Stack<>();
        stack.push(position);

        boolean isIsland = true;

        while(!stack.isEmpty()) {
            int[] currentPosition = stack.pop();
            int currentRow = currentPosition[0];
            int currentCol = currentPosition[1];
            visited[currentRow][currentCol] = true;

            // check top
            if (currentRow > 0 && !visited[currentRow - 1][currentCol] && matrix[currentRow - 1][currentCol] == 1) {
                visited[currentRow - 1][currentCol] = true;
                if (notPartOfIsland(currentRow - 1, currentCol, matrix)) {
                    isIsland = false;
                }

                int[] topPositions = {currentRow - 1, currentCol};
                positions.add(topPositions);
                stack.push(topPositions);
            }

            // check bottom
            if (currentRow < matrix.length - 1 && !visited[currentRow + 1][currentCol] && matrix[currentRow + 1][currentCol] == 1) {
                visited[currentRow + 1][currentCol] = true;
                if (notPartOfIsland(currentRow + 1, currentCol, matrix)) {
                    isIsland = false;
                }

                int[] bottomPositions = {currentRow + 1, currentCol};
                positions.add(bottomPositions);
                stack.push(bottomPositions);
            }

            // check left
            if (currentCol > 0 && !visited[currentRow][currentCol - 1] && matrix[currentRow][currentCol - 1] == 1) {
                visited[currentRow][currentCol - 1] = true;
                if (notPartOfIsland(currentRow, currentCol - 1, matrix)) {
                    isIsland = false;
                }

                int[] leftPositions = {currentRow, currentCol - 1};
                positions.add(leftPositions);
                stack.push(leftPositions);
            }

            // check right
            if (currentCol < matrix[0].length - 1 && !visited[currentRow][currentCol + 1] && matrix[currentRow][currentCol + 1] == 1) {
                visited[currentRow][currentCol + 1] = true;
                if (notPartOfIsland(currentRow, currentCol + 1, matrix)) {
                    isIsland = false;
                }

                int[] leftPositions = {currentRow, currentCol + 1};
                positions.add(leftPositions);
                stack.push(leftPositions);
            }
        }

        return isIsland ? positions : new ArrayList<>();
    }

    private static void replace(List<int[]> positions, int[][] matrix) {
        // valid island at that points
        // remote it -> replace 1s with 0s
        for (int[] ints : positions) {
            int rowToReplace = ints[0];
            int colToReplace = ints[1];

            matrix[rowToReplace][colToReplace] = 0;
        }
    }

    private static boolean notPartOfIsland(int row, int col, int[][] matrix) {
        int current = matrix[row][col];
        return current == 0 || row == 0 || col == 0 || row == (matrix.length - 1) || col == matrix[0].length - 1;
    }
}
