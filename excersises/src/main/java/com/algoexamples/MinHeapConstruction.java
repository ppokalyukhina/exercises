package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class MinHeapConstruction {
    private static final Logger logger = LoggerFactory.getLogger(MinHeapConstruction.class);

    public static void main(String[] args) {
        List<Integer> input = List.of(48, 12, 24, 7, 8, -5, 24, 391, 24, 56, 2, 6, 8, 41);
//        List<Integer> input = List.of(991, -731, -882, 100, 280, -43, 432, 771, -581, 180, -382, -998, 847, 80, -220, 680, 769, -75, -817, 366, 956, 749, 471, 228, -435, -269,
//                652, -331, -387, -657, -255, 382, -216, -6, -163, -681, 980, 913, -169, 972, -523, 354, 747, 805, 382, -827, -796, 372, 753, 519, 906);
        var heap = new MinHeap(input);
        heap.remove();
        heap.remove();
        heap.remove();
        heap.insert(992);
//        heap.insert(76);
//        logger.info(String.format("Peek first elem: %d", heap.peek()));
//        logger.info(String.format("Removed element: %d", heap.remove()));
//        logger.info(String.format("Peek first elem: %d", heap.peek()));
//        logger.info(String.format("Removed element: %d", heap.remove()));
//        logger.info(String.format("Peek first elem: %d", heap.peek()));
//        heap.insert(87);
    }

    static class MinHeap {
        List<Integer> heap;

        public MinHeap(List<Integer> array) {
            heap = buildHeap(array);
        }

        /**
         * O(N) time
         * O(1) space
         */
        public List<Integer> buildHeap(List<Integer> array) {
            List<Integer> heap = new ArrayList<>(array.size());
            if (array.isEmpty()) {
                return heap;
            }
            heap.addAll(array);
            int lastElemIndex = heap.size() - 1;
            int lastParentIndex = (lastElemIndex - 1) / 2;
            while(lastParentIndex >= 0) {
                siftDown(lastParentIndex, lastElemIndex, heap);
                lastParentIndex--;
            }

            return heap;
        }

        /**
         * O(log(N)) time
         * O(1) space
         */
        public void siftDown(int currentIdx, int endIdx, List<Integer> heap) {
            int leftIndx = (currentIdx * 2) + 1;

            while(leftIndx <= endIdx) {
                int rightIndx = leftIndx + 1;
                int indxToSwap = leftIndx;
                if (rightIndx <= endIdx && heap.get(rightIndx) < heap.get(leftIndx)) {
                    indxToSwap = rightIndx;
                }

                if (heap.get(indxToSwap) >= heap.get(currentIdx)) {
                    break;
                }

                swap(currentIdx, indxToSwap, heap);
                currentIdx = indxToSwap;
                leftIndx = (currentIdx * 2) + 1;
            }
        }

        /**
         * O(log(N)) time
         * O(1) space
         */
        public void siftUp(int currentIdx, List<Integer> heap) {
            int parentIndex = (currentIdx - 1) / 2;
            while(currentIdx > 0 && heap.get(parentIndex) > heap.get(currentIdx)) {
                swap(currentIdx, parentIndex, heap);
                currentIdx = parentIndex;
                parentIndex = (currentIdx - 1) / 2;
            }
        }

        private void swap(int currentIdx, int parentIndex, List<Integer> heap) {
            int tmp = heap.get(currentIdx);
            Integer parent = heap.get(parentIndex);
            heap.set(currentIdx, parent);
            heap.set(parentIndex, tmp);
        }

        /**
         * O(1) time
         * O(1) space
         */
        public int peek() {
            if (heap.isEmpty()) {
                return -1;
            }

            return heap.get(0);
        }

        /**
         * O(log(N)) time
         * O(1) space
         */
        public int remove() {
            if (heap.isEmpty()) {
                return -1;
            }

            // always delete root
            int root = heap.get(0);
            int lastElementIndex = heap.size() - 1;
            swap(0, lastElementIndex, heap);
            heap.remove(lastElementIndex);
            siftDown(0, heap.size() - 1, heap);

            return root;
        }

        /**
         * O(log(N)) time
         * O(1) space
         */
        public void insert(int value) {
            heap.add(value);
            siftUp(heap.size() - 1, heap);
        }
    }
}
