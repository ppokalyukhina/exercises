package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class FindSuccessor {
    private static final Logger logger = LoggerFactory.getLogger(FindSuccessor.class);

    public static void main(String[] args) {
        //       1
        //     /    \
        //    2      3
        //  /   \
        // 4     5
        // /
        // 6

        BinaryTree root = new BinaryTree(1);
        BinaryTree two = new BinaryTree(2);
        BinaryTree three = new BinaryTree(3);
        BinaryTree four = new BinaryTree(4);
        BinaryTree five = new BinaryTree(5);
        BinaryTree six = new BinaryTree(6);

        six.parent = four;
        four.left = six;
        four.parent = two;
        two.left = four;
        two.right = five;
        two.parent = root;
        root.left = two;
        root.right = three;

        three.parent = root;
        five.parent = two;

        FindSuccessor fs = new FindSuccessor();
        var result = fs.findSuccessorStack(root, five);
        logger.info(String.format("Next successor value is: %d", result.value));
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;
        public BinaryTree parent = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }

    public BinaryTree findSuccessorStack(BinaryTree tree, BinaryTree node) {
        if (node.right != null) {
            // find left most child
            return getLeftMostChild(node.right);
        }

        // find parent
        if (node.parent != null && node.parent.parent != null) {
            return currentRightMostParent(node);
        }

        return null;
    }

    private BinaryTree currentRightMostParent(BinaryTree node) {
        BinaryTree current = node;
        while(current.parent != null && current.parent.right == node) {
            current = current.parent;
        }

        return current.parent;
    }

    private BinaryTree getLeftMostChild(BinaryTree node) {
        BinaryTree current = node;
        while(current.left != null) {
            current = current.left;
        }

        return current;
    }

    public BinaryTree findSuccessor(BinaryTree tree, BinaryTree node) {
        List<BinaryTree> nodes = new ArrayList<>();
        inOrder(tree, nodes);

        for (int i = 0; i < nodes.size() - 1; i++) {
            if (nodes.get(i) == node) {
                return nodes.get(i + 1);
            }
        }

        return null;
    }

    private void inOrder(BinaryTree tree, List<BinaryTree> nodes) {
        if (tree == null) {
            return;
        }

        inOrder(tree.left, nodes);
        nodes.add(tree);
        inOrder(tree.right, nodes);
    }
}
