package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MinNumberOfJumps {
    private static final Logger logger = LoggerFactory.getLogger(MinNumberOfJumps.class);

    public static void main(String[] args) {
//        int[] input = {3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3};
        int[] input = {1};
//        int[] input = {2, 1, 1};
        var minJumps = minNumberOfJumpsAnotherSolution(input);

        logger.info(String.format("Min number of jumps is %d", minJumps));
    }

    private static int minNumberOfJumpsAnotherSolution(int[] array) {
        if (array.length <= 1) {
            return 0;
        }

        int maxJumps = 0;
        int maxReach = array[0];
        int steps = array[0];
        for (int i = 1; i < array.length; i++) {
            maxReach = Math.max(maxReach, i + array[i]);
            steps--;

            // if all steps are done till next jump
            if (steps == 0) {
                maxJumps++;
                steps = maxReach - i;
            }
        }

        return maxJumps + 1;
    }

    private static int minNumberOfJumpsSolutionWithJumpsArr(int[] array) {
        int[] jumps = new int[array.length];
        Arrays.fill(jumps, Integer.MAX_VALUE);
        jumps[0] = 0;

        for (int i = 0; i < array.length; i++) {
            if (jumps[jumps.length - 1] != Integer.MAX_VALUE) {
                break;
            }

            for (int j = i + 1; j <= i + array[i]; j++) {
                if (j >= array.length) {
                    break;
                }
                int currentMinJumps = Math.min(jumps[j], jumps[i] + 1);
                jumps[j] = currentMinJumps;
            }
        }

        return jumps[jumps.length - 1];
    }

    /**
     * O(1) space complexity
     * O(N) time complexity
     */
    private static int minNumberOfJumps(int[] array) {
        int maxJumps = 0;
        if (array.length <= 1) {
            return maxJumps;
        }

        int currentIndex = 0;
        while(currentIndex < array.length) {
            maxJumps++;
            if (currentIndex + array[currentIndex] >= array.length - 1) {
                return maxJumps;
            }

            // check all values of indexes from currentIdx to array[currentIdx]
            // pick maximum value
            int nextPotentialIdx = currentIndex + array[currentIndex];
            int idx = nextPotentialIdx;
            while(idx > currentIndex) {
                if (idx + array[idx] > nextPotentialIdx + array[nextPotentialIdx]) {
                    nextPotentialIdx = idx;
                }
                idx--;
            }

            currentIndex = nextPotentialIdx;
        }

        return maxJumps;
    }
}
