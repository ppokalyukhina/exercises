package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LevenshteinDistance {
    private static final Logger logger = LoggerFactory.getLogger(LevenshteinDistance.class);

    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = "yabd";
        logger.info(String.format("Distance is %d", levenshteinDistance(s1, s2)));
    }

    private static int levenshteinDistance(String str1, String str2) {
        int[][] matrix = new int[str1.length() + 1][str2.length() + 1];

        // build first rows
        for (int i = 0; i <  matrix.length; i++) {
            matrix[i][0] = i;
        }

        for (int j = 0; j < matrix[0].length; j++) {
            matrix[0][j] = j;
        }

        for (int string1 = 1; string1 <= str1.length(); string1++) {
            for (int string2 = 1; string2 <= str2.length(); string2++) {
                if (str1.charAt(string1 - 1) == str2.charAt(string2 - 1)) {
                    matrix[string1][string2] = matrix[string1 - 1][string2 - 1];
                    continue;
                }

                int minEdit = Math.min(matrix[string1][string2 - 1], matrix[string1 - 1][string2]);
                minEdit = Math.min(minEdit, matrix[string1 - 1][string2 - 1]);
                matrix[string1][string2] = minEdit + 1;
            }
        }

        return matrix[str1.length()][str2.length()];
    }
}
