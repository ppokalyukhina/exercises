package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Permutations {
    private static final Logger logger = LoggerFactory.getLogger(Permutations.class);

    public static void main(String[] args) {
        List<Integer> input = List.of(1, 2, 3);
        var result = getPermutations(input);

        for (List<Integer> integers : result) {
            StringBuilder permutation = new StringBuilder();
            for (Integer integer : integers) {
                permutation.append(integer);
            }

            logger.info(String.format("Next permutation is %s", permutation));
        }
    }

    public static List<List<Integer>> getPermutations(List<Integer> array) {
        ArrayList<List<Integer>> permutations = new ArrayList<>();
        if(array.isEmpty()) {
            return permutations;
        }
        fillArraysDifferentSolution(array, permutations,0);

        return permutations;
    }

    private static void fillArraysDifferentSolution(List<Integer> inputArray, ArrayList<List<Integer>> permutations, int currentIndex) {
        List<Integer> mutatedInputArray = new ArrayList<>(inputArray);
        if (currentIndex == inputArray.size() - 1) {
            permutations.add(mutatedInputArray);
            return;
        }

        for (int i = currentIndex; i < inputArray.size(); i++) {
            swap(i, currentIndex, mutatedInputArray);
            fillArraysDifferentSolution(mutatedInputArray, permutations, currentIndex + 1);
            swap(i, currentIndex, mutatedInputArray);
        }
    }

    private static void swap(int ind1, int ind2, List<Integer> array) {
        int tmp = array.get(ind1);
        array.set(ind1, array.get(ind2));
        array.set(ind2, tmp);
    }

    /**
     * O(N*N*N!) time complexity -> calls itself N times on each iteration (summed up N times)
     * O(N*N!) space complexity -> stores the calls in the memory stack on each iteration
     */
    private static void fillArrays(List<Integer> inputArray, ArrayList<List<Integer>> permutations, List<Integer> currentPermutation) {
        if (inputArray.isEmpty()) {
            permutations.add(currentPermutation);
            return;
        }

        for (Integer integer : inputArray) {
            List<Integer> reducedArray = new ArrayList<>(inputArray);
            reducedArray.remove(integer);
            List<Integer> newCurrentPermutation = new ArrayList<>(currentPermutation);
            newCurrentPermutation.add(integer);
            fillArrays(reducedArray, permutations, newCurrentPermutation);
        }
    }
}
