package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class BoggleBoard {
    private static final Logger logger = LoggerFactory.getLogger(BoggleBoard.class);

    public static void main(String[] args) {
        char[][] board = {
                {'t', 'h', 'i', 's', 'i', 's', 'a'},
                {'s', 'i', 'm', 'p', 'l', 'e', 'x'},
                {'b', 'x', 'x', 'x', 'x', 'e', 'b'},
                {'x', 'o', 'g', 'g', 'l', 'x', 'o'},
                {'x', 'x', 'x', 'D', 'T', 'r', 'a'},
                {'R', 'E', 'P', 'E', 'A', 'd', 'x'},
                {'x', 'x', 'x', 'x', 'x', 'x', 'x'},
                {'N', 'O', 'T', 'R', 'E', '-', 'P'},
                {'x', 'x', 'D', 'E', 'T', 'A', 'E'}
        };
        String[] words = {
                "this",
                "is",
                "not",
                "a",
                "simple",
                "boggle",
                "board",
                "test",
                "REPEATED",
                "NOTRE-PEATED"
        };

//        String[] words = {
//                "san",
//                "sana",
//                "at",
//                "vomit",
//                "yours",
//                "help",
//                "end",
//                "been",
//                "bed",
//                "danger",
//                "calm",
//                "ok",
//                "chaos",
//                "complete",
//                "rear",
//                "going",
//                "storm",
//                "face",
//                "epual",
//                "dangerous"
//        };
        var result = boggleBoard(board, words);
        for (String s : result) {
            logger.info(s);
        }
    }

    public static List<String> boggleBoard(char[][] board, String[] words) {
        TrieNode trie = buildTrie(words);
        Set<String> foundWords = new HashSet<>();

        boolean[][] visited = new boolean[board.length][board[0].length];
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                traverse(row, col, visited, board, trie, foundWords);
            }
        }

        return new ArrayList<>(foundWords);
    }

    private static void traverse(int row, int col, boolean[][] visited, char[][] board, TrieNode trie, Set<String> foundWords) {
        if (visited[row][col]) {
            return;
        }
        char currentChar = board[row][col];
        if (!trie.contains(currentChar)) {
            return;
        }

        visited[row][col] = true;
        // if traversed until the last symbol in the trie
        trie = trie.children.get(currentChar);
        if (trie.contains('*')) {
            foundWords.add(trie.getWord());
        }
        // traverse
        List<Integer[]> neighbours = getNeighbours(row, col, board);
        for (Integer[] neighbour : neighbours) {
            traverse(neighbour[0], neighbour[1], visited, board, trie, foundWords);
        }
        visited[row][col] = false;
    }

    private static List<Integer[]> getNeighbours(int row, int col, char[][] board) {
        List<Integer[]> neighbours = new ArrayList<>();
        // diagonal top left
        if (row > 0 && col > 0) {
            neighbours.add(new Integer[]{row - 1, col - 1});
        }
        // top
        if (row > 0) {
            neighbours.add(new Integer[]{row - 1, col});
        }
        // diagonal top right
        if (row > 0 && col < board[row - 1].length - 1) {
            neighbours.add(new Integer[]{row - 1, col + 1});
        }
        // right
        if (col < board[row].length - 1) {
            neighbours.add(new Integer[]{row, col + 1});
        }
        // diagonal bottom right
        if (row < board.length - 1 && col < board[row + 1].length - 1) {
            neighbours.add(new Integer[]{row + 1, col + 1});
        }
        // bottom
        if (row < board.length - 1) {
            neighbours.add(new Integer[]{row + 1, col});
        }
        // diagonal bottom left
        if (row < board.length - 1 && col > 0) {
            neighbours.add(new Integer[]{row + 1, col - 1});
        }
        // left
        if (col > 0) {
            neighbours.add(new Integer[]{row, col - 1});
        }

        return neighbours;
    }

    private static TrieNode buildTrie(String[] words) {
        TrieNode root = new TrieNode();
        char endSymbol = '*';

        for (String currentWord : words) {
            StringBuilder wordSoFar = new StringBuilder();
            char firstChar = currentWord.charAt(0);
            wordSoFar.append(firstChar);
            root.children.putIfAbsent(firstChar, new TrieNode());
            TrieNode previous = root.children.get(firstChar);
            previous.setWord(wordSoFar.toString());
            for (int j = 1; j < currentWord.length(); j++) {
                wordSoFar.append(currentWord.charAt(j));
                previous.children.putIfAbsent(currentWord.charAt(j), new TrieNode());
                previous = previous.children.get(currentWord.charAt(j));
                previous.setWord(wordSoFar.toString());
            }

            previous.children.put(endSymbol, null);
        }

        return root;
    }

    static class TrieNode {
        Map<Character, TrieNode> children = new HashMap<>();
        String word = "";

        public boolean contains(char character) {
            return children.containsKey(character);
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }
    }
}
