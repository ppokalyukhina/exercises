package com.algoexamples;

import java.util.ArrayList;
import java.util.List;

public class MergeTwoSortedArrays {
    public static void main(String[] args){
        List<List<Integer>> inputArray = new ArrayList<>();
//        inputArray.add(List.of(1, 5, 9, 21));
//        inputArray.add(List.of(-1, 0));
//        inputArray.add(List.of(-124, 81, 121));
//        inputArray.add(List.of(3, 6, 12, 20, 150));

        inputArray.add(List.of(-95, -74, 1));
        inputArray.add(List.of(-28, 28, 95));
        inputArray.add(List.of(-89, -78, -67, -66, -25, -22, 2, 38));
        inputArray.add(List.of(-86, -35, -25, -13, 41));
        inputArray.add(List.of(-85, -77, -21, 72));
        inputArray.add(List.of(-55, 4, 84, 98));
        inputArray.add(List.of(-75, -73, 22));
        var result = mergeSortedArrays(inputArray);
        for (Integer integer : result) {
            System.out.println(integer);
        }
    }

    public static List<Integer> mergeSortedArrays(List<List<Integer>> arrays) {
        List<Integer> sorted = new ArrayList<>();
        int index = 0;
        // build min heap from first elements
        // in arrays
        List<HeapNode> heap = new ArrayList<>();
        while(index < arrays.size()) {
            int firstN = arrays.get(index).get(0);
            // add to min heap
            addToHeap(heap, firstN, index, 0);
            index++;
        }

        while(!heap.isEmpty()) {
            // add min heap
            var minNode = pull(heap);
            sorted.add(minNode.value);

            int arrayIndex = minNode.arrayIndex;
            List<Integer> currentArray = arrays.get(arrayIndex);
            int nextPositionInArr = minNode.positionInArray + 1;
            if (nextPositionInArr < currentArray.size()) {
                addToHeap(heap, currentArray.get(nextPositionInArr), arrayIndex, nextPositionInArr);
            }
        }
        return sorted;
    }

    private static HeapNode pull(List<HeapNode> heap) {
        HeapNode min = heap.get(0);
        swap(heap, 0, heap.size() - 1);
        heap.remove(heap.size() - 1);
        siftDown(heap, 0);

        return min;
    }

    private static void siftDown(List<HeapNode> heap, int index) {
        int leftChildIndex = index * 2 + 1;
        while(leftChildIndex < heap.size()) {
            int indexToCompare = leftChildIndex;
            if (leftChildIndex + 1 < heap.size() && heap.get(leftChildIndex + 1).value < heap.get(leftChildIndex).value) {
                indexToCompare = leftChildIndex + 1;
            }

            if (heap.get(index).value <= heap.get(indexToCompare).value) {
                break;
            }
            swap(heap, index, indexToCompare);
            index = indexToCompare;
            leftChildIndex = index * 2 + 1;
        }
    }

    private static void addToHeap(List<HeapNode> heap, int element, int arrayIndex, int positionInArray) {
        var node = new HeapNode(element, arrayIndex, positionInArray);
        heap.add(node);
        // sift up
        siftUp(heap, heap.size() - 1);
    }

    private static void siftUp(List<HeapNode> heap, int indx) {
        int parentIndx = (indx - 1) / 2;
        while(indx > 0 && heap.get(parentIndx).value > heap.get(indx).value) {
            swap(heap, parentIndx, indx);
            indx = parentIndx;
            parentIndx = (indx - 1) / 2;
        }
    }

    private static void swap(List<HeapNode> heap, int indx1, int indx2) {
        HeapNode tmp = heap.get(indx1);
        heap.set(indx1, heap.get(indx2));
        heap.set(indx2, tmp);
    }

    private static class HeapNode {
        int value;
        int arrayIndex;
        int positionInArray;

        public HeapNode(int value, int arrayIndex, int positionInArray) {
            this.value = value;
            this.arrayIndex = arrayIndex;
            this.positionInArray = positionInArray;
        }
    }
}
