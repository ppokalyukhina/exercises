package com.algoexamples;

public class LinkedListPalindrome {
    public static void main(String[] args) {
        LinkedList head = new LinkedList(0);
        LinkedList one = new LinkedList(1);
        LinkedList two = new LinkedList(2);
        LinkedList three = new LinkedList(3);
        LinkedList two1 = new LinkedList(2);
        LinkedList one1 = new LinkedList(1);
        LinkedList zero = new LinkedList(0);
        head.next = one;
        one.next = two;
        two.next = three;
        three.next = two1;
        two1.next = one1;
        one1.next = zero;
        System.out.println(linkedListPalindrome(head));
    }

    // not so optimal, but for practicing
    public static boolean linkedListPalindromeRecursive(LinkedList head) {
        var nodeInfo = isPalindrome(head, head);
        return nodeInfo.isPalindrome;
    }

    private static NodeInfo isPalindrome(LinkedList leftNode, LinkedList rightNode) {
        if (rightNode == null) {
            return new NodeInfo(true, leftNode);
        }

        var rightNodeInfo = isPalindrome(leftNode, rightNode.next);
        var leftNodeToCompare = rightNodeInfo.leftNode;
        var isPalindromeSoFar = rightNodeInfo.isPalindrome;

        var equal = isPalindromeSoFar && leftNodeToCompare.value == rightNode.value;
        return new NodeInfo(equal, leftNodeToCompare.next);
    }

    private static class NodeInfo {
        boolean isPalindrome = false;
        LinkedList leftNode = null;

        public NodeInfo(boolean isPalindrome, LinkedList leftNode) {
            this.isPalindrome = isPalindrome;
            this.leftNode = leftNode;
        }
    }

    // O(N) time
    // O(1) space
    public static boolean linkedListPalindrome(LinkedList head) {
        // find mid of the list
        LinkedList slow = head;
        LinkedList fast = head;
        while(slow != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        // reverse second half
        LinkedList newHead = reverseList(slow);
        LinkedList currentNodeToCompare = head;
        // compare to first half
        while(newHead != null && currentNodeToCompare != null) {
            if (currentNodeToCompare.value != newHead.value) {
                return false;
            }
            currentNodeToCompare = currentNodeToCompare.next;
            newHead = newHead.next;
        }

        return true;
    }

    private static LinkedList reverseList(LinkedList head) {
        LinkedList previous = null;
        LinkedList current = head;
        while(current != null) {
            var next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        return previous;
    }

    public static class LinkedList {
        public int value;
        public LinkedList next;

        public LinkedList(int value) {
            this.value = value;
            this.next = null;
        }
    }
}
