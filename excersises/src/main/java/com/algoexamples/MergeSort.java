package com.algoexamples;

import java.util.Arrays;

public class MergeSort {
    public static void main(String[] args) {
        int[] input = {8, 5, 2, 9, 5, 6, 3};
        var result = sort(input);
        for (int i : result) {
            System.out.println(i);
        }
    }

    /**
     * O(N(logN)) time complexity
     * O(N(logN)) space complexity
     */
    public static int[] mergeSort(int[] array) {
        if (array.length <= 1) {
            return array;
        }
        int mid = array.length / 2;
        int[] left = Arrays.copyOfRange(array, 0, mid);
        int[] right = Arrays.copyOfRange(array, mid, array.length);
        return sort(mergeSort(left), mergeSort(right));
    }

    /**
     * O(N(logN)) time complexity
     * O(N) space complexity -> just one extra auxilary array
     */
    private static int[] sort(int[] array) {
        if (array.length <= 1) {
            return array;
        }

        int[] auxiliary = array.clone();
        mergeSortHelper(array, auxiliary, 0, array.length - 1);
        return array;
    }

    private static void mergeSortHelper(int[] mainArray, int[] auxiliary, int startIndex, int endIndex) {
        if (startIndex == endIndex) {
            return;
        }

        int mid = (startIndex + endIndex) / 2;
        mergeSortHelper(auxiliary, mainArray, startIndex, mid);
        mergeSortHelper(auxiliary, mainArray, mid + 1, endIndex);

        doMerge(mainArray, auxiliary, startIndex, mid, endIndex);
    }

    private static void doMerge(int[] mainArr, int[] auxiliary, int start, int mid, int end) {
        int main = start;
        int left = start;
        int right = mid + 1;
        while(left <= mid && right <= end) {
            if (auxiliary[left] <= auxiliary[right]) {
                mainArr[main++] = auxiliary[left++];
            } else {
                mainArr[main++] = auxiliary[right++];
            }
        }

        // leftovers in left arr
        while (left <= mid) {
            mainArr[main++] = auxiliary[left++];
        }

        // leftovers in right arr
        while (right <= end) {
            mainArr[main++] = auxiliary[right++];
        }
    }

    private static int[] sort(int[] left, int[] right) {
        int[] merged = new int[left.length + right.length];
        int leftIndex = 0;
        int rightIndex = 0;
        int mergedIndex = 0;
        while(leftIndex < left.length && rightIndex < right.length) {
            if (left[leftIndex] < right[rightIndex]) {
                merged[mergedIndex] = left[leftIndex++];
            } else {
                merged[mergedIndex] = right[rightIndex++];
            }
            mergedIndex++;
        }

        // leftovers in left arr
        while (leftIndex < left.length) {
            merged[mergedIndex++] = left[leftIndex++];
        }

        // leftovers in right arr
        while (rightIndex < right.length) {
            merged[mergedIndex++] = right[rightIndex++];
        }

        return merged;
    }
}
