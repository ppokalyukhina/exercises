package com.algoexamples;

import java.util.*;

public class MultiStringSearch {
    public static void main(String[] args) {
//        String bigStr = "this is a big string";
//        String[] smallStrings = {"this", "yo", "is", "a", "bigger", "string", "kappa"}; // should return [true, false, true, true, false, true, false]
        String bigStr = "Mary goes to the shopping center every week.";
        String[] smallStrings = {
                "to",
                "Mary",
                "centers",
                "shop",
                "shopping",
                "string",
                "kappa"
        };
        // should return [true, true, false, true, true, false, false]
//        String bigStr = "abcdefghijklmnopqrstuvwxyz";
//        String[] smallStrings = {"abc", "mnopqr", "wyz", "no", "e", "tuuv"}; // should return [true, true, false, true, true, false]

        var result = multiStringSearchSmallStringsTrie(bigStr, smallStrings);
        for (Boolean aBoolean : result) {
            System.out.println(aBoolean);
        }
    }

    /**
     * O(N*S) time -> building trie
     *  N -> n of small strings
     *  S -> length of longest small string
     * O(N*S) space -> storing small strings
     * O(N) space -> found strings
     *  N -> n of small strings
     * O(b*s) time -> finding strings
     *  b -> length of big string
     *  S -> length of longest small string
     */
    public static List<Boolean> multiStringSearchSmallStringsTrie(String bigString, String[] smallStrings) {
        List<Boolean> result = new ArrayList<>();
        // iterate through small strings
        // check if trie contains them
        TrieNode root = buildTrieSmallStrings(smallStrings);
        Set<String> foundStrings = new HashSet<>();
        findStrings(foundStrings, bigString, root);
        buildResultArray(foundStrings, smallStrings, result);

        return result;
    }

    private static void buildResultArray(Set<String> foundStrings, String[] smallStrings, List<Boolean> result) {
        for (String string : smallStrings) {
            if (foundStrings.contains(string)) {
                result.add(true);
            } else {
                result.add(false);
            }
        }
    }

    private static void findStrings(Set<String> foundStrings, String bigString, TrieNode root) {
        for (int i = 0; i < bigString.length(); i++) {
            var currentNode = root;
            int currentIndx = i;
            while(currentIndx < bigString.length()) {
                char currentCharacter = bigString.charAt(currentIndx);
                if (!currentNode.children.containsKey(currentCharacter)) {
                    break;
                }
                currentNode = currentNode.children.get(currentCharacter);
                if (currentNode.children.containsKey('*')) {
                    foundStrings.add(currentNode.word);
                }
                currentIndx++;
            }
        }
    }

    /**
     * Store longest string in a trie
     * iterate through small strings
     * try to match them to strings in trie
     *
     * O(Lpow2) + O(Npow2) time complexity
     *     L -> length of big string
     *     N -> n of small strings
     * Trie space complexity: O(Lpow2)
     *     L -> length of big string
     */
    public static List<Boolean> multiStringSearch(String bigString, String[] smallStrings) {
        List<Boolean> result =  new ArrayList<>();
        TrieNode trie = buildTrie(bigString);
        // iterate through small strings
        // check if trie contains them
        strings: for (String smallString : smallStrings) {
            TrieNode currentNode = trie;
            for (int i = 0; i < smallString.length(); i++) {
                char currentCharacter = smallString.charAt(i);
                if (!currentNode.children.containsKey(currentCharacter)) {
                    result.add(false);
                    continue strings;
                }
                currentNode = currentNode.children.get(currentCharacter);
            }
            result.add(true);
        }
        return result;
    }

    /**
     * O(N*S) time complexity
     *      S -> length of longest word
     * O(N*S) * W space complexity
     *      S -> length of longest word
     * N -> length of big string
     */
    private static TrieNode buildTrieSmallStrings(String[] words) {
        TrieNode trie = new TrieNode();
        for (String word : words) {
            var previous = trie;
            for (int j = 0; j < word.length(); j++) {
                char letter = word.charAt(j);
                previous.children.putIfAbsent(letter, new TrieNode());
                previous = previous.children.get(letter);
            }
            // add '*' at the end
            previous.children.put('*', null);
            previous.word = word;
        }
        return trie;
    }

    /**
     * O(Npow2) time complexity
     * O(Npow2) * W space complexity
     * N -> length of big string
     */
    private static TrieNode buildTrie(String str) {
        TrieNode trie = new TrieNode();
        String[] words = str.split(" ");
        for (String word : words) {
            char letter = word.charAt(0);
            trie.children.putIfAbsent(letter, new TrieNode());
            for (int j = 1; j < word.length(); j++) {
                var previous = trie.children.get(word.charAt(j - 1));
                int currentIndex = j;
                while (currentIndex < word.length()) {
                    char character = word.charAt(currentIndex);
                    previous.children.putIfAbsent(character, new TrieNode());
                    previous = previous.children.get(character);
                    currentIndex++;
                }
                // add '*' at the end
                previous.children.put('*', null);
                trie.children.putIfAbsent(word.charAt(j), new TrieNode());
            }
            // add '*' to the last child node.
            trie.children.get(word.charAt(word.length() - 1)).children.put('*', null);
        }

        return trie;
    }

    static class TrieNode {
        Map<Character, TrieNode> children = new HashMap<>();
        String word = "";
    }
}
