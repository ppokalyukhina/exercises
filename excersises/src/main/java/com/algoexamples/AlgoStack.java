package com.algoexamples;

public class AlgoStack {
    private int size;
    private int maxSize = 1;
    private Integer[] stack;
    private Integer[] sorted;

    public AlgoStack() {
        size = 0;
        stack = new Integer[maxSize];
        sorted = new Integer[maxSize];
    }

    public int peek() {
        if (isEmpty()) {
            return -1;
        }

        return stack[size - 1];
    }

    public int pop() {
        if (isEmpty()) {
            return -1;
        }

        int last = stack[size - 1];
        stack[size - 1] = null;

        // find position of deleted element
        int deletedItemPositionInSorted = getPositionOfDeletedItem(last);
        swapElementsInSortedArr(deletedItemPositionInSorted);
        sorted[size - 1] = null;

        size--;

        return last;
    }

    private void swapElementsInSortedArr(int deletedItemPositionInSorted) {
        for (int i = deletedItemPositionInSorted; i < size - 1; i++) {
            sorted[i] = sorted[i + 1];
        }
    }

    private int getPositionOfDeletedItem(int deleted) {
        int element = 0;
        for (int i = 0; i < size; i++) {
            if (sorted[i] == deleted) {
                element = i;
                sorted[i] = null;
                break;
            }
        }

        return element;
    }

    public void push(Integer number) {
        if (isFull()) {
            maxSize = this.maxSize * 2;
            Integer[] extendedStack = new Integer[maxSize];
            for (int i = 0; i < stack.length; i++) {
                extendedStack[i] = stack[i];
            }
            stack = extendedStack;

            Integer[] extendedSortedStack = new Integer[maxSize];
            for (int i = 0; i < sorted.length; i++) {
                extendedSortedStack[i] = sorted[i];
            }
            sorted = extendedSortedStack;
        }

        stack[size] = number;
        sorted[size] = number;

        // put number in the right position
        int pointer = size - 1;
        while(pointer >= 0 && number < sorted[pointer]) {
            sorted[pointer + 1] = sorted[pointer];
            sorted[pointer] = number;
            pointer--;
        }

        size++;
    }

    public int getMin() {
        if (isEmpty()) {
            return -1;
        }

        return sorted[0];
    }

    public int getMax() {
        if (isEmpty()) {
            return -1;
        }

        return sorted[size - 1];
    }

    private boolean isFull() {
        return size == maxSize;
    }

    private boolean isEmpty() {
        return size == 0;
    }
}
