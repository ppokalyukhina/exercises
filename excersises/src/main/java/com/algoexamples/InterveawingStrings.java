package com.algoexamples;

public class InterveawingStrings {
    public static void main(String[] args) {
//        String one = "abc";
//        String two = "123";
//        String three = "a1b2c3";
        String one = "aaa";
        String two = "aaaf";
        String three = "aaafaaa";
//        String one = "aabcc";
//        String two = "dbbca";
//        String three = "aadbbbaccc";

        System.out.println(interweavingStrings(one, two, three));
    }

    public static boolean interweavingStrings(String one, String two, String three) {
        if (three.length() != one.length() + two.length()) {
            return false;
        }

        Boolean[][] cache = new Boolean[one.length() + 1][two.length() + 1];
        return areInterviewing(one, two, three, 0, 0, cache);
    }

    /**
     * O(N*M), n -> one.length, m -> two.length time complexity (thank to cache)
     * O(N*M) space complexity (cache map)
     */
    public static boolean areInterviewing(String one, String two, String three, int stringOneIndex, int stringTwoIndex, Boolean[][] cache) {
        if (cache[stringOneIndex][stringTwoIndex] != null) {
            return cache[stringOneIndex][stringTwoIndex];
        }

        // if all strings are empty
        // all characters in last string have been found in first two
        int stringThreeIndex = stringOneIndex + stringTwoIndex;
        if (three.length() == stringThreeIndex) {
            return true;
        }

        if (stringOneIndex < one.length() && one.charAt(stringOneIndex) == three.charAt(stringThreeIndex)) {
            cache[stringOneIndex][stringTwoIndex] = areInterviewing(one, two, three, stringOneIndex + 1, stringTwoIndex, cache);
            if (cache[stringOneIndex][stringTwoIndex]) {
                return true;
            }
        }

        if (stringTwoIndex < two.length() && two.charAt(stringTwoIndex) == three.charAt(stringThreeIndex)) {
            var result = areInterviewing(one, two, three, stringOneIndex, stringTwoIndex + 1, cache);
            cache[stringOneIndex][stringTwoIndex] = result;
            return result;
        }

        cache[stringOneIndex][stringTwoIndex] = false;
        return false;
    }

    /**
     * O(2pow(N+M)), n -> one.length, m -> two.length time complexity
     * O(N+M) space complexity (all the recursive calls)
     */
    public static boolean areInterviewing(String one, String two, String three, int stringOneIndex, int stringTwoIndex) {
        // if all strings are empty
        // all characters in last string have been found in first two
        int stringThreeIndex = stringOneIndex + stringTwoIndex;
        if (three.length() == stringThreeIndex) {
            return true;
        }

        if (stringOneIndex < one.length() && one.charAt(stringOneIndex) == three.charAt(stringThreeIndex)) {
            if (areInterviewing(one, two, three, stringOneIndex + 1, stringTwoIndex)) {
                return true;
            }
        }

        else if (stringTwoIndex < two.length() && two.charAt(stringTwoIndex) == three.charAt(stringThreeIndex)) {
            return areInterviewing(one, two, three, stringOneIndex, stringTwoIndex + 1);
        }

        return false;
    }

    /**
     * O(2pow(N)) time complexity, N -> string three.length()
     * O(N) space complexity, N -> string three.length()
     */
    public static boolean interweavingStringsRecursive(String one, String two, String three) {
        // if all strings are empty
        // all characters in last string have been found in first two
        if (three.length() == 0 && one.length() == 0 && two.length() == 0) {
            return true;
        }

        // else there are some leftovers in first two strings
        // and therefore return false
        if (three.length() == 0) {
            return false;
        }

        if (one.length() == 0 && two.length() == 0) {
            return false;
        }

        char nextToFind = three.charAt(0);
        if (one.length() > 0 && one.charAt(0) == nextToFind && two.length() > 0 && two.charAt(0) == nextToFind) {
            three = three.substring(1);
            if (one.length() > 1 && one.charAt(1) == three.charAt(0)) {
                one = one.substring(1);
                return interweavingStringsRecursive(one, two, three);
            } else {
                two = two.substring(1);
                return interweavingStringsRecursive(one, two, three);
            }
        }

        else if (one.length() > 0 && one.charAt(0) == nextToFind) {
            // remove that element from string 1
            one = one.substring(1);
            // remove that char from third string
            three = three.substring(1);
            // call this method again with new strings
            return interweavingStringsRecursive(one, two, three);
        }

        else if(two.length() > 0 && two.charAt(0) == nextToFind) {
            // else do the same with second string no element was found in the first one
            // remove that element from string 1
            two = two.substring(1);
            // remove that char from third string
            three = three.substring(1);
            // call this method again with new strings
            return interweavingStringsRecursive(one, two, three);
        }

        return false;
    }
}
