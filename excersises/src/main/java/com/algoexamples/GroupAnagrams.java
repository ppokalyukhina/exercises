package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class GroupAnagrams {
    private static final Logger logger = LoggerFactory.getLogger(GroupAnagrams.class);

    public static void main(String[] args) {
        List<String> input = List.of("yo", "act", "flop", "tac", "foo", "cat", "oy", "olfp");
        List<List<String>> output = groupAnagrams(input);

        for (List<String> strings : output) {
            for (String string : strings) {
                logger.info(string);
            }
        }
    }

    private static List<List<String>> groupAnagrams(List<String> words) {
        Map<String, List<String>> sortedStrings = new HashMap<>();

        // sort words by letters and store them in a map
        // key -> sorted string, value -> list of corresponding strings
        for (String word : words) {
            String key = sortString(word);
            sortedStrings.putIfAbsent(key, new ArrayList<>());
            sortedStrings.get(key).add(word);
        }

        return new ArrayList<>(sortedStrings.values());
    }

    private static String sortString(String word) {
        char[] wordChars = word.toCharArray();
        Arrays.sort(wordChars);
        return String.valueOf(wordChars);
    }
}
