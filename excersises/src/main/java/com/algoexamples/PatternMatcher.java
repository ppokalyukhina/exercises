package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class PatternMatcher {
    private static final Logger logger = LoggerFactory.getLogger(PatternMatcher.class);

    public static void main(String[] args) {
//        String pattern = "yyxyyx";
//        String pattern = "xxyxxy";
//        String pattern = "yxyx";
        String pattern = "xxyxyy";
//        String st = "gogopowerrangergogopowerranger";
//        String st = "abab";
        String st = "testtestwrongtestwrongtest";
        var result = patternMatcher(pattern, st);
        for (String s : result) {
            logger.info(s);
        }
    }

    /**
     * O(n + m) space complexity ->
     * .. n string length (when building a string from substr., store the string on each iteration in memory)
     * .. m is pattern length (map)
     * Time complexity:
     * + O(m) -> m is pattern length (for building the map)
     * + O(N) * m -> matches method.. (m - pattern length, N - string length)
     * ==> O(Npow2 + m) (??)
     */
    public static String[] patternMatcher(String pattern, String str) {
        String[] result = new String[2];
        boolean reversedPattern = false;
        if (pattern.charAt(0) == 'y') {
            reversedPattern = true;
        }
        var newPattern = pattern;
        if (reversedPattern) {
            newPattern = convertPattern(pattern);
        }
        // key => x OR y of the pattern
        // value => how many times
        // each of them appears in the pattern
        Map<Character,Integer> patternFrequencies = new HashMap<>();
        fillPatternFrequencies(patternFrequencies, newPattern);
        if (!patternFrequencies.containsKey('y')) {
            return returnXsOnly(str, patternFrequencies, newPattern, reversedPattern);
        }

        int firstYInPatternPos = newPattern.indexOf('y');
        for (int i = 0; i < str.length(); i++) {
            // last elem is excluded
            // thus i + 1
            String x = str.substring(0, i + 1);
            int allXsLength = x.length() * patternFrequencies.get('x');
            if (allXsLength >= str.length()) {
                return new String[]{};
            }
            int ySubstrLength = (str.length() - allXsLength) / patternFrequencies.get('y');
            int firstYpositionInString = firstYInPatternPos * x.length();
            String y = str.substring(firstYpositionInString, firstYpositionInString + ySubstrLength);

            boolean matches = matchesPattern(x, y, newPattern, str);
            if (matches) {
                if (reversedPattern) {
                    result[0] = y;
                    result[1] = x;
                } else {
                    result[0] = x;
                    result[1] = y;
                }
                break;
            }
        }
        return result;
    }

    private static String[] returnXsOnly(String str, Map<Character, Integer> patternFrequencies, String pattern, boolean isReversed) {
        // check only X's then
        int xSize = str.length() / patternFrequencies.get('x');
        String x = str.substring(0, xSize);

        if (matchesPattern(x, "", pattern, str)) {
            if (isReversed) {
                return new String[]{"", x};
            }
            return new String[]{x, ""};
        } else {
            return new String[]{};
        }
    }

    private static boolean matchesPattern(String x, String y, String pattern, String stringToMatch) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < pattern.length(); i++) {
            String current = x;
            if (pattern.charAt(i) == 'y') {
                current = y;
            }
            builder.append(current);
        }

        return builder.toString().equals(stringToMatch);
    }

    private static void fillPatternFrequencies(Map<Character,Integer> patternFrequencies, String pattern) {
        for (int i = 0; i < pattern.length(); i++) {
            char currentCharacter = pattern.charAt(i);
            patternFrequencies.putIfAbsent(currentCharacter, 0);
            var frequency = patternFrequencies.get(currentCharacter);
            patternFrequencies.put(currentCharacter, frequency + 1);
        }
    }

    private static String convertPattern(String pattern) {
        char[] newPattern = new char[pattern.length()];
        for (int i = 0; i < pattern.length(); i++) {
            if (pattern.charAt(i) == 'x') {
                newPattern[i] = 'y';
            } else {
                newPattern[i] = 'x';
            }
        }
        return String.copyValueOf(newPattern);
    }
}
