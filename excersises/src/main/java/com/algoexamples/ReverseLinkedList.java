package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReverseLinkedList {
    private static final Logger logger = LoggerFactory.getLogger(ReverseLinkedList.class);

    public static void main(String[] args) {
        LinkedList head = new LinkedList(0);
        LinkedList one = new LinkedList(1);
        LinkedList two = new LinkedList(2);
        LinkedList three = new LinkedList(3);
        LinkedList four = new LinkedList(4);
        LinkedList five = new LinkedList(5);
//        head.next = one;
//        one.next = two;
//        two.next = three;
//        three.next = four;
//        four.next = five;

        var reversed = reverseLinkedList(head);
        LinkedList current = reversed;
        while(current != null) {
            logger.info(String.format("%d", current.value));
            current = current.next;
        }
    }

    public static LinkedList reverseLinkedList(LinkedList head) {
        LinkedList previous = null;
        LinkedList current = head;
        while(current != null) {
            var tmpNext = current.next;
            current.next = previous;
            previous = current;
            current = tmpNext;
        }

        return previous;
    }

    static class LinkedList {
        int value;
        LinkedList next = null;

        public LinkedList(int value) {
            this.value = value;
        }
    }
}
