package com.algoexamples;

/**
 * Linked list
 */
public class FindLoop {
    public static void main(String[] args) {
        LinkedList root = new LinkedList(0);
        var one = new LinkedList(1);
        var two = new LinkedList(2);
        var three = new LinkedList(3);
        var four = new LinkedList(4);
        var five = new LinkedList(5);
        var six = new LinkedList(6);
        var seven = new LinkedList(7);
        var eight = new LinkedList(8);
        var nine = new LinkedList(9);

        nine.next = four;
        eight.next = nine;
        seven.next = eight;
        six.next = seven;
        five.next = six;
        four.next = five;
        three.next = four;
        two.next = three;
        one.next = two;
        root.next = one;

        var result = findLoop(root);
        System.out.println(result.value);
    }

    public static LinkedList findLoop(LinkedList head) {
        LinkedList pointOne = head.next;
        LinkedList pointTwo = head.next.next;

        while (pointOne != pointTwo) {
            pointOne = pointOne.next;
            pointTwo = pointTwo.next.next;
        }

        pointOne = head;
        while(pointOne != pointTwo) {
            pointOne = pointOne.next;
            pointTwo = pointTwo.next;
        }

        return pointOne;
    }

    static class LinkedList {
        int value;
        LinkedList next = null;

        public LinkedList(int value) {
            this.value = value;
        }
    }
}
