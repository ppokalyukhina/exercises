package com.algoexamples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskAssignment {
    public static void main(String[] args) {
        int k = 3;
//        int k = 1;
//        int k = 7;
        List<Integer> tasksInput = List.of(1, 3, 5, 3, 1, 4);
//        List<Integer> tasksInput = List.of(3, 5);
//        List<Integer> tasksInput = List.of(2, 1, 3, 4, 5, 13, 12, 9, 11, 10, 6, 7, 14, 8);
        var tasks = taskAssignment(k, tasksInput);

        for (ArrayList<Integer> task : tasks) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            for (Integer integer : task) {
                builder.append(integer).append(',');
            }
            builder.append("]");
            System.out.println(builder);
        }
    }

    /**
     * O(N + N) -> space complexity
     * - O(N) tasks indexes
     * - O(N) sorted tasks
     *
     * O(NlogN) -> sorting time complexity
     * O(N / 2) -> building result array time complexity
     */
    public static ArrayList<ArrayList<Integer>> taskAssignment(int k, List<Integer> tasks) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        if ((tasks.size() / k) != 2) {
            return result;
        }
        Map<Integer, List<Integer>> tasksIndexes = new HashMap<>();
        for (int i = 0; i < tasks.size(); i++) {
            tasksIndexes.putIfAbsent(tasks.get(i), new ArrayList<>());
            tasksIndexes.get(tasks.get(i)).add(i);
        }

        List<Integer> copyTasks = new ArrayList<>(tasks);
        copyTasks.sort(Integer::compareTo);

        int firstIndx = 0;
        int lastIndx = tasks.size() - 1;
        while(firstIndx < lastIndx) {
            ArrayList<Integer> currentTasks = new ArrayList<>();
            // get tasks indexes
            List<Integer> firstTasks = tasksIndexes.get(copyTasks.get(firstIndx));
            int firstTaskIndex = firstTasks.get(firstTasks.size() - 1);
            List<Integer> secondTasks = tasksIndexes.get(copyTasks.get(lastIndx));

            int secondTaskIndex = secondTasks.get(secondTasks.size() - 1);
            currentTasks.add(firstTaskIndex);
            currentTasks.add(secondTaskIndex);

            // remove added indexes
            tasksIndexes.get(copyTasks.get(firstIndx)).remove(firstTasks.size() - 1);
            tasksIndexes.get(copyTasks.get(lastIndx)).remove(secondTasks.size() - 1);

            result.add(currentTasks);
            firstIndx++;
            lastIndx--;
        }

        return result;
    }
}
