package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SpiralTraverse {
    private static final Logger logger = LoggerFactory.getLogger(SpiralTraverse.class);

    public static void main(String[] args) {
        int[][] input = {new int[]{1, 2, 3, 4}, new int[]{12, 13, 14, 5}, new int[]{11, 16, 15, 6}, new int[]{10, 9, 8, 7}};
//        int[][] input = {new int[]{1}};

        var result = spiralTraverse(input);
        for (Integer integer : result) {
            logger.info(String.format("Next is: %d", integer));
        }
    }

    public static List<Integer> spiralTraverse(int[][] array) {
        ArrayList<Integer> merged = new ArrayList<>();
        if (array.length == 0) {
            return merged;
        }

        int firstVertical = 0;
        int firstHorizontal = 0;

        int lastVertical = array.length - 1;
        int lastHorizontal = array[0].length - 1;

        while(firstVertical <= lastVertical && firstHorizontal <= lastHorizontal) {
            // left -> right
            for (int i = firstHorizontal; i <= lastHorizontal; i++) {
                merged.add(array[firstVertical][i]);
            }

            // top -> bottom
            for (int j = firstVertical + 1; j <= lastVertical; j++) {
                merged.add(array[j][lastHorizontal]);
            }

            // right -> left
            for (int m = lastHorizontal - 1; m >= firstHorizontal; m--) {
                if (firstVertical == lastVertical) {
                    break;
                }
                merged.add(array[lastVertical][m]);
            }

            // bottom -> top
            for (int n = lastVertical - 1; n > firstVertical; n--) {
                if (firstHorizontal == lastHorizontal) {
                    break;
                }
                merged.add(array[n][firstHorizontal]);
            }

            firstHorizontal++;
            firstVertical++;
            lastHorizontal--;
            lastVertical--;
        }

        return merged;
    }
}
