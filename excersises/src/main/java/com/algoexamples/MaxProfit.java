package com.algoexamples;

public class MaxProfit {
    public static void main(String[] args) {
        int[] prices = {6,1,3,2,4,7};
        System.out.println(maxProfitTwoTransactions(prices));
    }

    // int[] prices = {3,2,6,5,0,3};
    // should return 7 with 2 transactions
    public static int maxProfitTwoTransactions(int[] prices) {
        if(prices.length <= 1) {
            return 0;
        }

        int k = 2;
        int[][] profits = new int[k + 1][prices.length];
        for (int day = 1; day <= k; day++) {
            int profitToday = Integer.MIN_VALUE;
            for (int price = 1; price < prices.length; price++) {
                // profit until current day
                profitToday = Math.max(profitToday, profits[day - 1][price - 1] - prices[price - 1]);
                // either profit from yesterday
                // or profit so far + current price
                // yesterday's profit - yesterday's val
                // (should be "sold" before it can be bought and sold today)
                int maxNewProfit = Math.max(profits[day][price - 1], (profitToday + prices[price]));
                profits[day][price] = maxNewProfit;
            }
        }

        return profits[k][prices.length - 1];
    }

    public static int maxProfitOneTransaction(int[] prices) {
        if(prices.length == 0) {
            return 0;
        }
        int[] maxProfit = new int[prices.length];
        int minValSoFar = prices[0];
        // first element can only be bought
        maxProfit[0] = 0;
        for(int i = 1; i < prices.length; i++) {
            int previousMaxProfit = maxProfit[i - 1];
            int profitIfSold = prices[i] - minValSoFar;
            maxProfit[i] = Math.max(previousMaxProfit, profitIfSold);
            minValSoFar = Math.min(minValSoFar, prices[i]);
        }

        return maxProfit[maxProfit.length - 1];
    }
}
