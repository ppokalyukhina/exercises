package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TopologicalSort {
    private static final Logger logger = LoggerFactory.getLogger(TopologicalSort.class);

    public static void main(String[] args) {
        var jobs = List.of(1, 2, 3, 4);
//        var jobs = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        var deps = List.of(
                new Integer[]{1, 2},
                new Integer[]{1, 3},
                new Integer[]{3, 2},
                new Integer[]{4, 2},
                new Integer[]{4, 3}
        );
//        var deps = List.of(
//                new Integer[]{3, 1},
//                new Integer[]{8, 1},
//                new Integer[]{8, 7},
//                new Integer[]{5, 7},
//                new Integer[]{5, 2},
//                new Integer[]{1, 4},
//                new Integer[]{1, 6},
//                new Integer[]{1, 2},
//                new Integer[]{7, 6}
//        );

        var result = topologicalSort(jobs, deps);
        for (Integer sortedJob : result) {
            logger.info(String.format("%d", sortedJob));
        }
    }

    /**
     * No DFS traversal
     * Keep track of nodes with no prerequisites
     * O(V + E) time complexity
     * O(V + E) space complexity
     */
    public static List<Integer> topologicalSort(List<Integer> jobs, List<Integer[]> deps) {
        ArrayList<Integer> result = new ArrayList<>();
        if (jobs.size() == 0) {
            return result;
        }

        var graph = buildGraph(jobs, deps);
        List<JobNode> noPrerequisites = new ArrayList<>();
        for (JobNode job : graph.getAllNodes()) {
            if (job.getPrerequisites().isEmpty()) {
                noPrerequisites.add(job);
            }
        }

        while(!noPrerequisites.isEmpty()) {
            var currentJob = noPrerequisites.get(noPrerequisites.size() - 1);
            var dependencies = currentJob.getDependencies();
            for (JobNode dependency : dependencies) {
                dependency.removePrerequisite(currentJob);
                if (dependency.getPrerequisites().isEmpty()) {
                    noPrerequisites.add(dependency);
                }
            }
            result.add(currentJob.jobValue);
            noPrerequisites.remove(currentJob);
        }

        // in case there were cycles in graph
        if (result.size() < jobs.size()) {
            return new ArrayList<>();
        }

        return result;
    }

    /**
     * O(V + E) time complexity
     * O(V + E) space complexity (Graph)
     * V -> vertix
     * E -> Edge (e.g. V1 -> V2)
     */
    public static List<Integer> topologicalSortDFSsolution(List<Integer> jobs, List<Integer[]> deps) {
        ArrayList<Integer> result = new ArrayList<>();
        if (jobs.size() == 0) {
            return result;
        }
        var graph = buildGraph(jobs, deps);
        return gerOrderedJobs(graph);
    }

    private static List<Integer> gerOrderedJobs(Graph graph) {
        List<Integer> orderedJobs = new ArrayList<>();
        List<JobNode> allNodes = new ArrayList<>(graph.getAllNodes());
        while(!allNodes.isEmpty()) {
            // remove node and traverse
            var currentNode = allNodes.get(allNodes.size() - 1);
            allNodes.remove(currentNode);
            // check if there is a cycle
            boolean foundCycle = foundCycle(currentNode, orderedJobs);
            if (foundCycle) {
                return new ArrayList<>();
            }
        }

        return orderedJobs;
    }

    private static boolean foundCycle(JobNode currentNode, List<Integer> orderedJobs) {
        if (currentNode.isVisited()) {
            return false;
        }

        if (currentNode.isVisiting()) {
            return true;
        }

        currentNode.setVisiting(true);
        var prerequisites = currentNode.getPrerequisites();
        for (JobNode prerequisite : prerequisites) {
            var foundCycle = foundCycle(prerequisite, orderedJobs);
            if (foundCycle) {
                return true;
            }
        }
        currentNode.setVisited(true);
        currentNode.setVisiting(false);
        orderedJobs.add(currentNode.jobValue);

        return false;
    }

    private static Graph buildGraph(List<Integer> jobs, List<Integer[]> deps) {
        var graph = new Graph(jobs);
        for (Integer[] dep : deps) {
            Integer node = dep[0];
            Integer dependency = dep[1];
            // add prerequisite
            graph.getNodeByJobId(dependency).addPrerequisite(graph.getNodeByJobId(node));
            // add dependency
            graph.getNodeByJobId(node).addDependency(graph.getNodeByJobId(dependency));
        }

        return graph;
    }

    private static class Graph {
        private final Map<Integer, JobNode> graph;
        private final List<JobNode> allNodes;

        public Graph(List<Integer> jobs) {
            graph = new HashMap<>();
            allNodes = new ArrayList<>();
            for (Integer job : jobs) {
                JobNode jobNode = new JobNode(job);
                graph.put(job, jobNode);
                allNodes.add(jobNode);
            }
        }

        public JobNode getNodeByJobId(Integer jobId) {
            if (graph.containsKey(jobId)) {
                return graph.get(jobId);
            }
            return new JobNode(jobId);
        }

        public List<JobNode> getAllNodes() {
            return allNodes;
        }
    }

    private static class JobNode {
        private final List<JobNode> prerequisites;
        private final List<JobNode> dependencies;
        private boolean visited;
        private boolean visiting;
        private final int jobValue;

        public JobNode(int jobValue) {
            prerequisites = new ArrayList<>();
            dependencies = new ArrayList<>();
            this.jobValue = jobValue;
            visited = false;
            visiting = false;
        }

        public void addPrerequisite(JobNode prerequisiteJob) {
            prerequisites.add(prerequisiteJob);
        }

        public void addDependency(JobNode dependency) {
            dependencies.add(dependency);
        }

        public void removePrerequisite(JobNode prerequisite) {
            prerequisites.remove(prerequisite);
        }

        public void removeDependency(JobNode dependency) {
            dependencies.remove(dependency);
        }

        public List<JobNode> getPrerequisites() {
            return prerequisites;
        }

        public List<JobNode> getDependencies() {
            return dependencies;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }

        public void setVisiting(boolean visiting) {
            this.visiting = visiting;
        }

        public boolean isVisited() {
            return visited;
        }

        public boolean isVisiting() {
            return visiting;
        }
    }
}
