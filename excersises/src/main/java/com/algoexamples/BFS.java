package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BFS {
    private static final Logger logger = LoggerFactory.getLogger(BFS.class);

    public static void main(String[] args) {
        //               A
        //           /   |   \
        //          B    C    D
        //         / \       / \
        //        E   F     G   H
        //           / \     \
        //          I  J      K

        Node a = new Node("A");

        Node b = new Node("B");
        Node e = new Node("E");
        b.addChild(e);
        Node f = new Node("F");
        Node i = new Node("I");
        Node j = new Node("J");
        f.addChild(i);
        f.addChild(j);
        b.addChild(f);

        Node c = new Node("C");

        Node d = new Node("D");
        Node g = new Node("G");
        Node h = new Node("H");
        Node k = new Node("K");
        g.addChild(k);
        d.addChild(g);
        d.addChild(h);

        a.addChild(b);
        a.addChild(c);
        a.addChild(d);

        List<String> list = new ArrayList<>();
        var bfs = a.breadthFirstSearch(list);

        for (String node : bfs) {
            logger.info(String.format("next node value is: %s", node));
        }
    }

    static class Node {
        String name;
        List<Node> children = new ArrayList<>();

        public Node(String name) {
            this.name = name;
        }

        public List<String> breadthFirstSearch(List<String> array) {
            Queue<Node> queue = new LinkedList<>();
            queue.add(this);
            while(!queue.isEmpty()) {
                Node current = queue.poll();
                array.add(current.name);
                queue.addAll(current.children);
            }

            return array;
        }

        public Node addChild(String name) {
            Node child = new Node(name);
            children.add(child);
            return this;
        }

        public void addChild(Node child) {
            children.add(child);
        }
    }
}
