package com.algoexamples;

public class ShiftedBinarySearch {
    public static void main(String[] args) {
//        int[] arr = {45, 61, 71, 72, 73, 0, 1, 21, 33, 37};
//        int[] arr = {5, 23, 111, 1};
        int[] arr = {72, 73, 0, 1, 21, 33, 37, 45, 61, 71};
//        int target = 33;
//        int target = 111;
        int target = 72;
        System.out.println(shiftedBinarySearch(arr, target));
    }

    /**
     * O(log(N)) time
     * O(1) space
     */
    public static int shiftedBinarySearch(int[] array, int target) {
        int left = 0;
        int right = array.length - 1;

        while(left < right) {
            int mid = (left + right) >>> 1;
            int leftVal = array[left];
            int rightVal = array[right];
            int midVal = array[mid];

            if (midVal == target) {
                return mid;
            }
            if (leftVal == target) {
                return left;
            }
            if (rightVal == target) {
                return right;
            }

            if (leftVal <= midVal) {
                // then left wing is properly sorted
                if (target > leftVal && target < midVal) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            } else {
                // left wing is messed up
                // somewhere in the middle there is shifted start of the sorting
                // right wing is then properly sorted
                if (target > midVal && target < rightVal) {
                    // search in right wing
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
        }

        return -1;
    }

    /**
     * O(log(N)) time
     * O(log(N)) space (callstack)
     */
    public static int shiftedBinarySearchRecursive(int[] array, int target) {
        return doSearch(array, target, 0, array.length - 1);
    }

    private static int doSearch(int[] array, int target, int left, int right) {
        if (left >= right) {
            return -1;
        }

        int mid = (left + right) >>> 1;
        int leftVal = array[left];
        int rightVal = array[right];
        int midVal = array[mid];
        if (midVal == target) {
            return mid;
        }
        if (leftVal == target) {
            return leftVal;
        }
        if (rightVal == target) {
            return rightVal;
        }

        if (leftVal <= midVal) {
            // then left wing is properly sorted
            if (target > leftVal && target < midVal) {
                return doSearch(array, target, left, mid - 1);
            }
            return doSearch(array, target, mid + 1, right);
        } else {
            // left wing is messed up
            // somewhere in the middle there is shifted start of the sorting
            // right wing is then properly sorted
            if (target > midVal && target < rightVal) {
                // search in right wing
                return doSearch(array, target, mid + 1, right);
            }
            return doSearch(array, target, left, mid - 1);
        }
    }
}
