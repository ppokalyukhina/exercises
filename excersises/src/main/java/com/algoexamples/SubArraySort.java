package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubArraySort {
    private static final Logger logger = LoggerFactory.getLogger(SubArraySort.class);

    public static void main(String[] args) {
//        int[] input = {1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19};
        int[] input = {1, 2, 4, 7, 10, 11, 7, 12, 7, 7, 16, 18, 19};
//        int[] input = {4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 11, 57};
        var result = subarraySort(input);

        logger.info(String.format("Sub array which needs to be sorted is: [%d, %d]", result[0], result[1]));
    }

    public static int[] subarraySort(int[] array) {
        int[] subarray = {-1, -1};
        int maxInSubArr = Integer.MIN_VALUE;
        int minInSubArr = Integer.MAX_VALUE;

        int leftIndx = 0;
        int rightIndx = leftIndx + 1;
        while(rightIndx < array.length) {
            int left = array[leftIndx];
            int right = array[rightIndx];
            int diff = right - left;

            if (diff >= 0) {
                leftIndx++;
                rightIndx++;
                continue;
            }

            maxInSubArr = Math.max(maxInSubArr, left);
            minInSubArr = Math.min(minInSubArr, right);

            leftIndx++;
            rightIndx++;
        }

        if (maxInSubArr == Integer.MIN_VALUE && minInSubArr == Integer.MAX_VALUE) {
            return subarray;
        }

        leftIndx = 0;
        while(array[leftIndx] <= minInSubArr) {
            leftIndx++;
        }

        rightIndx = array.length - 1;
        while(array[rightIndx] >= maxInSubArr) {
            rightIndx--;
        }

        return new int[]{leftIndx, rightIndx};
    }
}
