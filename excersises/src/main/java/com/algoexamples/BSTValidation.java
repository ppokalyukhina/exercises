package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

public class BSTValidation {
    private static final Logger logger = LoggerFactory.getLogger(BSTValidation.class);

    public static void main(String[] args) {
        var bst = new BST(10);
        var fifteen = new BST(15);
        var thirteen = new BST(13);
        var twentytwo = new BST(22);
        var forteen = new BST(14);

        thirteen.right = forteen;
        fifteen.left = thirteen;
        fifteen.right = twentytwo;

        var one = new BST(1);
        var two = new BST(2);
        two.left = one;

        var five = new BST(5);
        var five1 = new BST(5);
        five.left = two;
        five.right = five1;

        bst.left = five;
        bst.right = fifteen;

        logger.info(String.format("BST is valid: %s", validateBstRecursive(bst)));
    }

    public static boolean validateBstRecursive(BST tree) {
        return validate(tree, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private static boolean validate(BST current, int min, int max) {
        if (current.value < min || current.value >= max) {
            return false;
        }

        if (current.left != null && !validate(current.left, min, current.value)) {
            return false;
        }

        return current.right == null || validate(current.right, current.value, max);
    }

    public static boolean validateBst(BST tree) {
        Stack<BST> stack = new Stack<>();
        stack.push(tree);
        while(!stack.isEmpty()) {
            BST parent = stack.pop();
            if (parent.left != null) {
                if (parent.left.value >= parent.value) {
                    return false;
                }
                stack.push(parent.left);
            }

            if (parent.right != null) {
                if (parent.right.value < parent.value) {
                    return false;
                }
                stack.push(parent.right);
            }
        }

        return true;
    }

    static class BST {
        public int value;
        public BST left;
        public BST right;

        public BST(int value) {
            this.value = value;
        }
    }
}
