package com.algoexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxSumIncreasingSubsequence {
    public static void main(String[] args) {
//        int[] input = {10, 70, 20, 30, 50, 11, 30};
        int[] input = {8, 12, 2, 3, 15, 5, 7};
        var result = maxSumIncreasingSubsequence(input);

        for (List<Integer> integers : result) {
            StringBuilder string = new StringBuilder();
            for (Integer integer : integers) {
                string.append(integer).append(',');
            }
            System.out.println(string);
        }
    }

    public static List<List<Integer>> maxSumIncreasingSubsequence(int[] array) {
        int maxSum = array[0];
        int maxIndx = 0;

        int[] sums = new int[array.length];
        Arrays.fill(sums, Integer.MIN_VALUE);
        Integer[] sequences = new Integer[array.length];
        sums[0] = array[0];
        for (int i = 1; i < array.length; i++) {
            sums[i] = array[i];
            for (int j = 0; j < i; j++) {
                int tmpNewSum = sums[j] + array[i];
                if (array[j] < array[i] && tmpNewSum >= sums[i]) {
                    sequences[i] = j;
                    sums[i] = tmpNewSum;
                }
            }

            var currentSum = sums[i];
            if (currentSum > maxSum) {
                maxSum = currentSum;
                maxIndx = i;
            }
        }

        return List.of(
                List.of(maxSum),
                buildSequence(sequences, sums, maxIndx, array)
        );
    }

    private static List<Integer> buildSequence(Integer[] sequences, int[] sums, Integer maxSumIndx, int[] array) {
        List<Integer> sequence = new ArrayList<>();
        while(maxSumIndx != null) {
            sequence.add(0, array[maxSumIndx]);
            maxSumIndx = sequences[maxSumIndx];
        }

        return sequence;
    }
}
