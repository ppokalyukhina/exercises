package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveKthNode {
    private static final Logger logger = LoggerFactory.getLogger(RemoveKthNode.class);

    public static void main(String[] args) {
        var head = new LinkedList(0);
        var one = new LinkedList(1);
        head.next = one;
        var two = new LinkedList(2);
        one.next = two;
        var three = new LinkedList(3);
        two.next = three;
        var four = new LinkedList(4);
        three.next = four;
        var five = new LinkedList(5);
        four.next = five;
        var six = new LinkedList(6);
        five.next = six;
        var seven = new LinkedList(7);
        six.next = seven;
        var eight = new LinkedList(8);
        seven.next = eight;
        var nine = new LinkedList(9);
        eight.next = nine;

        int k = 10;
        removeKthNodeFromEnd1(head, k);
    }

    public static void removeKthNodeFromEnd1(LinkedList head, int k) {
        if (head == null) {
            return;
        }

        LinkedList current = head;
        LinkedList nodeBeforeDeleted = head;
        int lastPointer = 0;
        // calculate list length
        while(current != null) {
            if (lastPointer < k + 1) {
                lastPointer++;
            } else {
                nodeBeforeDeleted = nodeBeforeDeleted.next;
            }
            current = current.next;
        }

        // if it's a head
        if (lastPointer - k == 0) {
            head.value = head.next.value;
            head.next = head.next.next;
            return;
        }

        if (nodeBeforeDeleted.next != null) {
            nodeBeforeDeleted.next = nodeBeforeDeleted.next.next;
        }
    }

    /**
     * O(N*2) time -> 1. traverse to get the length, 2. traverse to find and delete the node
     * O(1) space
     */
    public static void removeKthNodeFromEnd(LinkedList head, int k) {
        if (head == null) {
            return;
        }

        int listLength = 1;
        LinkedList current = head;

        // calculate list length
        while(current != null) {
            listLength++;
            current = current.next;
        }

        // if position is head
        if (listLength - k == 1) {
            head.value = head.next.value;
            head.next = head.next.next;
            return;
        }

        int deletePosition = listLength - k;
        current = head;
        int currentPosition = 1;
        // stop iterating when found element
        // previous of the soon to be deleted one
        while(current != null && currentPosition < deletePosition - 1) {
            current = current.next;
            currentPosition++;
        }

        // in case delete position was never found
        if (current == null) {
            return;
        }

        // delete element
        if (current.next != null) {
            current.next = current.next.next;
        }
    }

    static class LinkedList {
        int value;
        LinkedList next = null;

        public LinkedList(int value) {
            this.value = value;
        }
    }
}
