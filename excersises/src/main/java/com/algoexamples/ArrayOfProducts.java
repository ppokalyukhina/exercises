package com.algoexamples;

public class ArrayOfProducts {
    public static void main(String[] args) {
        int[] arr = {5, 1, 4, 2};

        int[] res = twoLoopsSolution(arr);
        for (int re : res) {
            System.out.println(re);
        }
    }

    private static int[] twoLoopsSolution(int[] array) {
        int[] result = new int[array.length];

        // calculate left products
        result[0] = 1;
        for (int i = 1; i < array.length; i++) {
            result[i] = result[i - 1] * array[i - 1];
        }

        // calculate left && right products together
        int rightProduct = 1;
        for (int j = array.length-1; j >= 0; j--) {
            int currentElem = array[j];
            result[j] = rightProduct * result[j];
            rightProduct = rightProduct * currentElem;
        }

        return result;
    }

    public static int[] arrayOfProducts(int[] array) {
        if (array.length == 0) {
            return new int[]{};
        }

        int[] result = new int[array.length];
        int previousProduct = 1;

        for (int i = 0; i < array.length; i++) {
            int currentProduct = countRemainingProduct(array, i + 1) * previousProduct;
            result[i] = currentProduct;
            previousProduct = previousProduct * array[i];
        }

        return result;
    }

    private static int countRemainingProduct(int[] array, int fromIndex) {
        int product = 1;

        for (int i = fromIndex; i < array.length; i++) {
            product = product * array[i];
        }

        return product;
    }
}
