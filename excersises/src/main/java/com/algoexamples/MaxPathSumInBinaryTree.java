package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaxPathSumInBinaryTree {
    private static final Logger logger = LoggerFactory.getLogger(MaxPathSumInBinaryTree.class);

    public static void main(String[] args) {
        var root = new BinaryTree(-9);
        var two = new BinaryTree(2);
        two.left = new BinaryTree(4);
        two.right = new BinaryTree(5);

        var three = new BinaryTree(3);
        three.left = new BinaryTree(6);
        three.right = new BinaryTree(7);

        root.left = two;
        root.right = three;

        logger.info(String.format("Max path sume is: %d", maxPathSum(root)));
    }

    public static int maxPathSum(BinaryTree tree) {
        var paths = getMaxSubtree(tree, 0, 0, tree.value);

        var maxSubtree = Math.max(paths.maxLeftPath, paths.maxRightPath);
        return Math.max(paths.maxSubtree, Math.max(tree.value, maxSubtree));
    }

    private static Node getMaxSubtree(BinaryTree root, int leftPathLength, int rightPathLength, int maxSubtreeVal) {
        if (root == null) {
            return new Node(leftPathLength, rightPathLength, maxSubtreeVal);
        }

        var leftMaxSubtree = getMaxSubtree(root.left, leftPathLength, rightPathLength, maxSubtreeVal);
        var rightMaxSubtree = getMaxSubtree(root.right, leftPathLength, rightPathLength, maxSubtreeVal);

        var maxLeftPathLength = Math.max(leftMaxSubtree.maxLeftPath, leftMaxSubtree.maxRightPath) + root.value;
        var maxRightPathLength = Math.max(rightMaxSubtree.maxLeftPath, rightMaxSubtree.maxRightPath) + root.value;
        var currentSubtreeVal = maxLeftPathLength + maxRightPathLength - root.value;
        var maxPathSoFar = Math.max(maxLeftPathLength, maxRightPathLength);
        maxSubtreeVal = Math.max(Math.max(currentSubtreeVal, maxPathSoFar), Math.max(leftMaxSubtree.maxSubtree, rightMaxSubtree.maxSubtree));

        return new Node(maxLeftPathLength, maxRightPathLength, maxSubtreeVal);
    }

    static class Node {
        private final int maxLeftPath;
        private final int maxRightPath;
        private final int maxSubtree;

        public Node(int maxLeftPath, int maxRightPath, int maxSubtree) {
            this.maxLeftPath = maxLeftPath;
            this.maxRightPath = maxRightPath;
            this.maxSubtree = maxSubtree;
        }
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left;
        public BinaryTree right;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
