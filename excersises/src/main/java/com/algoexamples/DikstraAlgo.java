package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class DikstraAlgo {
    private static final Logger logger = LoggerFactory.getLogger(DikstraAlgo.class);

    public static void main(String[] args) {
        int start = 3;
//        int[][][] adjacency = {
//                {{1, 7}},
//                {{2, 6}, {3, 20}, {4, 3}},
//                {{3, 14}},
//                {{4, 2}},
//                {},
//                {}
//        };
        int[][][] adjacency = {
                {{2, 4}},
                {{0, 2}},
                {{1, 1}, {3, 2}},
                {{0, 3}}
        };

        // 7, 8, 9, 8, 10, 11, 10, 0
//        int[][][] adjacency = {
//                {{1, 1}, {3, 1}},
//                {{2, 1}},
//                {{6, 1}},
//                {{1, 3}, {2, 4}, {4, 2}, {5, 3}, {6, 5}},
//                {{5, 1}},
//                {{4, 1}},
//                {{5, 2}},
//                {{0, 7}}
//        };

//        int[][][] adjacency = {
//                {},
//                {},
//                {},
//                {}
//        };

//        int[][][] adjacency = {
//                {{1, 2}}, {{0, 1}}, {{3, 1}}, {{2, 2}}
//        };

        int[] paths = dijkstrasAlgorithmSolutionWithMinHeap(start, adjacency);
        for (int path : paths) {
            logger.info(String.format("%d", path));
        }
    }

    public static int[] dijkstrasAlgorithmSolutionWithMinHeap(int start, int[][][] edges) {
        int[] paths = new int[edges.length];
        Arrays.fill(paths, Integer.MAX_VALUE);
        Set<Integer> visited = new HashSet<>();
        paths[start] = 0;
        List<Integer[]> minHeap = buildMinHeap(start, 0);

        while(minHeap.size() != 0) {
            // find min current edge
            Integer[] nodeWithMinDistance = poll(minHeap);
            int nodeIndex = nodeWithMinDistance[0];
            int nodeMinDistance = nodeWithMinDistance[1];

            if (nodeMinDistance == Integer.MAX_VALUE) {
                break;
            }
            if (visited.contains(nodeIndex)) {
                continue;
            }

            visited.add(nodeIndex);
            int[][] toEdges = edges[nodeIndex];
            for (int[] toEdge : toEdges) {
                int toIndex = toEdge[0];
                int toNodeMinDistance = paths[toIndex];
                int potentialMinDistance = nodeMinDistance + toEdge[1];
                if (potentialMinDistance < toNodeMinDistance) {
                    paths[toIndex] = potentialMinDistance;
                }
                addToHeap(toIndex, paths[toIndex], minHeap);
            }
        }

        for (int i = 0; i < paths.length; i++) {
            if (paths[i] == Integer.MAX_VALUE) {
                paths[i] = -1;
            }
        }

        return paths;
    }

    private static List<Integer[]> buildMinHeap(int index, int minDistance) {
        List<Integer[]> heap = new ArrayList<>();
        heap.add(new Integer[]{index, minDistance});
        int lastElemIndex = heap.size() - 1;
        siftUp(lastElemIndex, heap);

        return heap;
    }

    private static Integer[] poll(List<Integer[]> heap) {
        int firstIndex = 0;
        int lastIndex = heap.size() - 1;
        // swap root and last elem
        swap(firstIndex, lastIndex, heap);
        // remove last elem
        Integer[] lastNodeInHeap = heap.remove(lastIndex);
        // sift root if needed
        siftDown(0, heap);

        return lastNodeInHeap;
    }

    private static void siftDown(int index, List<Integer[]> heap) {
        int leftChildIndex = index * 2 + 1;
        int currentIndex = index;
        while(leftChildIndex < heap.size()) {
            int currentValue = heap.get(currentIndex)[1];
            int indexToCompare = leftChildIndex;
            int rightChildIndex = currentIndex * 2 + 1;
            // compare min distance of left and right children
            if (rightChildIndex < heap.size() && heap.get(rightChildIndex)[1] < heap.get(leftChildIndex)[1]) {
                indexToCompare = rightChildIndex;
            }

            if (currentValue > heap.get(indexToCompare)[1]) {
                swap(currentIndex, indexToCompare, heap);
            }

            currentIndex = indexToCompare;
            leftChildIndex = currentIndex * 2 + 1;
        }
    }

    private static void addToHeap(int index, int minDistance, List<Integer[]> heap) {
        heap.add(new Integer[]{index, minDistance});
        siftUp(heap.size() - 1, heap);
    }

    private static void siftUp(int lastElemIndex, List<Integer[]> heap) {
        int parentIndex = (lastElemIndex - 1) / 2;
        while (parentIndex >= 0 && heap.get(parentIndex)[1] > heap.get(lastElemIndex)[1]) {
            swap(parentIndex, lastElemIndex, heap);
            lastElemIndex = parentIndex;
            parentIndex = (lastElemIndex - 1) / 2;
        }
    }

    private static void swap(int idx1, int idx2, List<Integer[]> heap) {
        Integer[] tmp = heap.get(idx1);
        heap.set(idx1, heap.get(idx2));
        heap.set(idx2, tmp);
    }

    public static int[] dijkstrasAlgorithmWithMinDistances(int start, int[][][] edges) {
        int[] paths = new int[edges.length];
        Arrays.fill(paths, Integer.MAX_VALUE);
        Set<Integer> visited = new HashSet<>();
        paths[start] = 0;
        while(visited.size() < edges.length) {
            // find min current edge
            int[] nodeWithMinDistance = findToNodeWithMinDistance(paths, visited);
            int nodeIndex = nodeWithMinDistance[0];
            int nodeMinDistance = nodeWithMinDistance[1];
            if (nodeMinDistance == Integer.MAX_VALUE) {
                break;
            }
            if (visited.contains(nodeIndex)) {
                continue;
            }

            visited.add(nodeIndex);
            int[][] toEdges = edges[nodeIndex];
            for (int[] toEdge : toEdges) {
                int toIndex = toEdge[0];
                int toNodeMinDistance = paths[toIndex];
                int potentialMinDistance = nodeMinDistance + toEdge[1];
                if (potentialMinDistance < toNodeMinDistance) {
                    paths[toIndex] = potentialMinDistance;
                }
            }
        }

        for (int i = 0; i < paths.length; i++) {
            if (paths[i] == Integer.MAX_VALUE) {
                paths[i] = -1;
            }
        }

        return paths;
    }

    private static int[] findToNodeWithMinDistance(int[] paths, Set<Integer> visited) {
        int minDistance = Integer.MAX_VALUE;
        int[] nodeWithMinDistance = new int[]{-1, minDistance};
        for (int i = 0; i < paths.length; i++) {
            int currentMinDistance = paths[i];
            if (visited.contains(i)) {
                continue;
            }

            if (currentMinDistance < minDistance) {
                nodeWithMinDistance[0] = i;
                nodeWithMinDistance[1] = currentMinDistance;
                minDistance = currentMinDistance;
            }
        }

        return nodeWithMinDistance;
    }

    public static int[] dijkstrasAlgorithmWithStackWithQueue(int start, int[][][] edges) {
        int[] paths = new int[edges.length];
        Arrays.fill(paths, -1);
        boolean[] visited = new boolean[edges.length];
        Arrays.fill(visited, false);
        paths[start] = 0;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(start);
        while(!queue.isEmpty()) {
            int nodeIndex = queue.poll();
            if (visited[nodeIndex]) {
                continue;
            }
            int[][] toEdges = edges[nodeIndex];
            for (int[] toEdge : toEdges) {
                int toIndex = toEdge[0];
                int distanceToIndex = toEdge[1];

                int existingPathLength = paths[toIndex];
                int potentialDistanceToIndex = paths[nodeIndex] + distanceToIndex;
                if (paths[toIndex] == -1) {
                    paths[toIndex] = potentialDistanceToIndex;
                } else {
                    paths[toIndex] = Math.min(existingPathLength, potentialDistanceToIndex);
                }
                queue.add(toIndex);
            }
            visited[nodeIndex] = true;
        }

        return paths;
    }
}
