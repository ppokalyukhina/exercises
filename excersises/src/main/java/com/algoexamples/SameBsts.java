package com.algoexamples;

import java.util.ArrayList;
import java.util.List;

public class SameBsts {
    public static void main(String[] args) {
        var arrOne = List.of(10, 15, 8, 12, 94, 81, 5, 2, 11);
        var arrTwo = List.of(10, 8, 5, 15, 2, 12, 11, 94, 81);

        System.out.println("Trees are the same: " + sameBstsNoSubarrays(arrOne, arrTwo));
    }

    /**
     * O(N*N) time
     * O(d) space -> d is depth of the tree (O(logN))
     */
    public static boolean sameBstsNoSubarrays(List<Integer> arrayOne, List<Integer> arrayTwo) {
        return recursive(arrayOne, arrayTwo, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private static boolean recursive(List<Integer> arrayOne, List<Integer> arrayTwo, int rootIndxOne, int rootIndxTwo, int minVal, int maxVal) {
        if (arrayOne.size() != arrayTwo.size()) {
            return false;
        }

        if (rootIndxOne == -1 || rootIndxTwo == -1) {
            return rootIndxOne == rootIndxTwo;
        }

        if (!arrayOne.get(rootIndxOne).equals(arrayTwo.get(rootIndxTwo))) {
            return false;
        }

        var leftIndxArrOne = findIndxOfMinVal(arrayOne, rootIndxOne, minVal);
        var leftIndxArrTwo = findIndxOfMinVal(arrayTwo, rootIndxTwo, minVal);
        var rightIndxArrOne = findIndxOfMaxVal(arrayOne, rootIndxOne, maxVal);
        var rightIndxArrTwo = findIndxOfMaxVal(arrayTwo, rootIndxTwo ,maxVal);

        int currentVal = arrayOne.get(rootIndxOne);
        boolean leftAreSame = recursive(arrayOne, arrayTwo, leftIndxArrOne, leftIndxArrTwo, minVal, currentVal);
        boolean rightAreSame = recursive(arrayOne, arrayTwo, rightIndxArrOne, rightIndxArrTwo, currentVal, maxVal);

        return leftAreSame && rightAreSame;
    }

    private static int findIndxOfMinVal(List<Integer> array, int currentIndx, int minVal) {
        int indx = currentIndx + 1;

        for (int i = indx; i < array.size(); i++) {
            if (array.get(i) < array.get(currentIndx) && array.get(i) >= minVal) {
                return i;
            }
        }

        return -1;
    }

    private static int findIndxOfMaxVal(List<Integer> array, int currentIndx, int maxVal) {
        int indx = currentIndx + 1;

        for (int i = indx; i < array.size(); i++) {
            if (array.get(i) >= array.get(currentIndx) && array.get(i) < maxVal) {
                return i;
            }
        }

        return -1;
    }

    public static boolean sameBsts(List<Integer> arrayOne, List<Integer> arrayTwo) {
        if (arrayOne.size() != arrayTwo.size()) {
            return false;
        }

        if (arrayOne.isEmpty()) {
            return true;
        }

        if (!arrayOne.get(0).equals(arrayTwo.get(0))) {
            return false;
        }

        // construct left min vals from both arrs
        List<Integer> smallerArrOne = buildSmaller(arrayOne);
        List<Integer> smallerArrTwo = buildSmaller(arrayTwo);

        // construct right max vals from both arrs
        List<Integer> biggerArrOne = buildBigger(arrayOne);
        List<Integer> biggerArrTwo = buildBigger(arrayTwo);

        return sameBsts(smallerArrOne, smallerArrTwo) && sameBsts(biggerArrOne, biggerArrTwo);
    }

    private static List<Integer> buildSmaller(List<Integer> array) {
        List<Integer> smaller = new ArrayList<>();
        for (int i = 1; i < array.size(); i++) {
            int current = array.get(i);
            int root = array.get(0);

            if (current < root) {
                smaller.add(current);
            }
        }

        return smaller;
    }

    private static List<Integer> buildBigger(List<Integer> array) {
        List<Integer> biggerArrTwo = new ArrayList<>();
        for (int i = 1; i < array.size(); i++) {
            int current = array.get(i);
            int root = array.get(0);

            if (current >= root) {
                biggerArrTwo.add(current);
            }
        }

        return biggerArrTwo;
    }
}
