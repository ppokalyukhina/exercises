package com.algoexamples;

import java.util.Arrays;

public class MinWaitingTime {
    public static void main(String[] args) {
        int[] input = {3, 2, 1, 2, 6};
        System.out.println(minimumWaitingTime(input));
    }
    public static int minimumWaitingTime(int[] queries) {
        Arrays.sort(queries);
        int minWaitingTime = 0;
        for (int i = 0; i < queries.length; i++) {
            int duration = queries[i];
            int queriesInQueue = queries.length - (i + 1);
            minWaitingTime += duration * queriesInQueue;
        }

        return minWaitingTime;
    }
}
