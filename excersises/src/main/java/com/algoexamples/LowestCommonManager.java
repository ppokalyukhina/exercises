package com.algoexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LowestCommonManager {
    public static void main(String[] args) {
        OrgChart manager = new OrgChart('A');
        OrgChart reportOne = new OrgChart('E');
        OrgChart reportTwo = new OrgChart('I');

        OrgChart b = new OrgChart('B');
        OrgChart c = new OrgChart('C');
        OrgChart d = new OrgChart('D');
        OrgChart H = new OrgChart('H');
        OrgChart C = new OrgChart('C');
        OrgChart F = new OrgChart('F');
        OrgChart G = new OrgChart('G');

        d.addDirectReports(new OrgChart[]{H, reportTwo});
        b.addDirectReports(new OrgChart[]{d, reportOne});
        c.addDirectReports(new OrgChart[]{F, G});
        manager.addDirectReports(new OrgChart[]{b, c});

        var report = getLowestCommonManager(manager, reportOne, reportTwo);
        System.out.println(report.name);
    }

    /**
     * O(N) time
     * O(d) space
     */
    public static OrgChart getLowestCommonManager(OrgChart topManager, OrgChart reportOne, OrgChart reportTwo) {
        return find(topManager, reportOne, reportTwo).getLowestManager();
    }

    private static OrgInfo find(OrgChart currentManager, OrgChart reportOne, OrgChart reportTwo) {
        int numberOfReports = 0;
        var reports = currentManager.directReports;
        for (OrgChart report : reports) {
            var orgInfo = find(report, reportOne, reportTwo);
            if (orgInfo.getLowestManager() != null) {
                return orgInfo;
            }
            numberOfReports += orgInfo.getNumberOfImportantReports();
        }
        if (currentManager == reportOne || currentManager == reportTwo) {
            numberOfReports++;
        }
        if (numberOfReports == 2) {
            return new OrgInfo(currentManager, numberOfReports);
        }

        return new OrgInfo(null, numberOfReports);
    }

    static class OrgChart {
        public char name;
        public List<OrgChart> directReports;

        OrgChart(char name) {
            this.name = name;
            this.directReports = new ArrayList<>();
        }

        public void addDirectReports(OrgChart[] directReports) {
            this.directReports.addAll(Arrays.asList(directReports));
        }
    }

    private static class OrgInfo {
        private final OrgChart lowestManager;
        private final int numberOfImportantReports;

        public OrgInfo(OrgChart lowestManager, int numberOfImportantReports) {
            this.lowestManager = lowestManager;
            this.numberOfImportantReports = numberOfImportantReports;
        }

        public int getNumberOfImportantReports() {
            return numberOfImportantReports;
        }

        public OrgChart getLowestManager() {
            return lowestManager;
        }
    }
}
