package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinMaxStack {
    private static final Logger logger = LoggerFactory.getLogger(MinMaxStack.class);

    public static void main(String[] args) {
        AlgoStack minMax = new AlgoStack();
        minMax.push(5);
        logger.info("Min value is: " + minMax.getMin()); // 5
        logger.info("Max value is: " + minMax.getMax()); // 5
        logger.info("Last value is: " + minMax.peek()); // 5

        minMax.push(7);
        logger.info("Min value is: " + minMax.getMin()); // 5
        logger.info("Max value is: " + minMax.getMax()); // 7
        logger.info("Last value is: " + minMax.peek()); // 7

        minMax.push(2);
        logger.info("Min value is: " + minMax.getMin()); // 2
        logger.info("Max value is: " + minMax.getMax()); // 7
        logger.info("Last value is: " + minMax.peek()); // 2

        minMax.pop();
        minMax.pop();
        logger.info("Min value is: " + minMax.getMin()); // 5
        logger.info("Max value is: " + minMax.getMax()); // 5
        logger.info("Last value is: " + minMax.peek()); // 5
    }
}
