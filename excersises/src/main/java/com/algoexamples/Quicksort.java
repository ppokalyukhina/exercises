package com.algoexamples;

public class Quicksort {
    public static void main(String[] args) {
        int[] arr = {8, 5, 2, 9, 5, 6, 3};
        var sorted = quickSort(arr);
        for (int i : sorted) {
            System.out.println(i);
        }
    }

    public static int[] quickSort(int[] array) {
        doSort(array, 0, array.length - 1);
        return array;
    }

    private static void doSort(int[] array, int start, int end) {
        if (start >= end) {
            return;
        }

        int left = start + 1;
        int right = end;
        while(left <= right) {
            if (array[left] > array[start] && array[right] < array[start]) {
                swap(array, left, right);
            }

            if (array[left] <= array[start]) {
                left++;
            }

            if (array[right] >= array[start]) {
                right--;
            }
        }
        swap(array, start, right);
        boolean leftSubarraySmaller = right - start - 1 < end - (right + 1);
        if (leftSubarraySmaller) {
            // sort left sub array first
            doSort(array, start, right - 1);
            // then sort right sub array
            doSort(array, right + 1, end);
        } else {
            // sort right sub array first
            doSort(array, right + 1, end);
            // then sort left sub array first
            doSort(array, start, right - 1);
        }
    }

    private static void swap(int[] array, int indx1, int idx2) {
        int tmpRight = array[indx1];
        array[indx1] = array[idx2];
        array[idx2] = tmpRight;
    }
}
