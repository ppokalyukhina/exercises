package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleCycleCheck {
    private static final Logger logger = LoggerFactory.getLogger(SingleCycleCheck.class);

    public static void main(String[] args) {
//        int[] input = {2, 3, 1, -4, -4, 2};
        int[] input = {1, -1, 1, -1};
        logger.info(String.format("Has a single cycle: %s", hasSingleCycle1(input)));
    }

    private static boolean hasSingleCycle1(int[] array) {
        int currentIndx = 0;
        int visits = 0;
        while(visits < array.length) {
            if (visits > 0 && currentIndx == 0) {
                return false;
            }

            int jumps = array[currentIndx];
            int next = (currentIndx + jumps) % array.length;
            if (next >= 0) {
                currentIndx = next;
            } else {
                currentIndx = next + array.length;
            }

            visits++;
        }

        return currentIndx == 0;
    }

    private static boolean hasSingleCycle(int[] array) {
        int currentIndex = 0;
        int counter = 0;
        while(counter != array.length) {
            int current = array[currentIndex];
            int next = current + currentIndex;

            if (next >= array.length) {
                next = next % array.length;
            }

            if (next < 0) {
                next = Math.abs(next) % array.length;
                next = (array.length - next) % array.length;
            }

            counter++;
            if (next == 0 && counter < array.length) {
                return false;
            }
            currentIndex = next;
        }

        return currentIndex == 0;
    }
}
