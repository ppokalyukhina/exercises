package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class RiverSizes {
    private static final Logger logger = LoggerFactory.getLogger(RiverSizes.class);

    public static void main(String[] args) {
        int[][] input = {
                {1, 0, 0, 1, 0},
                {1, 0, 1, 0, 0},
                {0, 1, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0}
        };

        var result = riverSizes(input);

        for (Integer integer : result) {
            logger.info(String.format("Next one is: %d", integer));
        }
    }

    private static List<Integer> riverSizes(int[][] matrix) {
        boolean[][] visited = new boolean[matrix.length][matrix[0].length];
        List<Integer> sizes = new ArrayList<>();

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                int current = matrix[row][col];
                if (current == 0 || visited[row][col]) {
                    continue;
                }

                visit(matrix, row, col, visited, sizes);
            }
        }

        return sizes;
    }

    private static void visit(int[][] matrix, int row, int col, boolean[][] visited, List<Integer> sizes) {
        Stack<int[]> toTraverse = new Stack<>();
        toTraverse.push(new int[]{row, col});
        int size = 0;

        while(!toTraverse.isEmpty()) {
            size++;
            int[] indexes = toTraverse.pop();
            int currentRow = indexes[0];
            int currentCol = indexes[1];
            visited[currentRow][currentCol] = true;

            // check right neighbour
            if (currentCol < matrix[0].length - 1 && !visited[currentRow][currentCol + 1] && matrix[currentRow][currentCol + 1] == 1) {
                visited[currentRow][currentCol + 1] = true;
                toTraverse.push(new int[]{currentRow, currentCol + 1});
            }

            // check left neighbour
            if (currentCol > 0 && !visited[currentRow][currentCol - 1] && matrix[currentRow][currentCol - 1] == 1) {
                visited[currentRow][currentCol - 1] = true;
                toTraverse.push(new int[]{currentRow, currentCol - 1});
            }

            // check bottom neighbour
            if (currentRow < matrix.length - 1 && !visited[currentRow + 1][currentCol] && matrix[currentRow + 1][currentCol] == 1) {
                visited[currentRow + 1][currentCol] = true;
                toTraverse.push(new int[]{currentRow + 1, currentCol});
            }

            // check top neighbour
            if (currentRow > 0 && !visited[currentRow - 1][currentCol] && matrix[currentRow - 1][currentCol] == 1) {
                visited[currentRow - 1][currentCol] = true;
                toTraverse.push(new int[]{currentRow - 1, currentCol});
            }
        }

        sizes.add(size);
    }
}
