package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Find 3 largest numbers
 */
public class LargestNumbers {
    private static final Logger logger = LoggerFactory.getLogger(LargestNumbers.class);

    public static void main(String[] args) {
        int[] arr = {141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7};

        var result = findThreeLargestNumbers(arr);
        for (int i : result) {
            logger.info(String.format("Next number is: %d", i));
        }
    }

    public static int[] findThreeLargestNumbers(int[] array) {
        int[] result = {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};

        if (array.length < 3) {
            return result;
        }

        for (int i : array) {
            int firstElem = result[0];
            if (i < firstElem) {
                continue;
            }
            checkAndMoveElements(result, i);
        }

        return result;
    }

    private static void checkAndMoveElements(int[] result, int current) {
        for (int i = result.length - 1; i >= 0; i--) {
            int elem = result[i];
            if (current > elem) {
                moveElements(result, i);
                result[i] = current;
                break;
            }
        }
    }

    private static void moveElements(int[] result, int index) {
        for (int i = 0; i <= index - 1; i++) {
            result[i] = result[i + 1];
        }
    }
}
