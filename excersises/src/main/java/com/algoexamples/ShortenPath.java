package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class ShortenPath {
    private static final Logger logger = LoggerFactory.getLogger(ShortenPath.class);

    public static void main(String[] args) {
//        String input = "../../foo/bar/baz"; // should return ../../foo/bar/baz
//        String input = "/../../foo/bar/baz"; // should return /foo/bar/baz
        String input = "/../../../this////one/./../../is/../../going/../../to/be/./././../../../just/a/forward/slash/../../../../../..";

        logger.info(shortenPath(input));
    }


    public static String shortenPath(String path) {
        String[] split = path.split("/");
        if (split.length == 0) {
            return path;
        }
        List<String> filtered = Arrays.stream(split).filter(s -> !s.isEmpty() && !s.equals(".")).collect(Collectors.toList());
        boolean isRelative = split[0].equals("");
        Stack<String> stack = new Stack<>();
        if (isRelative) {
            stack.push("");
        }

        for (String string : filtered) {
            if (string.equals("..")) {
                if (stack.isEmpty() || stack.peek().equals(string)) {
                    stack.push(string);
                } else if (!stack.peek().equals("")){
                    stack.pop();
                }
            } else {
                stack.push(string);
            }
        }

        if (stack.size() == 1 && stack.peek().equals("")) {
            return "/";
        }
        return String.join("/", stack);
    }

    public static String shortenPath1(String path) {
        String[] split = path.split("/");
        if (split.length == 0) {
            return path;
        }

        boolean isRelative = split[0].equals("");
        Stack<String> stack = new Stack<>();
        if (isRelative) {
            stack.push("");
        }
        // traverse through split path
        // based on requirements either add string to stack
        // or pop last element
        for (String directory : split) {
            if (directory.equals(".") || directory.isEmpty()) {
                continue;
            }

            if (directory.equals("..")) {
                if (isRelative) {
                    if (stack.isEmpty()) {
                        continue;
                    } else {
                        if (stack.peek().equals(directory)) {
                            stack.push(directory);
                        } else {
                            stack.pop();
                        }
                    }
                } else {
                    if (!stack.isEmpty() && !stack.peek().equals(directory)) {
                        stack.pop();
                    } else {
                        stack.push(directory);
                    }
                }
                continue;
            }
            stack.push(directory);
        }

        if (stack.size() == 1 && stack.peek().equals("")) {
            return "/";
        }

        return String.join("/", stack);
    }
}
