package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaterArea {
    private static final Logger logger = LoggerFactory.getLogger(WaterArea.class);

    public static void main(String[] args) {
//        int[] input = {0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3};
        int[] input = {0, 100, 0, 0, 10, 1, 1, 10, 1, 0, 1, 1, 0, 0};
        logger.info(String.format("Water area is: %d", waterArea(input)));
    }

    /**
     * O(N*2) time complexity
     * O(N*2) space complexity
     */
    public static int waterArea(int[] heights) {
        int[] tallestHeightLeft = new int[heights.length];
        int[] tallestHeightsRight = new int[heights.length];
        int waterArea = 0;

        for (int i = 1; i < tallestHeightLeft.length; i++) {
            tallestHeightLeft[i] = Math.max(tallestHeightLeft[i - 1], heights[i - 1]);
        }

        for (int j = heights.length - 2; j >= 0; j--) {
            tallestHeightsRight[j] = Math.max(tallestHeightsRight[j + 1], heights[j + 1]);
            var currentMaxRight = tallestHeightsRight[j];
            int currentMaxLeft = tallestHeightLeft[j];
            int min = Math.min(currentMaxLeft, currentMaxRight);
            if (heights[j] < min) {
                waterArea += min - heights[j];
            }
        }

        return waterArea;
    }
}
