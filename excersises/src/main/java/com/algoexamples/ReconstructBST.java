package com.algoexamples;

import java.util.List;

public class ReconstructBST {
    private static int rootIndex = 0;
    public static void main(String[] args) {
        List<Integer> preordered = List.of(100);
        BST root = reconstructBst1(preordered);
        display(root);
    }

    private static void display(BST root) {
        if (root == null) {
            return;
        }
        System.out.println(root.value);
        display(root.left);
        display(root.right);
    }

    // O(n) time complexity -> no need to search for right index on every call
    // O(n) space -> store all nodes
    // + O(h) space -> recursion
    public static BST reconstructBst1(List<Integer> preOrderTraversalValues) {
        // boundaries for values of children
        int from = Integer.MIN_VALUE;
        int to = Integer.MAX_VALUE;

        return construct(preOrderTraversalValues, from, to);
    }

    private static BST construct(List<Integer> preOrderTraversalValues, int from, int to) {
        if (rootIndex >= preOrderTraversalValues.size()) {
            return null;
        }
        Integer rootVal = preOrderTraversalValues.get(rootIndex);
        if (!isInBound(rootVal, from, to)) {
            return null;
        }
        BST root = new BST(rootVal);
        rootIndex++;
        BST left = construct(preOrderTraversalValues, from, rootVal);
        BST right = construct(preOrderTraversalValues, rootVal, to);
        root.left = left;
        root.right = right;

        return root;
    }

    private static boolean isInBound(int val, int min, int max) {
        return val >= min && val < max;
    }

    // O(N*N) time
    // O(N) + O(h) space
    // h -> for recursive stack
    public static BST reconstructBst(List<Integer> preOrderTraversalValues) {
        if (preOrderTraversalValues.size() == 0) {
            return null;
        }

        BST root = new BST(preOrderTraversalValues.get(0));
        int rightIndex = preOrderTraversalValues.size();
        for (int i = 1; i < preOrderTraversalValues.size(); i++) {
            if (preOrderTraversalValues.get(i) >= root.value) {
                rightIndex = i;
                break;
            }
        }
        root.left = reconstructBst(preOrderTraversalValues.subList(1, rightIndex));
        root.right = reconstructBst(preOrderTraversalValues.subList(rightIndex, preOrderTraversalValues.size()));

        return root;
    }

    private static class BST {
        public int value;
        public BST left = null;
        public BST right = null;

        public BST(int value) {
            this.value = value;
        }
    }
}
