package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DiskStacking {
    private static final Logger logger = LoggerFactory.getLogger(DiskStacking.class);

    public static void main(String[] args) {
//        List<Integer[]> input = List.of(
//                new Integer[]{2, 1, 2},
//                new Integer[]{3, 2, 3},
//                new Integer[]{2, 2, 8},
//                new Integer[]{2, 3, 4},
//                new Integer[]{1, 3, 1},
//                new Integer[]{4, 4, 5}
//        );

        List<Integer[]> input = List.of(
                new Integer[]{3, 3, 4},
                new Integer[]{2, 1, 2},
                new Integer[]{3, 2, 3},
                new Integer[]{2, 2, 8},
                new Integer[]{2, 3, 4},
                new Integer[]{5, 5, 6},
                new Integer[]{1, 2, 1},
                new Integer[]{4, 4, 5},
                new Integer[]{1, 1, 4},
                new Integer[]{2, 2, 3}
        );

//        List<Integer[]> input = List.of(
//                new Integer[]{2, 1, 2},
//                new Integer[]{3, 2, 3},
//                new Integer[]{2, 2, 8},
//                new Integer[]{2, 3, 4},
//                new Integer[]{1, 2, 1},
//                new Integer[]{4, 4, 5},
//                new Integer[]{1, 1, 4}
//        );
        var res = diskStacking(input);
        for (Integer[] re : res) {
            StringBuilder builder = new StringBuilder();
            builder.append('[');
            for (Integer integer : re) {
                builder.append(integer).append(',');
            }
            builder.append(']');

            logger.info(builder.toString());
        }
    }

    /**
     * Time:
     * - sorting O(logN)
     * - iterating and storing heights O(N*N)
     *
     * Space:
     * O(N) max heights + O(N) previous items
     */
    public static List<Integer[]> diskStacking(List<Integer[]> disks) {
        // sort disks by height
        List<Integer[]> sorted = new ArrayList<>(disks);
        sorted.sort(Comparator.comparing((Integer[] disk) -> disk[2]));

        // initiate arr with max heights
        int[] maxHeights = new int[disks.size()];
        // keep track of the previous item
        Integer[] previousItems = new Integer[disks.size()];
        int maxHeightIndex = 0;

        for (int i = 0; i < sorted.size(); i++) {
            Integer[] currentDisk = sorted.get(i);
            // first store current height
            Integer currentHeight = currentDisk[2];
            maxHeights[i] = currentHeight;
            for (int j = 0; j < i; j++) {
                Integer[] otherDisk = sorted.get(j);
                if (satisfiesConditions(otherDisk, currentDisk)) {
                    if (currentHeight + maxHeights[j] > maxHeights[i]) {
                        maxHeights[i] = currentHeight + maxHeights[j];
                        previousItems[i] = j;
                    }
                }
            }

            if (maxHeights[i] > maxHeights[maxHeightIndex]) {
                maxHeightIndex = i;
            }
        }

       return buildSequence(previousItems, sorted, maxHeightIndex);
    }

    private static boolean satisfiesConditions(Integer[] otherDisk, Integer[] currentDisk) {
        var disk1Width = otherDisk[0];
        var disk1Depth = otherDisk[1];
        var disk1Height = otherDisk[2];

        var disk2Width = currentDisk[0];
        var disk2Depth = currentDisk[1];
        var disk2Height = currentDisk[2];

        return disk1Width < disk2Width && disk1Depth < disk2Depth && disk1Height < disk2Height;
    }

    private static List<Integer[]> buildSequence(Integer[] previousItems, List<Integer[]> disks, int maxHeightIndex) {
        List<Integer[]> sequence = new ArrayList<>();
        int currentIdx = maxHeightIndex;
        while(currentIdx >= 0) {
            sequence.add(0, disks.get(currentIdx));
            if (previousItems[currentIdx] == null) {
                break;
            }
            currentIdx = previousItems[currentIdx];
        }

        return sequence;
    }
}
