package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ValidIpAddress {
    private static final Logger logger = LoggerFactory.getLogger(ValidIpAddress.class);

    public static void main(String[] args) {
        String input = "1921680";
        var result = validIPAddresses(input);

        for (String ip : result) {
            logger.info(ip);
        }
    }

    private static ArrayList<String> validIPAddresses(String string) {
        ArrayList<String> ips = new ArrayList<>();
        for (int firstDotPos = 1; firstDotPos < string.length() - 2; firstDotPos++) {
            String first = string.substring(0, firstDotPos);
            if (!isValid(first)) {
                continue;
            }
            for (int secondDotPos = firstDotPos + 1; secondDotPos < string.length() - 1; secondDotPos++) {
                String second = string.substring(firstDotPos, secondDotPos);
                if (!isValid(second)) {
                    continue;
                }

                for (int thirdDotpos = secondDotPos + 1; thirdDotpos < string.length(); thirdDotpos++) {
                    String third = string.substring(secondDotPos, thirdDotpos);
                    String fourth = string.substring(thirdDotpos);

                    if (isValid(third) && isValid(fourth)) {
                        String ip = first + "." + second + "." + third + "." + fourth;
                        ips.add(ip);
                    }
                }
            }
        }


        return ips;
    }

    private static boolean isValid(String substr) {
        int n = Integer.parseInt(substr);
        if (n < 0 || n > 255) {
            return false;
        }

        return substr.length() <= 1 || substr.charAt(0) != '0';
    }
}
