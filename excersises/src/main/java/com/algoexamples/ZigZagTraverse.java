package com.algoexamples;

import java.util.ArrayList;
import java.util.List;

public class ZigZagTraverse {
    public static void main(String[] args) {
        var a1 = List.of(1, 3, 4, 10);
//        var a1 = List.of(1, 3, 4, 7, 8);
        var a2 = List.of(2, 5, 9, 11);
//        var a2 = List.of(2, 5, 6, 9, 10);

        var a3 = List.of(6, 8, 12, 15);
        var a4 = List.of(7, 13, 14, 16);
//        List<List<Integer>> arr = List.of(a1, a2, a3, a4);
//        List<List<Integer>> arr = List.of(List.of(1));
        List<List<Integer>> arr = List.of(a1, a2, a3, a4);
        var result = zigzagTraverse1(arr);
        for (Integer integer : result) {
            System.out.println(integer);
        }
    }

    /**
     * O(N) space complexity
     * O(N*M) time complexity (N = length, M = height)
     */
    public static List<Integer> zigzagTraverse1(List<List<Integer>> array) {
        List<Integer> resultArray = new ArrayList<>();
        int row = 0;
        int col = 0;
        int matrixLength = array.get(0).size();
        int height = array.size();

        String direction = "down";
        while(isInBounds(row, col, height, matrixLength)) {
            resultArray.add(array.get(row).get(col));

            if (direction.equals("down")) {
                // if left bottom corner was hit
                // if left border was hit
                if (col == 0 && row == height - 1) {
                    col++;
                    direction = "up";
                    continue;
                }

                // if left border was hit
                if (col == 0) {
                    row++;
                    direction = "up";
                }

                // else if bottom was hit
                else if (row == array.size() - 1) {
                    col++;
                    direction = "up";
                }

                else {
                    row++;
                    col--;
                }
            }

            // traverse diagonal up
            else {
                // if top right corner was hit
                // if top border was hit
                if (row == 0 && col == matrixLength - 1) {
                    row++;
                    direction = "down";
                    continue;
                }

                // if top border was hit
                if (row == 0) {
                    col++;
                    direction = "down";
                }
                // else if right border was hit
                else if (col == array.get(row).size() - 1) {
                    row++;
                    direction = "down";
                }
                else {
                    col++;
                    row--;
                }
            }
        }

        return resultArray;
    }

    private static boolean isInBounds(int row, int col, int height, int length) {
        return row >= 0 && row < height && col >= 0 && col < length;
    }
}
