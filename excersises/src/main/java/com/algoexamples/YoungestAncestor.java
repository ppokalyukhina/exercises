package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class YoungestAncestor {
    private static final Logger logger = LoggerFactory.getLogger(YoungestAncestor.class);

    public static void main(String[] args) {
        //        A
        //      /   \
        //     B     C
        //   /  \   /  \
        //  D    E F    G
        // / \
        //H   I

        AncestralTree root = new AncestralTree('A');
        AncestralTree b = new AncestralTree('B');
        AncestralTree c = new AncestralTree('C');
        AncestralTree d = new AncestralTree('D');
        AncestralTree e = new AncestralTree('E');
        AncestralTree f = new AncestralTree('F');
        AncestralTree g = new AncestralTree('G');
        AncestralTree h = new AncestralTree('H');
        AncestralTree i = new AncestralTree('I');
        AncestralTree[] ansToD = {h, i};
        d.addAsAncestor(ansToD);
        AncestralTree[] ansToB = {d, e};
        b.addAsAncestor(ansToB);
        AncestralTree[] ansToC = {f, g};
        c.addAsAncestor(ansToC);
        AncestralTree[] ansToRoot = {b, c};
        root.addAsAncestor(ansToRoot);

        var ancTree = getYoungestCommonAncestor(root, e, i);
        logger.info(String.format("Youngest ancessor is: %s", ancTree.name));
    }

    // O(d) time -> d is depth of the lower ancestor
    // O(1) space
    public static AncestralTree getYoungestCommonAncestor(
            AncestralTree topAncestor,
            AncestralTree descendantOne,
            AncestralTree descendantTwo
    ) {
        int oneDepth = 0;
        int twoDepth = 0;

        var current = descendantOne;
        while(current != null && current != topAncestor) {
            oneDepth++;
            current = current.ancestor;
        }

        current = descendantTwo;
        while(current != null && current != topAncestor) {
            twoDepth++;
            current = current.ancestor;
        }


        var currentOne = descendantOne;
        var currentTwo = descendantTwo;
        // adjust depth of one of the ancestors if needed
        if (oneDepth > twoDepth) {
            while(oneDepth > twoDepth) {
                currentOne = currentOne.ancestor;
                oneDepth--;
            }
        } else if (oneDepth < twoDepth) {
            while(oneDepth < twoDepth) {
                currentTwo = currentTwo.ancestor;
                twoDepth--;
            }
        }

        // oneDepth = twoDepth
        while(currentOne != topAncestor && currentTwo != topAncestor) {
            if (currentOne == currentTwo) {
                return currentOne;
            }

            currentOne = currentOne.ancestor;
            currentTwo = currentTwo.ancestor;
        }

        return topAncestor;
    }

    // O(d) time -> d is the depth of the tree
    // O(n) space -> n is the number of one ancestor's nodes
    public static AncestralTree getYoungestCommonAncestorSet(
        AncestralTree topAncestor,
        AncestralTree descendantOne,
        AncestralTree descendantTwo
    ) {
        Set<AncestralTree> ancessorsFirstDesc = new HashSet<>();
        AncestralTree current = descendantOne;
        while(current != null && current != topAncestor) {
            ancessorsFirstDesc.add(current);
            current = current.ancestor;
        }

        current = descendantTwo;
        while(current != null && current != topAncestor) {
            if (ancessorsFirstDesc.contains(current)) {
                return current;
            }

            ancessorsFirstDesc.add(current);
            current = current.ancestor;
        }

        return topAncestor;
    }

    static class AncestralTree {
        public char name;
        public AncestralTree ancestor;

        AncestralTree(char name) {
            this.name = name;
            this.ancestor = null;
        }

        // This method is for testing only.
        void addAsAncestor(AncestralTree[] descendants) {
            for (AncestralTree descendant : descendants) {
                descendant.ancestor = this;
            }
        }
    }
}
