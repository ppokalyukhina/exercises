package com.algoexamples;

public class ValidStartingCity {
    public static void main(String[] args) {
        int[] distances = {5, 25, 15, 10, 15};
        int[] fuel = {1, 2, 1, 0, 3};
        int mpg = 10;
        System.out.println(validStartingCity(distances, fuel, mpg));
    }

    // O(n), n -> cities
    private static int validStartingCity(int[] distances, int[] fuel, int mpg) {
        if (distances.length == 0 || distances.length != fuel.length) {
            return -1;
        }

        int startingCity = 0;
        int leftOverFuel = 0;
        int minLeftOverFuel = Integer.MAX_VALUE;
        for(int city = 0; city < distances.length; city++) {
            int availableDistanceToReach = fuel[city] * mpg + leftOverFuel;
            int distanceToGo = distances[city];
            int currentLeftOverFuel = availableDistanceToReach - distanceToGo;
            if (currentLeftOverFuel < minLeftOverFuel) {
                minLeftOverFuel = currentLeftOverFuel;
                startingCity = (city + 1) % distances.length;
            }

            leftOverFuel = currentLeftOverFuel;
        }

        return startingCity;
    }
}
