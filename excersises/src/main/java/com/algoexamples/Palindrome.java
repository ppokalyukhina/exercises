package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Palindrome {
    private static final Logger logger = LoggerFactory.getLogger(Palindrome.class);

    public static void main(String[] args) {
        String palindrome = "abcd";
        logger.info(String.format("Longest palindromic substring is: %s", longestPalindrome(palindrome)));
    }

    private static String longestPalindromicSubstring(String str) {
        if (str.length() <= 1 || isPalindrome(str)) {
            return str;
        }

        int longest = Integer.MIN_VALUE;
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            for (int j = i; j < str.length(); j++) {
                String substring = str.substring(i, j+1);
                if (isPalindrome(substring)) {
                    if (substring.length() > longest) {
                        result = substring;
                        longest = substring.length();
                    }
                }
            }
        }

        return result;
    }

    private static String longestPalindrome(String str) {
        if (str.length() == 0) {
            return str;
        }

        String longest = str.substring(0, 1);
        for (int i = 1; i < str.length(); i++) {
            String substr = str.substring(i, i + 1);
            String longestOdd = longestPalindromeAroundSubstr(str, i - 1, i + 1, substr);
            String longestEven = longestPalindromeAroundSubstr(str, i - 1, i, "");
            if (longestOdd.length() > longestEven.length() && longestOdd.length() > longest.length()) {
                longest = longestOdd;
            } else if (longestEven.length() > longestOdd.length() && longestEven.length() > longest.length()) {
                longest = longestEven;
            }
        }

        return longest;
    }

    private static String longestPalindromeAroundSubstr(String str, int leftPointer, int rightPointer, String substr) {
        while(leftPointer >=0 && rightPointer < str.length() && str.charAt(leftPointer) == str.charAt(rightPointer)) {
            substr = str.substring(leftPointer, rightPointer + 1);
            leftPointer--;
            rightPointer++;
        }

        return substr;
    }

    public static boolean isPalindrome(String str) {
        int leftPointer = 0;
        int rightPointer = str.length() - 1;
        while(leftPointer < rightPointer) {
            char left = str.charAt(leftPointer);
            char right = str.charAt(rightPointer);

            if (left != right) {
                return false;
            }

            leftPointer++;
            rightPointer--;
        }

        return true;
    }
}
