package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LongestCommonSubsequence {
    private static final Logger logger = LoggerFactory.getLogger(LongestCommonSubsequence.class);

    public static void main(String[] args) {
        var s1 = "ZXVVYZW";
        var s2 = "XKYKZPW";
        var result = longestCommonSubsequenceNoLocalArrs(s1, s2);
        StringBuilder builder = new StringBuilder();
        for (Character character : result) {
            builder.append(character);
        }

        logger.info(builder.toString());
    }

    public static List<Character> longestCommonSubsequenceNoLocalArrs(String str1, String str2) {
        char[] str1Char = str1.toCharArray();
        char[] str2Char = str2.toCharArray();

        int[][] subsequenceLengths = new int[str2Char.length + 1][str1Char.length + 1];

        for (int i = 1; i <= str2Char.length; i++) {
            for (int j = 1; j <= str1Char.length; j++) {
                char s2Char = str2Char[i - 1];
                char s1Char = str1Char[j - 1];

                if (s2Char == s1Char) {
                    // take left top length + 1
                    subsequenceLengths[i][j] = subsequenceLengths[i - 1][j - 1] + 1;
                } else {
                    // if characters are different
                    // take max length from either top or left position
                    subsequenceLengths[i][j] = Math.max(subsequenceLengths[i][j - 1], subsequenceLengths[i - 1][j]);
                }
            }
        }

        // build sequence
        List<Character> sequence = new ArrayList<>();
        int col = str1Char.length;
        int row = str2Char.length;
        while(row != 0 && col != 0) {
            int currentLength = subsequenceLengths[row][col];
            int topLength = subsequenceLengths[row - 1][col];
            int leftLength = subsequenceLengths[row][col - 1];

            if (topLength == currentLength) {
                row--;
            } else if (leftLength == currentLength) {
                col--;
            } else {
                sequence.add(0, str2Char[row - 1]);
                row--;
                col--;
            }
        }

        return sequence;
    }

    /**
     * O(N*N) space complexity
     * O(N*N) time complexity
     */
    public static List<Character> longestCommonSubsequence(String str1, String str2) {
        // rows => str2, cols => str1
        List<List<List<Character>>> matrix = new ArrayList<>(str2.length());
        char[] str1Char = str1.toCharArray();
        char[] str2Char = str2.toCharArray();

        // fill columns with empty arrays (reserve first col for empty arrays)
        for (int i = 0; i < str2Char.length + 1; i++) {
            // fill rows with empty arrays (reserve first row for empty arrays)
            matrix.add(new ArrayList<>());
            for (int j = 0; j < str1Char.length + 1; j++) {
                matrix.get(i).add(new ArrayList<>());
            }
        }

        for (int i = 1; i <= str2Char.length; i++) {
            for (int j = 1; j <= str1Char.length; j++) {
                char s2Char = str2Char[i - 1];
                char s1Char = str1Char[j - 1];

                // char Arr top
                List<Character> top = matrix.get(i - 1).get(j);
                // char Arr left
                List<Character> left = matrix.get(i).get(j - 1);

                // in case they are equal, add check for top left + current length
                if (s2Char == s1Char) {
                    List<Character> topLeft = matrix.get(i - 1).get(j - 1);
                    if (topLeft.size() >= top.size() && topLeft.size() >= left.size()) {
                        var tmpTopLeft = new ArrayList<>(topLeft);
                        tmpTopLeft.add(s2Char);
                        matrix.get(i).set(j, tmpTopLeft);
                    }
                    else if (top.size() >= left.size()) {
                        var tmpTop = new ArrayList<>(top);
                        matrix.get(i).set(j, tmpTop);
                    } else {
                        var tmpLeft = new ArrayList<>(left);
                        matrix.get(i).set(j, tmpLeft);
                    }
                } else {
                    // current is not equal
                    // if top >= left, pick it, else left
                    if (top.size() >= left.size()) {
                        var tmpTop = new ArrayList<>(top);
                        matrix.get(i).set(j, tmpTop);
                    } else {
                        var tmpLeft = new ArrayList<>(left);
                        matrix.get(i).set(j, tmpLeft);
                    }
                }
            }
        }

        return matrix.get(str2Char.length).get(str1Char.length);
    }
}
