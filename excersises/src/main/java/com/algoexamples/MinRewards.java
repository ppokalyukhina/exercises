package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.IntStream;

public class MinRewards {
    private static final Logger logger = LoggerFactory.getLogger(MinRewards.class);

    public static void main(String[] args) {
        int[] input = {8, 4, 2, 1, 3, 6, 7, 9, 5};
        var rewards = minRewards(input);
        logger.info(String.format("Min reward is: %s", rewards));
    }

    /**
     * O(N*2) time complexity
     * O(N) space complexity
     */
    public static int minRewards1(int[] scores) {
        int[] degradedScores = new int[scores.length];
        Arrays.fill(degradedScores, 1);

        for (int i = 0; i < scores.length - 1; i++) {
            int current = scores[i];
            int next = scores[i + 1];
            if (next > current) {
                degradedScores[i + 1] = degradedScores[i] + 1;
            }
        }

        int sum = 0;
        for (int j = scores.length - 1; j > 0; j--) {
            int current = scores[j];
            int previous = scores[j - 1];

            if (current < previous) {
                degradedScores[j - 1] = Math.max(degradedScores[j - 1], degradedScores[j] + 1);
            }
            sum += degradedScores[j];
        }

        sum += degradedScores[0];
        return sum;
    }

    /**
     * O(N*2) time complexity
     * O(N) space complexity
     */
    public static int minRewards(int[] scores) {
        int[] degradedScores = new int[scores.length];
        Arrays.fill(degradedScores, 1);

        for (int i = 1; i < scores.length; i++) {
            int next = scores[i];
            int current = scores[i - 1];
            int j = i - 1;
            if (next > current) {
                degradedScores[i] = degradedScores[i - 1] + 1;
            } else {
                while(j >= 0 && scores[j + 1] < scores[j]) {
                    degradedScores[j] = Math.max(degradedScores[j], degradedScores[j + 1] + 1);
                    j--;
                }
            }
        }

        return IntStream.of(degradedScores).sum();
    }
}
