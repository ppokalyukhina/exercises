package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShortestPalindrome {
    private static final Logger logger = LoggerFactory.getLogger(ShortestPalindrome.class);

    public static void main(String[] args) {
        String palindrome = "aaaaa";
        logger.info(String.format("Longest palindromic substring is: %s", shortestPalindrome(palindrome)));
    }

    private static String shortestPalindrome(String s) {
        if (s.length() <= 1) {
            return s;
        }

        int lastPalindromePointer = 1;
        for (int i = 1; i < s.length(); i++) {
            // check even
            int oddPalindrome = isPalendrome(s, i - 1, i + 1);
            if (oddPalindrome != -1) {
                lastPalindromePointer = oddPalindrome;
            }
            // check odd
            int evenPalindrome = isPalendrome(s, i - 1, i);
            if (evenPalindrome != -1) {
                lastPalindromePointer = evenPalindrome;
            }
        }

        StringBuilder leftSubstr = new StringBuilder();
        int last = s.length() - 1;
        while(last >= lastPalindromePointer) {
            leftSubstr.append(s.charAt(last));
            last--;
        }

        return leftSubstr.append(s).toString();
    }

    private static int isPalendrome(String str, int left, int right) {
        while (left >= 0 && right < str.length()) {
            if (str.charAt(left) != str.charAt(right)) {
                return -1;
            }

            left--;
            right++;
        }

        // less characters left on right side of the substr
        if (left < 0) {
            return right;
        }

        return -1;
    }
}
