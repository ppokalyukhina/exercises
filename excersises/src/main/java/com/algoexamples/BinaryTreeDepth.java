package com.algoexamples;

public class BinaryTreeDepth {
    public static void main(String[] args) {
        var root = new MaxPathSumInBinaryTree.BinaryTree(1);
        var two = new MaxPathSumInBinaryTree.BinaryTree(2);
        two.left = new MaxPathSumInBinaryTree.BinaryTree(4);
        two.right = new MaxPathSumInBinaryTree.BinaryTree(5);
        var three = new MaxPathSumInBinaryTree.BinaryTree(3);
        root.left = two;
        root.right = three;

        System.out.println(getHeight(root));
    }

    private static int getHeight(MaxPathSumInBinaryTree.BinaryTree root) {
        if (root == null) {
            return 0;
        }

        int left = getHeight(root.left);
        int right = getHeight(root.right);

        if (left > right) {
            return left + 1;
        } else {
            return right + 1;
        }
    }
}
