package com.algoexamples;

import java.util.*;

public class PhoneNumberMnemonics {
    public static void main(String[] args) {
        String phoneNumber = "1905";
        var res = phoneNumberMnemonics(phoneNumber);
        for (String re : res) {
            System.out.println(re);
        }
    }

    public static ArrayList<String> phoneNumberMnemonics(String phoneNumber) {
        ArrayList<String> combinations = new ArrayList<>();
        Map<Character, List<Character>> digitsToLetter = new HashMap<>();
        digitsToLetter.put('1', Collections.emptyList());
        digitsToLetter.put('2', List.of('a', 'b', 'c'));
        digitsToLetter.put('3', List.of('d', 'e', 'f'));
        digitsToLetter.put('4', List.of('g', 'h', 'i'));
        digitsToLetter.put('5', List.of('j', 'k', 'l'));
        digitsToLetter.put('6', List.of('m', 'n', 'o'));
        digitsToLetter.put('7', List.of('p', 'r', 'q', 's'));
        digitsToLetter.put('8', List.of('t', 'u', 'v'));
        digitsToLetter.put('9', List.of('w', 'x', 'y', 'z'));
        digitsToLetter.put('0', Collections.emptyList());

        buildStrings(0, phoneNumber.toCharArray(), new StringBuilder(), combinations, digitsToLetter);

        return combinations;
    }

    private static void buildStrings(int currentIndex, char[] givenNumber, StringBuilder currentString, ArrayList<String> combinations, Map<Character, List<Character>> digitsToLetter) {
        if (currentIndex >= givenNumber.length) {
            combinations.add(currentString.toString());
            return;
        }

        char c = givenNumber[currentIndex];
        List<Character> allChars = digitsToLetter.get(c);
        currentIndex++;
        if (allChars.isEmpty()) {
            currentString.append(c);
            buildStrings(currentIndex, givenNumber, currentString, combinations, digitsToLetter);
            return;
        }

        for (Character allChar : allChars) {
            StringBuilder current = new StringBuilder();
            current.append(currentString);
            current.append(allChar);
            buildStrings(currentIndex, givenNumber, current, combinations, digitsToLetter);
        }
    }
}
