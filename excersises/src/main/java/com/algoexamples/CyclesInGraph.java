package com.algoexamples;

public class CyclesInGraph {
    public static void main(String[] args) {
//        int[][] edges = {
//                {1, 3},
//                {2, 3, 4},
//                {0},
//                {},
//                {2, 5},
//                {}
//        };
//        int[][] edges = {
//                {0},
//                {1}
//        };
        int[][] edges = {
                {}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0}
        };
        System.out.println(cycleInGraph(edges));
    }

    public static boolean cycleInGraph(int[][] edges) {
        boolean[] visited = new boolean[edges.length];
        boolean[] currentlyInStack = new boolean[edges.length];
        for (int i = 0; i < edges.length; i++) {
            if (visited[i]) {
                continue;
            }
            if (detectCycle(edges, currentlyInStack, visited, i)) {
                return true;
            }
        }

        return false;
    }

    private static boolean detectCycle(int[][] edges, boolean[] inStack, boolean[] visited, int currentNodeId) {
        visited[currentNodeId] = true;
        inStack[currentNodeId] = true;

        int[] neighbours = edges[currentNodeId];
        var detectCycle = false;
        for (int neighbour : neighbours) {
            if (!visited[neighbour]) {
                detectCycle = detectCycle(edges, inStack, visited, neighbour);
            }
            if (detectCycle) {
                return true;
            }

            if (inStack[neighbour]) {
                return true;
            }
        }
        inStack[currentNodeId] = false;
        return false;
    }
}
