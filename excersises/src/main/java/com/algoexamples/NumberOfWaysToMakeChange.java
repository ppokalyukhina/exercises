package com.algoexamples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberOfWaysToMakeChange {
    private static final Logger logger = LoggerFactory.getLogger(NumberOfWaysToMakeChange.class);

    public static void main(String[] args) {
        int[] denoms = {1, 5, 10, 25};
        int n = 10;
        logger.info(String.format("Found %d ways to change", numberOfWaysToMakeChange(n, denoms)));
    }

    private static int numberOfWaysToMakeChange(int n, int[] denoms) {
        if (n == 0) {
            // do not use any coins
            return 1;
        }

        // amount of ways for change from 0 to n
        int[] ways = new int[n + 1];
        ways[0] = 1;
        for (int currentCoin : denoms) {
            if (currentCoin > n) {
                continue;
            }
            for (int waysIndx = currentCoin; waysIndx < ways.length; waysIndx++) {
                ways[waysIndx] += ways[waysIndx - currentCoin];
            }
        }

        return ways[n];
    }
}
