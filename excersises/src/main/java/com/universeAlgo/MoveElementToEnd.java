package com.universeAlgo;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an array of integers and value which has to be moved
 * All values equal to the given should be moved to the end of the array
 */
public class MoveElementToEnd {
//    private static final Logger logger = LoggerFactory.getLogger(MoveElementToEnd.class);

    public static void main(String[] args) {
        List<Integer> inputArr = new ArrayList<>();
        inputArr.add(2);
        inputArr.add(1);
        inputArr.add(2);
        inputArr.add(2);
        inputArr.add(2);
        inputArr.add(3);
        inputArr.add(4);
        inputArr.add(2);

        var result = pointersSolution(inputArr, 2);
        for (Integer integer : result) {
//            logger.info("Next element in array is {}", integer);
        }
    }

    /**
     * O(N) time complexity
     * O(1) space complexity
     */
    private static List<Integer> pointersSolution(List<Integer> array, int toMove) {
        int left = 0;
        int right = array.size() - 1;

        while(left < right) {
            if (array.get(right) == toMove) {
                right--;
                continue;
            }

            if (array.get(left) != toMove) {
                left++;
                continue;
            }

            // swap
            int tmp = array.get(left);
            array.set(left, array.get(right));
            array.set(right, tmp);

            left++;
            right--;
        }

        return array;
    }

    /**
     * O(N) time complexity
     * O(N) space complexity
     */
    public static List<Integer> moveElementToEnd(List<Integer> array, int toMove) {
        List<Integer> result = new ArrayList<>(array.size());

        for (Integer integer : array) {
            if (integer != toMove) {
                result.add(integer);
            }
        }

        int lastIndexFirstArr = result.size();
        while (lastIndexFirstArr < array.size()) {
            result.add(toMove);
            lastIndexFirstArr++;
        }

        return result;
    }
}
