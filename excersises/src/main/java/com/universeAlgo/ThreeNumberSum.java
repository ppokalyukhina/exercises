package com.universeAlgo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeNumberSum {
    private static final Logger logger = LoggerFactory.getLogger(ThreeNumberSum.class);

    public static void main(String[] args) {
        int[] array = {12, 3, 1, 2, -6, 5, -8, 6};
        int sum = 0;

        List<Integer[]> result = sortSolution(array, sum);
        for (Integer[] ints : result) {
            logger.info("[{}, {}, {}] give in total requested sum {}", ints[0], ints[1], ints[2], sum);
        }
    }

    /**
     * O(N*N*N) time complexity
     * O(N) space complexity
     */
    private static List<int[]> naiveSolution(int[] array, int targetSum) {
        List<int[]> result = new ArrayList<>();
        for (int i = 0; i < array.length - 2; i++) {
            for (int j = i + 1; j < array.length - 1; j++) {
                for (int t = j + 1; t < array.length; t++) {
                    int sum = array[i] + array[j] + array[t];
                    if (sum == targetSum) {
                        int[] sumArr = new int[]{array[i], array[j], array[t]};
                        result.add(sumArr);
                    }
                }
            }
        }

        return result;
    }

    /**
     * O(N*N) time complexity (+ N*logN sorting)
     * O(N) space complexity
     */
    private static List<Integer[]> sortSolution(int[] array, int targetSum) {
        List<Integer[]> result = new ArrayList<>();
        Arrays.sort(array);
        for (int i = 0; i < array.length - 2; i++) {
            int left = i + 1;
            int right = array.length - 1;

            while(left < right) {
                int sum = array[i] + array[left] + array[right];
                if (sum == targetSum) {
                    Integer[] sumArr = {array[i], array[left], array[right]};
                    result.add(sumArr);
                    left++;
                    right--;
                    continue;
                }

                if (sum < targetSum) {
                    left++;
                    continue;
                }

                right--;
            }
        }

        return result;
    }
}
