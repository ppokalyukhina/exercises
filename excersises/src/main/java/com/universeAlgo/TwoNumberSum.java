package com.universeAlgo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TwoNumberSum {
    private static final Logger logger = LoggerFactory.getLogger(TwoNumberSum.class);

    public static void main(String[] args) {
        int[] array = {3, 5, -4, 8, 11, 1, -1, 6};
        int sum = 10;

        int[] result = twoSum(array, sum);
        logger.info("Elements {} and {} give in total requested sum {}", result[0], result[1], sum);
    }

    public static int[] twoSum(int[] nums, int target) {
        // number -> index
        Map<Integer, Integer> arr = new HashMap<>();

        for(int i = 0; i < nums.length; i++) {
            int potentialSum = target - nums[i];

            if(arr.containsKey(potentialSum)) {
                return new int[]{i, arr.get(potentialSum)};
            }

            arr.put(nums[i], i);
        }

        return new int[2];
    }

    // super naive solution
    // O(N * N) time complexity
    // O(1) space complexity
    private static int[] naiveSolution(int[] array, int targetSum) {
        int[] result = new int[2];
        for(int i = 0; i < array.length - 1; i++) {
            for(int j = i + 1; j < array.length; j++) {
                int sum = array[i] + array[j];
                if(sum == targetSum) {
                    result[0] = array[i];
                    result[1] = array[j];

                    return result;
                }
            }
        }

        return new int[]{};
    }

    // O(N) time complexity
    // O(N) space complexity
    private static int[] setSolution(int[] array, int targetSum) {
        Set<Integer> nums = new HashSet<>();
        for (int i : array) {
            int potentialMatch = targetSum - i;
            if (nums.contains(potentialMatch)) {
                return new int[]{potentialMatch, i};
            } else {
                nums.add(i);
            }
        }

        return new int[0];
    }

    // O(NlogN) + O(N) time complexity
    // O(1) space complexity
    private static int[] sortingSolution(int[] array, int targetSum) {
        Arrays.sort(array);
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int currentSum = array[left] + array[right];
            if (currentSum == targetSum) {
                return new int[]{array[left], array[right]};
            }

            if (currentSum < targetSum) {
                left++;
                continue;
            }

            right--;
        }

        return new int[0];
    }
}
