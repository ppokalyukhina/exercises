package com.universeAlgo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Given 2 arrays of integers
 * Find the smallest diff (closest to 0) between 2 elements in these arrays
 */
public class SmallestDifference {
    private static final Logger logger = LoggerFactory.getLogger(SmallestDifference.class);

    public static void main(String[] args) {
        int[] arr1 = {-1, 5, 10, 20, 28, 3};
        int[] arr2 = {26, 134, 135, 15, 17};

        int[] smallest = sortingSolution(arr1, arr2);
        logger.info("Smallest diff from 0 give elements {} and {}", smallest[0], smallest[1]);
    }

    /**
     * O(N*N) time complexity
     * O(N) space complexity
     */
    private static int[] naiveSolution(int[] arrayOne, int[] arrayTwo) {
        int smallestDiff = Integer.MAX_VALUE;
        int[] res = new int[2];

        for (int int1 : arrayOne) {
            for (int int2 : arrayTwo) {
                int diff = Math.abs(int1 - int2);

                if (diff < smallestDiff) {
                    smallestDiff = diff;
                    res[0] = int1;
                    res[1] = int2;
                }
            }
        }

        return res;
    }

    /**
     * O(n*logN) + O(m*logM) time complexity. N is first arr length and M is second arr length.
     * O(1) space complexity
     */
    private static int[] sortingSolution(int[] arrayOne, int[] arrayTwo) {
        Arrays.sort(arrayOne);
        Arrays.sort(arrayTwo);

        int[] result = new int[2];
        int smallestDiff = Integer.MAX_VALUE;
        int currentDiff = Integer.MAX_VALUE;

        int firstArrPointer = 0;
        int secondArrPointer = 0;

        while(firstArrPointer < arrayOne.length && secondArrPointer < arrayTwo.length) {
            int firstArrNum = arrayOne[firstArrPointer];
            int secondArrNum = arrayTwo[secondArrPointer];

            if (firstArrNum < secondArrNum) {
                currentDiff = secondArrNum - firstArrNum;
                firstArrPointer++;
            }

            if (firstArrNum > secondArrNum) {
                currentDiff = firstArrNum - secondArrNum;
                secondArrPointer++;
            }

            if (firstArrNum == secondArrNum) {
                return new int[]{firstArrNum, secondArrNum};
            }

            if (smallestDiff > currentDiff) {
                smallestDiff = currentDiff;
                result[0] = firstArrNum;
                result[1] = secondArrNum;
            }
        }

        return result;
    }
}
