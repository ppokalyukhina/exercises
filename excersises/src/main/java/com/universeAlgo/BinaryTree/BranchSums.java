package com.universeAlgo.BinaryTree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BranchSums {
    private static final Logger logger = LoggerFactory.getLogger(BranchSums.class);

    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree(1);
        binaryTree.left = new BinaryTree(2);
        binaryTree.left.left = new BinaryTree(4);
        binaryTree.left.left.left = new BinaryTree(8);
        binaryTree.left.left.right = new BinaryTree(9);

        binaryTree.left.right = new BinaryTree(5);
        binaryTree.left.right.left = new BinaryTree(10);

        binaryTree.right = new BinaryTree(3);
        binaryTree.right.left = new BinaryTree(6);
        binaryTree.right.right = new BinaryTree(7);

        var result = branchSums(binaryTree);
        for (Integer branchSum : result) {
            logger.info(String.format("Branch sum is: %d", branchSum));
        }
    }

    public static List<Integer> branchSums(BinaryTree root) {
        ArrayList<Integer> branchSums = new ArrayList<>();
        recursive(root, branchSums, 0);

        return branchSums;
    }

    private static void recursive(BinaryTree root, ArrayList<Integer> branchSums, int sum) {
        BinaryTree left = root.left;
        BinaryTree right = root.right;
        sum += root.value;

        if (left == null && right == null) {
            branchSums.add(sum);
            return;
        }

        if (left != null) {
            recursive(left, branchSums, sum);
        }

        if (right != null) {
            recursive(right, branchSums, sum);
        }
    }
}
