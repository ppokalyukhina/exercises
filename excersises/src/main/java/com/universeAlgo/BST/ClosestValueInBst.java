package com.universeAlgo.BST;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClosestValueInBst {
    private static final Logger logger = LoggerFactory.getLogger(ClosestValueInBst.class);

    public static void main(String[] args) {
        BST root = new BST(10);
        root.left = new BST(5);
        root.right = new BST(15);
        root.right.left = new BST(13);
        root.right.left.right = new BST(14);
        root.right.right = new BST(22);
        root.left.left = new BST(2);
        root.left.right = new BST(5);
        root.left.left.left = new BST(1);

        int target = 12;
        logger.info(String.format("Closest value found is: %d", findClosestValueInBst(root, target)));
    }

    public static int findClosestValueInBst(BST tree, int target) {
        int min = Integer.MAX_VALUE;
        int nodeValue = tree.value;

        return notRecursive(tree, target);
    }

    private static int recursive(BST current, int target, int min, int nodeValue) {
        if (current == null) {
            return nodeValue;
        }

        if (target == current.value) {
            return current.value;
        }

        if (Math.abs(target - current.value) < min) {
            min = Math.abs(target - current.value);
            nodeValue = current.value;
        }

        if (target < current.value) {
            current = current.left;
        } else {
            current = current.right;
        }

        return recursive(current, target, min, nodeValue);
    }

    private static int notRecursive(BST current, int target) {
        int min = Integer.MAX_VALUE;
        int nodeValue = current.value;

        while(current != null) {
            if (target == current.value) {
                return target;
            }

            if (Math.abs(target - current.value) < min) {
                min = Math.abs(target - current.value);
                nodeValue = current.value;
            }

            if (target < current.value) {
                current = current.left;
                continue;
            }

            current = current.right;
        }

        return nodeValue;
    }
}
