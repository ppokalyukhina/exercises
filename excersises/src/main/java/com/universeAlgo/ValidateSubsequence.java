package com.universeAlgo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ValidateSubsequence {
    private static final Logger logger = LoggerFactory.getLogger(ValidateSubsequence.class);

    public static void main(String[] args) {
        List<Integer> array = List.of(5, 1, 22, 25, 6, -1, 8, 10);
        List<Integer> subsequence = List.of(1, 6, -1, 10);

        logger.info(String.format("Result is: %s", isValidSubsequence(array, subsequence)));
    }

    public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {
        int arrPointer = 0;
        int sequencePointer = 0;

        while(arrPointer < array.size() && sequencePointer < sequence.size()) {
            if (array.get(arrPointer).equals(sequence.get(sequencePointer))) {
                arrPointer++;
                sequencePointer++;
            } else {
                arrPointer++;
            }

        }

        return sequencePointer == sequence.size();
    }
}
