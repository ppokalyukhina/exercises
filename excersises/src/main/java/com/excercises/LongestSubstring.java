package com.excercises;

import java.util.*;

public class LongestSubstring {
    private static int lengthOfLongestSubstring(String s) {
        int length = s.length();
        int leftIndx = 0;
        int rightIndx = 0;
        // character -> letter in String s.
        // Integer -> Index of that letter
        Map<Character, Integer> map = new HashMap<>();

        int result = 0;
        while(leftIndx < length && rightIndx < length) {
            char currentCur = s.charAt(rightIndx);
            if (map.containsKey(currentCur)) {
                leftIndx = Math.max(map.get(currentCur), leftIndx);
            }

            map.put(currentCur, rightIndx + 1); // rightIndx + 1 because we already stored left index in as current index
            result = Math.max(result, rightIndx - leftIndx + 1);
            rightIndx++;
        }
        return result;
    }

    public static void main(String[] args) {
        String s = "abcdde";

        System.out.println(lengthOfLongestSubstring(s));
    }
}
