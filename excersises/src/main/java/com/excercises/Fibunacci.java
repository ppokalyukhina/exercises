package com.excercises;

public class Fibunacci {
    public static void main(String[] args) {
        System.out.println(iterative(6));
    }

    private static int iterative(int n) {
        if (n < 2) {
            return n;
        }

        int first = 0;
        int second = 1;

        int sum = 1;
        for (int i = 2; i < n; i++) {
            first = second;
            second = sum;
            sum = (first + second);
        }

        return sum;
    }

    private static int recursive(int n) {
        if (n < 2) {
            return n;
        }

        return recursive(n - 1) + recursive(n - 2);
    }

    public static int getNthFib(int n) {
        if(n == 0) {
            return 0;
        }

        if(n == 1) {
            return 1;
        }

        int sum = 1;
        int previousOne = sum;
        for(int i = 3; i < n; i++) {
            int oldPrevious = sum;
            sum += previousOne;
            previousOne = oldPrevious;
        }

        return sum;
    }
}

