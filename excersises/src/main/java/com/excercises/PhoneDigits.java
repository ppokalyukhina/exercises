package com.excercises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneDigits {
    private static List<String> combinations(String numbers) {
        List<String> result = new ArrayList<>();

        if(numbers.length() == 0) {
            return result;
        }

        Map<Integer, String> givenMap = new HashMap<>();
        givenMap.put(2, "abc");
        givenMap.put(3, "def");
        givenMap.put(4, "ghi");
        givenMap.put(5, "jkl");
        givenMap.put(6, "mno");
        givenMap.put(7, "pqrs");
        givenMap.put(8, "tuv");
        givenMap.put(9, "wxyz");

        int index = 0;
        char[] numbersCharArr = numbers.toCharArray();
        String firstArr = givenMap.get(Character.getNumericValue(numbersCharArr[index]));

        index++;
        while(index < numbers.length()) {
           String nextArr = givenMap.get(Character.getNumericValue(numbersCharArr[index]));

           for (int i = 0; i < firstArr.length(); i++) {
               for (int j = 0; j < nextArr.length(); j++) {
                   result.add(firstArr.substring(i, i+1) + nextArr.substring(j, j+1));
               }
           }

           index++;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(combinations("234"));
    }
}
