package com.excercises;

public class RemoveNthNodeFromEndOfTheList {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode two = new ListNode(2);
        ListNode three = new ListNode(3);
        ListNode four = new ListNode(4);
        ListNode five = new ListNode(5);
//        head.next = two;
//        two.next = three;
//        three.next = four;
//        four.next = five;

        var result = removeNthFromEnd(head, 1);
        while(result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }

    private static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode current = head;
        ListNode previousToRemoved = null;
        ListNode nodeToRemove = null;
        int count = 0;
        while(current != null) {
            // store reference to current_node - n
            if (count >= n - 1) {
                if (nodeToRemove == null) {
                    nodeToRemove = head;
                } else {
                    previousToRemoved = nodeToRemove;
                    nodeToRemove = nodeToRemove.next;
                }
            }
            current = current.next;
            count++;
        }

        if (nodeToRemove == null) {
            return head;
        }

        if (previousToRemoved == null) {
            return nodeToRemove.next;
        }

        // detach next node from nodeToRemove
        // detach current list from nodeToRemove
        previousToRemoved.next = nodeToRemove.next;

        return head;
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
