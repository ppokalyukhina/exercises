package com.excercises;

public class NextPermutation {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 4};
        nextPermutation(input);
        for (int i : input) {
            System.out.println(i);
        }
    }

    // O(N*2) time
    // O(1) space
    public static void nextPermutation(int[] nums) {
        int right = nums.length - 2;

        // find first decreasing element
        while(right >= 0 && nums[right + 1] <= nums[right]) {
            right--;
        }

        int left = right;
        if(right >= 0) {
            right = nums.length - 1;
            while(left <= right && nums[right] <= nums[left]) {
                right--;
            }
            swap(nums, left, right);
        }

        // reverse all after left right
        left = left + 1;
        right = nums.length - 1;
        while(left <= right) {
            swap(nums, left, right);
            left++;
            right--;
        }
    }

    private static void swap(int[] nums, int left, int right) {
        int tmp = nums[left];
        nums[left] = nums[right];
        nums[right] = tmp;
    }
}
