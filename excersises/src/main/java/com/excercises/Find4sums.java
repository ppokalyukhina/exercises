package com.excercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Find4sums {
    public static List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();

        Arrays.sort(nums);

        for(int i = 0; i < nums.length - 3; i++) {
            for(int j = i+1; j < nums.length - 2; j++) {
                int lowerInx = j+1;
                int upperIndex = nums.length - 1;

                while(lowerInx < upperIndex) {
                    int subSum = nums[i] + nums[j] + nums[lowerInx] + nums[upperIndex];
                    if (subSum == target) {
                        List<Integer> sums = new ArrayList<>();
                        sums.add(nums[i]);
                        sums.add(nums[j]);
                        sums.add(nums[lowerInx]);
                        sums.add(nums[upperIndex]);

                        if (!result.contains(sums)) {
                            result.add(sums);
                        }
                        lowerInx++;
                        upperIndex--;
                    }

                    if (subSum < target) {
                        lowerInx++;
                    }

                    else if (subSum > target) {
                        upperIndex--;
                    }
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] givenArr = {1, 0, -1, 0, -2, 2};

        var result = fourSum(givenArr, 0);

        for (List<Integer> integers : result) {
            String elems = integers.toString();
            System.out.println(elems);
        }
    }
}
