package com.excercises;

public class ReverseStrings {
    private static String reverse(String givenStr) {
        var chars = givenStr.toCharArray();
        int lastIndx = chars.length - 1;
        for (int i = 0; i < chars.length; i++) {
            var first = givenStr.charAt(i);
            var last = givenStr.charAt(lastIndx);

            chars[i] = last;
            chars[lastIndx] = first;

            lastIndx--;
        }

        return String.valueOf(chars);
    }

    //different approach given char[]
    private static void reverse(char[] arrayOfChars) {

        int leftIndex = 0;
        int rightIndex = arrayOfChars.length - 1;

        while (leftIndex < rightIndex) {

            // swap characters
            char temp = arrayOfChars[leftIndex];
            arrayOfChars[leftIndex] = arrayOfChars[rightIndex];
            arrayOfChars[rightIndex] = temp;

            // move towards middle
            leftIndex++;
            rightIndex--;
        }
    }

    public static void main(String[] args) {
        String given = "abcd";

        System.out.println(reverse(given));
    }
}
