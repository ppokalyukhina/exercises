package com.excercises;

// Locate largest number in a sorted array
public class CircularArr {
    private static int findLargestNu(int[] arr) {
        if (arr.length == 0) {
            return 0;
        }

        if (arr.length == 1) {
            return arr[0];
        }

        int circulations = 0;
        for (int i = 0; i <= arr.length; i++) {
            if (i == arr.length - 1) {
                return arr[i];
            }

            if (arr[i] > arr[i + 1]) {
                break;
            }

            circulations++;
        }

        int max = arr[circulations];
        for (int i = circulations; i < arr.length; i+=circulations) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        return max;
    }


    private static int find(int[] givenArr, int find) {
        // if given arr is empty return -1
        if (givenArr.length == 0) {
            return -1;
        }

        // if only 1 elem given return elem or -1 if it's not the one we are looking for
        if (givenArr.length == 1) {
            if (find != givenArr[0]) {
                return -1;
            } else {
                return givenArr[0];
            }
        }

        // check the number of elems in each iteration
        int circulations = 0;
        for (int i = 0; i <= givenArr.length; i++) {
            if (i == givenArr.length - 1) {
                return givenArr[i];
            }

            if (givenArr[i] > givenArr[i + 1]) {
                break;
            }

            circulations++;
        }

        int circlesSize = givenArr.length / circulations;

        for (int i = 0; i < circlesSize; i+=circulations) {
            int highest = givenArr.length - 1;
            int lowestInx = i;
            int mid = (i + highest) >>> 1;

            while(lowestInx <= highest) {
                if (find == givenArr[mid]) {
                    return givenArr[mid];
                }

                if (find < givenArr[mid]) {
                    highest = mid - 1;
                    mid = (lowestInx + highest) >>> 1;
                }

                else if (find > givenArr[mid]) {
                    lowestInx = mid + 1;
                    mid = (lowestInx + highest) >>> 1;
                }
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 3, 4, 8, 2, 5, 7, 10, 6, 8, 9, 11};

        int find = 12;
        System.out.println(find(arr, find));
    }
}
