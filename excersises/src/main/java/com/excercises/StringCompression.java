package com.excercises;

public class StringCompression {
    private String compress(String s1) {
        String compressedStr = "";

        int repetition = 1;
        for (int i = 0; i < s1.length(); i++) {
            repetition++;
            if (i+ 1 >= s1.length() || s1.charAt(i+1) != s1.charAt(i)) {
                compressedStr = compressedStr.concat(String.valueOf(s1.charAt(i)) + repetition);

                repetition = 0;
            }
        }

        if (compressedStr.length() == s1.length()) {
            return s1;
        }

        return compressedStr;
    }

    public static void main(String[] args) {
        String s1 = "aaabbbxxx";

        var compr = new StringCompression();

        System.out.println(compr.compress(s1));
    }
}
