package com.excercises;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * In a town, there are N people labelled from 1 to N.  There is a rumor that one of these people is secretly the town judge.
 *
 * If the town judge exists, then:
 *
 *     The town judge trusts nobody.
 *     Everybody (except for the town judge) trusts the town judge.
 *     There is exactly one person that satisfies properties 1 and 2.
 *
 * You are given trust, an array of pairs trust[i] = [a, b] representing that the person labelled a trusts the person labelled b.
 *
 * If the town judge exists and can be identified, return the label of the town judge.  Otherwise, return -1.
 *
 * Example 1:
 *
 * Input: N = 2, trust = [[1,2]]
 * Output: 2
 *
 * Example 2:
 *
 * Input: N = 3, trust = [[1,3],[2,3]]
 * Output: 3
 *
 * Example 3:
 *
 * Input: N = 3, trust = [[1,3],[2,3],[3,1]]
 * Output: -1
 *
 * Example 4:
 *
 * Input: N = 3, trust = [[1,2],[2,3]]
 * Output: -1
 *
 * Example 5:
 *
 * Input: N = 4, trust = [[1,3],[1,4],[2,3],[2,4],[4,3]]
 * Output: 3
 *
 * Note:
 *
 *     1 <= N <= 1000
 *     trust.length <= 10000
 *     trust[i] are all different
 *     trust[i][0] != trust[i][1]
 *     1 <= trust[i][0], trust[i][1] <= N
 */
public class FindTheJudge {
    private static final Logger logger = LoggerFactory.getLogger(FindTheJudge.class);

    public static void main(String[] args) {
        int[][] input = {{1,3},{1,4},{2,3},{2,4},{4,3}};
        logger.info("Judge id is: {}", findJudge(3, input));
    }

    private static int findJudge(int N, int[][] trust) {
        Map<Integer, Integer> citizens = new HashMap<>(N);
        for (int[] ints : trust) {
            citizens.remove(ints[0]);

            if (citizens.containsKey(ints[1])) {
                int trustedCitizensNum = citizens.get(ints[1]);
                citizens.put(ints[1], ++trustedCitizensNum);

                continue;
            }

            citizens.put(ints[1], 1);
        }

        if (citizens.size() == 0 || citizens.values().stream().findFirst().get() != N - 1) {
            return -1;
        }

        return citizens.keySet().stream().findFirst().get();
    }
}
