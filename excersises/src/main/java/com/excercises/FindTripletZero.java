package com.excercises;

import java.util.HashSet;
import java.util.Set;

public class FindTripletZero {
    private static boolean findTriplet(int arr[], int n) {
        // merge sorting
        mergeSort(arr, 0, n-1);

        for (int i = 0; i < n-2; i++) {
            int firstPointer = i+1; // next el after i
            int lastPointer = n-1; // last element in arr

            while(firstPointer < lastPointer) {
                int sum = arr[i] + arr[firstPointer] + arr[lastPointer];
                if (sum == 0) {
                    return true;
                }

                if (sum < 0) {
                    firstPointer++;
                } else {
                    lastPointer--;
                }
            }
        }

        return false;
    }

    private static void mergeSort(int[] arr, int low, int lastElem) {
        if (low < lastElem) {
            int high = lastElem - 1;
            int mid = (low + high) >>> 1;

            mergeSort(arr, low, mid);
            mergeSort(arr, mid + 1, lastElem);

            merge(arr, low, mid, lastElem);
        }
    }

    private static void merge(int[] arr, int low, int mid, int high) {
        int[] firstArr = new int[mid - low + 1];
        int[] secondArr = new int[high - mid];

        // copy to subarrays
        for (int i = 0; i < firstArr.length; i++) {
            firstArr[i] = arr[i + low];
        }

        for (int j = 0; j < secondArr.length; j++) {
            secondArr[j] = arr[j + mid + 1];
        }

        /* Merge the temp arrays back into arr[l..r]*/
        int firstArrPointer = 0; // Initial index of first subarray
        int secondArrPointer = 0; // Initial index of second subarray
        int indxMergedArr = low; // Initial index of merged subarray

        while (firstArrPointer < firstArr.length && secondArrPointer < secondArr.length) {
            if (firstArr[firstArrPointer] <= secondArr[secondArrPointer]) {
                arr[indxMergedArr] = firstArr[firstArrPointer];
                firstArrPointer++;
                indxMergedArr++;
            }
            else if (firstArr[firstArrPointer] > secondArr[secondArrPointer]) {
                arr[indxMergedArr] = secondArr[secondArrPointer];
                secondArrPointer++;
                indxMergedArr++;
            }
        }

        // leftovers in first arr
        while (firstArrPointer < firstArr.length - 1) {
            arr[indxMergedArr] = firstArr[firstArrPointer];
            indxMergedArr++;
            firstArrPointer++;
        }

        while (secondArrPointer < secondArr.length - 1) {
            arr[indxMergedArr] = secondArr[secondArrPointer];
            secondArrPointer++;
            indxMergedArr++;
        }
    }

    // hash table solution
    private static boolean findSum(int[] arr, int sum) {
        for (int i = 0; i < arr.length - 2; i++) {
            Set<Integer> pairSum = new HashSet<>();

            for (int j = i+1; j < arr.length; j++) {
                int diff = sum - arr[i];

                if (pairSum.contains(diff - arr[j])) {
                    System.out.println("Found numbers: " + arr[i] + ", " + arr[j] + ", " + (diff - arr[j]));
                    return true;
                }

                pairSum.add(arr[j]);
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int[] withTriplet = new int[]{1, 0, -3, 3};
        int[] woTriplet = new int[]{0, -2, 3};

        findSum(withTriplet, 0);

//        System.out.println(findTriplet(withTriplet, withTriplet.length));
//        System.out.println(findTriplet(woTriplet, woTriplet.length));
    }
}
