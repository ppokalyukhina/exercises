package com.excercises;

import java.util.*;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache.
 * It should support the following operations: get and put.
 *
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
 * put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
 *
 * The cache is initialized with a positive capacity.
 *
 * Follow up:
 * Could you do both operations in O(1) time complexity?
 */
public class LRUcache {
    public static void main(String[] args) {
        var cache = new LRUcache(2);
//        cache.put(1, 1);
//        cache.put(2, 2);
//        cache.get(1);       // returns 1
//        cache.put(3, 3);    // evicts key 2
//        cache.get(2);       // returns -1 (not found)
//        cache.put(4, 4);    // evicts key 1
//        cache.get(1);       // returns -1 (not found)
//        cache.get(3);       // returns 3
//        cache.get(4);       // returns 4

        var firstTwo = cache.get(2);
        cache.put(2, 6);
        var firstOne = cache.get(1);
        cache.put(1, 5);
        cache.put(1, 2);
        var one = cache.get(1);
        var two = cache.get(2);
    }

    private int capacity;
    private Queue<Integer> order;
    private Map<Integer, Integer> map;

    public LRUcache(int capacity) {
        this.capacity = capacity;
        order = new LinkedList<>();
        map = new HashMap<>();
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }

        order.remove(key);
        order.add(key);

        return map.get(key);
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            map.put(key, value);
            order.remove(key);
            order.add(key);

            return;
        }

        if (isFull()) {
            removeLeastUsedElem();
        }

        order.add(key);
        map.put(key, value);
    }

    private void removeLeastUsedElem() {
        if (order.isEmpty()) {
            return;
        }
        map.remove(order.poll());
    }

    private boolean isFull() {
        return map.size() == capacity;
    }
}
