package com.excercises;

public class ListNodeSolution {
    private static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode(0);
        ListNode p = l1, q = l2, curr = dummyHead;
        int carry = 0;
        while (p != null || q != null) {
            int x = (p != null) ? p.val : 0;
            int y = (q != null) ? q.val : 0;
            int sum = carry + x + y;
            carry = sum / 10;
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            if (p != null) p = p.next;
            if (q != null) q = q.next;
        }
        if (carry > 0) {
            curr.next = new ListNode(carry);
        }
        return dummyHead.next;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(3);
        ListNode nextNode = new ListNode(2);
        l1.next = nextNode;
        nextNode.next = new ListNode(4);

        ListNode l2 = new ListNode(4);
        ListNode next = new ListNode(6);
        l2.next = next;
        next.next = new ListNode(9);

        var node = addTwoNumbers(l1, l2);

        System.out.println(node.val);
        var nextNode1 = node.next;
        while(nextNode1 != null) {
            System.out.println(nextNode1.val);
            nextNode1 = nextNode1.next;
        }
    }
}
