package com.excercises;

import java.util.List;

/**
 * A binary matrix means that all elements are 0 or 1. For each individual row of the matrix, this row is sorted in non-decreasing order.
 *
 * Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with at least a 1 in it.
 * If such index doesn't exist, return -1.
 *
 * You can't access the Binary Matrix directly.  You may only access the matrix using a BinaryMatrix interface:
 *
 *     BinaryMatrix.get(x, y) returns the element of the matrix at index (x, y) (0-indexed).
 *     BinaryMatrix.dimensions() returns a list of 2 elements [n, m], which means the matrix is n * m.
 *
 * Submissions making more than 1000 calls to BinaryMatrix.get will be judged Wrong Answer.
 * Also, any solutions that attempt to circumvent the judge will result in disqualification.
 *
 * For custom testing purposes you're given the binary matrix mat as input in the following four examples.
 * You will not have access the binary matrix directly.
 */
public class LeftColumnWithOnes {
    public static void main(String[] args) {
        int[][] grid = {{0,0,0,1}, {0,0,1,1}, {0,1,1,1}};
        var matrix = new BinaryMatrix(grid);

        var leftColumn = new LeftColumnWithOnes();
        System.out.println(leftColumn.leftMostColumnWithOne(matrix));
    }

    /**
     * // This is the BinaryMatrix's API interface.
     * // You should not implement it, or speculate about its implementation
     * interface BinaryMatrix {
     *     public int get(int x, int y) {}
     *     public List<Integer> dimensions {}
     * };
     */
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimensions = binaryMatrix.dimensions();
        int x = 0; // first row index
        int y = dimensions.get(1) - 1; // last column index

        int res = -1;
        while(y >= 0 && x < dimensions.get(0)) {
            int currentVal = binaryMatrix.get(x, y);
            if (currentVal == 0) {
                x++;
            } else {
                res = y;
                y--;
            }
        }

        return res;
    }

    private static class BinaryMatrix {
        private int[][] grid;
        public BinaryMatrix(int[][] grid) {
            this.grid = grid;
        }

        public int get(int x, int y) {
            return grid[x][y];
        }

        public List<Integer> dimensions() {
            return List.of(grid.length, grid[0].length);
        }
    }
}
