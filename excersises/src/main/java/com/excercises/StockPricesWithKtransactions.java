package com.excercises;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.PriorityQueue;

public class StockPricesWithKtransactions {
    private static final Logger logger = LoggerFactory.getLogger(StockPricesWithKtransactions.class);
    public static void main(String[] args) {
        int[] prices = {5, 11, 3, 50, 60, 90}; // should return 93
        // buy 5, sell 11, buy 3, sell 90
//        int k = 1;
//        int[] prices = {1, 25, 24, 23, 12, 36, 14, 40, 31, 41, 5};
        int k = 2; // should return 62
        logger.info("Max profit with {} transaction is {}", k, maxProfitWithKTransactionsNKSpace(prices, k));
    }

    private static int maxProfitWithKTransactionsNKSpace(int[] prices, int k) {
        if (prices.length == 0 || k == 0) {
            return 0;
        }

        int[][] profits = new int[k + 1][prices.length];
        for (int transaction = 1; transaction < profits.length; transaction++) {
            int maxCashPerDay = Integer.MIN_VALUE;
            for (int day = 1; day < prices.length; day++) {
                maxCashPerDay = Math.max(maxCashPerDay, profits[transaction - 1][day - 1] - prices[day - 1]);
                profits[transaction][day] = Math.max(profits[transaction][day - 1], maxCashPerDay + prices[day]);
            }
        }

        return profits[k][prices.length - 1];
    }

    private static int maxProfitWithKTransactionsNSpace(int[] prices, int k) {
        if (prices.length == 0 || k == 0) {
            return 0;
        }

        int[] evenProfits = new int[prices.length];
        int[] oddProfits = new int[prices.length];
        for (int i = 1; i < k + 1; i++) {
            int[] previousProfits;
            int[] currentProfits;
            if (i % 2 == 0) {
                previousProfits = oddProfits;
                currentProfits = evenProfits;
            } else {
                previousProfits = evenProfits;
                currentProfits = oddProfits;
            }

            int maxProfitPerDay = Integer.MIN_VALUE;
            for (int j = 1; j < prices.length; j++) {
                maxProfitPerDay = Math.max(maxProfitPerDay, previousProfits[j - 1] - prices[j - 1]);
                currentProfits[j] = Math.max(currentProfits[j - 1], maxProfitPerDay + prices[j]);
            }
        }

        return k % 2 == 0 ? evenProfits[prices.length - 1] : oddProfits[prices.length - 1];
    }

    private static int maxProfitWithKTransactions(int[] prices, int k) {
        if (prices.length == 0 || k == 0) {
            return 0;
        }

        PriorityQueue<Integer> transactions = new PriorityQueue<>((x, y) -> y - x);
        int profitPerTransaction = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            if (prices[i + 1] > prices[i]) {
                profitPerTransaction += (prices[i + 1] - prices[i]);
            } else {
                transactions.add(profitPerTransaction);
                profitPerTransaction = 0;
            }
        }

        if (profitPerTransaction != 0) {
            transactions.add(profitPerTransaction);
        }

        if (transactions.isEmpty()) {
            return 0;
        }

        int result = 0;
        if (transactions.size() <= k) {
            while(!transactions.isEmpty()) {
                result += transactions.poll();
            }

            return result;
        }

        int indx = k;
        while(!transactions.isEmpty() && indx > 0) {
            result += transactions.poll();
            indx--;
        }

        return result;
    }
}
