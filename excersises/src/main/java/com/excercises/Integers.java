package com.excercises;

import java.util.Arrays;
import java.util.Set;

public class Integers {
    // return 2 largest ints in array
    private static int[] largestLinear(int[] givenArr) {
        if (givenArr.length == 0) {
            return new int[]{};
        }

        if (givenArr.length == 1) {
            return givenArr;
        }

        int firstLargest = 0;
        int secondLargest = 0;

        for (int i = 0; i < givenArr.length; i++) {
            for (int j = 0; j < givenArr.length; j++) {
                if (givenArr[i] > givenArr[j]) {
                    if (firstLargest < givenArr[i]) {
                        secondLargest = firstLargest;
                        firstLargest = givenArr[i];
                    }
                } else {
                    if (firstLargest < givenArr[j]) {
                        secondLargest = firstLargest;
                        firstLargest = givenArr[j];
                    }
                }
            }
        }

        return new int[]{firstLargest, secondLargest};
    }

    private static int findNthSmallestFromArr(int[] arr, int n) {
        if (arr.length == 0) {
            return 0;
        }

        // sort arr
        return arr[n-1];
    }

    private static int thirdSmallesFromSet(Set<Integer> candidates) {
        if (candidates.size() < 3) {
            return 0;
        }

        Object[] arr = candidates.toArray();
        Arrays.sort(arr);

        return (int) arr[2];
    }

    public static void main(String[] args) {
        int[] givenArr = new int[]{2, 5, 1, 9};

        var result = largestLinear(givenArr);

        for (int i : result) {
            System.out.println(i);
        }
    }
}
