package com.excercises;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
 *              Not 7-1 = 6, as selling price needs to be larger than buying price.
 */
public class StockPricesWithOneTransaction {
    private static final Logger logger = LoggerFactory.getLogger(StockPricesWithOneTransaction.class);
    public static void main(String[] args) {
        int[] arr = {3,3,5,0,0,3,1,4}; // output 6
        logger.info("The best profit is {}", maxProfit1(arr));
    }

    public static int maxProfit1(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }

        int[][] transactions = new int[3][prices.length];
        for (int transaction = 1; transaction < 3; transaction++) {
            int maxLeftoverPerDay = Integer.MIN_VALUE;
            for (int day = 1; day < prices.length; day++) {
                maxLeftoverPerDay = Math.max(maxLeftoverPerDay, transactions[transaction - 1][day - 1] - prices[day - 1]);
                transactions[transaction][day] = Math.max(transactions[transaction][day - 1], maxLeftoverPerDay + prices[day]);
            }
        }

        return transactions[2][prices.length -1];
    }

    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        if (prices.length <= 1) {
            return maxProfit;
        }

        for (int buy = 0; buy < prices.length - 1; buy++) {
            for (int sell = buy + 1; sell < prices.length; sell++) {
                int profit = 0;
                int diff = prices[sell] - prices[buy];

                if (diff > 0) {
                    profit += diff;
                    maxProfit = Math.max(maxProfit, profit);
                }
            }
        }

        return maxProfit;
    }
}
