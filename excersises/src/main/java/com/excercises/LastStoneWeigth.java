package com.excercises;

import java.util.*;

/**
 * We have a collection of stones, each stone has a positive integer weight.
 *
 * Each turn, we choose the two heaviest stones and smash them together.
 * Suppose the stones have weights x and y with x <= y.  The result of this smash is:
 *
 *     If x == y, both stones are totally destroyed;
 *     If x != y, the stone of weight x is totally destroyed, and the stone of weight y has new weight y-x.
 *
 * At the end, there is at most 1 stone left.  Return the weight of this stone (or 0 if there are no stones left.)
 */
public class LastStoneWeigth {
    public static void main(String[] args) {
        int[] arr = {};
        System.out.println(lastStoneWeight(arr));
    }

    public static int lastStoneWeight(int[] stones) {
        if (stones.length == 0) {
            return 0;
        }

        PriorityQueue<Integer> q = new PriorityQueue<>((a, b) -> b - a);
        for (int stone : stones) {
            q.add(stone);
        }

        while(q.size() > 1) {
            int heavier = q.poll();
            int lighter = q.poll();
            if (heavier != lighter) {
                q.offer(Math.abs(heavier - lighter));
            }
        }

        if (q.isEmpty()) {
            return 0;
        }

        return q.poll();
    }
}
