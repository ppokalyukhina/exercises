package com.excercises;

import java.util.*;

public class GroupAnagrams {
    public static void main(String[] args) {
        var groupAnagrams = new GroupAnagrams();
        String[] given = {"eat", "tea", "tan", "ate", "nat", "bat"};
        var res = groupAnagrams.groupAnagrams(given);
        for (List<String> re : res) {
            for (String s : re) {
                System.out.println(s);
            }
        }
    }
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> groups = new HashMap<>();

        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = String.valueOf(chars);
            groups.putIfAbsent(key, new ArrayList<>());
            if (groups.containsKey(key)) {
                groups.get(key).add(str);
                continue;
            }

            groups.get(key).add(str);
        }

        List<List<String>> res = new ArrayList<>();
        for (String chars : groups.keySet()) {
            res.add(groups.get(chars));
        }

        return res;
    }
}
