package com.excercises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LongestWord {
    public static void main(String[] args) {
        String s = "abppplee";
        String[] arr = {"able", "ale", "apple", "bale", "kangaroo"};

        var result = findLongestWordMap(s, arr);

        System.out.println(result);
    }

    private static String longestWordOptimized(String givenString, String[] arr) {
        // sort arr based on length of words
        // search subsequences based on given string
        // return first subsequence

        //sort arr based on length of the words. Longest should be first
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i].length() > arr[j].length()) {
                    // swap
                    String tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        // check if it was a subsequence of the given string
        for (int j = 0; j < arr.length; j++) {
            int size = 0;
            for (int i = 0; i < givenString.length(); i++) {
                int givenStringPointer = size;
                while (givenStringPointer < arr[j].length()) {
                    if (givenString.charAt(i) == arr[j].charAt(givenStringPointer)) {
                        size++;
                    }

                    givenStringPointer++;
                }
            }

            if (size == arr[j].length()) {
                return arr[j];
            }
        }

        return "No sunsequences found";
    }


    private static String findLongestWordMap(String givenString, String[] arr) {
        // sort arr based on word length
        // create a map of given string with keys letters and values numbers in asc order
        // search for a letter of the current word and check the position of previous elem and current

        //sort arr based on length of the words. Longest should be first
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i].length() > arr[j].length()) {
                    // swap
                    String tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        Map<Character, List<Integer>> letters = new HashMap<>();
        for (int i = 0; i < givenString.length(); i++) {
            letters.putIfAbsent(givenString.charAt(i), new ArrayList<>());
            letters.get(givenString.charAt(i)).add(i);
        }

        for (int i = 0; i < arr.length; i++) {
            int lastIndex = 0;
            int size = 0; // size of word

            // get letters of given word
            for (int j = 0; j < arr[i].length(); j++) {
                if(!letters.containsKey(arr[i].charAt(j))) {
                     break;
                }

                List<Integer> allIndexes = letters.get(arr[i].charAt(j));

                if (lastIndex >= allIndexes.size()) {
                    break;
                }

                if (lastIndex == 0) {
                    lastIndex = allIndexes.get(0);
                    continue;
                }

                int nextIndex = allIndexes.get(lastIndex) + 1;

                if (nextIndex < lastIndex) {
                    break;
                }

                lastIndex = nextIndex;
                size++;
            }

            if (size == arr[i].length()) {
                   return arr[i];
                }
            }

        return "No combinations found";
    }
}
