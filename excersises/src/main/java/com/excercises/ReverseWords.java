package com.excercises;

public class ReverseWords {
    private static void reverse(char[] message) {
        String words = String.valueOf(message);
        var wordsArr = words.split(" ");

        int firstIndx = 0;
        int lastIndx = wordsArr.length - 1;
        while (firstIndx < lastIndx) {
            var tmpFirst = wordsArr[firstIndx];
            wordsArr[firstIndx] = wordsArr[lastIndx];
            wordsArr[lastIndx] = tmpFirst;
            lastIndx--;
            firstIndx++;
        }
    }

    public static void main(String[] args) {
        char[] message = { 'c', 'a', 'k', 'e', ' ',
                'p', 'o', 'u', 'n', 'd', ' ',
                's', 't', 'e', 'a', 'l' };

        reverse(message);
        System.out.println(message);
    }
}
