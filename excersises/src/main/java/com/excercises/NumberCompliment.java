package com.excercises;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Given a positive integer, output its complement number.
 * The complement strategy is to flip the bits of its binary representation.
 *
 * Input: 5
 * Output: 2
 * Explanation: The binary representation of 5 is 101 (no leading zero bits),
 * and its complement is 010. So you need to output 2.
 */
public class NumberCompliment {
    private static final Logger logger = LoggerFactory.getLogger(NumberCompliment.class);
    public static void main(String[] args) {
        int num = 1;
        logger.info("Number compliment for {} is {}", num, findComplement1(num));
    }

    private static int findComplement(int num) {
        String binaryRepresentation = Integer.toBinaryString(num);
        StringBuilder resultBinary = new StringBuilder();

        for (char c : binaryRepresentation.toCharArray()) {
            resultBinary.append('1' - c);
        }

        return Integer.parseInt(resultBinary.toString(), 2);
    }

    public static int findComplement1(int num) {
        int cp = num;
        int sum = 0;
        while(num > 0) {
            sum = (sum << 1) + 1;
            num >>= 1;
        }
        return sum - cp;

    }
}
