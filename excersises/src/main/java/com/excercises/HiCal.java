package com.excercises;

public class HiCal {
    private static Meeting[] getOverlaps(Meeting[] givenArr) {
        var res = new Meeting[givenArr.length];

        for (int i=0; i < givenArr.length - 1; i++) {
            for (int j = 1; j < givenArr.length; j++) {
                if (givenArr[i].getStartTime() > givenArr[j].getStartTime()) {
                    var tmpHigherTime = givenArr[i];
                    givenArr[i] = givenArr[j];
                    givenArr[j] = tmpHigherTime;
                }
            }
        }

        int indexToCheck = 0;
        res[indexToCheck] = givenArr[indexToCheck];

        for (int i = 1; i < givenArr.length; i++) {
            if (givenArr[i].getStartTime() >= givenArr[indexToCheck].getEndTime()) {
                indexToCheck++;
                continue;
            }
            if (givenArr[i].getEndTime() >= givenArr[indexToCheck].getEndTime()) {
                givenArr[i-1].setEndTime(givenArr[i].getEndTime());
                indexToCheck++;
            }
        }

        return res;
    }

    public static void main(String[] args) {
        var givenArr = new Meeting[]{new Meeting(1, 10), new Meeting(2, 6), new Meeting(3, 5), new Meeting(7, 9)};

        var res = getOverlaps(givenArr);

        for (Meeting re : res) {
            if (re == null) {
                System.out.println("No results");
                return;
            }
            System.out.println("from: " + re.getStartTime() + " to: " + re.getEndTime());
        }
    }
}

