package com.excercises;

public class Strings {
    public static void main(String[] args) {
        System.out.println(withoutString("Hi HoHo", "Ho"));
    }

    public static String withoutString(String base, String remove) {
        boolean allSameLetters = true;
        if (remove.length() == 1) {
            allSameLetters = false;
        }

        int pointer = 1;
        char previous = remove.charAt(0);

        while(pointer < remove.length()) {
            if (previous != remove.charAt(pointer)) {
                allSameLetters = false;
                break;
            } else {
                pointer++;
            }
        }

        char[] baseChars = base.toCharArray();
        char[] removeChars = remove.toCharArray();
        String str = "";
        int removeCharsPointer = 0;
        int countSameValue = 1;
        for (int i = 0; i < baseChars.length; i++) {
            if (removeCharsPointer == removeChars.length) {
                removeCharsPointer = 0;
            }

            if (i != baseChars.length - 1 && allSameLetters) {
                if (baseChars[i] == baseChars[i + 1]) {
                    countSameValue++;
                    continue;
                }
            }

            if (!String.valueOf(baseChars[i]).toLowerCase().equals(String.valueOf(removeChars[removeCharsPointer]).toLowerCase())) {
                str += String.valueOf(baseChars[i]);
            } else {
                if (countSameValue > removeChars.length && (countSameValue % removeChars.length) != 0) {
                    str += String.valueOf(baseChars[i]);
                }

                // if previous equals current and count is > 2, it should be added to the string as other letters
                removeCharsPointer++;
            }

            countSameValue = 1;
        }

        return str;
    }
}
