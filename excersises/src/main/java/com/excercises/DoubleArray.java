package com.excercises;

public class DoubleArray {
    private static int[] copyAndDoubleArr(int[] givenArr) {
        if (givenArr.length == 0) {
            return new int[]{};
        }

        int newSize = givenArr.length * 2;
        int[] newArr = new int[newSize];

        for (int i = 0; i < givenArr.length; i++) {
            newArr[i] = givenArr[i];
            newArr[i + givenArr.length] = givenArr[i];
        }

        return newArr;
    }

    public static void main(String[] args) {
        int[] givenArr = new int[]{1, 1, 1};

        var res = copyAndDoubleArr(givenArr);

        for (int re : res) {
            System.out.println(re);
        }
    }
}
