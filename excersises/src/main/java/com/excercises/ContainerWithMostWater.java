package com.excercises;

/**
 * Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai).
 * n vertical lines are drawn such that the two endpoints of the line i is at (i, ai) and (i, 0).
 * Find two lines, which, together with the x-axis forms a container, such that the container contains the most water.
 */
public class ContainerWithMostWater {
    public static void main(String[] args) {
        int[] input = {1,8,6,2,5,4,8,3,7};
        System.out.println(maxArea(input));
    }

    // O(N) time
    // O(1) space
    private static int maxArea(int[] height) {
        int maxArea = 0;
        if (height.length == 0) {
            return maxArea;
        }
        int left = 0;
        int right = height.length - 1;
        while(left < right) {
            int leftHeight = height[left];
            int rightHeight = height[right];
            int length = right - left;
            int area = Math.min(leftHeight, rightHeight) * length;
            maxArea = Math.max(maxArea, area);
            if (leftHeight <= rightHeight) {
                left++;
            } else {
                right--;
            }
        }

        return maxArea;
    }
}
