package com.excercises;

import com.trees.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class NextRightPointers {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        var nodeTwo = new TreeNode(2);
        var nodeThree = new TreeNode(3);
        var nodeFour = new TreeNode(4);
        var nodeFive = new TreeNode(5);
        var nodeSix = new TreeNode(6);
        var nodeSeven = new TreeNode(7);

        root.left = nodeTwo;
        root.right = nodeThree;

        nodeTwo.left = nodeFour;
        nodeTwo.right = nodeFive;

//        nodeThree.left = nodeSix;
        nodeThree.right = nodeSeven;

        String given =
                "{\"$id\":\"1\",\"left\":{\"$id\":\"2\",\"left\":{\"$id\":\"3\",\"left\":null,\"next\":null,\"right\":null,\"val\":4},\"next\":null,\"right\":null,\"val\":2},\"next\":null,\"right\":{\"$id\":\"4\",\"left\":null,\"next\":null,\"right\":{\"$id\":\"5\",\"left\":null,\"next\":null,\"right\":null,\"val\":5},\"val\":3},\"val\":1}\n";

        nextRightNodeNonPerfectTree(root);
    }

    private static void nextRightNode(TreeNode root) {
        Queue<TreeNode> current = new LinkedList<>();
        current.add(root);

        while(!current.isEmpty()) {
            Queue<TreeNode> parents = current;
            current = new LinkedList<>();

            while(!parents.isEmpty()) {
                var parent = parents.poll();

                if (parents.isEmpty()) {
                    System.out.println("Next is null");
                } else {
                    System.out.println(parent.next.getKey());
                }

                if (parent.left != null) {
                    current.add(parent.left);
                    if (parent.right != null) {
                        parent.left.next = parent.right;
                    }
                }

                if (parent.right != null) {
                    current.add(parent.right);
                    if (parents.peek() != null) {
                        parent.right.next = parents.peek().left;
                    }
                }
            }
        }
    }

    private static void nextRightNodeNonPerfectTree(TreeNode root) {
        if(root == null) {
            return;
        }
        LinkedList<TreeNode> current = new LinkedList<>();
        current.add(root);

        while(!current.isEmpty()) {
            LinkedList<TreeNode> parents = current;
            current = new LinkedList<>();

            while(!parents.isEmpty()) {
                TreeNode parent = parents.poll();

                if (parents.isEmpty()) {
                    System.out.println("Next is null");
                } else {
                    System.out.println(parent.next.getKey());
                }

                if(parent.left != null) {
                    current.add(parent.left);
                    if(parent.right != null) {
                        parent.left.next = parent.right;
                    }
                }

                if(parent.right != null) {
                    current.add(parent.right);
                    TreeNode nextParent = parents.peek();
                    if(nextParent != null) {
                        if(nextParent.left != null) {
                            parent.right.next = nextParent.left;
                        } else if(parent.right != null) {
                            parent.right.next = nextParent.right;
                        }
                    }
                }
            }
        }
    }
}
