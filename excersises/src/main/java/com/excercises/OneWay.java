package com.excercises;

public class OneWay {
    public boolean isOneWay(String firstStr, String secondStr) {
        boolean oneStep = false;

        int strLengthsDiff = firstStr.length() - secondStr.length();

        if (strLengthsDiff < -1 || strLengthsDiff > 1) {
            return oneStep;
        }

        char[] firstStringChars = firstStr.toCharArray();
        char[] secondArrayChars = secondStr.toCharArray();

        if (firstStringChars.length == secondArrayChars.length) {
            return editAction(firstStringChars, secondArrayChars);
        } else if (strLengthsDiff == 1) {
            return insertAction(firstStringChars, secondArrayChars);
        } else if (strLengthsDiff == -1) {
            return insertAction(secondArrayChars, firstStringChars);
        }

        return oneStep;
    }

    private boolean editAction(char[] s1, char[] s2) {
        int changes = 0;

        for (int i = 0; i < s1.length; i++) {
            for (int j = 0; j < s2.length; j++) {
                if (s1[i] != s2[j]) {
                    if (changes > 1) {
                        return false;
                    }

                    changes++;
                }
            }
        }

        return true;
    }

    private boolean insertAction(char[] s1, char[] s2) {
        int x1 = 0;
        int x2 = 0;

       while(x1 < s1.length && x2 < s2.length) {
           if(s1[x1] != s2[x2]) {
               if (x1 != x2) {
                   return false;
               }

               x1++;
           }
           else {
               x1++;
               x2++;
           }
       }

        return true;
    }

    public static void main(String[] args) {
        String firstStr = "pale";
        String secondStr = "ple";

        var onWay = new OneWay();
        System.out.println(onWay.isOneWay(firstStr, secondStr));
    }
}
