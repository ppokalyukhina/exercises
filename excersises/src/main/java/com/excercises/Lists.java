package com.excercises;

public class Lists {
    private static boolean equivalentLists(int[] firstList, int[] secondList) {
        if (firstList.length != secondList.length) {
            return false;
        }

        // linear, O(N) time, O(1) space
        int sameChars = 0;
        for (int i : firstList) {
            for (int i1 : secondList) {
                if (i == i1) {
                    sameChars++;
                }
            }
        }

        // sort both arrs
        // iterate and return false when elem != elem1

        return sameChars == firstList.length;
    }

    public static void main(String[] args) {
        int[] firstArr = {1, 5, 9};
        int[] secondArr = {5, 9, 1};

        System.out.println(equivalentLists(firstArr, secondArr));
    }
}
