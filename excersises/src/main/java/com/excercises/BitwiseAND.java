package com.excercises;

/**
 * Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers in this range, inclusive.
 */
public class BitwiseAND {
    public static void main(String[] args) {
        var bitwise = new BitwiseAND();

        System.out.println(bitwise.rangeBitwiseAnd(5, 7));
    }

    private int rangeBitwiseAnd(int m, int n) {
        while (n > m) {
            n = n & n - 1;
        }
        return m & n;
    }
}
