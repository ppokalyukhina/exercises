package com.excercises;

/**
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.
 *
 * Each element in the array represents your maximum jump length at that position.
 *
 * Determine if you are able to reach the last index.
 */
public class JumpGame {
    public static void main(String[] args) {
        var jumG = new JumpGame();

//        int[] arr = {2,3,1,1,4};
//        int[] arr = {3,2,1,0,4};
//        int[] arr = {2, 0, 0};
        int[] arr = {3,0,8,2,0,0,1};
        System.out.println(jumG.canJump(arr));
    }

    public boolean canJump(int[] nums) {
        if (nums.length <= 1) {
            return true;
        }

        int max = nums[0];
        for(int i = 0; i < nums.length; i++){
            if(max <= i && nums[i] == 0)
                return false;

            if(i + nums[i] > max){
                max = i + nums[i];
            }

            if(max >= nums.length-1)
                return true;
        }

        return false;
    }
}
