package com.excercises;

import java.util.Arrays;

/**
 *

 An array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)],
 which means that exactly one element is missing.

 Your goal is to find that missing element.

 Write a function:

 class Solution { public int solution(int[] A); }

 that, given an array A, returns the value of the missing element.

 For example, given array A such that:
 A[0] = 2
 A[1] = 3
 A[2] = 1
 A[3] = 5

 the function should return 4, as it is the missing element.

 Write an efficient algorithm for the following assumptions:

 N is an integer within the range [0..100,000];
 the elements of A are all distinct;
 each element of array A is an integer within the range [1..(N + 1)].
 */
public class MissingElement {
    public static void main(String[] args) {
        int[] given = {2, 4};
        System.out.println(test(given));
    }

    private static int test(int[] A) {
        if (A.length == 0) {
            return 0;
        }

        if (A.length == 1) {
            return A[0];
        }

        Arrays.sort(A);

        int firstIndx = 0;
        int lastIndx = A.length - 1;

        while (firstIndx != lastIndx) {
            /// check lower indexes
            int lowerFirst = A[firstIndx];
            int higherFirst = A[firstIndx + 1];

            if (higherFirst - lowerFirst > 1) {
                return higherFirst - 1;
            }

            firstIndx++;

            // check last indxes
            int higherLast = A[lastIndx];
            int lowerLast = A[lastIndx - 1];

            if (higherLast - lowerLast > 1) {
                return higherLast - 1;
            }

            lastIndx--;
        }

        return A[lastIndx] - A[firstIndx];
    }

    private static int solution(int[] A) {
        if (A.length == 0) {
            return 0;
        }

        if (A.length == 1) {
            return A[0];
        }

        Arrays.sort(A);

        int firstIndx = 0;
        int secondIndx = 2;

        while(secondIndx < A.length) {
            if (A[firstIndx + 1] - A[firstIndx] > 1) {
                return A[firstIndx + 1] - 1;
            }

            if (firstIndx > 0 && A[firstIndx] - A[firstIndx - 1] > 1) {
                return A[firstIndx] - 1;
            }

            if (secondIndx < A.length - 1 && A[secondIndx + 1] - A[secondIndx] > 1) {
                return A[secondIndx + 1] - 1;
            }

            if (A[secondIndx] - A[secondIndx - 1] > 1) {
                return A[secondIndx] - 1;
            }

            firstIndx+=4;
            secondIndx+=4;
        }

        if (firstIndx < A.length) {
            if (firstIndx < A.length - 1 && A[firstIndx + 1] - A[firstIndx] > 1) {
                return A[firstIndx + 1] - 1;
            }

            if (firstIndx > 0 && A[firstIndx] - A[firstIndx - 1] > 1) {
                return A[firstIndx] - 1;
            }
        }

        return 0;
    }
}
