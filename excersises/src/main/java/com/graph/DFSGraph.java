package com.graph;

import java.util.LinkedList;

public class DFSGraph {
    public static void main(String[] args) {
        Graph graph = new Graph(5);

        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(3, 4);

        printGraph(graph);
    }

    static class Graph {
        private int V; // vertex
        LinkedList<Integer> adjListArray[];
        private Integer root;

        Graph(int v) {
            V = v;

            // set max size of linked list
            adjListArray = new LinkedList[v];

            // fill the adjacenties with empty linked lists
            for (int i = 0; i < v; i++) {
                adjListArray[i] = new LinkedList<>();
            }

            root = null;
        }

        void addEdge(int src, int dest) {
            if (root == null) {
                root = src;
            }
            adjListArray[src].add(dest);
            adjListArray[dest].add(src);
        }
    }

    private static void printGraph(Graph graph)
    {
        for(int v = 0; v < graph.V; v++)
        {
            System.out.println("Adjacency list of vertex "+ v);
            System.out.print("head");
            for(Integer pCrawl: graph.adjListArray[v]){
                System.out.print(" -> "+pCrawl);
            }
            System.out.println("\n");
        }
    }
}
