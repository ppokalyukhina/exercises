package com.graph;

import java.util.Stack;

public class PathFinder {
    private boolean areConnected(Node from, Node to, GraphWithNodes givenGraph) {
        if (from == to) {
            return true;
        }

        Stack<Node> stack = new Stack<>();
        givenGraph.setRoot(from);
        Node root = givenGraph.getRoot();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node current = stack.pop();
            for (Node neighbour : current.getAdjacencies()) {
                if (neighbour.isVisited()) {
                    continue;
                }
                if (neighbour.getKey() == to.getKey()) {
                    return true;
                }
                stack.push(neighbour);
                current.visit();
            }

            current.visit();
        }

        return false;
    }

    public static void main(String[] args) {
        GraphWithNodes graph = new GraphWithNodes();

        Node nodeA = new Node(0);
        Node nodeB = new Node(1);
        Node nodeM = new Node(2);
        Node nodeC = new Node(3);
        Node nodeS = new Node(4);
        Node nodeN = new Node(5);
        Node nodeE = new Node(6);

        graph.addRoute(nodeA, nodeB);
        graph.addRoute(nodeA, nodeM);
        graph.addRoute(nodeA, nodeC);
        graph.addRoute(nodeB, nodeS);
        graph.addRoute(nodeM, nodeN);
        graph.addRoute(nodeC, nodeE);

        var pathFinder = new PathFinder();
        boolean areConnected = pathFinder.areConnected(nodeB, nodeB, graph);

        System.out.println(areConnected);
    }
}
