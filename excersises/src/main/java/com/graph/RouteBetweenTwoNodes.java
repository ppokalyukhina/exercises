package com.graph;

import java.util.LinkedList;
import java.util.Stack;

/**
 * Given directed graph. Find out if there is a path between two nodes.
 */
public class RouteBetweenTwoNodes {
    /**
     * Find if 2 nodes are connected using BFS traverser
     */
    public boolean foundPath(Node from, Node to) {
        LinkedList<Node> queue = new LinkedList<>();
        queue.add(from);

        while (!queue.isEmpty()) {
            Node currentNode = queue.removeFirst();
            for (Node v : currentNode.getAdjacencies()) {
                if (!v.isVisited()) {
                    if (v == to) {
                        return true;
                    } else {
                        queue.add(v);
                    }
                }
            }

            currentNode.visit();
        }

        return false;
    }

    /**
     * Find if 2 nodes are connected via DFS
     */
    public boolean foundPathDfs(Node from, Node to) {
        Stack<Node> stack = new Stack<>();
        stack.push(from);

        while(!stack.isEmpty()) {
            Node current = stack.pop();
            if (current != null) {
                if (current.right != null) {
                    if (current.right == to) {
                        return true;
                    }

                    stack.push(current.right);
                }

                if (current.left != null) {
                    if (current.left == to) {
                        return true;
                    }

                    stack.push(current.left);
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {
        RouteBetweenTwoNodes routeBetweenTwoNodes = new RouteBetweenTwoNodes();

        GraphWithNodes graphWithNodes = new GraphWithNodes();

        Node node0 = new Node(0);
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);

        graphWithNodes.addRoute(node0, node1);
        graphWithNodes.addRoute(node0, node2);
        graphWithNodes.addRoute(node0, node3);
        graphWithNodes.addRoute(node2, node3);
        graphWithNodes.addRoute(node3, node1);
        graphWithNodes.addRoute(node1, node4);


        System.out.println(routeBetweenTwoNodes.foundPath(node1, node3));
    }
}
