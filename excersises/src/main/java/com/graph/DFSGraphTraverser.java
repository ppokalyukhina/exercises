package com.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

public class DFSGraphTraverser {
    // preorder
    private static void preorder(Graph graph) {
        Node root = graph.getRoot();

        Stack<Node> nodes = new Stack<>();
        nodes.push(root);
        while(!nodes.isEmpty()) {
            Node current = nodes.pop();
            System.out.println(current.key);

            if (current.right != null) {
                nodes.push(current.right);
            }

            if (current.left != null) {
                nodes.push(current.left);
            }
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(7);

        Node root = new Node(25);
        Node node15 = new Node(15);
        Node node50 = new Node(50);
        Node node5 = new Node(5);
        Node node20 = new Node(20);
        Node node40 = new Node(40);
        Node node60 = new Node(60);


        graph.addEdge(root, node15);
        graph.addEdge(root, node50);
        graph.addEdge(node15, node5);
        graph.addEdge(node15, node20);
        graph.addEdge(node50, node40);
        graph.addEdge(node50, node60);


        preorder(graph);
    }

    static class Graph {
        private int V; // vertex
        LinkedList<Node> adjListArray[];
        private Node root;
        private Set<Node> visited = new HashSet<>();

        Graph(int v) {
            V = v;

            // set max size of linked list
            adjListArray = new LinkedList[v];

            // fill the adjacenties with empty linked lists
            for (int i = 0; i < v; i++) {
                adjListArray[i] = new LinkedList<>();
            }

            root = null;
        }

        void visit(Node node) {
            visited.add(node);
        }

        boolean isVisited(Node node) {
            return visited.contains(node);
        }

        void addEdge(Node src, Node dest) {
            if (root == null) {
                root = src;
            }
            if (dest.key <= src.key) {
                src.left = dest;
            } else {
                src.right = dest;
            }
        }

        Node getRoot() {
            return root;
        }

        int getSize() {
            return V;
        }
    }
}
