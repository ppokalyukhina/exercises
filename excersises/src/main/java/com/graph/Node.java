package com.graph;

import java.util.LinkedList;

public class Node {
    public int key;
    public Node left;
    public Node right;
    public Node previous;
    public Node next;
    public boolean visited;
    private LinkedList<Node> adjacencies;

    public Node(int key) {
        this.key = key;
        this.left = null;
        this.right = null;
        this.next = null;
        this.visited = false;
        adjacencies = new LinkedList<>();

        previous = null;
    }

    public int getKey() {
        return key;
    }

    public void visit() {
        visited = true;
    }

    public void addNext(Node next) {
        adjacencies.add(next);
    }

    public LinkedList<Node> getAdjacencies() {
        return adjacencies;
    }

    public boolean isVisited() {
        return visited;
    }
}
