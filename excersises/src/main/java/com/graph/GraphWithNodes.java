package com.graph;

import java.util.LinkedList;

public class GraphWithNodes {
    private Node root;
    private LinkedList<Node> allNodes;

    GraphWithNodes() {
        allNodes = new LinkedList<>();
    }

    void addRoute(Node from, Node to) {
        if(root == null) {
            root = from;
        }

        from.addNext(to);

        allNodes.add(from);
        allNodes.add(to);
    }

    public LinkedList<Node> getNodes() {
        return allNodes;
    }

    Node getRoot() {
        return root;
    }

    void setRoot(Node node) {
        root = node;
    }
}
