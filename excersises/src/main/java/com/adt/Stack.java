package com.adt;

// last in, first out.
public class Stack {
    private int size;
    private Integer[] arr;

    public Stack(int limit) {
        size = 0;
        arr = new Integer[limit];
    }

    public void push(int item) {
        if (isFull()) {
            // throw an exception...
        }

       arr[size++] = item;
    }

    public int pop() {
        if (isEmpty()) {
            // throw an exception
        }

        int lastItem = arr[size-1];
        arr[size-1] = null;
        size--;

        return lastItem;
    }

    public int getSize() {
        return size;
    }

    public boolean isFull() {
        return size == arr.length;
    }

    public boolean isEmpty() {
        return arr.length == 0;
    }

    public Integer[] getArr() {
        return arr;
    }

    public static void main(String[] args) {
        var stack = new Stack(5);

        stack.push(1);
        stack.push(2);
        stack.push(3);

        stack.pop();

        for (Integer integer : stack.getArr()) {
            System.out.println(integer + "\n");
        }
    }
}
