package com.adt;

// adds items and prioritises based on sorting
public class PriorityQueue {
    private int size;
    private int limit;
    private Integer[] arr;

    public PriorityQueue(int limit) {
        arr = new Integer[limit];
        size = 0;
        this.limit = limit;
    }

    public int getCurrentSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == arr.length;
    }

    public void insert(int item) {
        if (isEmpty()) {
            // add first item
            arr[size++] = item;
        }

        for (int i = size; i >= 0; i--) {

        }
    }

    public int remove() {
        return 0; //todo
    }
}
