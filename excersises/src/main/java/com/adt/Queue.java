package com.adt;

// first in, first out
public class Queue {
    private Integer[] arr;
    private int size;

    public Queue(int limit) {
        arr = new Integer[limit];
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public boolean isFull() {
        return size == arr.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    // add first item
    public void push(int item) {
        if (isFull()) {
            //throw an exception
        }

        if (isEmpty()) {
            arr[0] = item;
            size++;
            return;
        } else {
            for (int i = size + 1; i > 0; i--) {
                arr[i] = arr[i-1];
            }
        }

        arr[0] = item;
        size++;
    }

    // remove first element and return its value.
    public int pop() {
        if (isEmpty()) {
            // throw an exception..
        }

        int firstElem = arr[0];

        for (int i = 1; i <= size; i++) {
            int current = arr[i];
            arr[i-1] = current;
        }

        size--;
        return firstElem;
    }

    public Integer[] getArr() {
        return arr;
    }

    public static void main(String[] args) {
        Queue queue = new Queue(5);

        queue.push(1);
        queue.push(2);
        queue.push(3);

        queue.pop();

        for (Integer integer : queue.arr) {
            System.out.println(integer + "\n");
        }
    }
}
