package com.queue;

import com.stacks.Stack;

public class QueueViaStacks {
    private Stack oldestStack;
    private Stack newestStack;

    public QueueViaStacks() {
        oldestStack = new Stack();
        newestStack = new Stack();
    }

    public void add(int element) {
        newestStack.push(element);
    }

    public int remove() {
        shitfStacks();

        return oldestStack.pop();
    }

    public int peek() {
        shitfStacks();

        return oldestStack.peek();
    }

    private void shitfStacks() {
        if (oldestStack.isEmpty()) {
            while(!newestStack.isEmpty()) {
                oldestStack.push(newestStack.pop());
            }
        }
    }

    public int getSize() {
        return oldestStack.getCurrentSize() + newestStack.getCurrentSize();
    }
}
