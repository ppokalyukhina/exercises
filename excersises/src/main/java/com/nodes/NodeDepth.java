package com.nodes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

public class NodeDepth {
    private static final Logger logger = LoggerFactory.getLogger(NodeDepth.class);

    public static void main(String[] args) {
        var eight = new BinaryTree(8);
        var nine = new BinaryTree(9);
        var four = new BinaryTree(4);
        four.left = eight;
        four.right = nine;
        var five = new BinaryTree(5);
        var two = new BinaryTree(2);
        two.left = four;
        two.right = five;
        var six = new BinaryTree(6);
        var seven = new BinaryTree(7);
        var three = new BinaryTree(3);
        three.left = six;
        three.right = seven;

        var root = new BinaryTree(1);
        root.left = two;
        root.right = three;

        logger.info(String.format("The sum node depth is %d", stackSolution(root)));
    }

    private static int stackSolution(BinaryTree root) {
        int result = 0;
        if (root == null) {
            return result;
        }

        root.depth = 0;
        Stack<BinaryTree> stack = new Stack<>();
        stack.push(root);
        while(!stack.empty()) {
            var current = stack.pop();
            result += current.depth;

            if (current.left != null) {
                current.left.depth = current.depth + 1;
                stack.push(current.left);
            }

            if (current.right != null) {
                current.right.depth = current.depth + 1;
                stack.push(current.right);
            }
        }

        return result;
    }

    public static int nodeDepths(BinaryTree root) {
        int result = 0;
        if (root == null) {
            return result;
        }

        return recursive(result, root);
    }

    private static int recursive(int result, BinaryTree current) {
        if (current == null) {
            return 0;
        }

        return result + recursive(result + 1, current.left) + recursive(result + 1, current.right);
    }

    static class BinaryTree {
        int value;
        BinaryTree left;
        BinaryTree right;
        int depth;

        public BinaryTree(int value) {
            this.value = value;
            left = null;
            right = null;
            depth = 0;
        }
    }
}
