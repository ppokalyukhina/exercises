package com.nodes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InsertSort {
    private static final Logger logger = LoggerFactory.getLogger(InsertSort.class);

    public static void main(String[] args) {
        //Input: 4->2->1->3
        //Output: 1->2->3->4

        ListNode one = new ListNode(1);
        one.next = new ListNode(3);
        ListNode two = new ListNode(2);
        two.next = one;
        ListNode root = new ListNode(4);
        root.next = two;

        ListNode result = insertionSortList(root);
        while(result.next != null) {
            logger.info("Next node is: " + result.val);
            result = result.next;
        }
    }

    private static ListNode insertionSortList(ListNode head) {
       ListNode sorted = new ListNode();
       ListNode current = head;

       while(current != null) {
           ListNode previous = sorted;
           while(previous.next != null && previous.next.val < current.val) {
               previous = previous.next;
           }

           ListNode next = current.next;
           // insert the current node to the new list
           current.next = previous.next;
           previous.next = current;

           // moving on to the next iteration
           current = next;
       }

        return sorted.next;
    }
}