package com.search;

import java.util.HashSet;
import java.util.Set;

/**
 * You are a product manager and currently leading a team to develop a new product.
 * Unfortunately, the latest version of your product fails the quality check. Since each version is developed based on the previous version, all the versions after a bad version are also bad.
 *
 * Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one,
 * which causes all the following ones to be bad.
 *
 * You are given an API bool isBadVersion(version) which will return whether version is bad.
 * Implement a function to find the first bad version. You should minimize the number of calls to the API.
 *
 * Given n = 5, and version = 4 is the first bad version.
 *
 * call isBadVersion(3) -> false
 * call isBadVersion(5) -> true
 * call isBadVersion(4) -> true
 *
 * Then 4 is the first bad version.
 */
public class FirstBadVersion {
    private static VersionControl versionControl = new VersionControl(5);
    public static void main(String[] args) {
        versionControl.setFirstBadVersio(5);
        var firstBadVersion = new FirstBadVersion();
        System.out.println(firstBadVersion.firstBadVersion(5));
    }

    public int firstBadVersion(int n) {
        return find(0, n);
    }

    private int find(int low, int high) {
        int mid = low + (high - low) / 2;
        if (high <= low) {
            return high;
        }

        if (versionControl.isBadVersion(mid)) {
            return find(low, mid);
        }

        return find(mid + 1, high);
    }
}

class VersionControl {
    private Set<Integer> badVersions;
    private int n;

    public VersionControl(int n) {
        this.n = n;
        badVersions = new HashSet<>();
    }

    void setFirstBadVersio(int version) {
        while(version <= n) {
            badVersions.add(version);
            version++;
        }
    }

    boolean isBadVersion(int version) {
        return badVersions.contains(version);
    }
}
