package com.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Binary {
    private static final Logger logger = LoggerFactory.getLogger(Binary.class);

    public static void main(String[] args) {
        int[] arr = {0, 1, 21, 33, 45, 45, 61, 71, 72, 73};
        int target = 71;

        logger.info(String.format("Found result value position: %d", binarySearch(arr, target)));
    }

    private static int iterativePivot(int[] nums) {
        int sum = 0;
        int leftsum = 0;

        for (int x: nums) {
            sum += x;
        }

        for (int i = 0; i < nums.length; ++i) {
            if (leftsum == sum - leftsum - nums[i]) {
                return i;
            }

            leftsum += nums[i];
        }

        return -1;
    }

    public static int binarySearch(int[] array, int target) {
        return recursive(array, target, 0, array.length - 1);
    }

    private static int recursive(int[] array, int target, int low, int high) {
        if (high <= low) {
            return -1;
        }

        if (target == array[low]) {
            return low;
        }

        if (target == array[high]) {
            return high;
        }

        int mid = low + (high - low) / 2;
        if (target == array[mid]) {
            return mid;
        }

        if (target < array[mid]) {
            return recursive(array, target, low,  mid - 1);
        }

        return recursive(array, target, mid + 1,  high);
    }
}
