package com.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DFS {
    private static final Logger logger = LoggerFactory.getLogger(DFS.class);

    public static void main(String[] args) {
        Node root = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");
        Node f = new Node("F");
        Node g = new Node("G");
        Node h = new Node("H");
        Node i = new Node("I");
        Node j = new Node("J");
        f.addChild(i);
        f.addChild(j);
        Node k = new Node("K");
        g.addChild(k);

        d.addChild(g);
        d.addChild(h);
        b.addChild(e);
        b.addChild(f);

        root.addChild(b);
        root.addChild(c);
        root.addChild(d);

        List<String> finalArr = new ArrayList<>();
        var result = root.dfsRecursive(finalArr);

        for (String s : result) {
            logger.info(String.format("%s", s));
        }
    }

    static class Node {
        String name;
        List<Node> children = new ArrayList<>();
        boolean visited;

        public Node(String name) {
            this.name = name;
            visited = false;
        }

        public List<String> depthFirstSearchStack(List<String> array) {
            Stack<Node> stack = new Stack<>();
            stack.push(this);

            while(!stack.isEmpty()) {
                Node current = stack.pop();
                if (!current.visited) {
                    array.add(current.name);
                    current.visited = true;
                }

                for (int i = current.children.size() - 1; i >= 0; i--) {
                    Node child = current.children.get(i);
                    stack.push(child);
                }
            }

            return array;
        }

        public List<String> dfsRecursive(List<String> array) {
            Node root = this;
            recursive(root, array);

            return array;
        }

        private void recursive(Node current, List<String> array) {
            array.add(current.name);
            for (Node child : current.children) {
                recursive(child, array);
            }
        }

        public Node addChild(Node name) {
            children.add(name);
            return this;
        }
    }
}
