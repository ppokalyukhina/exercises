package com.traverser;

import java.util.Stack;

public class NumberOfIslands {
    public static void main(String[] args) {
        var numberOfIslands = new NumberOfIslands();

        char[][] givenGrid = {{'1', '1', '0', '0', '0'}, {'1', '1', '0', '0', '0'}, {'0', '0', '1', '0', '0'}, {'0', '0', '0', '1', '1'}};

        System.out.println(numberOfIslands.numIslands(givenGrid));
    }

    private int numIslands(char[][] grid) {
        int nOfIslands = 0;

        if (grid.length == 0) {
            return nOfIslands;
        }

        boolean[][] visited = new boolean[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (visited[i][j]) {
                    continue;
                }

                if (grid[i][j] == '0') {
                    visited[i][j] = true;
                    continue;
                }

                traverse(visited, i, j, grid);
                nOfIslands++;
            }
        }

        return nOfIslands;
    }

    private void traverse(boolean[][] visited, int i, int j, char[][] grid) {
        Stack<int[]> nodesToVisit = new Stack<>();
        nodesToVisit.push(new int[]{i, j});

        while(!nodesToVisit.isEmpty()) {
            int[] current = nodesToVisit.pop();
            int horizontal = current[0];
            int vertical = current[1];

            if (visited[horizontal][vertical]) {
                continue;
            }

            visited[horizontal][vertical] = true;
            if (grid[horizontal][vertical] == '0') {
                continue;
            }

            findNeighbours(nodesToVisit, visited, horizontal, vertical, grid);
        }
    }

    private void findNeighbours(Stack<int[]> stack, boolean[][] visited, int vertical, int horizontal, char[][] grid) {
        // check top
        if (vertical > 0 && !visited[vertical - 1][horizontal]) {
            stack.push(new int[]{vertical - 1, horizontal});
        }

        // check bottom
        if (vertical < grid.length - 1 && !visited[vertical + 1][horizontal]) {
            stack.push(new int[]{vertical + 1, horizontal});
        }

        // check left neighbour
        if (horizontal > 0 && !visited[vertical][horizontal - 1]) {
            stack.push(new int[]{vertical, horizontal - 1});
        }

        // check right neighbour
        if (horizontal < grid[0].length - 1 && !visited[vertical][horizontal + 1]) {
            stack.push(new int[]{vertical, horizontal + 1});
        }
    }
}
