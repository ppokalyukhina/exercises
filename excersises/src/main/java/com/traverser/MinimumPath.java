package com.traverser;

import java.util.Stack;

/**
 * Given a m x n grid filled with non-negative numbers,
 * find a path from top left to bottom right which minimizes the sum of all numbers along its path.
 *
 * Note: You can only move either down or right at any point in time.
 */
public class MinimumPath {
    /**
     * input
     * [
     *   [1,3,1],
     *   [1,5,1],
     *   [4,2,1]
     * ]
     *  result = 7
     */
    public static void main(String[] args) {
        int[][] grid = {
                {0,7,7,8,1,2,4,3,0,0,5,9,8,3,6,5,1,0},
                {2,1,1,0,8,1,3,3,9,9,5,8,7,5,7,5,5,8},
                {9,2,3,1,2,8,1,2,3,7,9,7,9,3,0,0,3,8},
                {3,9,3,4,8,1,2,6,8,9,3,4,9,4,8,3,6,2},
                {3,7,4,7,6,5,6,5,8,6,7,3,6,2,2,9,9,3},
                {2,3,1,1,5,4,7,4,0,7,7,6,9,1,5,5,0,3},
                {0,8,8,8,4,7,1,0,2,6,1,1,1,6,4,2,7,9},
                {8,6,6,8,3,3,5,4,6,2,9,8,6,9,6,6,9,2},
                {6,2,2,8,0,6,1,1,4,5,3,1,7,3,9,3,2,2},
                {8,9,8,5,3,7,5,9,8,2,8,7,4,4,1,9,2,2},
                {7,3,3,1,0,9,4,7,2,3,2,6,7,1,7,7,8,1},
                {4,3,2,2,7,0,1,4,4,4,3,8,6,2,1,2,5,4},
                {1,9,3,5,4,6,4,3,7,1,0,7,2,4,0,7,8,0},
                {7,1,4,2,5,9,0,4,1,4,6,6,8,9,7,1,4,3},
                {9,8,6,8,2,6,5,6,2,8,3,2,8,1,5,4,5,2},
                {3,7,8,6,3,4,2,3,5,1,7,2,4,6,0,2,5,4},
                {8,2,1,2,2,6,6,0,7,3,6,4,5,9,4,4,5,7}
        };

//        int[][] grid = {
//                {1, 3, 1},
//                {1, 5, 1},
//                {4, 2, 1}
//        };

        MinimumPath minimumPath = new MinimumPath();
        System.out.println(minimumPath.minPathSum1(grid));
    }

    int minPathSum1(int[][] grid) {
        int height = grid.length;
        int width = grid[0].length;

        int[][] paths = new int[height][width];

        paths[0][0] = grid[0][0];

        // copy & calculate first row paths
        for(int i = 1; i < width; i++) {
            paths[0][i] = grid[0][i] + paths[0][i - 1];
        }

        // copy & calculate first column
        for(int j = 1; j < height; j++) {
            paths[j][0] = grid[j][0] + paths[j - 1][0];
        }

        for(int i = 1; i < width; i++) {
            for(int j = 1; j < height; j++) {
                paths[j][i] = grid[j][i] + Math.min(paths[j-1][i], paths[j][i-1]);
            }
        }

        return paths[height - 1][width - 1];
    }

    // less efficient solution
    private int minPathSum(int[][] grid) {
        int height = grid.length;
        if (height == 0) {
            return 0;
        }
        // stack coordinates + distance from first Node
        // min sum = 0 start
        // bool[][] visited
        // lastNode
        // traverse till the last node, store distances, pick the minimum one
        int width = grid[0].length;
        Stack<Node> stack = new Stack<>();
        int minPath = Integer.MAX_VALUE;
        int[] lastNodeCoords = {height - 1, width - 1};
        Node root = new Node();
        root.distance = grid[0][0];
        root.coordinates = new int[]{0, 0};
        stack.push(root);

        while (!stack.isEmpty()) {
            Node current = stack.pop();
            int i = current.coordinates[0];
            int j = current.coordinates[1];

            // if distance >= current min Path, do not traverse anymore..
            if (current.distance >= minPath) {
                continue;
            }

            // check if destination is reached
            if (i == lastNodeCoords[0] && j == lastNodeCoords[1]) {
                minPath = Math.min(minPath, current.distance);
            }

            findNeighbours(current, stack, grid, height, width);
        }

        return minPath;
    }

    private void findNeighbours(Node current, Stack<Node> stack, int[][] grid, int gridHeight, int gridWidth) {
        int[] currentCoords = current.coordinates;
        int j = currentCoords[0];
        int i = currentCoords[1];
        // check right neightbour
        if (i < gridWidth - 1) {
            Node rigthNeighbour = new Node();
            rigthNeighbour.coordinates = new int[]{j, i + 1};
            rigthNeighbour.distance = current.distance + grid[j][i + 1];
            stack.push(rigthNeighbour);
        }

        // check bottom neighrour
        if (j < gridHeight - 1) {
            Node bottomNeighbour = new Node();
            bottomNeighbour.coordinates = new int[]{j + 1, i};
            bottomNeighbour.distance = current.distance + grid[j + 1][i];
            stack.push(bottomNeighbour);
        }
    }

    private static class Node {
        int distance;
        int[] coordinates;
    }
}
