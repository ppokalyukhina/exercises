package com.traverser;

/**
 * Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.
 * Input:
 *
 * 1 0 1 0 0
 * 1 0 1 1 1
 * 1 1 1 1 1
 * 1 0 0 1 0
 *
 * Output: 4
 */
public class MaximalSquare {
    public static void main(String[] args) {
        char[][] grid = {
                {'1', '0', '1', '0', '0'},
                {'1', '0', '1', '1', '1'},
                {'1', '1', '1', '1', '1'},
                {'1', '0', '0', '1', '0'}
        };

        var maximalSquare = new MaximalSquare();
        System.out.println(maximalSquare.maximalSquare(grid));
    }

    public int maximalSquare(char[][] matrix) {
        if(matrix == null || matrix.length == 0){
            return 0;
        }

        int result = 0;

        // create a new empty grid
        int[][] sums = new int[matrix.length][matrix[0].length];
        // push values from first row and column
        for(int i = 0; i < matrix.length; i++) {
            sums[i][0] = matrix[i][0] - '0';
            result = Math.max(result, sums[i][0]);
        }

        for(int j = 0; j < matrix[0].length; j++) {
            sums[0][j] = matrix[0][j]- '0';
            result = Math.max(result, sums[0][j]);
        }

        // iterate through matrix, check top and left neighbours
        for(int i = 1; i < matrix.length; i++) {
            for(int j = 1; j < matrix[0].length; j++) {
                if(matrix[i][j] == '1') {
                    int min = Math.min(sums[i-1][j], sums[i][j-1]);
                    min = Math.min(min, sums[i-1][j-1]);
                    sums[i][j] = min + 1;

                    result = Math.max(result, min+1);
                } else {
                    sums[i][j] = 0;
                }

            }
        }

        return result * result;
    }
}
