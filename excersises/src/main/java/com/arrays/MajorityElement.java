package com.arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
 *
 * You may assume that the array is non-empty and the majority element always exist in the array.
 *
 * Example 1:
 *
 * Input: [3,2,3]
 * Output: 3
 *
 * Example 2:
 *
 * Input: [2,2,1,1,1,2,2]
 * Output: 2
 */
public class MajorityElement {
    private static final Logger logger = LoggerFactory.getLogger(MajorityElement.class);

    public static void main(String[] args) {
        int[] input = {2,2,1,1,1,2,2};
        logger.info("Majority element is: {}", majorityElement(input));
    }

    private static int majorityElement(int[] nums) {
        // int value -> frequency
        Map<Integer, Integer> elements = new HashMap<>();
        int maxFrequency = 1;
        int element = nums[0];
        elements.put(element, maxFrequency);

        for (int i = 1; i < nums.length; i++) {
            if (elements.containsKey(nums[i])) {
                int frequency = elements.get(nums[i]);
                elements.put(nums[i], ++frequency);

                if (frequency > maxFrequency) {
                    maxFrequency = frequency;
                    element = nums[i];
                }
                continue;
            }

            elements.put(nums[i], 1);
        }

        return element;
    }
}
