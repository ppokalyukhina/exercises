package com.arrays;

/**
 * Given an array nums,
 * write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 */
public class MoveZeroes {
    public static void main(String[] args) {
        int[] nums = {4,2,4,0,0,3,0,5,1,0};

        var moveZ = new MoveZeroes();
        moveZ.moveZwithPointersOnly(nums);

        for (int re : nums) {
            System.out.println(re);
        }
    }

    private void moveZwithPointersOnly(int[] nums) {
        int firstInx = 0;
        int nonZerIndx = 1;

        while(firstInx < nums.length - 1 && nonZerIndx < nums.length) {
            if (nums[firstInx] == 0) {
                if (nums[nonZerIndx] == 0) {
                    nonZerIndx++;
                    continue;
                }

                nums[firstInx++] = nums[nonZerIndx];
                nums[nonZerIndx++] = 0;
                continue;
            }

            firstInx++;
            nonZerIndx++;
        }
    }

    public void moveZeroes(int[] nums) {
        int indx = 0;
        int lastIndx = nums.length - 1;

        while(indx < lastIndx) {
            if (nums[indx] == 0) {
                moveAllElements(nums, indx, lastIndx);
                lastIndx--;
            }

            indx++;
        }
    }

    private void moveAllElements(int[] nums, int indx, int lastIndex) {
        while (indx < lastIndex) {
            nums[indx] = nums[indx+1];
            indx++;
        }

        nums[lastIndex] = 0;
    }
}
