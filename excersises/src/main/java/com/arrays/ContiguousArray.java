package com.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.
 */
public class ContiguousArray {
    public static void main(String[] args) {
        var contiguousArr = new ContiguousArray();
//        int[] given = {0,0,0,1,1,1,0};

        int[] given = {0,1,0};
//        int[] given = {0, 1, 1, 0, 1, 1, 1, 0};
        System.out.println(contiguousArr.findMaxLength(given));
    }

    public int findMaxLength(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int maxlen = 0, count = 0;
        for (int i = 0; i < nums.length; i++) {
            int val = nums[i] == 1 ? 1 : -1;
            count += val;
            if (map.containsKey(count)) {
                maxlen = Math.max(maxlen, i - map.get(count));
            } else {
                map.put(count, i);
            }
        }

        return maxlen;
    }
}
