package com.arrays;

/**
 * Given an array nums of n integers where n > 1,
 * return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].
 */
public class ArrayExceptSelf {
    public static void main(String[] args) {
        var arrayExceptSelf = new ArrayExceptSelf();
        int[] given = {1,2,3,4,1};
        var res = arrayExceptSelf.productExceptSelf(given);
        for (int re : res) {
            System.out.println(re);
        }
    }

    public int[] productExceptSelf(int[] nums) {
        int[] output = new int[nums.length];
        int firstIndx = 0;
        int lastIndx = output.length - 1;

        int firstProduct = 1;
        int lastProduct = 1;
        while (firstIndx < lastIndx) {
            int firstElem = nums[firstIndx];
            int lastElem = nums[lastIndx];

            int commonProduct = 1;
            for(int i = firstIndx + 1; i < lastIndx; i++) {
                commonProduct *= nums[i];
            }

            output[firstIndx] = commonProduct * lastProduct * lastElem * firstProduct;
            output[lastIndx] = commonProduct * firstProduct * firstElem * lastProduct;

            firstProduct *= firstElem;
            lastProduct *= lastElem;

            firstIndx++;
            lastIndx--;
        }

        if (firstIndx == lastIndx) {
            output[firstIndx] = lastProduct * firstProduct;
        }

        return output;
    }
}
