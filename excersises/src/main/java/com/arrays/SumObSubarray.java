package com.arrays;

/**
 * Given an array of integers and an integer k,
 * you need to find the total number of continuous subarrays whose sum equals to k.
 *
 * Example 1:
 *
 * Input:nums = [1,1,1], k = 2
 * Output: 2
 *
 * Note:
 *     The length of the array is in range [1, 20,000].
 *     The range of numbers in the array is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].
 */
public class SumObSubarray {
    public static void main(String[] args) {
        var sum = new SumObSubarray();
        int[] arr = {1, 2, 3};
        System.out.println(sum.subarraySum(arr, 3));
    }

    public int subarraySum(int[] nums, int k) {
        if (nums.length == 1) {
            return nums[0] == k ? 1 : 0;
        }

        int low = 0;
        int result = 0;
        while(low < nums.length) {
            int high = low  + 1;
            int currentSum = nums[low];
            if (currentSum == k) {
                result++;
            }

            while(high < nums.length) {
                currentSum += nums[high];
                if (currentSum == k) {
                    result++;
                }
                high++;
            }
            low++;
        }

        return result;
    }
}
