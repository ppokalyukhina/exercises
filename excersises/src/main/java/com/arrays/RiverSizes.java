package com.arrays;

import java.util.*;

public class RiverSizes {
    public static void main(String[] args) {
        var riverSizes = new RiverSizes();
        int[][] given = {{1, 0, 0, 1, 0}, {1, 1, 1, 0, 0}, {0, 0, 1, 1, 0}};
        var res = riverSizes.riverSizes(given);

        for (Integer re : res) {
            System.out.println(re);
        }
    }
    public List<Integer> riverSizes(int[][] matrix) {
        List<Integer> result = new ArrayList<>();
        if (matrix.length == 0) {
            return result;
        }

        boolean[][] visited = new boolean[matrix.length][matrix[0].length];

        for(int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (visited[i][j]) {
                    continue;
                }

                if (matrix[i][j] == 0) {
                    visited[i][j] = true;
                    continue;
                }

                traverseNode(i, j, matrix, visited, result);
            }
        }

        return result;
    }

    private void traverseNode(int i, int j, int[][] matrix, boolean[][] visited, List<Integer> riversSizes) {
        Stack<int[]> nodesToTraverse = new Stack<>();
        nodesToTraverse.push(new int[]{i, j});
        int size = 0;
        while(!nodesToTraverse.isEmpty()) {
            int[] current = nodesToTraverse.pop();
            int horizontal = current[0];
            int vertical = current[1];

            if (visited[horizontal][vertical]) {
                continue;
            }

            visited[horizontal][vertical] = true;
            if (matrix[horizontal][vertical] == 0) {
                continue;
            }

            size++;
            findNeighbours(nodesToTraverse, visited, matrix, horizontal, vertical);
        }

        if (size > 0) {
            riversSizes.add(size);
        }
    }

    private void findNeighbours(Stack<int[]> stack, boolean[][] visited, int[][] matrix, int vertical, int horizontal) {
        // check left neighbour
        if (horizontal > 0 && !visited[vertical][horizontal - 1]) {
            stack.push(new int[]{vertical, horizontal - 1});
        }

        // check right
        if (horizontal < matrix[0].length - 1 && !visited[vertical][horizontal + 1]) {
            stack.push(new int[]{vertical, horizontal + 1});
        }

        // check top
        if (vertical > 0 && !visited[vertical - 1][horizontal]) {
            stack.push(new int[]{vertical - 1, horizontal});
        }

        // check bottom
        if (vertical < matrix.length - 1 && !visited[vertical + 1][horizontal]) {
            stack.push(new int[]{vertical + 1, horizontal});
        }
    }
}
