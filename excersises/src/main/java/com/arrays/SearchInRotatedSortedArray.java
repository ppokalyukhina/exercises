package com.arrays;

public class SearchInRotatedSortedArray {
    public static void main(String[] args) {
//        int[] arr = {2, 3, 0, 1};
//        int[] arr = {3, 1};
        int[] arr = {4,5,6,7,0,1,2};
        var searchInRotatedArr = new SearchInRotatedSortedArray();
        System.out.println(searchInRotatedArr.search(arr, 0));
    }

    private int search(int[] nums, int target) {
        if (nums.length == 0) {
            return -1;
        }

        // if first element is smaller than last,
        // array is not rotated.
        // Regular binary search
        if (nums[0] < nums[nums.length - 1]) {
            return binarySearch(nums, 0, nums.length - 1, target);
        }

        int pivot = findPivot(nums, 0, nums.length - 1);
        if (nums[pivot] == target) {
            return pivot;
        }

        if (nums[0] <= target) {
            return binarySearch(nums, 0, pivot, target);
        }

        return binarySearch(nums, pivot, nums.length - 1, target);
    }

    private int binarySearch(int[] nums, int low, int high, int target) {
        if (high <= low) {
            return -1;
        }

        if (target == nums[low]) {
            return low;
        }

        if (target == nums[high]) {
            return high;
        }

        int mid = low + (high - low) / 2;
        if (target == nums[mid]) {
            return mid;
        }

        if (target < nums[mid]) {
            return binarySearch(nums, low, mid - 1, target);
        }

        return binarySearch(nums, mid + 1, high, target);
    }

    private int findPivot(int[] nums, int low, int high) {
        // base cases
        if (high < low)
            return -1;
        if (high == low)
            return low;

        /* low + (high - low)/2; */
        int mid = (low + high)/2;
        if (mid < high && nums[mid] > nums[mid + 1])
            return mid;
        if (mid > low && nums[mid] < nums[mid - 1])
            return (mid-1);
        if (nums[low] >= nums[mid])
            return findPivot(nums, low, mid-1);
        return findPivot(nums, mid + 1, high);
    }
}
