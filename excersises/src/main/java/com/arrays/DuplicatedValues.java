package com.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Given sorted array of ints
 * Check for duplicates W/O using extra space
 * Return int => size of new array +
 * Change on the fly array (remove duplicates)
 */
public class DuplicatedValues {
    public static void main(String[] args) {
        int[] given = {1,2,3,1,2,3};
        var res = containsDuplicates(given, 2);

        DuplicatedValues duplicatedValues = new DuplicatedValues();

        int[] givenArr = {1, 1, 2};
       // System.out.println(duplicatedValues.removeDuplicates(givenArr));
        duplicatedValues.removeDuplicates(givenArr);

        for (int i : givenArr) {
            System.out.println(i);
        }
    }

    private int removeDuplicates(int[] nums) {
        if (nums.length == 0)  {
            return 0;
        }

        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }

        return i + 1;
    }

    /**
     * Given an array of integers and an integer k,
     * find out whether there are two distinct indices i and j in the array
     * such that nums[i] = nums[j] and the absolute difference between i and j is at most k.
     */
    private static boolean containsDuplicates(int[] nums, int k) {
        // <value, position in array>
        Map<Integer, Integer> valueAndPosition = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            if (valueAndPosition.containsKey(nums[i])) {
                int position = valueAndPosition.get(nums[i]);
                if ((i - position) <= k) {
                    return true;
                } else {
                    valueAndPosition.put(nums[i], i);
                }
            } else {
                valueAndPosition.put(nums[i], i);
            }
        }

        return false;
    }
}
