package com.arrays;

import java.util.LinkedList;
import java.util.List;

/**
 * Given an integer array nums,
 * find the contiguous subarray (containing at least one number)
 * which has the largest sum and return its sum.
 */
public class MaximumSubarray {
    public static void main(String[] args) {
        int[] nums = {-1, 0,-2};
        System.out.println(maxSubArray(nums));
    }

    public static int maxSubArray(int[] nums) {
        int max = nums[0];
        int index = 0;

        while (index < nums.length) {
            int sum = nums[index];
            if (sum > max) {
                max = sum;
            }

            for (int i = index + 1; i < nums.length; i++) {
                sum += nums[i];
                if (sum > max) {
                    max = sum;
                }
            }
            index++;
        }

        return max;
    }

    public static int maxSubArrayDivideAndConquer(int[] nums) {


        return 0;
    }
}
