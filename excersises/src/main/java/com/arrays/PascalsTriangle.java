package com.arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PascalsTriangle {
    public static void main(String[] args) {
        var res = triangleTwo(3);

    }

    public static List<List<Integer>> generate(int numRows) {
        if(numRows == 0) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();

        Stack<List<Integer>> stack = new Stack<>();
        List<Integer> firstRow = new ArrayList<>();
        firstRow.add(1);

        stack.push(firstRow);
        List<Integer> children;
        for(int i = 1; i <= numRows; i++) {
            List<Integer> parents = stack.pop();
            children = new ArrayList<>();

            Integer previous = 0;
            for(int j = 0; j < i - 1; j++) {
                int val = previous + parents.get(j);
                children.add(val);
                previous = parents.get(j);
            }

            children.add(1);

            stack.push(children);
            result.add(children);
        }

        return result;
    }

    /**
     * Return Kth index row
     */
    public static List<Integer> triangleTwo(int rowIndex) {
        List<Integer> result = new ArrayList<>();
        if (rowIndex == 0 || rowIndex > 33) {
            return result;
        }

        Stack<List<Integer>> stack = new Stack<>();
        List<Integer> parent = new ArrayList<>();
        parent.add(1);
        stack.push(parent);

        List<Integer> children;
        for (int i = 1; i <= rowIndex; i++) {
            List<Integer> parents = stack.pop();
            children = new ArrayList<>();

            int previous = 0;
            for (Integer integer : parents) {
                children.add(previous + integer);
                previous = integer;
            }
            children.add(1);

            if (i == rowIndex) {
                result.addAll(children);
                break;
            }

            stack.push(children);
        }

        return result;
    }
}
