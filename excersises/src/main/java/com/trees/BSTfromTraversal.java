package com.trees;

import java.util.Stack;

/**
 * Return the root node of a binary search tree that matches the given preorder traversal.
 *
 * (Recall that a binary search tree is a binary tree where for every node,
 * any descendant of node.left has a value < node.val,
 * and any descendant of node.right has a value > node.val.
 * Also recall that a preorder traversal displays the value of the node first, then traverses node.left, then traverses node.right.)
 */
public class BSTfromTraversal {
    public static void main(String[] args) {
        var bstFromTraversal = new BSTfromTraversal();
        int[] arr = {8,5,1,7,10,12};

        TreeNode x = bstFromTraversal.traversal(arr);
        System.out.println(x.val);
    }

    private TreeNode traversal(int[] preorder) {
        return traverse(preorder, 0, preorder.length - 1);
    }

    private TreeNode traverse(int[] preorder, int start, int end) {
        if (start > end) {
            return null;
        }

        TreeNode current = new TreeNode(preorder[start]);
        int i;
        for(i = start; i <= end; i++) {
            if (preorder[i] > current.val) {
                break;
            }
        }

        current.left = traverse(preorder, start + 1, i - 1);
        current.right = traverse(preorder, i, end);

        return current;
    }

    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    public TreeNode bstFromPreorder(int[] preorder) {
        if (preorder.length == 1) {
            return new TreeNode(preorder[0]);
        }

        TreeNode root = new TreeNode(preorder[0]);
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);

        int minVal = 1;
        int indx = 1;
        while(!stack.isEmpty()) {
            TreeNode current = stack.pop();

            // create left side of the tree
            while(indx < preorder.length && preorder[indx] > minVal && preorder[indx] < current.val) {
                TreeNode left = new TreeNode(preorder[indx]);
                current.left = left;
                stack.push(left);
                current = current.left;
                indx++;
            }

            TreeNode left = new TreeNode(preorder[indx]);
            current.left = left;
            stack.push(left);
           // current = current.left;
            indx++;

            while(indx < preorder.length && preorder[indx] < root.val && preorder[indx] > current.val) {
                TreeNode right = new TreeNode(preorder[indx]);
                current.right = right;
                stack.push(right);
                current = current.right;
                indx++;
            }
        }

        return root;
    }
}
