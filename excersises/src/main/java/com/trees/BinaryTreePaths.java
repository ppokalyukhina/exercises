package com.trees;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode five = new TreeNode(5);

        root.left = two;
        root.right = three;
        two.right = five;

        BinaryTreePaths binarySearchTree = new BinaryTreePaths();
        var result = binarySearchTree.binaryTreePaths(root);

        for (String s : result) {
            System.out.println(s);
        }
    }

    public List<String> binaryTreePaths(TreeNode root) {
        ArrayList<String> finalArr = new ArrayList<>();

        String given = "";
        appendString(root, given, finalArr);

        return finalArr;
    }

    private void appendString(TreeNode current, String given, List<String> finalArr) {
        if (current == null) {
            return;
        }

        if (current.left == null && current.right == null) {
            given += String.format("->%d", current.val);
            finalArr.add(given);
            return;
        } else {
            if (!given.equals("")) {
                given += String.format("->%d", current.val);
            } else {
                given += String.format("%d", current.val);
            }
        }

        appendString(current.left, given, finalArr);
        appendString(current.right, given, finalArr);
    }
}
