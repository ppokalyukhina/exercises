package com.trees;

public class Successor {
    private TreeNode nextSuccessor(TreeNode node) {
        if (node == null) {
            return null;
        }

        if (node.right != null) {
            return node.right;
        }

        TreeNode current = node;

        if (current.parent == null) {
            return current;
        }

        TreeNode parent = current.parent;

        while(parent != null && current != parent.left) {
            current = parent;
            parent = current.parent;
        }

        if (parent == null) {
            System.out.println("Traversal is finished at that point");
            return node;
        }

        return parent;
    }

    public static void main(String[] args) {
        Successor successor = new Successor();

        TreeNode root = new TreeNode(0);
        TreeNode one = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode four = new TreeNode(4);
        TreeNode five = new TreeNode(5);
        TreeNode six = new TreeNode(6);
        TreeNode seven = new TreeNode(7);
        TreeNode eight = new TreeNode(8);
        one.parent = root;
        two.parent = root;
        three.parent = one;
        four.parent = one;
        seven.parent = three;
        eight.parent = three;
        five.parent = two;
        six.parent = two;

        root.right = two;
        root.left = one;

        one.left = three;
        one.right = four;

        two.left = five;
        two.right = six;

        three.left = seven;
        three.right = eight;

        System.out.println(successor.nextSuccessor(six).getKey());
    }
}
