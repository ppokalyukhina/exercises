package com.trees;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a binary tree, you need to compute the length of the diameter of the tree.
 * The diameter of a binary tree is the length of the longest path between any two nodes in a tree.
 * This path may or may not pass through the root.
 *
 * Note: The length of path between two nodes is represented by the number of edges between them.
 */
public class BinaryTreeDiameter {
    int maxLength = 1;
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    public static void main(String[] args) {
        var binaryTree = new BinaryTreeDiameter();
        var root = new TreeNode(1);
        var two = new TreeNode(2);
        var three = new TreeNode(3);
        var four = new TreeNode(4);
        var five = new TreeNode(5);
        var six = new TreeNode(6);
        root.left = two;
        root.right = three;
        two.left = four;
        two.right = five;
//        four.left = six;

//        root.left = two;
//        two.right = three;

        binaryTree.recursive(root);
        System.out.println(binaryTree.maxLength - 1);
    }

    private int recursive(TreeNode root) {
        if (root == null) {
            return 0;
        }

        int left = recursive(root.left);
        int right = recursive(root.right);

        maxLength = Math.max(maxLength, left + right + 1);
        return Math.max(left, right) + 1;
    }
}
