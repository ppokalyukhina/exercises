package com.trees;

public class Heap {
    private static void buildHeap(int arr[], int n) {
        for (int i = 0; i < n; i++) {
            heapifyMax(arr, n, i);
        }
    }

    // move all elems with higher value up
    // check every child
    // if child is bigger than parent, swap, else move to the next elem
    // call inself recursively
    private static void heapifyMax(int arr[], int n, int root) {
        int largestIndx = root;
        int leftIndx = 2*root + 1;
        int rightIndx = 2*root + 2;

            if (leftIndx < n && arr[leftIndx] > arr[largestIndx] ) {
                largestIndx = leftIndx;
            }

            if (rightIndx < n && arr[rightIndx] > arr[largestIndx]) {
                largestIndx = rightIndx;
            }

            if (largestIndx != root) {
                // swap
                int tmpLargest = arr[root];
                arr[root] = arr[largestIndx];
                arr[largestIndx] = tmpLargest;

                heapifyMax(arr, n, largestIndx);
            }
    }

    private static void heapifyin(int arr[], int root, int n) {
        int minIndex = root;
        int leftIndx = root * 2 + 1;
        int rightIndx = root * 2 + 2;

        if (leftIndx < n && arr[minIndex] > arr[leftIndx] ) {
            minIndex = leftIndx;
        }

        if (rightIndx < n && arr[minIndex] > arr[rightIndx]) {
            minIndex = rightIndx;
        }

        if (minIndex != root) {
            // swap
            int tmpLargest = arr[root];
            arr[root] = arr[minIndex];
            arr[minIndex] = tmpLargest;

            heapifyin(arr, minIndex, n);
        }
    }

    private static void heapSort(int arr[], int n) {
        int i = (arr.length - 2) / 2;
        while (i >= 0) {
            heapifyin(arr, i--, arr.length);
        }

//        buildHeap(arr, n);
//
//        // One by one extract an element from heap
//        for (int i=n-1; i>=0; i--)
//        {
//            // Move current root to end
//            int temp = arr[0];
//            arr[0] = arr[i];
//            arr[i] = temp;
//
//            // call max heapify on the reduced heap
//            heapifyMax(arr, i, 0);
//        }
    }

    public static void main(String[] args) {
        int[] arr = {12, 11, 13, 5, 6, 7};

        heapSort(arr, arr.length);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
