package com.trees;

import java.util.LinkedList;

/**
 * Given sorted array.
 * Create a minimal binary search tree
 *
 * Steps:
 * Pick mid of the array, make it root,
 * Add left and right child,
 * Recursively call the function on each of children
 */
public class BinarySearchTree {
    private Node root;
    private LinkedList<Node> nodes;

    public BinarySearchTree() {
        nodes = new LinkedList<>();
    }

    public void addLeft(Node original, Node child) {
        if (root == null) {
            root = original;
        }

        original.left = child;
        if (!nodes.contains(original)) {
            nodes.add(original);
        }

        nodes.add(child);
    }

    public void addRight(Node original, Node child) {
        if (root == null) {
            root = original;
        }

        original.right = child;
        if (!nodes.contains(original)) {
            nodes.add(original);
        }

        nodes.add(child);
    }

    public LinkedList<Node> getNodes() {
        return nodes;
    }
}
