package com.trees;

public class BSTsecondLargest {
    public static void main(String[] args) {
        BinaryTreeNode root = new BinaryTreeNode(4);
        BinaryTreeNode two = new BinaryTreeNode(2);
        BinaryTreeNode one = new BinaryTreeNode(1);
        BinaryTreeNode three = new BinaryTreeNode(3);
        BinaryTreeNode seven = new BinaryTreeNode(7);
        BinaryTreeNode five = new BinaryTreeNode(5);
        BinaryTreeNode eight = new BinaryTreeNode(8);
        BinaryTreeNode six = new BinaryTreeNode(6);
        BinaryTreeNode nine = new BinaryTreeNode(9);

        root.left = two;
//        root.right = seven;

        two.left = one;
        two.right = three;

        seven.left = five;
        seven.right = eight;

        eight.left = six;
        eight.right = nine;

        System.out.println(findSecondLargest(root).value);
    }

    private static BinaryTreeNode findSecondLargest(BinaryTreeNode root) {
        if (root == null) {
            return null;
        }

        if (root.right == null && root.left == null) {
            return root;
        }

        BinaryTreeNode result = root;
        if (root.right == null) {
            root = root.left;

            while(root.right != null) {
                root = root.right;
            }

            result = root;
        }

        while(root.right != null) {
            result = root;
            root = root.right;
        }

        return result;
    }
}
