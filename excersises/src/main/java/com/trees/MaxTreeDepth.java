package com.trees;

import java.util.LinkedList;
import java.util.Queue;

public class MaxTreeDepth {
    public static void main(String[] args) {

    }

    private static int maxDepth(TreeNode root) {
        int depth = 0;
        if(root == null) {
            return depth;
        }

        Queue<TreeNode> current = new LinkedList<>();
        current.add(root);

        while(!current.isEmpty()) {
            Queue<TreeNode> parents = current;
            current = new LinkedList<>();

            while(!parents.isEmpty()) {
                TreeNode currentNode = parents.poll();
                if(currentNode.left != null) {
                    current.add(currentNode.left);
                }

                if(currentNode.right != null) {
                    current.add(currentNode.right);
                }
            }

            depth++;
        }

        return depth;
    }
}
