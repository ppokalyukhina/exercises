package com.trees;

public class MinimalTree {
    private BST resultTree = new BST();

    private void createMinimalBST(int[] arr) {
        buildBST(arr, 0, arr.length - 1);
    }

    private TreeNode buildBST(int[] givenArr, int startIndex, int endIndex) {
        if (endIndex < startIndex) {
            return null;
        }
        int mid = (startIndex + endIndex) / 2;
        TreeNode n = new TreeNode(givenArr[mid]);
        resultTree.addNode(n);
        n.left = buildBST(givenArr, startIndex, mid - 1);
        n.right = buildBST(givenArr, mid + 1, endIndex);

        return n;
    }


    public static void main(String[] args) {
        int[] sortedArr = {0, 1, 2, 3, 4, 5};
        MinimalTree minimalTree = new MinimalTree();

        minimalTree.createMinimalBST(sortedArr);

        for (TreeNode node : minimalTree.resultTree.getNodes()) {
            System.out.println(node.getKey() + " root id");
            if (node.left != null) {
                System.out.println(node.left.getKey() + " left neighbour ");
            } else {
                System.out.println("left neighbour is null ");
            }

            if (node.right != null) {
                System.out.println(node.right.getKey() + "right neighbour ");
            } else {
                System.out.println("right neighbour is null ");
            }
        }
    }
}
