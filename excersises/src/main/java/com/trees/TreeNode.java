package com.trees;

public class TreeNode {
    private int key;
    public TreeNode left;
    public TreeNode right;
    TreeNode parent;
    public TreeNode next;
    private boolean isVisited;
    public int val;

    public TreeNode(int key) {
        this.key = key;
        isVisited = false;
        val = key;
    }

    public int getKey() {
        return key;
    }

    public void visit() {
        isVisited = true;
    }

    public boolean isVisited() {
        return isVisited;
    }
}
