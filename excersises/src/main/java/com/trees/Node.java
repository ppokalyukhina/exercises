package com.trees;

public class Node {
    private int key;
    public Node left, right;

    public Node(int key) {
        this.key = key;
        this.left = null;
        this.right = null;
    }

    public int getKey() {
        return key;
    }
}
