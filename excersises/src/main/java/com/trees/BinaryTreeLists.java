package com.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BinaryTreeLists {
    private List<LinkedList<TreeNode>> getLists(TreeNode root) {
        List<LinkedList<TreeNode>> levels = new ArrayList<>();
        LinkedList<TreeNode> rootLinkedList = new LinkedList<>();
        if(root != null) {
            rootLinkedList.add(root);
        }

        LinkedList<TreeNode> current = rootLinkedList;
        while(current.size() > 0) {
            levels.add(current);
            LinkedList<TreeNode> parents = current;
            current = new LinkedList<>();

            for(TreeNode parent : parents) {
                if(parent.left != null) {
                    current.add(parent.left);
                }

                if(parent.right != null) {
                    current.add(parent.right);
                }
            }
        }

        return levels;
    }

    public static void main(String[] args) {
        BinaryTreeLists btLists = new BinaryTreeLists();

        TreeNode root = new TreeNode(0);
        TreeNode one = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode four = new TreeNode(4);
        TreeNode five = new TreeNode(5);
        TreeNode six = new TreeNode(6);
        TreeNode seven = new TreeNode(7);

        root.right = two;
        root.left = one;

        one.left = three;
        one.right = four;

        two.left = five;
        two.right = six;

        three.left = seven;

        var result = btLists.getLists(root);
        for (int i = 0; i < result.size(); i++) {
            System.out.println("level " + (i + 1));
            LinkedList<TreeNode> treeNodes = result.get(i);

            for (TreeNode treeNode : treeNodes) {
                System.out.println(treeNode.getKey() + " root id");
                if (treeNode.left != null) {
                    System.out.println(treeNode.left.getKey() + " left");
                } else {
                    System.out.println("Left neighbout is null");
                }
                if (treeNode.right != null) {
                    System.out.println(treeNode.right.getKey() + " right");
                } else {
                    System.out.println("Right neighbour is null");
                }
            }
        }
    }
}
