package com.trees;

public class BinaryTree {
    private static int searchFor(int find, Node root) {
        int rootInt = root.getKey();
        Node next = root;

        if (find == rootInt) {
            return find;
        }

        int max = rootInt;
        int nextIntToCompare = rootInt;
        while(nextIntToCompare <= max) {
            if (find == nextIntToCompare) {
                return find;
            }
            else if (find < nextIntToCompare) {
                if (next == root) {
                    next = root.left;
                } else {
                    next = next.left;
                }

                nextIntToCompare = next.getKey();
            }
            else {
                if (next == root) {
                    next = root.right;
                } else {
                    next = next.right;
                }

                if (next == null) {
                    return -1;
                }
                max = next.getKey();
                nextIntToCompare = next.getKey();
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        var root = new Node(1);
        var left = new Node(2);
        var right = new Node(3);

        root.left = left;
        root.right = right;

        int find = 4;
        System.out.println(searchFor(find, root));
    }
}
