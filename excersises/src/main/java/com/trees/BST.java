package com.trees;

import java.util.LinkedList;

public class BST {
    private LinkedList<TreeNode> nodes;

    public BST() {
        nodes = new LinkedList<>();
    }

    void addNode(TreeNode node) {
        nodes.add(node);
    }

    LinkedList<TreeNode> getNodes() {
        return nodes;
    }
}
