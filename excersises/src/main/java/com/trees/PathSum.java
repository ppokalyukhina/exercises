package com.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PathSum {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        TreeNode four = new TreeNode(4);
        TreeNode eight = new TreeNode(8);
        TreeNode eleven = new TreeNode(11);
        TreeNode thirteen = new TreeNode(13);
        TreeNode four2 = new TreeNode(4);
        TreeNode seven = new TreeNode(7);
        TreeNode two = new TreeNode(2);
        TreeNode five = new TreeNode(5);
        TreeNode one = new TreeNode(1);

        root.left = four;
        root.right = eight;

        four.left = eleven;
        eleven.left = seven;
        eleven.right = two;

        eight.left = thirteen;
        eight.right = four2;

        four2.left = five;
        four2.right = one;

        PathSum pathSum = new PathSum();
        List<List<Integer>> result = pathSum.pathSum(root, 22);
        for (List<Integer> integers : result) {
            for (Integer integer : integers) {
                System.out.println(integer);
            }
        }
    }

    private List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        Stack<Context> stack = new Stack<>();
        stack.push(new Context(root));

        while(!stack.isEmpty()) {
            Context current = stack.pop();

            if(current.node.left == null && current.node.right == null) {
                if(current.getSum() == sum) {
                    current.pathVals.add(current.node.val);
                    result.add(current.getPathVals());
                } else {
                    continue;
                }
            }

            if(current.node.left != null) {
                Context left = new Context(current.node.left, current.getPathVals(), current.sum);
                left.pathVals.add(current.node.val);
                stack.push(left);
            }

            if(current.node.right != null) {
                Context right = new Context(current.node.right, current.getPathVals(), current.sum);
                right.pathVals.add(current.node.val);
                stack.push(right);
            }
        }

        return result;
    }

    private class Context {
        public TreeNode node;
        private List<Integer> pathVals;
        private int sum = 0;

        Context(TreeNode root) {
            node = root;
            pathVals = new ArrayList<>();
            sum += root.val;
        }

        Context(TreeNode root, List<Integer> visitedPaths, int visitedSum) {
            node = root;
            pathVals = new ArrayList<>(visitedPaths);
            sum = visitedSum + root.val;
        }

        int getSum() {
            return sum;
        }

        List<Integer> getPathVals() {
            return pathVals;
        }
    }
}
