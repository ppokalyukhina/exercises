package com.trees;

public class BSTValidate {
    public boolean BSisBST(TreeNode root) {
        if (root == null) {
            return false;
        }

        TreeNode currentLeft = root.left;
        TreeNode currentRight = root.right;
        while (currentLeft.right != null && currentRight.left != null) {
            if (currentLeft.right.getKey() > currentRight.left.getKey()) {
                return false;
            }

            currentLeft = currentLeft.right;
            currentRight = currentRight.left;
        }

        return true;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(0);
        TreeNode one = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode four = new TreeNode(4);
        TreeNode five = new TreeNode(5);
        TreeNode six = new TreeNode(6);
        TreeNode ten = new TreeNode(10);

        root.right = four;
        root.left = one;

        one.left = two;
        one.right = ten;

        four.left = five;
        four.right = six;


        BSTValidate validator = new BSTValidate();
        System.out.println(validator.BSisBST(root));
    }
}
