package com.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraverser {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode four = new TreeNode(4);
        TreeNode five = new TreeNode(5);

        root.left = two;
        root.right = three;
        two.left = four;
        three.right = five;

        var result = levelOrder(root);

        for (List<Integer> integers : result) {
            for (Integer integer : integers) {
                System.out.println(integer);
            }
        }
    }

    private static List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();

        Queue<TreeNode> current = new LinkedList<>();
        current.add(root);

        while(!current.isEmpty()){
            Queue<TreeNode> parents = current;
            current = new LinkedList<>();
            List<Integer> vals = new ArrayList<>();

            for (TreeNode parent : parents) {
                vals.add(parent.getKey());

                if (parent.left != null) {
                    current.add(parent.left);
                }

                if (parent.right != null) {
                    current.add(parent.right);
                }
            }

            result.add(vals);
        }

        return result;
    }
}
