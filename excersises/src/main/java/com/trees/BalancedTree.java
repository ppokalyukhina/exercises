package com.trees;

public class BalancedTree {
    private boolean isBalanced(TreeNode root) {
        return checkHeight(root) != Integer.MIN_VALUE;
    }

    private int checkHeight(TreeNode root) {
        if (root == null) {
            return -1;
        }

        int leftHeight = checkHeight(root.left);
        if(leftHeight == Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }

        int rightHeight = checkHeight(root.right);
        if(rightHeight == Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        int result = leftHeight - rightHeight;
        if(Math.abs(result) > 1) {
            return Integer.MIN_VALUE;
        } else {
            return result;
        }
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(0);
        TreeNode one = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        TreeNode four = new TreeNode(4);
        TreeNode five = new TreeNode(5);
        TreeNode six = new TreeNode(6);
        TreeNode seven = new TreeNode(7);
        TreeNode eight = new TreeNode(8);

        // balanced
        root.right = two;
        root.left = one;

        one.left = three;
        one.right = four;

        two.left = five;
        two.right = six;

        three.left = seven;

        // with addition not balanced
        seven.left = eight;

        BalancedTree balancedTree = new BalancedTree();
        System.out.println(balancedTree.isBalanced(root));
    }
}
