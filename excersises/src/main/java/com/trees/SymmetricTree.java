package com.trees;

public class SymmetricTree {
    public static void main(String[] args) {
        SymmetricTree tree = new SymmetricTree();

        TreeNode root = new TreeNode(2);
        var twoLeft = new TreeNode(3);
        var twoRight = new TreeNode(3);
        var threeLeft = new TreeNode(4);
        var threeRight = new TreeNode(5);
        var fourRight = new TreeNode(4);

        root.left = twoLeft;
        root.right = twoRight;
        twoLeft.right = threeRight;
        twoLeft.left = threeLeft;
        twoRight.right = fourRight;

        System.out.println(tree.isSymmetric(root));
    }

    private boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }

        return traverse(root, root);
    }

    private boolean traverse(TreeNode leftRoot, TreeNode rightRoot) {
        if (leftRoot == null && rightRoot == null) {
            return true;
        }

        if (leftRoot == null || rightRoot == null) {
            return false;
        }

        return leftRoot.getKey() != rightRoot.getKey() && traverse(leftRoot.left, rightRoot.right) && traverse(leftRoot.right, rightRoot.left);
    }
}
