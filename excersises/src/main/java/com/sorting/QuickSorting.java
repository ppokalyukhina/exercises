package com.sorting;

// O(logN) time, O(N) space
public class QuickSorting {
    public static void main(String[] args) {
        int[] givenarr = {4, 2, 7, 5, 10, 3};
        int lowIndex = 0;
        int highIndex = givenarr.length - 1;

        quickSort(givenarr, lowIndex, highIndex);

        for (int i : givenarr) {
            System.out.println(i);
        }
    }

    // pivot is always last element in array
    private static void quickSortWithLastElemAsPivot(int[] array, int lowerIndex, int higherIndex) {
        int pivot = array[higherIndex];
        int leftPointer = lowerIndex;
        int rightPointer = higherIndex;

        while(leftPointer <= rightPointer) {
            while (array[leftPointer] < pivot) {
                leftPointer++;
            }

            while (array[rightPointer] > pivot) {
                rightPointer--;
            }

            if (leftPointer <= rightPointer) {
                swap(leftPointer, rightPointer, array);
                leftPointer++;
                rightPointer--;
            }
        }

        if (lowerIndex < rightPointer)
            quickSort(array, lowerIndex, rightPointer);

        if (higherIndex > leftPointer)
            quickSort(array, leftPointer, higherIndex);
    }

    private static void sortPivotMedian(int[] arr, int lowerIndex, int higherIndex) {
        int midIndex = lowerIndex + higherIndex >>> 1;
        int pivot = arr[midIndex];
        int leftPointer = lowerIndex;
        int rightPointer = higherIndex;

        while(leftPointer <= rightPointer) {
            while(arr[leftPointer] < pivot) {
                leftPointer++;
            }

            while(arr[rightPointer] > pivot) {
                rightPointer--;
            }

            if (leftPointer <= rightPointer) {
                swap(leftPointer, rightPointer, arr);
                leftPointer++;
                rightPointer--;
            }
        }

        // check that right pointer didn't ge to the first element
        if (rightPointer > lowerIndex) {
            sortPivotMedian(arr, lowerIndex, rightPointer);
        }

        // check that right pointer is lower than original index
        if (leftPointer < higherIndex) {
            sortPivotMedian(arr, leftPointer, higherIndex);
        }
    }

    /**
     * Picking always mid as pivot
     */
    private static void quickSort(int[] givenArr, int lowerIndex, int higherIndex) {
        int midIndex = lowerIndex + higherIndex >>> 1;
        int pivot = givenArr[midIndex]; // median
        int leftPointer = lowerIndex; // first element of left side of arr from pivot
        int rightPointer = higherIndex; // last element of right side from pivot

        while(leftPointer <= rightPointer) {
            // as long as elements on left side are smaller than pivot
            while(givenArr[leftPointer] < pivot) {
                // move on on left side
                leftPointer++;
            }

            // as long as elements on right side are bigger than pivot
            while(givenArr[rightPointer] > pivot) {
                // move left closer to pivot
                rightPointer--;
            }

            // if last picked value on left side is > pivot
            // or last picked value on right side is < pivot
            // and as long as left pointer is before right pointer
            // swap the elements, and move on, compare remaining values with pivot again
            if (leftPointer <= rightPointer) {
                int tmp = givenArr[leftPointer];
                givenArr[leftPointer] = givenArr[rightPointer];
                givenArr[rightPointer] = tmp;

                // move pointers towards mid
                leftPointer++;
                rightPointer--;
            }
        }

        // after while loop is done, it means that left pointer moved to the right side of mid
        // and right pointer moved to the left side of the mid
        // as long as left pointer is smaller than high index
        if(leftPointer < higherIndex) {
            // call quick sort recursively again
            quickSort(givenArr, leftPointer, higherIndex);
        }

        // as long as right index is bigger than lower index
        // call the function again recursively
        if(rightPointer > lowerIndex) {
            quickSort(givenArr, lowerIndex, rightPointer);
        }
    }

    private static void swap(int firstIndex, int secondIndex, int[] arr) {
        int tmp = arr[firstIndex];
        arr[firstIndex] = arr[secondIndex];
        arr[secondIndex] = tmp;
    }
}
