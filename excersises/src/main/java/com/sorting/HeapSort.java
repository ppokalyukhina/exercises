package com.sorting;

public class HeapSort {
    /**
     * 1. Build max heap from array -> start with index (arrSize/2 - 1)
     * 2. Swap first and last elements in array
     * 3. Repeat heapify with root = 0 and n-- (reduce size of array on each iteration)
     */
    private static void heapSort(int[] givenArr, int arrSize) {
        int root = arrSize/2 - 1;
        while(root >= 0) {
            // build max heap
            buildMaxHeap(givenArr, arrSize, root);
            root--;
            // go to previous element in array and repeat max heap
        }

        // heap is already built
        // swap first and last elements
        // repeat heapify in reduced by 1 array
        int size = arrSize - 1;
        while(size >= 0) {
            int tmpLastElem = givenArr[size];
            givenArr[size] = givenArr[0];
            givenArr[0] = tmpLastElem;

            buildMaxHeap(givenArr, size, 0);
            size--;
        }
    }

    private static void buildMaxHeap(int[] givenArr, int arrSize, int root) {
        // find children
        // check if given root is still larger than children
        // if not, swap with largest
        int largest = root;
        int left = 2*largest + 1;
        int right = 2*largest + 2;

        if (left < arrSize && givenArr[left] > givenArr[largest]) {
            largest = left;
        }

        if (right < arrSize && givenArr[right] > givenArr[largest]) {
            largest = right;
        }

        if (largest != root) {
            // swap elements
            int largestTmp = givenArr[largest];
            givenArr[largest] = givenArr[root];
            givenArr[root] = largestTmp;

            // repeat heapify on largest element
            buildMaxHeap(givenArr, arrSize, largest);
        }
    }

    public static void main(String[] args) {
        int[] arr = {5,2,3,1};

        heapSort(arr, arr.length);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
