package com.sorting;

public class MergeSortedArrays {
    private static int[] merge(int[] firstArr, int[] secondArr) {
        // O(n) time and space
        var finalArr = new int[firstArr.length + secondArr.length];

        int indexFinalArr = 0;
        int indexFirstArr = 0;
        int indexSecondArr = 0;
        while(indexFinalArr < finalArr.length) {
            while(indexFirstArr < firstArr.length && indexSecondArr < secondArr.length) {
                if (firstArr[indexFirstArr] < secondArr[indexSecondArr]) {
                    finalArr[indexFinalArr] = firstArr[indexFirstArr];
                    indexFinalArr++;
                    indexFirstArr++;
                } else if (firstArr[indexFirstArr] > secondArr[indexSecondArr]) {
                    finalArr[indexFinalArr] = secondArr[indexSecondArr];
                    indexFinalArr++;
                    indexSecondArr++;
                } else if (firstArr[indexFirstArr] == secondArr[indexSecondArr]) {
                    finalArr[indexFinalArr++] = firstArr[indexFirstArr];
                    finalArr[indexFinalArr] = secondArr[indexSecondArr];
                    indexFinalArr++;
                    indexFirstArr++;
                    indexSecondArr++;
                }
            }

            if (indexFirstArr < firstArr.length) {
                for (int j = indexFirstArr; j < firstArr.length; j++) {
                    finalArr[indexFinalArr] = firstArr[j];
                    indexFinalArr++;
                }
            } else if (indexSecondArr < secondArr.length) {
                for (int j = indexSecondArr; j < secondArr.length; j++) {
                    finalArr[indexFinalArr] = secondArr[j];
                    indexFinalArr++;
                }
            }
        }

        return finalArr;
    }

    public static void main(String[] args) {
        int[] firstArr = new int[]{3, 4};
        int[] secondArr = new int[]{1, 5, 7, 9};

        int[] merged = merge(firstArr, secondArr);

        for (int i : merged) {
            System.out.println(i);
        }
    }
}
