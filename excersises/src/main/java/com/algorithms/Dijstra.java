package com.algorithms;

/**
 * Given graph with vertices and distances between them
 * Find shortest paths from given vertix to the rest
 */
public class Dijstra {
    // N of vertices
    private static final int vertices = 9;

    public void dijstra(int[][] givenGraph, int start) {
        // will include only elements from shortest paths
        Boolean[] sptSet = new Boolean[vertices];

        // shortest paths output
        int[] distances = new int[vertices];

        for (int i = 0; i < vertices; i++) {
            // initialize all distances as max value
            distances[i] = Integer.MAX_VALUE;
            // not yet known which vertices will be true in sptSet
            sptSet[i] = false;
        }

        distances[start] = 0;

        for (int count = 0; count < vertices - 1; count++) {
            // Pick the minimum distance vertex from the set of vertices
            // not yet processed. u is always equal to src in first
            // iteration.
            int u = minDistance(distances, sptSet);

            // Mark the picked vertex as processed
            sptSet[u] = true;

            // Update dist value of the adjacent vertices of the
            // picked vertex.
            for (int v = 0; v < vertices; v++)

                // Update dist[v] only if is not in sptSet, there is an
                // edge from u to v, and total weight of path from src to
                // v through u is smaller than current value of dist[v]
                if (!sptSet[v] && givenGraph[u][v] != 0 &&
                        distances[u] != Integer.MAX_VALUE && distances[u] + givenGraph[u][v] < distances[v])
                    distances[v] = distances[u] + givenGraph[u][v];
        }

        // print the constructed distance array
       printSolution(distances);
}

    void printSolution(int dist[])
    {
        System.out.println("Vertex   Distance from Source");
        for (int i = 0; i < vertices; i++)
            System.out.println(i + " tt " + dist[i]);
    }

    private int minDistance(int dist[], Boolean sptSet[])
    {
        // Initialize min value
        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < vertices; v++)
            if (!sptSet[v] && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }

    public static void main(String[] args) {
        int[][] graph = new int[][]{{0, 4, 0, 0, 0, 0, 0, 8, 0},
                {4, 0, 8, 0, 0, 0, 0, 11, 0},
                {0, 8, 0, 7, 0, 4, 0, 0, 2},
                {0, 0, 7, 0, 9, 14, 0, 0, 0},
                {0, 0, 0, 9, 0, 10, 0, 0, 0},
                {0, 0, 4, 14, 10, 0, 2, 0, 0},
                {0, 0, 0, 0, 0, 2, 0, 1, 6},
                {8, 11, 0, 0, 0, 0, 1, 0, 7},
                {0, 0, 2, 0, 0, 0, 6, 7, 0}};

        var dijstra = new Dijstra();
        dijstra.dijstra(graph, 0);
    }
}
