package com.algorithms;

import java.util.*;

public class DijstraWithNodes {
    private int vertices = 6;

    public void findShortestPaths(int[][] graph, int start) {
        Set<Node> shortestPaths = new HashSet<>();
        LinkedList<Node> nodes = new LinkedList<>();

        Node node = new Node(start);
        shortestPaths.add(node);
        nodes.add(node);
        for (int i = start; i < vertices - 1; i++) {
            Node current = nodes.getLast();
            for (int j = start; j < vertices - 1; j++) {
                if (graph[i][j] != 0) {
                    Node visitedNode = new Node(j);
                    if (current.getMin() > graph[i][j] && !visitedNode.isVisited()) {
                        current.setMin(graph[i][j]);
                    }
                }
            }

            current.visit();
            shortestPaths.add(current);
        }



        for (Node shortestPath : shortestPaths) {
            System.out.println("min path from 0 to " + shortestPath.getKey() + " is " +  shortestPath.getMin());
        }
    }

    public void findShortestPaths(Graph graph, int start, int dest) {
        Set<Node> bestPathBetweenStartAndDest = new LinkedHashSet<>();
        Stack<Node> stack = new Stack<>();

        if(graph.getAdjacencies().get(start).getAdjacencies().isEmpty()) {
            System.out.println("No adjacencies found between " + start + " and " + dest);
            return;
        }

        Node current = graph.getAdjacencies().get(start);
        current.setMin(0);
        stack.push(current);
        while (!stack.empty()) {
            current = stack.pop();

            current.visit();
            bestPathBetweenStartAndDest.add(current);

            int currentMin = current.getMin();
            int currentIndex = current.getKey();
            int neighbourMin = Integer.MAX_VALUE;

            if (current.getAdjacencies().isEmpty()) {
                stack.push(graph.getAdjacencies().get(start));
                continue;
            }

            for (Integer neighbourKey : current.getAdjacencies().keySet()) {
                Node neighbour = graph.getAdjacencies().get(neighbourKey);

                if (neighbour.isVisited()) {
                    continue;
                }

                neighbour.setMin(current.getMin() + current.getAdjacencies().get(neighbourKey));

                if (neighbour.getKey() == dest) {
                    bestPathBetweenStartAndDest.add(neighbour);
                    System.out.println(
                            "From " + start + " to " + neighbour.getKey() + " min path is " +
                                    neighbour.getMin());

                    for (Node node : bestPathBetweenStartAndDest) {
                        System.out.println(node.getKey());
                    }

                    return;
                }

                if ((currentMin + neighbour.getMin()) < neighbourMin) {
                    neighbourMin = currentMin + neighbour.getMin();
                    currentIndex = neighbourKey;

                    graph.getAdjacencies().get(currentIndex).setMin(neighbourMin);
                }
            }

            if (!graph.getAdjacencies().get(currentIndex).isVisited()) {
                stack.push(graph.getAdjacencies().get(currentIndex));
            }
        }

        System.out.println("No adjacencies found between " + start + " and " + dest);
    }

    public static void main(String[] args) {
        int[][] givenGraph = new int[][] {
                { 0, 3, 2, 0, 0 },
                { 3, 0, 1, 1, 0 },
                { 2, 1, 0, 0, 4 },
                { 0, 1, 0, 0, 5 },
                { 0, 0, 4, 5, 0 }
        };

        Graph graph = new Graph();
        Node zero = new Node(0);
        Node one = new Node(1);
        Node two = new Node(2);
        Node three = new Node(3);
        Node four = new Node(4);
        // connect nodes
        zero.addAdjacency(one.getKey(), 3);
        zero.addAdjacency(three.getKey(), 2);

        one.addAdjacency(three.getKey(), 1);
        one.addAdjacency(two.getKey(), 4);

        three.addAdjacency(two.getKey(), 5);
        three.addAdjacency(four.getKey(), 7);

        graph.addNode(zero);
        graph.addNode(one);
        graph.addNode(two);
        graph.addNode(three);
        graph.addNode(four);

        var dijstra = new DijstraWithNodes();
        dijstra.findShortestPaths(graph, 1, 4);
    }
}
