package com.algorithms;

import java.util.LinkedList;

public class Graph {
    private LinkedList<Node> adjacencies;
    private Node root;

    public Graph() {
        adjacencies = new LinkedList<>();
        root = null;
    }

    public void addNode(Node node) {
        if (root == null) {
            root = node;
        }

        adjacencies.add(node);
    }

    public LinkedList<Node> getAdjacencies() {
        return adjacencies;
    }
}
