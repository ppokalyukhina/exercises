package com.algorithms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Node {
    private int key;
    private int minDistToSrc;
    private boolean visited;
    private Map<Integer, Integer> adjacencies;

    public Node(int key) {
        this.key = key;
        minDistToSrc = Integer.MAX_VALUE;
        visited = false;

        adjacencies = new HashMap<>();
    }

    public void setMin(int min) {
        minDistToSrc = min;
    }

    public int getMin() {
        return minDistToSrc;
    }

    public int getKey() {
        return key;
    }

    public void visit() {
        visited = true;
    }

    public boolean isVisited() {
        return visited;
    }

    public void addAdjacency(int nodeKey, int dist) {
        adjacencies.put(nodeKey, dist);
    }

    public Map<Integer, Integer> getAdjacencies() {
        return adjacencies;
    }
}
