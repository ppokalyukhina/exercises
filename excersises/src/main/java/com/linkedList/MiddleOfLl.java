package com.linkedList;

import com.excercises.ListNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a non-empty, singly linked list with head node head,
 * return a middle node of linked list.
 *
 * If there are two middle nodes,
 * return the second middle node.
 *
 * The number of nodes in the given list will be between 1 and 100
 */
public class MiddleOfLl {
    public static void main(String[] args) {
        // 1,2,3,4,5,6
        ListNode head = new ListNode(1);
        var two = new ListNode(2);
        head.next = two;
        var three = new ListNode(3);
        two.next = three;
        var four = new ListNode(4);
        three.next = four;
        ListNode five = new ListNode(5);
        four.next = five;
        five.next = new ListNode(6);

        var middle = new MiddleOfLl();
        var res = middle.middleNode(head);
        System.out.println(res.val);
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     *     int val;
     *     ListNode next;
     *     ListNode(int x) { val = x; }
     * }
     */
    public ListNode middleNode(ListNode head) {
        List<ListNode> list = new ArrayList<>();
        ListNode current = head;
        while(current != null) {
            list.add(current);
            current = current.next;
        }

        return list.get(list.size() / 2);
    }

    public ListNode betterSolution(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
