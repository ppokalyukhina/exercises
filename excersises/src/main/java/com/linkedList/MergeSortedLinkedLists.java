package com.linkedList;

import com.adt.PriorityQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MergeSortedLinkedLists {
    private static final Logger logger = LoggerFactory.getLogger(MergeSortedLinkedLists.class);

    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        node1.next = new ListNode(4, new ListNode(5));

        ListNode node2 = new ListNode(1);
        node2.next = new ListNode(3, new ListNode(4));

        var node3 = new ListNode(2, new ListNode(6));

        ListNode[] nodes = {node1, node2, node3};
        var r = mergeKLists(nodes);

        while(r.next != null) {
            logger.info(String.format("next is %d", r.next.val));
        }
    }

    public static ListNode mergeKLists(ListNode[] lists) {
        ListNode sorted = new ListNode();
        java.util.PriorityQueue<Integer> q = new java.util.PriorityQueue<>();

        for (ListNode list : lists) {
            var current = list;
            while (current != null) {
                q.add(current.val);
                current = current.next;
            }
        }


        ListNode previous = sorted;
        while(!q.isEmpty()) {
            previous.next = new ListNode(q.poll());
            previous = previous.next;
        }
        previous.next = null;

        return sorted.next;
    }
}
