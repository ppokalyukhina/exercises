package com.year22;

import java.util.Arrays;

public class MergeSort {
    public static void main(String[] args) {
        int[] input = {5,2,3,1};
        int[] sorted = sortArray(input);

        for (int i : sorted) {
            System.out.println(i);
        }
    }

    public static int[] sortArray(int[] nums) {
        if (nums.length <= 1) {
            return nums;
        }
        int mid = nums.length / 2;

        int[] left = sortArray(Arrays.copyOfRange(nums, 0, mid));
        int[] right = sortArray(Arrays.copyOfRange(nums, mid, nums.length));

        return merge(left, right);
    }

    private static int[] merge(int[] left, int[] right) {
        int[] sorted = new int[left.length + right.length];

        int leftPointer = 0;
        int rightPointer = 0;
        int sortedPointer = 0;
        while(leftPointer < left.length && rightPointer < right.length) {
            int leftVal = left[leftPointer];
            int rightVal = right[rightPointer];

            if (leftVal <= rightVal) {
                sorted[sortedPointer++] = leftVal;
                leftPointer++;
            } else {
                sorted[sortedPointer++] = rightVal;
                rightPointer++;
            }
        }

        // remaining elements in left subarray
        while (leftPointer < left.length) {
            sorted[sortedPointer++] = left[leftPointer++];
        }

        // remaining elements in right subarray
        while (rightPointer < right.length) {
            sorted[sortedPointer++] = right[rightPointer++];
        }

        return sorted;
    }
}
