package com.year22;

public class SearchInSortedMatrix {
    public static void main(String[] args) {
        SearchInSortedMatrix searchMatrix = new SearchInSortedMatrix();

        int[][] input = {
                {1, 4, 7, 11, 15},
                {2, 5, 8, 12, 19},
                {3, 6, 9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}
        };

//        int[][] input = {{25}};
        int target = 25;
        System.out.println("Target was found: " + searchMatrix.searchMatrix1(input, target));

    }

    public boolean searchMatrix1(int[][] matrix, int target) {
        return doSearch(matrix, 0, matrix[0].length - 1,  target);
    }

    private boolean doSearch(int[][] matrix, int row, int col, int target) {
        if (row >= matrix.length || col < 0) {
            return false;
        }

        if (matrix[row][col] == target) {
            return true;
        }

        if (matrix[row][col] > target) {
            col--;
        } else {
            row++;
        }

        return doSearch(matrix, row, col, target);
    }
}
