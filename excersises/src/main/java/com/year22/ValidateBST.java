package com.year22;

public class ValidateBST {
    public static void main(String[] args) {
        ValidateBST validateBST = new ValidateBST();
        TreeNode root = new TreeNode(3);
        TreeNode one = new TreeNode(1);
        TreeNode four = new TreeNode(4);
        TreeNode three = new TreeNode(3);

        root.left = one;
        root.right = four;
        four.right = three;

        System.out.println(validateBST.isValidBST(root));
    }

    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public boolean isValidBST(TreeNode root, long minRange, long maxRange) {
        if (root == null) {
            return true;
        }

        if (root.val >= maxRange || root.val <= minRange) {
            return false;
        }

        return isValidBST(root.left, minRange, root.val) && isValidBST(root.right, root.val, maxRange);
    }
}
