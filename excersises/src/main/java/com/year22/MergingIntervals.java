package com.year22;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MergingIntervals {
    public static void main(String[] args) {
        int[][] input = {
                {1, 2},
                {3, 5},
                {4, 7},
                {6, 8},
                {9, 10}
        };

        int[][] mergedIntervals = mergeOverlappingIntervals(input);
        for (int[] mergedInterval : mergedIntervals) {
            System.out.println("[" + mergedInterval[0] + ", " + mergedInterval[1] + "]");
        }
    }

    // O(Nlog(N)) time -> sorting+
    // O(N) space -> local array to be converted
    public static int[][] mergeOverlappingIntervals(int[][] intervals) {
        if(intervals.length <= 1) {
            return intervals;
        }

        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));

        List<Integer[]> result = new ArrayList<>();
        result.add(new Integer[]{intervals[0][0], intervals[0][1]});
        for (int i = 1; i < intervals.length; i++) {
            int resultIndex = result.size() - 1;
            Integer[] previous = result.get(resultIndex);
            int[] current = intervals[i];

            if (previous[1] < current[0]) {
                result.add(new Integer[]{current[0], current[1]});
                continue;
            }

            result.set(resultIndex, new Integer[]{previous[0], Math.max(previous[1], current[1])});
        }

        // convert result
        int[][] converted = new int[result.size()][2];
        int convertedIdx = 0;
        for (Integer[] resultNums : result) {
            converted[convertedIdx][0] = resultNums[0];
            converted[convertedIdx][1] = resultNums[1];
            convertedIdx++;
        }

        return converted;
    }
}
