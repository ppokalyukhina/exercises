package com.year22;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElement {
    public static void main(String[] args) {
        int[] inputArr = {2, 5, -3, -4, 6, 7, 2};
        var result = nextGreaterElement(inputArr);

        for (int i : result) {
            System.out.println(i);
        }
    }

    public static int[] nextGreaterElement(int[] array) {
        int[] result = new int[array.length];
        Arrays.fill(result, -1);
        Stack<Integer> stack = new Stack<>();

        for (int idx = 0; idx < 2 * array.length; idx++) {
            int currentIdx = idx % array.length;
            int currentVal = array[currentIdx];
            while(!stack.isEmpty() && array[stack.peek()] < currentVal) {
                var resultIndex = stack.pop();
                result[resultIndex] = currentVal;
            }

            stack.push(currentIdx);
        }

        return result;
    }
}
