package com.year22;

import java.util.HashMap;
import java.util.Map;

public class ClimbStairs {
    public static void main(String[] args) {
        int n = 4;
        System.out.println(climbStairs(n));
    }

    /**
     * One can make only 1 or 2 long steps at a time
     */
    public static int climbStairs(int n) {
        // key -> Nth step
        // value -> N of ways to get there
        return calculate(new HashMap<>(), 0, n, 0);
    }

    private static int calculate(Map<Integer, Integer> cache, int currentStair, int targetStair, int waysToGetHere) {
        if (currentStair == targetStair + 1) {
            return waysToGetHere;
        }

        if (cache.containsKey(currentStair)) {
            return cache.get(currentStair);
        }

        int waysToGetHereNew;
        if (currentStair <= 1) {
            waysToGetHereNew = 1;
        } else {
            waysToGetHereNew = calculate(cache, currentStair - 1, targetStair, waysToGetHere)
                    + calculate(cache, currentStair - 2, targetStair, waysToGetHere);
        }

        cache.put(currentStair, waysToGetHereNew);
        return calculate(cache, currentStair + 1, targetStair, waysToGetHereNew);
    }
}
