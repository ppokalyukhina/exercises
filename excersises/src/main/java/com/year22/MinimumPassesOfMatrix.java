package com.year22;

import java.util.*;

public class MinimumPassesOfMatrix {
    public static void main(String[] args) {
//        int[][] matrix = {
//                {0, -1, -3, 2, 0},
//                {1, -2, -5, -1, -3},
//                {3, 0, 0, -4, -1}
//        };


//        int[][] matrix = {
//                {-1, -1},
//                {- 1, -1}
//        };

        int[][] matrix = {
                {1, 0, 0, -2, -3},
                {-4, -5, -6, -2, -1},
                {0, 0, 0, 0, -1},
                {1, 2, 3, 0, -2}
        };

        int minTries = minimumPassesOfMatrix(matrix);
        System.out.println(minTries);
    }

    public static int minimumPassesOfMatrix(int[][] matrix) {
        int minimumPasses = 0;
        Queue<int[]> queue = new LinkedList<>();

        collectPositivesCoordinates(matrix, queue);

        while(!queue.isEmpty()) {
            Queue<int[]> converted = queue;
            queue = new LinkedList<>();

            while(!converted.isEmpty()) {
                int[] currentCoords = converted.poll();
                List<int[]> neighboursCoordinates = collectNeighbours(currentCoords, matrix);

                for (int[] currentNeighbour: neighboursCoordinates) {
                    int row = currentNeighbour[0];
                    int col = currentNeighbour[1];

                    matrix[row][col] *= -1;
                    queue.add(new int[]{row, col});
                }
            }

            minimumPasses++;
        }

        return matrixContainsNegatives(matrix) ? -1 : minimumPasses - 1;
    }

    private static List<int[]> collectNeighbours(int[] currentCoords, int[][] matrix) {
        List<int[]> neighboursCoordinates = new ArrayList<>();
        int row = currentCoords[0];
        int col = currentCoords[1];
        if (matrix[row][col] == 0) {
            return Collections.emptyList();
        }

        // left n
        if (col > 0 && matrix[row][col - 1] < 0) {
            neighboursCoordinates.add(new int[]{row, col - 1});
        }

        // right n
        if (col < matrix[0].length - 1 && matrix[row][col + 1] < 0) {
            neighboursCoordinates.add(new int[]{row, col + 1});
        }
        // top n
        if (row > 0 && matrix[row - 1][col] < 0) {
            neighboursCoordinates.add(new int[]{row - 1, col});
        }
        // bottom n
        if (row < matrix.length - 1 && matrix[row + 1][col] < 0) {
            neighboursCoordinates.add(new int[]{row + 1, col});
        }

        return neighboursCoordinates;
    }

    private static void collectPositivesCoordinates(int[][] matrix, Queue<int[]> queue) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if(matrix[row][col] > 0) {
                    queue.add(new int[]{row, col});
                }
            }
        }
    }

    private static boolean matrixContainsNegatives(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (ints[col] < 0) {
                    return true;
                }
            }
        }

        return false;
    }
}
