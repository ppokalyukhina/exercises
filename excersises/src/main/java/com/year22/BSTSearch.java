package com.year22;

public class BSTSearch {
    public static void main(String[] args) {
        var root = new TreeNode(4);
        var two = new TreeNode(2);
        var seven = new TreeNode(7);
        var one = new TreeNode(1);
        var three = new TreeNode(3);

        root.left = two;
        root.right = seven;
        two.left = one;
        two.right = three;

        int val = 2;

        TreeNode searchBST = searchBST(root, val);
    }

    public static TreeNode searchBST(TreeNode root, int val) {
        return search(root, val);
    }

    private static TreeNode search(TreeNode root, int val) {
        if (root == null) {
            return null;
        }

        // base case
        if(root.val == val) {
            return root;
        }

        if (root.val < val) {
            return search(root.right, val);
        }

        return search(root.left, val);
    }
}
