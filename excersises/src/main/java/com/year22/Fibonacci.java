package com.year22;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
    public static void main(String[] args) {
        int n = 2;
        System.out.println(fib(n));
    }

    public static int fib(int n) {
        Map<Integer, Integer> cache = new HashMap<>();
        return calculate(cache, n);
    }

    private static int calculate(Map<Integer, Integer> cache, int n) {
        if (cache.containsKey(n)) {
            return cache.get(n);
        }

        int result;
        if (n < 2) {
            result = n;
        } else {
            result = calculate(cache, n - 1) + calculate(cache, n - 2);
        }

        cache.put(n, result);
        return result;
    }
}
