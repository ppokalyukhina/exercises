package com.year22;

public class QuickSort {
    public static void main(String[] args) {
        int[] input = {1, 5, 3, 2, 8, 7, 6, 4};
        quickSort(input);

        for (int i : input) {
            System.out.println(i);
        }
    }

    public static void quickSort(int [] list) {
        doSort(list, 0, list.length - 1);
    }

    private static void doSort(int[] list, int from, int to) {
        if (from >= to) {
            return;
        }

        int partition = getPartition(list, from, to);
        // sort left subarray
        doSort(list, from, partition - 1);
        // sort right subarray
        doSort(list, partition + 1, to);
    }

    private static int getPartition(int[] list, int from, int to) {
        // choose last element as pivot
        int pivot = list[to];
        int partitionIndex = from - 1;
        int currentPointer = from;

        // iterate through elements
        // swap the low and high values
        while(currentPointer < to) {
            if(list[currentPointer] <= pivot) {
                partitionIndex++;
                int tmpLow = list[partitionIndex];
                list[partitionIndex] = list[currentPointer];
                list[currentPointer] = tmpLow;
            }
            currentPointer++;
        }

        // swap pivot with the first higher element
        list[to] = list[++partitionIndex];
        list[partitionIndex] = pivot;

        return partitionIndex;
    }
}
