package com.year22;

public class ReverseLinkedList {
    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        ListNode two = new ListNode(2);
        ListNode three = new ListNode(3);
        ListNode four = new ListNode(4);
        ListNode five = new ListNode(5);

        root.next = two;
        two.next = three;
        three.next = four;
        four.next = five;

        var reversed = reverseList(root);
    }

    public static ListNode reverseList(ListNode head) {
        return reverse(null, head);
    }

    private static ListNode reverse(ListNode reversed, ListNode initial) {
       if (initial == null) {
           return reversed;
       }
       var next = initial.next;
       initial.next = reversed;

       return reverse(initial, next);
    }
}
