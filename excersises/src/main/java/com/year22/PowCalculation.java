package com.year22;

public class PowCalculation {
    public static void main(String[] args) {
        double x = 2.10000;
        int n = 3; // return 9.26100

        x = 2.00000;
        n = -2; // return 0.25000

        x = 0.00001;
        n = 2147483647;

        System.out.println(myPow(x, n));
    }

    public static double myPow(double x, int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 2) {
            return x * x;
        }
        if (n % 2 == 0) {
            return myPow(myPow(x, n / 2), 2);
        }
        else if (n > 0) {
            return x * myPow(myPow(x, n / 2), 2);
        }
        else {
            return 1 / x * myPow(myPow(x, n / 2), 2);
        }
    }
}
