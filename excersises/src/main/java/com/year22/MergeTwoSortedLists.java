package com.year22;

public class MergeTwoSortedLists {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode two = new ListNode(2);
        ListNode four = new ListNode(4);
        head.next = two;
        two.next = four;

        ListNode head2 = new ListNode(1);
        ListNode three = new ListNode(3);
        ListNode four2 = new ListNode(4);
        head2.next = three;
        three.next = four2;

        var merged = mergeTwoListsIterative(head, head2);
        // test
        while(merged != null) {
            System.out.println(merged.val);
            merged = merged.next;
        }
    }

    // Recursion
    public static ListNode mergeTwoListsIterative(ListNode list1, ListNode list2) {
        if (list1 == null) {
            return list2;
        }

        if (list2 == null) {
            return list1;
        }

        ListNode head1 = list1;
        ListNode head2 = list2;
        ListNode previous = null;

        while(head1 != null && head2 != null) {
            if (head1.val < head2.val) {
                previous = head1;
                head1 = head1.next;
            } else {
                ListNode tmpHead2Next = head2.next;
                if(previous != null) {
                    previous.next = head2;
                }
                // point previous
                // to the next element
                previous = head2;
                head2.next = head1;
                head2 = tmpHead2Next;
            }
        }

        if (head1 == null) {
            previous.next = head2;
        }

        return list1.val < list2.val ? list1 : list2;
    }

    // Recursion
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        return null;
    }
}
