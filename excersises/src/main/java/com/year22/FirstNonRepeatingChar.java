package com.year22;

import java.util.HashMap;
import java.util.Map;

public class FirstNonRepeatingChar {
    public static void main(String[] args) {
        String input = "abcdaf";
        int firstNonRepeating = firstNonRepeatingCharacter(input);
        System.out.println(firstNonRepeating);
    }

    public static int firstNonRepeatingCharacter(String string) {
        Map<Character, Integer> charsCount = new HashMap<>();
        // fill in the map
        for (Character stringChar: string.toCharArray()) {
            charsCount.putIfAbsent(stringChar, 0);
            charsCount.put(stringChar, charsCount.get(stringChar) + 1);
        }

        for (int idx = 0; idx < string.length(); idx++) {
            if (charsCount.get(string.charAt(idx)) == 1) {
                return idx;
            }
        }

        return -1;
    }
}
