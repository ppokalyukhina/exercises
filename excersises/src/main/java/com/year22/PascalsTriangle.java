package com.year22;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle {
    public static void main(String[] args) {
        int rowIndex = 4;
        // output should be {1,3,3,1};
        List<Integer> result = getRow(rowIndex);
        for (Integer integer : result) {
            System.out.println(integer);
        }
    }

    public static List<Integer> getRow(int rowIndex) {
        return findRow(new ArrayList<>(), rowIndex, 0);
    }

    private static List<Integer> findRow(List<Integer> previousRow, int targetRow, int currentRow) {
        if (targetRow + 1 == currentRow) {
            return previousRow;
        }

        List<Integer> currentRowVals = new ArrayList<>(previousRow);
        currentRowVals.add(1);

        int currentIdx = 1;
        int previousIdx = 1;
        while(previousIdx < previousRow.size() && currentIdx < currentRowVals.size() - 1) {
            int left = previousRow.get(previousIdx - 1);
            int right = previousRow.get(previousIdx);

            currentRowVals.set(currentIdx, left + right);

            previousIdx++;
            currentIdx++;
        }

        return findRow(currentRowVals, targetRow, currentRow + 1);
    }
}
