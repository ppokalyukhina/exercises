package com.am;

import java.util.*;

public class CloneDirectedGraph {
    public static void main(String[] args) {
        Node root = new Node(1);
        Node two = new Node(2);
        Node three = new Node(3);
        Node four = new Node(4);
        root.neighbors.add(two);
        root.neighbors.add(four);

        two.neighbors.add(root);
        two.neighbors.add(three);

        three.neighbors.add(two);
        three.neighbors.add(four);

        four.neighbors.add(root);
        four.neighbors.add(three);

        var newRoot = cloneGraph(root);
    }

    private static Node cloneGraph(Node node) {
        Node newRoot = new Node(node.val);
        Stack<Node> stack = new Stack<>();
        Map<Node, Node> oldToNew = new HashMap<>();
        Set<Node> visited = new HashSet<>();
        oldToNew.put(node, newRoot);
        stack.push(node);
        while(!stack.isEmpty()) {
            var oldNode = stack.pop();
            if (visited.contains(oldNode)) {
                continue;
            }
            visited.add(oldNode);
            var newNode = oldToNew.get(oldNode);
            var neighbours = oldNode.neighbors;
            for (Node neighbour : neighbours) {
                Node copiedNeighbour;
                if (oldToNew.containsKey(neighbour)) {
                    copiedNeighbour = oldToNew.get(neighbour);
                } else {
                    copiedNeighbour = new Node(neighbour.val);
                    oldToNew.put(neighbour, copiedNeighbour);
                }
                newNode.neighbors.add(copiedNeighbour);
                stack.push(neighbour);
            }
        }
        return newRoot;
    }

    private static class Node {
        public int val;
        public List<Node> neighbors;
        public boolean visited = false;
        public Node() {
            val = 0;
            neighbors = new ArrayList<>();
        }
        public Node(int val) {
            this.val = val;
            neighbors = new ArrayList<>();
        }
        public Node(int val, ArrayList<Node> neighbors) {
            this.val = val;
            this.neighbors = neighbors;
        }
    }
}
