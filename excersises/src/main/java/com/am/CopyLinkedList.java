package com.am;

import java.util.HashMap;
import java.util.Map;

/**
 * You are given a linked list where the node has two pointers.
 * The first is the regular next pointer.
 * The second pointer is called arbitrary_pointer and it can point to any node in the linked list.
 * Your job is to write code to make a deep copy of the given linked list.
 * Here, deep copy means that any operations on the original list should not affect the copied list.
 */
public class CopyLinkedList {
    public static void main(String[] args) {
        Node head = new Node(7);
        Node thirteen = new Node(13);
        Node eleven = new Node(11);
        Node ten = new Node(10);
        Node one = new Node(1);

        head.next = thirteen;
        head.random = null;
        thirteen.next = eleven;
        thirteen.random = head;
        eleven.next = ten;
        eleven.random = one;
        ten.next = one;
        ten.random = eleven;
        one.next = null;
        one.random = head;

        var newHead = copyRandomList(head);
        while(newHead != null) {
            System.out.println(newHead.val);
            newHead = newHead.next;
        }
    }

    private static Node copyRandomList(Node head) {
        Node current = head;
        Node newPrevious = null;
        Node newHead = null;
        Map<Node, Node> oldToNew = new HashMap<>();

        while(current != null) {
            var copied = new Node(current.val);
            if (newPrevious == null) {
                newPrevious = copied;
                newHead = copied;
            } else {
                newPrevious.next = copied;
                newPrevious = newPrevious.next;
            }
            oldToNew.put(current, copied);
            current = current.next;
        }

        // add random nodes with new ones
        Node currentNew = newHead;
        current = head;
        while(currentNew != null && current != null) {
            var random = current.random;
            currentNew.random = oldToNew.get(random);
            currentNew = currentNew.next;
            current = current.next;
        }

        return newHead;
    }

    private static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }
}
