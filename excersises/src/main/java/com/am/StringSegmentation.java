package com.am;

import java.util.Set;

/**
 * You are given a dictionary of words and a large input string.
 * You have to find out whether the input string can be completely segmented into the words of a given dictionary.
 * The following two examples elaborate on the problem further.
 *
 * Given a dictionary of words.
 *
 * apple
 * apple
 * pear
 * pie
 * Input string of “applepie” can be segmented into dictionary words.
 *
 * apple
 * pie
 * Input string “applepeer” cannot be segmented into dictionary words.
 *
 * apple
 * peer
 */
public class StringSegmentation {
    public static void main(String[] args) {
        String s = "applepie";
        Set<String> dictionary = Set.of("apple", "pear", "pie");
        System.out.println(canSegmentString(s, dictionary));
    }

    /**
     * O(2Npow2) time
     * O(Npow2) space (recursion)
     */
    public static boolean canSegmentString(String s, Set<String> dictionary) {
        for (int i = 0; i < s.length(); i++) {
            String firstSubstr = s.substring(0, i);
            if (dictionary.contains(firstSubstr)) {
                String secondSubstr = s.substring(i);
                boolean containsSecondStr = dictionary.contains(secondSubstr);
                if (containsSecondStr || canSegmentString(secondSubstr, dictionary)) {
                    return true;
                }
            }
        }

        return false;
    }
}
