package com.am;

import java.util.*;

public class FindKthPermutation {
    public static void main(String[] args) {
        List<Character> input = List.of('1', '2', '3');
        int k = 2;
        var result = getPermutation(input, k);
        System.out.println(result);
    }

    // educative solution
    private static String getPermutation(List<Character> v, int k) {
        StringBuilder result = new StringBuilder();
        findKthPermutation(v, k, result);
        return result.toString();
    }

    private static void findKthPermutation(List<Character> v, int k, StringBuilder result) {
        if (v.isEmpty()) {
            return;
        }

        int n = v.size();
        // count is number of permutations starting with first digit
        int count = factorial(n - 1);
        int selected = (k - 1) / count;

        result.append(v.get(selected));
        List<Character> copied = new ArrayList<>(v);
        copied.remove(selected);

        k = k - (count * selected);
        findKthPermutation(copied, k, result);
    }

    private static int factorial(int n) {
        if (n == 0 || n == 1) return 1;
        return n * factorial(n - 1);
    }
}
