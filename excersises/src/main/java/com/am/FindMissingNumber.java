package com.am;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * You are given an array of positive numbers from 1 to n,
 * such that all numbers from 1 to n are present except one number x.
 * You have to find x. The input array is not sorted.
 * Look at the below array and give it a try before checking the solution.
 */
public class FindMissingNumber {
    public static void main(String[] args) {
        int[] input = {3, 7, 1, 2, 8, 4, 5};
        System.out.println(getMissingNumberEfficient(input));
    }

    /**
     * O(N) time
     * O(1) space
     */
    private static int getMissingNumberEfficient(int[] arr) {
        // multiply values by -1
        for (int i = 0; i < arr.length; i++) {
            int current = Math.abs(arr[i]);
            if (current > arr.length) {
                continue;
            }

            // e.g. 0 - 1
            arr[current - 1] *= -1;
        }

        // find value which had not been updated
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                return i + 1;
            }
        }

        return -1;
    }

    /**
     * O(NlogN) time
     * O(1) space
     */
    private static int getMissingNumberSort(int[] arr) {
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            int next = arr[i + 1];
            if (next - arr[i] > 1) {
                return arr[i] + 1;
            }
        }

        return -1;
    }

    /**
     * O(N) time
     * O(N) space (Set)
     */
    private static int getMissingNumber(int[] arr) {
        Set<Integer> neighbours = new HashSet<>();
        for (int i : arr) {
            neighbours.add(i);
        }

        for (int i : arr) {
            if (i > 1 && !neighbours.contains(i - 1)) {
                return i - 1;
            }

            if (i <= arr.length && !neighbours.contains(i + 1)) {
                return i + 1;
            }
        }

        return -1;
    }
}
