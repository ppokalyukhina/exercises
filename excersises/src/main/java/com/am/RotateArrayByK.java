package com.am;

public class RotateArrayByK {
    public static void main(String[] args) {
//        int[] input = {1,2,3,4,5,6,7};
        int[] input = {-1,-100, 3, 99};
        int k = 2;
        rotate(input, k);

        for (int i : input) {
            System.out.println(i);
        }
    }

    public static void rotate(int[] nums, int k) {
        if (nums.length == 0 || k == 0) {
            return;
        }

        int maxIterations = gcd(k, nums.length);
        for (int i = 0; i < maxIterations; i++) {
            // always store last value
            int currentIndex = i;
            int currentValue = nums[i];
            while(true) {
                int previousIndex = currentIndex - k;
                if (previousIndex < 0) {
                    previousIndex = nums.length + previousIndex;
                }
                if (previousIndex == i) {
                    break;
                }
                nums[currentIndex] = nums[previousIndex];
                currentIndex = previousIndex;
            }
            nums[currentIndex] = currentValue;
        }
    }

    private static int gcd(int k, int lastElem)
    {
        if (lastElem == 0) {
            return k;
        }
        else {
            return gcd(lastElem, k % lastElem);
        }
    }
}
