package com.am;

import java.util.Stack;

/**
 * Given a Binary Tree, figure out whether it’s a Binary Search Tree.
 * In a binary search tree, each node’s key value is smaller than the key value of all nodes in the right subtree,
 * and is greater than the key values of all nodes in the left subtree.
 */
public class BinaryTreeIsBst {
    public static void main(String[] args) {
        Node root = new Node(100);
        Node fifty = new Node(50);
        Node twoHundred = new Node(200);
        Node twentyFive = new Node(25);
        Node seventyFive = new Node(75);
        Node oneTwentyFive = new Node(125);
        Node threeFifty = new Node(350);

        twoHundred.left = oneTwentyFive;
        twoHundred.right = threeFifty;

        fifty.left = twentyFive;
        fifty.right = seventyFive;

        root.left = fifty;
        root.right = twoHundred;

        System.out.println(isBSTIterative(root));
    }

    /**
     * Recursively
     * O(N) time
     * O(N) space (for calls stack), else no extra space is needed
     */
    private static boolean isBST(Node root) {
        if (root == null) {
            return true;
        }

        if (root.left != null && root.left.value >= root.value) {
            return false;
        }

        if (root.right != null && root.right.value <= root.value) {
            return false;
        }

        var leftValid = isBST(root.left);
        var rightValid = isBST(root.right);

        return leftValid && rightValid;
    }

    /**
     * O(N) time
     * O(N) space (stack)
     */
    private static boolean isBSTIterative(Node root) {
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()) {
            Node current = stack.pop();
            if (current == null) {
                continue;
            }

            if (current.left != null && current.left.value >= current.value) {
                return false;
            }

            if (current.right != null && current.right.value <= current.value) {
                return false;
            }

            stack.push(current.left);
            stack.push(current.right);
        }

        return true;
    }

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int value) {
            this.value = value;
        }
    }
}
