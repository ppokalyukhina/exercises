package com.am;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Given the root of a binary tree, display the node values at each level.
 * Node values for all levels should be displayed on separate lines.
 * Let’s take a look at the below binary tree.
 */
public class LevelOrderTraversalOfTree {

    public static void main(String[] args) {
        Node root = new Node(100);
        Node fifty = new Node(50);
        Node twoHundred = new Node(200);
        Node twentyFive = new Node(25);
        Node seventyFive = new Node(75);
        Node oneTwentyFive = new Node(125);
        Node threeFifty = new Node(350);

        twoHundred.children.add(oneTwentyFive);
        twoHundred.children.add(threeFifty);

        fifty.children.add(twentyFive);
        fifty.children.add(seventyFive);

        root.children.add(fifty);
        root.children.add(twoHundred);

        System.out.println(levelOrderTraversal(root));
    }

    private static String levelOrderTraversal(Node root) {
        StringBuilder builder = new StringBuilder();
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()) {
            Node current = queue.poll();
            builder.append(current.value).append("\n");

            var children = current.children;
            queue.addAll(children);
        }

        return builder.toString();
    }

    private static class Node {
        int value;
        List<Node> children;

        public Node(int value) {
            this.value = value;
            children = new ArrayList<>();
        }
    }
}
