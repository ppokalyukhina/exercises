package com.am;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Given an array of integers and a value,
 * determine if there are any two integers in the array whose sum is equal to the given value.
 * Return true if the sum exists and return false if it does not.
 * Consider this array and the target sums:
 * 5
 * 7
 * 1
 * 2
 * 8
 * 4
 * 3
 */
public class TwoSumNumber {

    public static void main(String[] args) {
        int[] input = {5, 7, 1, 2, 8, 4, 3};
        int sum = 10;
        System.out.println(findSum(input, sum));
    }

    /**
     * O(NlogN) time
     * O(1) space
     */
    private static boolean findSum(int[] input, int val) {
        Arrays.sort(input);
        int left = 0;
        int right = input.length - 1;
        while(left < right) {
            int smallerVal = input[left];
            int higherVal = input[right];
            if (smallerVal + higherVal == val) {
                return true;
            }

            if (smallerVal + higherVal < val) {
                left++;
            }

            else if (smallerVal + higherVal > val) {
                right--;
            }
        }

        return false;
    }

    /**
     * O(N) time
     * O(N) space
     */
    private static boolean findSumOfTwo(int[] A, int val) {
        Set<Integer> set = new HashSet<>();
        for (int i : A) {
            int potentialPair = val - i;
            if (set.contains(potentialPair)) {
                return true;
            }
            set.add(i);
        }

        return false;
    }
}
