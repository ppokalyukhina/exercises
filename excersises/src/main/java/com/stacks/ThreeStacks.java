package com.stacks;

public class ThreeStacks {
    private int filledElements;
    private int firstPointer;
    private int secondPointer;
    private int thirdPointer;
    private Integer[] givenArray;

    private int secondPointerStatic;
    private int thirdPointerStatic;

    public ThreeStacks(Integer[] givenArray) {
        filledElements = 0;
        this.firstPointer = 0;
        this.secondPointer = secondPointerStatic = givenArray.length / 3;
        this.thirdPointer = thirdPointerStatic = givenArray.length - secondPointer;
        this.givenArray = givenArray;
    }

    public void push(int element, int stackId) {
        if (isFull()) {
            // throw exception
            System.out.println("The whole arr is full");
        }

        if (stackId == 1) {
            if (givenArray[firstPointer] != null) {
                // throw exception -> full for that arr
                System.out.println("This array is already full");
            }

            givenArray[firstPointer] = element;
            if (firstPointer < secondPointerStatic - 1) {
                firstPointer++;
            }
        }

        else if(stackId == 2) {
            if (givenArray[secondPointer] != null) {
                // throw exception -> full for that arr
                System.out.println("This array is already full");
            }

            givenArray[secondPointer] = element;
            if (secondPointer < thirdPointerStatic - 1) {
                secondPointer++;
            }
        }

        else if (stackId == 3) {
            if (givenArray[thirdPointer] != null) {
                // throw exception -> full for that arr
                System.out.println("This array is already full");
            }

            givenArray[thirdPointer++] = element;
        }

        else {
            // throw an exception "only three stacks possible"
        }

        filledElements++;
    }

    public int pop(int stackId) {
        if (isEmpty()) {
            // throw an exception
            System.out.println("Arr is empty!");
        }

        int lastElem = givenArray[firstPointer];

        if (stackId == 1) {
            givenArray[firstPointer--] = null;
        }

        else if (stackId == 2) {
            if (givenArray[secondPointer] == null) {
                // throw exception -> full for that arr
                System.out.println("This array is already empty");
            }

            lastElem = givenArray[secondPointer];
            givenArray[secondPointer--] = null;
        }

        else if (stackId == 3) {
            if (givenArray[thirdPointer] == null) {
                // throw exception -> full for that arr
                System.out.println("This array is already empty");
            }

            lastElem = givenArray[thirdPointer];
            givenArray[thirdPointer--] = null;
        }

        else {
            // only 3 stacks allowed
        }

        return lastElem;
    }

    public boolean isEmpty() {
        return filledElements == 0;
    }

    public boolean isFull() {
        return  filledElements == givenArray.length;
    }

    public static void main(String[] args) {
        Integer[] givenArr = new Integer[6];

        ThreeStacks stacks = new ThreeStacks(givenArr);

        stacks.push(1, 1);
        stacks.push(2, 2);
        stacks.push(3, 3);
        stacks.push(6, 1);
        stacks.pop(1);

        for (Integer integer : givenArr) {
            System.out.println(integer);
        }
    }
}
