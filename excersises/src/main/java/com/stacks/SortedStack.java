package com.stacks;

/**
 * Classic stack, but sorted asc. The smallest number appears on the top.
 * pop & peek -> O(1)
 * push -> O(n) worse case scenario
 */
public class SortedStack {
    private int[] stack;
    private int size;

    public SortedStack(int capacity) {
        stack = new int[capacity];
        size = 0;
    }

    public void push(int element) {
        if (isFull()) {
            // throw an exception
        }

        if (isEmpty()) {
            stack[size++] = element;
        }

        else if(element <= stack[size - 1]) {
            // regular update of stack
            stack[size++] = element;
        }

        else {
            pushElementItoRightPos(element);
        }
    }

    private void pushElementItoRightPos(int element) {
        stack[size++] = element;
        int pointer = size - 1;
        while(pointer - 1 >= 0) {
            if(stack[pointer] < stack[pointer - 1]) {
                break;
            }

            int smaller = stack[pointer - 1];
            stack[pointer - 1] = stack[pointer];
            stack[pointer] = smaller;

            pointer--;
        }
    }

    public int pop() {
        int lastElem = stack[size];
        stack[size--] = 0;

        return lastElem;
    }

    public int peek() {
        return stack[size];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == stack.length;
    }

    public static void main(String[] args) {
        SortedStack sortedStack = new SortedStack(10);

        sortedStack.push(2);
        sortedStack.push(1);
        sortedStack.push(6);

        for (int i : sortedStack.stack) {
            System.out.println(i);
        }
    }
}
