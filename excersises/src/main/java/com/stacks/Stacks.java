package com.stacks;

public class Stacks {
    public static void main(String[] args) {
        int[] givenArr = new int[10];

        Stack firstStack = new Stack();
        firstStack.push(1);
        firstStack.push(2);
        firstStack.push(3);

        Stack secondStack = new Stack();
        secondStack.push(4);
        secondStack.push(5);
        secondStack.push(6);
    }

    private static void createTwoStacksInOneArray(int[] givenArr, Stack firstStack, Stack secondStack) {
        int firstPointer = 0;
        int lastPointer = givenArr.length - 1;

        while(firstPointer < lastPointer) {
            if (!firstStack.isEmpty()) {
                givenArr[firstPointer] = firstStack.pop();
            }

            if (!secondStack.isEmpty()) {
                givenArr[lastPointer] = secondStack.pop();
            }

            firstPointer++;
            lastPointer--;
        }
    }

    private static void threeStacksInOneArray(int[] givenArr, Stack first, Stack second, Stack third) {
        int firstPointer = 0;
        int secondPointer = givenArr.length / 3; // inclusive
        int thirdPointer = givenArr.length - secondPointer;
    }
}
