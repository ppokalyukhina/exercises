package com.stacks;

public class MinStack {
    private Integer[] arr;
    private int size;
    private int maxSize;

    /**
     * initialize your data structure here.
     */
    public MinStack() {
        size = 0;
        maxSize = 10;
        arr = new Integer[maxSize];
    }

    public void push(int x) {
        if (isFull()) {
            return;
        }

        arr[size++] = x;
    }

    public void pop() {
        if (isEmpty()) {
            return;
        }

        arr[size--] = null;
    }

    public int top() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException("The stack is empty...");
        }
        return arr[size - 1];
    }

    public int getMin() {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < size; i++) {
            min = Math.min(arr[i], min);
        }

        return min;
    }

    private boolean isFull() {
        return maxSize == size;
    }

    private boolean isEmpty() {
        return size == 0;
    }

    public static void main(String[] args) {
        var minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.push(-2);

        minStack.pop();
        minStack.pop();

        System.out.println(minStack.top());
        System.out.println(minStack.getMin());
    }
}
