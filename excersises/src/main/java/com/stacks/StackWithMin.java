package com.stacks;


/**
 * Stack which also return min value in O(1) time
 */
public class StackWithMin extends Stack {
    private Stack stack;

    public StackWithMin() {
        stack = new Stack();
    }

    public void push(int element) {
        if(element < getMin()) {
            stack.push(element);
        }
        super.push(element);
    }

    public int pop() {
        int value = super.pop();

        if(value == getMin()) {
            stack.pop();
        }

        return value;
    }

    public int getMin() {
        if(stack.isEmpty()) {
            // throw an exception
            return Integer.MAX_VALUE;
        }

        return stack.peek();
    }

    public static void main(String[] args) {
        StackWithMin stackWithMin = new StackWithMin();

        stackWithMin.push(3);
        stackWithMin.push(1);
        stackWithMin.push(9);

        stackWithMin.pop();
        stackWithMin.pop();

        System.out.println(stackWithMin.getMin());
    }

}
