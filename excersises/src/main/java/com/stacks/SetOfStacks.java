package com.stacks;

public class SetOfStacks {
    private int capacityPerStack;
    private int sizeOfSet;
    private Stack[] set;
    private int stacksCapacity;

    public SetOfStacks(int capacityPerStack) {
        this.capacityPerStack = capacityPerStack;
        sizeOfSet = 0;
        stacksCapacity = 2;
        set = new Stack[stacksCapacity];
    }

    public void push(int element) {
        if (stacksCapacity == sizeOfSet) {
            resizeSet();
        }

        if (isSetEmpty()) {
            Stack stack = new Stack();
            stack.push(element);
            set[sizeOfSet++] = stack;

            return;
        }

        Stack lastStack = peek();
        if (isCurrentStackFull(lastStack)) {
            Stack nextStack = new Stack();
            nextStack.push(element);

            set[sizeOfSet++] = nextStack;

            return;
        }

        lastStack.push(element);
    }

    private void resizeSet() {
        int currentSetSize = set.length;
        int newSize = set.length * 2;

        Stack[] newSet = new Stack[newSize];

        for(int i = 0; i < set.length; i++) {
            newSet[i] = set[i];
        }

        set = newSet;
    }

    public int pop() {
        if (isSetEmpty()) {
            // throw an exception..
        }

        Stack lastStack = set[sizeOfSet];
        int lastElem = lastStack.pop();
        if (lastStack.isEmpty()) {
            set[sizeOfSet--] = null;
        }

        return lastElem;
    }

    public int popAt(int index) {
        if (index >= sizeOfSet || index < 0) {
            // throw an exception..,.
            // "given index should be smaller < sizeOfSet"
        }

        int removedElem = 0;
        for (int i = 0; i < sizeOfSet; i++) {
            if (i == index) {
                removedElem = set[i].pop();
            }

            if (set[i].isEmpty()) {
                moveAllStacks(i + 1);
            }
        }

        return removedElem;
    }

    public Stack[] getAllStacks() {
        return set;
    }

    private void moveAllStacks(int fromIndex) {
        for (int i = fromIndex; i < sizeOfSet; i++) {
            Stack lastElem = set[i];
            set[i - 1] = lastElem;
        }

        set[sizeOfSet--] = null;
    }

    public boolean isSetEmpty() {
        return sizeOfSet == 0;
    }

    public Stack peek() {
        return set[sizeOfSet - 1];
    }

    public int getSize() {
        return sizeOfSet;
    }

    private boolean isCurrentStackFull(Stack stack) {
        return stack.getCurrentSize() == capacityPerStack;
    }

    public static void main(String[] args) {
        SetOfStacks set = new SetOfStacks(2);

        set.push(1);
        set.push(3);
        set.push(5);
        set.push(4);

        System.out.println(set.getSize());

        set.popAt(0);

        for (int i = 0; i < set.sizeOfSet; i++) {
            System.out.println(set.set[i].getCurrentSize() + " current size of stack n " + i);
        }

        set.popAt(0);

        System.out.println(set.getSize());
    }
}
