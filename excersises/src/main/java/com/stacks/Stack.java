package com.stacks;

public class Stack {
    private int size = 0;
    private int maxSize;
    private Integer[] arr;

    public Stack(int maxSize) {
        this.maxSize = maxSize;
        arr = new Integer[maxSize];
    }

    public Stack() {
        this.maxSize = 10;
        arr = new Integer[maxSize];
    }

    public void push(int element) {
        if (isFull()) {
            // throw an exception
            System.out.println("Stack is full.. you cannot push anymore");
            return;
        }

        arr[size] = element;
        size++;
    }

    public int pop() {
        if (isEmpty()) {
            // throw an exception
            System.out.println("Stack is empty.. there is nothing to pop");
        }

        int lastElement = arr[size - 1];
        arr[size - 1] = null;

        size--;
        return lastElement;
    }

    public int peek() {
        if (isEmpty()) {
            // throw an exception
        }

        return arr[size - 1];
    }

    public int getCurrentSize() {
        return size;
    }

    public Integer[] getArr() {
        return arr;
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == maxSize;
    }

    public static void main(String[] args) {
        var stack = new Stack();

        stack.push(1);
        stack.push(3);
        stack.push(4);

        stack.pop();

        for (Integer elem : stack.getArr()) {
            System.out.println(elem);
        }
    }
}
