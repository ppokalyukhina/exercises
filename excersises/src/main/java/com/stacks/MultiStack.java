package com.stacks;

public class MultiStack {
    private class StackInfo {
        private int start, capacity, size;

        public StackInfo(int start, int capacity) {
            this.start = start;
            this.capacity = capacity;
        }

        public boolean isWithinStackCapacity(int index) {
            if (index < 0 || index >= values.length) {
                return false;
            }

            int contiguousIndex = index < start ? index + values.length : index;
            int end = start + capacity;

            return start <= contiguousIndex && contiguousIndex < end;
        }

        public int lastElementIndex() {
            return start + capacity - 1;
        }

        public boolean isFull() {
            return size == capacity;
        }

        public boolean isEmpty() {
            return size == 0;
        }
    }

    private int[] values;
    private StackInfo[] info;

    public MultiStack(int numberOfStacks, int defaultSize) {
        info = new StackInfo[numberOfStacks];
        values = new int[defaultSize * numberOfStacks];

        for (int i = 0; i < numberOfStacks; i++) {
            info[i] = new StackInfo(defaultSize * i, defaultSize);
        }
    }

    public void push(int stackNum, int value) {
        if (allStacksAreFull()) {
            // throw an exception..
        }

        StackInfo stack = info[stackNum];
        if (stack.isFull()) {
            expand(stackNum);
        }

        stack.size++;
        values[stack.lastElementIndex()] = value;
    }

    public int pop(int stackNum) {
        StackInfo stack = info[stackNum];
        if (stack.isEmpty()) {
            // throw an exception... it's already empty
        }

        int lastElem = values[stack.lastElementIndex()];
        values[stack.lastElementIndex()] = 0;
        stack.size--;

        return lastElem;
    }

    private boolean allStacksAreFull() {
        return numberOfFilledElements() == values.length;
    }

    private int numberOfFilledElements() {
        int number = 0;
        for (StackInfo stackInfo : info) {
            number += stackInfo.size;
        }

        return number;
    }

    private void expand(int stackNum) {
        shift((stackNum + 1) % info.length);
        info[stackNum].capacity++;
    }

    private void shift(int stackNum) {
        StackInfo stack = info[stackNum];

        if (stack.size >= stack.capacity) {
            int nextStackPosition = (stackNum + 1) % info.length;
            shift(nextStackPosition);
            stack.capacity++;
        }

        int index = stack.lastElementIndex();
        while(stack.isWithinStackCapacity(index)) {
            values[index] = values[previousIndex(index)];
            index = previousIndex(index);
        }

        // adjust stack data
        values[stack.start] = 0;
        stack.start = nextIndex(stack.start);
        stack.capacity--;
    }

    private int previousIndex(int index) {
        return index - 1;
    }

    private int nextIndex(int index) {
        return index + 1;
    }
}
