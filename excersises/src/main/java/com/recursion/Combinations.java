package com.recursion;

import java.util.ArrayList;
import java.util.List;

public class Combinations {
    public static void main(String[] args) {
        int n = 4;
        int k = 3;
        var result = combine(n, k);
        for (List<Integer> integers : result) {
            StringBuilder combination = new StringBuilder();
            combination.append("[");
            for (Integer integer : integers) {
                combination.append(integer).append(',');
            }
            combination.append("]");
            System.out.println(combination.toString());
        }
    }

    private static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> permutations = new ArrayList<>();
        findPermutations(1, n, k, permutations, new ArrayList<>());
        return permutations;
    }

    private static void findPermutations(int start, int end, int maxLength, List<List<Integer>> permutations, List<Integer> currentList) {
        if (currentList.size() == maxLength) {
            permutations.add(currentList);
            return;
        }

        for (int i = start; i <= end; i++) {
            List<Integer> copy = new ArrayList<>(currentList);
            copy.add(i);
            findPermutations(i + 1, end, maxLength, permutations, copy);
        }
    }
}
