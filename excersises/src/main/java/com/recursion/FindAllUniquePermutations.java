package com.recursion;

import java.util.*;

public class FindAllUniquePermutations {
    public static void main(String[] args) {
        int[] input = {1, 1, 3};
        var result = find(input);
        for (List<Integer> integers : result) {
            StringBuilder permutation = new StringBuilder();
            permutation.append('[');
            for (Integer integer : integers) {
                permutation.append(integer).append(',');
            }
            permutation.append(']');
            System.out.println(permutation.toString());
        }
    }

    private static List<List<Integer>> find(int[] nums) {
        if (nums.length == 0) {
            return Collections.emptyList();
        }

        List<List<Integer>> permutations = new ArrayList<>();
        // key -> val from nums
        // value -> how many times repeats in the nums arr
        Map<Integer, Integer> map = new HashMap<>();
        // fill map
        fillUniqueMap(nums, map);
        generatePermutations(permutations, nums, new ArrayList<>(), map);
        return permutations;
    }

    private static void fillUniqueMap(int[] nums, Map<Integer, Integer> map) {
        for (int num : nums) {
            map.putIfAbsent(num, 0);
            map.put(num, map.get(num) + 1);
        }
    }

    private static void generatePermutations(List<List<Integer>> permutations, int[] nums, List<Integer> currentPerm, Map<Integer, Integer> map) {
        if (currentPerm.size() == nums.length) {
            permutations.add(currentPerm);
            return;
        }

        for (Integer num : map.keySet()) {
            int visited = map.get(num);
            // if still allowed to use that num
            if (visited > 0) {
                List<Integer> permCopy = new ArrayList<>(currentPerm);
                permCopy.add(num);
                map.put(num, visited - 1);
                generatePermutations(permutations, nums, permCopy, map);
                map.put(num, visited);
            }
        }
    }
}
