package com.mcs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class Test {
    private static final Logger logger = LoggerFactory.getLogger(Test.class);

    public static void main(String[] args) {
//        int[] input = {1, 3};
//        var result = solution(input);

        var result = solution2(6, 1, 1);
        logger.info(String.format("%s", result));
    }


//    public static int solution(String S) {
//        // key -> substring (2)
//        // value -> index in the string
//        Map<String, Integer> substrToIndex = new HashMap<>();
//        int maxDistance = -1;
//
//        for (int i = 0; i < S.length() - 1; i++) {
//            String currentDiagram = S.substring(i, i + 2);
//            if (substrToIndex.containsKey(currentDiagram)) {
//                maxDistance = Math.max(i - substrToIndex.get(currentDiagram), maxDistance);
//            } else {
//                substrToIndex.put(currentDiagram, i);
//            }
//        }
//
//        return maxDistance;
//    }

    // O(N) time complexity (N = A + B + C)
    // O(1) space complexity (storing nothing but 1 string builder)
    public static String solution2(int A, int B, int C) {
        StringBuilder builder = new StringBuilder();
        while (0 < A || 0 < B || C > 0) {
            // if A is maximum number
            // add 2*"a" and if b and c are > 0, add one of them to the sequence
            if (A >= C && A >= B) {
                A = appendString("a", builder, A);

                if (B > 0) {
                    builder.append("b");
                    B--;
                } else if (C > 0) {
                    builder.append("c");
                    C--;
                }

                if (B == 0 && C == 0 && A > 0) {
                    if (builder.toString().charAt(builder.length() - 1) == 'a') {
                        break;
                    }
                    appendString("a", builder, A);
                    break;
                }
            // if C is maximum number
            // add 2*"c" and if B and A are > 0, add one of them to the sequence
            } else if (C >= A && C >= B) {
                C = appendString("c", builder, C);

                if (B > 0) {
                    builder.append("b");
                    B--;
                } else if (A > 0) {
                    builder.append("a");
                    A--;
                }
                if (B == 0 && A == 0 && C > 0) {
                    if (builder.toString().charAt(builder.length() - 1) == 'c') {
                        break;
                    }
                    appendString("c", builder, C);
                    break;
                }
                // if B is maximum number
                // add 2*"b" and if A and C are > 0, add one of them to the sequence
            } else {
                B = appendString("b", builder, B);

                if (A > 0) {
                    builder.append("a");
                    A--;
                } else if (C > 0) {
                    builder.append("c");
                    C--;
                }
                if (A == 0 && C == 0 && B > 0) {
                    if (builder.toString().charAt(builder.length() - 1) == 'b') {
                        break;
                    }
                    appendString("b", builder, B);
                    break;
                }
            }
        }

        return builder.toString();
    }

    private static int appendString(String character, StringBuilder builder, int count) {
        builder.append(character);
        count--;
        if (count > 0) {
            builder.append(character);
            count--;
        }

        return count;
    }

//    public static String solution(int A, int B, int C) {
//        StringBuilder result = new StringBuilder();
//
//        String longesthChar = "a";
//        int maxLength = A;
//        if (B > maxLength) {
//            maxLength = B;
//            longesthChar = "b";
//        }
//        if (C > maxLength) {
//            maxLength = C;
//            longesthChar = "c";
//        }
//
//        String[] chars = new String[3];
//        int[] number = new int[3];
//        chars[0] = longesthChar;
//        number[0] = maxLength;
//
//
//        for (int i = 1; i > 3; i++) {
//            chars[i] =
//        }
//        while(maxLength > 0) {
//            result.append(longesthChar);
//            maxLength--;
//            if (maxLength > 0) {
//                result.append(longesthChar);
//            }
//        }
//
//        return result.toString();
//    }

//    public static String solution(int A, int B, int C) {
//        StringBuilder result = new StringBuilder();
//        Map<Integer, List<String>> stringValues = new HashMap<>();
//        stringValues.putIfAbsent(A, new ArrayList<>());
//        stringValues.get(A).add("a");
//        stringValues.putIfAbsent(B, new ArrayList<>());
//        stringValues.get(B).add("b");
//        stringValues.putIfAbsent(C, new ArrayList<>());
//        stringValues.get(C).add("c");
//
//        int maxLength = A;
//        if (B > maxLength) {
//            maxLength = B;
//        }
//        if (C > maxLength) {
//            maxLength = C;
//        }
//
//        Map<String, Integer> leftoverCount = new HashMap<>();
//        for (Integer integer : stringValues.keySet()) {
//            List<String> strings = stringValues.get(integer);
//            for (String string : strings) {
//                leftoverCount.put(string, integer);
//            }
//        }
//
//        List<String> mainCharacter = stringValues.get(maxLength);
//        while(maxLength > 0) {
//            result.append(mainCharacter);
//            maxLength -= 2;
//            boolean contains = leftoverCount.containsKey(stringValues.get(A)[0]);
//            if (contains && leftoverCount.get(stringValues.get(A)) > 0) {
//                result.append(stringValues.get(A));
//                // reduce its count
//                int currentCount = leftoverCount.get(stringValues.get(A)[0]);
//                leftoverCount.put(stringValues.get(A), --currentCount);
//            }
//
//            boolean containsB = leftoverCount.containsKey(stringValues.get(B));
//            if (containsB && leftoverCount.get(stringValues.get(B)) > 0) {
//                result.append(stringValues.get(B));
//                // reduce its count
//                int currentCount = leftoverCount.get(stringValues.get(B));
//                leftoverCount.put(stringValues.get(B), --currentCount);
//            }
//
//            boolean containsC = leftoverCount.containsKey(stringValues.get(C));
//            if (containsC && leftoverCount.get(stringValues.get(C)) > 0) {
//                result.append(stringValues.get(C));
//                // reduce its count
//                int currentCount = leftoverCount.get(stringValues.get(C));
//                leftoverCount.put(stringValues.get(C), --currentCount);
//            }
//        }
//
//        return result.toString();
//    }

    public static boolean solution(int[] A) {
        if (A.length % 2 != 0 || A.length <= 2) {
            return false;
        }

        Set<Integer> ints = new HashSet<>();
        for (int i : A) {
            if (!ints.contains(i)) {
                ints.add(i);
            } else {
                ints.remove(i);
            }
        }

        return ints.isEmpty();
    }

    private static int circles(int[] A) {
        if (A.length <= 1) {
            return 0;
        }

        boolean[] paired = new boolean[A.length];
        int index = 0;
        int pairsCount = 0;
        while(index < A.length - 1) {
            int current = A[index];
            int next = A[index + 1];

            if ((current + next) % 2 == 0) {
                pairsCount++;
                paired[index] = true;
                paired[index + 1] = true;
                index += 2;
            } else {
                index ++;
            }

        }

        if (!paired[A.length - 1] && !paired[0]) {
            int current = A[A.length - 1];
            int next = A[0];

            if ((current + next) % 2 == 0) {
                pairsCount++;
            }
        }

        return pairsCount;
    }

    public static int solution1(int[] A) {
        Arrays.sort(A);

        int mid = A.length / 2;
        int midElem = A[mid];

        int left = 0;
        int right = A.length - 1;

        int minSteps = 0;
        while(left < mid) {
            int leftElem = A[left];
            minSteps = minSteps + (Math.abs(leftElem - midElem));
            left++;

        }

        while(right > mid) {
            int rightElem = A[right];
            minSteps = minSteps + (Math.abs(rightElem - midElem));
            right--;
        }

        return minSteps;
    }
}
