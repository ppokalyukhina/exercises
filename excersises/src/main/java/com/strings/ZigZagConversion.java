package com.strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ZigZagConversion {
    public static void main(String[] args) {
        String s = "PAYPALISHIRING";
//        String s = "AB";
        int row = 3;
        System.out.println(convert(s, row));
    }

    public static String convert(String s, int numRows) {
        if (numRows <= 1) {
            return s;
        }
        // numRows arrays of characters
        List<StringBuilder> generatedStrings = new ArrayList<>(numRows);
        // fill list with empty arrays
        for (int i = 0; i < numRows; i++) {
            generatedStrings.add(new StringBuilder());
        }

        boolean goingDown = false;
        int rowIndex = 0;
        for (int charIdx = 0; charIdx < s.length(); charIdx++) {
            generatedStrings.get(rowIndex).append(s.charAt(charIdx));
            if (rowIndex == 0 || rowIndex == numRows - 1) {
                goingDown = !goingDown;
            }

            if (goingDown) {
                rowIndex++;
            } else {
                rowIndex--;
            }
        }

        // build final string
        StringBuilder result = new StringBuilder();
        for (StringBuilder generatedString : generatedStrings) {
            result.append(generatedString);
        }
        return result.toString();
    }

    public static String convert1(String s, int numRows) {
        if (numRows <= 1) {
            return s;
        }
        // numRows arrays of characters
        List<List<Character>> generatedStrings = new ArrayList<>(numRows);
        boolean goingDown = true;
        int currentIndex = 0;
        int rowIndex = 0;
        while(currentIndex < s.length()) {
            if (goingDown) {
                // going down logic
                while(rowIndex < numRows && currentIndex < s.length()) {
                    if (generatedStrings.size() <= rowIndex) {
                        generatedStrings.add(rowIndex, new ArrayList<>());
                    }
                    generatedStrings.get(rowIndex).add(s.charAt(currentIndex));
                    currentIndex++;
                    rowIndex++;
                }
                goingDown = false;
                rowIndex = numRows - 2;
            } else {
                // going up logic
                // exclude last and first rows
                while(rowIndex > 0 && currentIndex < s.length()) {
                    if (generatedStrings.size() <= rowIndex) {
                        generatedStrings.add(rowIndex, new ArrayList<>());
                    }
                    generatedStrings.get(rowIndex).add(s.charAt(currentIndex));
                    currentIndex++;
                    rowIndex--;
                }
                goingDown = true;
            }
        }

        // build final string
        StringBuilder builder = new StringBuilder();
        for (List<Character> generatedString : generatedStrings) {
            for (Character character : generatedString) {
                builder.append(character);
            }
        }
        return builder.toString();
    }
}