package com.strings;

import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioning {
    public static void main(String[] args) {
        String s = "aabaa";

        var result = getPalindromes(s);
        System.out.println(result);
    }


    private static List<List<String>> getPalindromes(String input) {
        List<List<String>> result = new ArrayList<>();
        List<String> combinations = new ArrayList<>();

        int firstPointer = 0;
        while(firstPointer < input.length()) {
            int secondPointer = firstPointer + 1;

            while(secondPointer <= input.length()) {
                String substr = input.substring(firstPointer, secondPointer);

                if (isPalindrome(substr)) {
                    combinations.add(substr);
                }
                secondPointer++;
            }

            firstPointer++;
        }

        result.add(combinations);
        return result;
    }

    // A utility function to check
    // if input is Palindrome
    private static boolean isPalindrome(String input) {
        int start = 0;
        int end = input.length() - 1;
        while (start < end) {
            if (input.charAt(start++) != input.charAt(end--))
                return false;
        }
        return true;
    }
}
