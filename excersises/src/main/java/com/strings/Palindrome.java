package com.strings;

public class Palindrome {
    public static void main(String[] args) {
        String str = "valve";

//        System.out.println(palindroms(str));

        String s = "asbrt";

        System.out.println(s.substring(0, 1));
    }

    private static int palindroms(String str) {
        if (str.length() == 1) {
            System.out.println(str);
            return 1;
        }

        // example
        // "volvol" => 2
        // "voabcvo" => 3
        int result = 0;

        int firstSbstrFirstIndx = 0;
        int firstSbstrLastIndx = firstSbstrFirstIndx + 1;

        int secondSbstrLastIndx = str.length();
        int secondSbstrFirstIndx = secondSbstrLastIndx - 1;

        while(firstSbstrFirstIndx < secondSbstrFirstIndx) {
            String firstSubstring = str.substring(firstSbstrFirstIndx, firstSbstrLastIndx);
            String secondSubstring = str.substring(secondSbstrFirstIndx, secondSbstrLastIndx);
            if (firstSubstring.equals(secondSubstring)) {
                System.out.println(firstSubstring);
                result+=2;

                firstSbstrFirstIndx = firstSbstrLastIndx;
                firstSbstrLastIndx = firstSbstrFirstIndx + 1;

                secondSbstrLastIndx = secondSbstrFirstIndx;
                secondSbstrFirstIndx = secondSbstrLastIndx - 1;
            } else {
                firstSbstrLastIndx++;
                secondSbstrFirstIndx--;
            }
        }

        if (firstSbstrFirstIndx == secondSbstrFirstIndx) {
            System.out.println(str.substring(firstSbstrFirstIndx, firstSbstrLastIndx));
            result++;
        }

        return result;
    }
}
