package com.strings;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {
    public static void main(String[] args) {
        int n = 3;
        var result = generateParenthesis(n);
        for (String s : result) {
            System.out.println(s);
        }
    }

    public static List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        StringBuilder currentCombination = new StringBuilder();
        addStringsRecursive(n, 0, 0, currentCombination, result);
        return result;
    }

    private static void addStringsRecursive(int n, int leftSideIndex, int rightSideIndex, StringBuilder currentCombination, List<String> result) {
        if (leftSideIndex >= n && rightSideIndex >= n) {
            result.add(currentCombination.toString());
        }

        if (leftSideIndex < n) {
            currentCombination.append("(");
            addStringsRecursive(n, leftSideIndex + 1, rightSideIndex, currentCombination, result);
            // pop last elem
            currentCombination.deleteCharAt(currentCombination.length() - 1);
        }

        if (rightSideIndex < leftSideIndex) {
            currentCombination.append(")");
            addStringsRecursive(n, leftSideIndex, rightSideIndex + 1, currentCombination, result);
            // pop last elem
            currentCombination.deleteCharAt(currentCombination.length() - 1);
        }
    }
}
