package com.strings;

import java.util.*;

public class LongestSubstrWithLeastKRepeatingChars {
    public static void main(String[] args) {
        String s = "baaabcb";
        int k = 4;
        System.out.println(longestSubstring(s, k));
    }

    public static int longestSubstring(String s, int k) {
        char[] str = s.toCharArray();
        int[] countMap = new int[26];
        int maxUnique = maxUnique(s);
        int result = 0;
        for (int currUnique = 1; currUnique <= maxUnique; currUnique++) {
            Map<Character, Integer> visited = new HashMap<>();
            // reset countMap
            Arrays.fill(countMap, 0);
            int windowStart = 0, windowEnd = 0, idx = 0, unique = 0, countAtLeastK = 0;
            while (windowEnd < str.length) {
                // expand the sliding window
                if (unique <= currUnique) {
                    idx = str[windowEnd] - 'a';
                    if (countMap[idx] == 0) unique++;
                    countMap[idx]++;
                    if (countMap[idx] == k) countAtLeastK++;
                    windowEnd++;
                }
                // shrink the sliding window
                else {
                    idx = str[windowStart] - 'a';
                    if (countMap[idx] == k) countAtLeastK--;
                    countMap[idx]--;
                    if (countMap[idx] == 0) unique--;
                    windowStart++;
                }
                if (unique == currUnique && unique == countAtLeastK)
                    result = Math.max(windowEnd - windowStart, result);
            }
        }

        return result;
    }

    private static void increaseVisit(Map<Character, Integer> countMap, char current) {
        countMap.putIfAbsent(current, 0);
        countMap.put(current, countMap.get(current) + 1);
    }

    private static int maxUnique(String s) {
        Set<Character> set = new HashSet<>();
        int maxUnique = 0;

        for (char c : s.toCharArray()) {
            if (set.contains(c)) {
                continue;
            }
            maxUnique++;
            set.add(c);
        }

        return maxUnique;
    }
}
