package com.strings;

public class StringShift {
    public static void main(String[] args) {
        int[][] shift = {{1,1}, {1,1}, {0,2}, {1,3}};
        System.out.println(stringShift("abcdefg", shift));
    }

    public static String stringShift(String s, int[][] shift) {
        if(s.length() == 1) {
            return s;
        }
        int indx = 0;
        while(indx < shift.length) {
            if (shift[indx][0] == 0) {
                // move left
                int moveSteps = shift[indx][1];
                s = s.substring(moveSteps) + s.substring(0, moveSteps);
            } else {
                // move right
                int moveSteps = shift[indx][1];
                int lastTakenIndx = s.length() - moveSteps;
                s = s.substring(lastTakenIndx) + s.substring(0, lastTakenIndx);
            }

            indx++;
        }

        return s;
    }
}
