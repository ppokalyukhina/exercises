package com.strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *  Given an arbitrary ransom note string and another string containing letters from all the magazines,
 *  write a function that will return true if the ransom note can be constructed from the magazines;
 *  otherwise, it will return false.
 *
 * Each letter in the magazine string can only be used once in your ransom note.
 *
 * Note:
 * You may assume that both strings contain only lowercase letters.
 *
 * canConstruct("a", "b") -> false
 * canConstruct("aa", "ab") -> false
 * canConstruct("aa", "aab") -> true
 */
public class RansomNote {
    private static final Logger logger = LoggerFactory.getLogger(RansomNote.class);
    public static void main(String[] args) {
//        "bjaajgea"
//        "affhiiicabhbdchbidghccijjbfjfhjeddgggbajhidhjchiedhdibgeaecffbbbefiabjdhggihccec"
//        String s1 = "bjaajgea";
//        String s2 = "affhiiicabhbdchbidghccijjbfjfhjeddgggbajhidhjchiedhdibgeaecffbbbefiabjdhggihccec";
        String s1 = "aa";
        String s2 = "dbac";
        logger.info("String {} can be constructed from string {}: {}", s1, s2, canConstruct1(s1, s2));
    }

    private static boolean canConstruct(String ransomNote, String magazine) {
        // characters in magazine -> how many of them are found
        Map<Character, Integer> magazineChars = new HashMap<>();
        for (char c : magazine.toCharArray()) {
            if (magazineChars.containsKey(c)) {
                int frequence = magazineChars.get(c);
                magazineChars.put(c, ++frequence);
                continue;
            }

            magazineChars.put(c, 1);
        }

        for (char note : ransomNote.toCharArray()) {
            if (!magazineChars.containsKey(note)) {
                return false;
            }

            int frequence = magazineChars.get(note);
            if (frequence > 1) {
                magazineChars.put(note, --frequence);
            } else {
                magazineChars.remove(note);
            }
        }

        return true;
    }

    //different solution
    private static boolean canConstruct1(String ransomNote, String magazine) {
        char[] dict = getMap(magazine);
        for (int i = 0; i < ransomNote.length(); i++) {
            if (dict[ransomNote.charAt(i)-'a'] == 0) {
                return false;
            }

            dict[ransomNote.charAt(i)-'a']--;
        }
        return true;
    }

    private  static char[] getMap(String magazine) {
        char[] dict = new char[26];
        for (char ch : magazine.toCharArray()) {
            dict[ch-'a']++;
        }
        return dict;
    }
}
