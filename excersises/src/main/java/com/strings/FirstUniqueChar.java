package com.strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Given a string, find the first non-repeating character in it and return it's index.
 * If it doesn't exist, return -1.
 *
 * Examples:
 *
 * s = "leetcode"
 * return 0.
 *
 * s = "loveleetcode",
 * return 2.
 *
 * Note: You may assume the string contain only lowercase letters.
 */
public class FirstUniqueChar {
    private static final Logger logger = LoggerFactory.getLogger(FirstUniqueChar.class);

    public static void main(String[] args) {
        String input = "aabbcc";
        logger.info("First unique character position is: {}", firstUniqChar(input));
    }

    private static int firstUniqChar(String s) {
        if (s.length() == 0) {
            return -1;
        }

        int[] chars = new int[26];
        for (char c : s.toCharArray()) {
            chars[c - 'a']++;
        }

        for (int i = 0; i < s.length(); i++) {
            if (chars[s.charAt(i) - 'a'] == 1) {
                return i;
            }
        }

        return -1;
    }
}
