package com.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string s, the power of the string is the maximum length of a non-empty substring that contains only one unique character.
 *
 * Return the power of the string.
 *
 * Input: s = "leetcode"
 * Output: 2
 * Explanation: The substring "ee" is of length 2 with the character 'e' only.
 *
 *
 1 <= s.length <= 500
 s contains only lowercase English letters.
 */
public class ConsecutiveCharacters {
    public static void main(String[] args) {
        System.out.println(maxPowerAnotherSolution("tourist"));
    }

    private static int maxPowerAnotherSolution(String s) {
        int maxPower = 0;
        if (s.isEmpty()) {
            return maxPower;
        }

        if (s.length() == 1) {
            return 1;
        }

        int[] maxPowers = new int[s.length()];
        maxPowers[0] = 1;
        maxPower = 1;
        for (int i = 1; i < s.length(); i++) {
            char previous = s.charAt(i - 1);
            char current = s.charAt(i);
            if (previous == current) {
                maxPowers[i] = maxPowers[i - 1] + 1;
                maxPower = Math.max(maxPower, maxPowers[i]);
                continue;
            }

            maxPowers[i] = 1;
        }

        return maxPower;
    }

    private static int maxPower(String s) {
        if (s.isEmpty()) {
            return 0;
        }
        int result = 1;
        int pointer = 0;
        while(pointer < (s.length() - 1)) {
            char current = s.charAt(pointer);
            char next = s.charAt(pointer + 1);

            if (current == next) {
                int maxPower = 0;
                while(current == next && pointer < (s.length() - 1)) {
                    maxPower++;
                    current = next;
                    next = s.charAt(++pointer);
                }
                result = Math.max(maxPower, result);
            } else {
                pointer++;
            }
        }

        return result;
    }
}
