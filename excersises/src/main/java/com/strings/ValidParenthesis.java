package com.strings;

/**
 *  Given a string containing only three types of characters: '(', ')' and '*',
 *  write a function to check whether this string is valid. We define the validity of a string by these rules:
 *
 *     Any left parenthesis '(' must have a corresponding right parenthesis ')'.
 *     Any right parenthesis ')' must have a corresponding left parenthesis '('.
 *     Left parenthesis '(' must go before the corresponding right parenthesis ')'.
 *     '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
 *     An empty string is also valid.
 */
public class ValidParenthesis {
    public static void main(String[] args) {
        var validP = new ValidParenthesis();
//        System.out.println(validP.checkValidString("(())((())()()(*)(*()(())())())()()((()())((()))(*"));

        System.out.println(validP.valid3("(())((())()()(*)(*()(())())())()()((()())((()))(*"));
    }

    private boolean valid3(String s) {
        String[] array = s.split("");
        int low = 0, high = 0;
        for (String value : array) {
            if (value.equals("(")) {
                low++;
                high++;
            } else if (value.equals(")")) {
                if (low > 0) {
                    low--;
                }
                high--;
            } else {
                if (low > 0) {
                    low--;
                }
                high++;
            }

            if (high < 0) {
                return false;
            }
        }

        return low == 0;
    }
}
