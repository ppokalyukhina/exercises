package com.strings;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstrWoRepeating {
    public static void main(String[] args) {
        String s = "abcabcbb";
        System.out.println(lengthOfLongestSubstring(s));
    }

    // O(N) time
    // O(N) space
    public static int lengthOfLongestSubstring(String s) {
        int longest = 0;
        Set<Character> unique = new HashSet<>();
        int left = 0;
        int right = 0;

        while(right < s.length()) {
            char current = s.charAt(right);
            if (unique.contains(current)) {
                unique.remove(s.charAt(left));
                left++;
            } else {
                unique.add(current);
                right++;
            }
            longest = Math.max(longest, right - left);
        }

        return longest;
    }
}
