package com.strings;

public class CompressString {
    private static String compress(String input) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(input.charAt(0));
        int counter = 1;

        for(int i = 1; i < input.length(); i++) {
            if (input.charAt(i - 1) == input.charAt(i)) {
                counter++;
            } else {
                stringBuffer.append(counter);
                stringBuffer.append(input.charAt(i));
                counter = 1;
            }
        }

        stringBuffer.append(counter);

        return stringBuffer.toString();
    }

    public static void main(String[] args) {
        String input = "aabcccccaaa";

        System.out.println(compress(input));
    }
}
