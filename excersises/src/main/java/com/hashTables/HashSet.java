package com.hashTables;

public class HashSet {
    public static void main(String[] args) {
        
    }

    private Node[] arr;
    private Node nonNode;
    /** Initialize your data structure here. */
    public HashSet() {
        arr = new Node[1000000];
        nonNode = new Node(-1);
    }

    public void add(int key) {
        int hashVal = hashFunc(key);
        while(arr[hashVal] != null && arr[hashVal].getKey() != -1) {
            ++hashVal;
            hashVal %= arr.length;
        }
        arr[hashVal] = new Node(key);
    }

    public void remove(int key) {
        int hashVal = hashFunc(key);
        while(arr[hashVal] != null) {
            if (arr[hashVal].getKey() == key) {
                arr[hashVal] = nonNode;
            }

            hashVal++;
            hashVal %= arr.length;
        }
    }

    /** Returns true if this set contains the specified element */
    public boolean contains(int key) {
        int index = hashFunc(key);
        while(arr[index] != null) {
            if (arr[index].getKey() == key) {
                return true;
            }

            index++;
            index %= arr.length;
        }

        return false;
    }

    private int hashFunc(int key) {
        return key % arr.length;
    }

    private class Node {
        private int key;

        Node(int key) {
            this.key = key;
        }

        public int getKey() {
            return key;
        }
    }
}
